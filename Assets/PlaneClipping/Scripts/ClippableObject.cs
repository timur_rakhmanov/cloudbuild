﻿/*
 The original version of this script was taken from http://www.toxicfork.com/194/per-object-clipping-planes-shader-in-unity3d-5
 Shader was modified to fix unity 5.5 compatibility and adding 4th plane
 This script is hard modified for our purposes of 3D map rendering.
 */

using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;


[ExecuteInEditMode]
public class ClippableObject : MonoBehaviour
{
    private const string CLIP_ONE = "CLIP_ONE";
    private const string CLIP_TWO = "CLIP_TWO";
    private const string CLIP_THREE = "CLIP_THREE";
    private const string CLIP_FOUR = "CLIP_FOUR";
    private Material _sharedMaterial;

    private bool _isInited = false;

    public IEnumerator Start()
    {
        clipPlanes = 4;
        planePreviewSize = 1;
        if (GetComponent<MeshRenderer>() == null && GetComponent<SpriteRenderer>() == null) {
            yield return null;
        }

        UpdateShader();
    }

    //only 3 clip planes for now, will need to modify the shader for more.
    [UnityEngine.Range(0, 4)]
    public int clipPlanes = 0;

    //[HideInInspector]
    //public Color Color;

    //preview size for the planes. Shown when the object is selected.
    public float planePreviewSize = 5.0f;

    //Positions and rotations for the planes. The rotations will be converted into normals to be used by the shaders.
    public Vector3 plane1Position = Vector3.zero;
    public Vector3 plane1Rotation = new Vector3(0, 0, 0);

    public Vector3 plane2Position = Vector3.zero;
    public Vector3 plane2Rotation = new Vector3(0, 90, 90);

    public Vector3 plane3Position = Vector3.zero;
    public Vector3 plane3Rotation = new Vector3(0, 0, 90);

    public Vector3 plane4Position = Vector3.zero;
    public Vector3 plane4Rotation = new Vector3(0, 0, 90);

    //Only used for previewing a plane. Draws diagonals and edges of a limited flat plane.
    private void DrawPlane(Vector3 position, Vector3 euler)
    {
        var forward = Quaternion.Euler(euler) * Vector3.forward;
        var left = Quaternion.Euler(euler) * Vector3.left;

        var forwardLeft = position + forward * planePreviewSize * 0.5f + left * planePreviewSize * 0.5f;
        var forwardRight = forwardLeft - left * planePreviewSize;
        var backRight = forwardRight - forward * planePreviewSize;
        var backLeft = forwardLeft - forward * planePreviewSize;

        Gizmos.DrawLine(position, forwardLeft);
        Gizmos.DrawLine(position, forwardRight);
        Gizmos.DrawLine(position, backRight);
        Gizmos.DrawLine(position, backLeft);

        Gizmos.DrawLine(forwardLeft, forwardRight);
        Gizmos.DrawLine(forwardRight, backRight);
        Gizmos.DrawLine(backRight, backLeft);
        Gizmos.DrawLine(backLeft, forwardLeft);
    }

    //private void OnDrawGizmosSelected()
    //{
    //    if (clipPlanes >= 1) {
    //        DrawPlane(plane1Position, plane1Rotation);
    //    }
    //    if (clipPlanes >= 2) {
    //        DrawPlane(plane2Position, plane2Rotation);
    //    }
    //    if (clipPlanes >= 3) {
    //        DrawPlane(plane3Position, plane3Rotation);
    //    }
    //    if (clipPlanes >= 4) {
    //        DrawPlane(plane4Position, plane4Rotation);
    //    }
    //}

    //Ideally the planes do not need to be updated every frame, but we'll just keep the logic here for simplicity purposes.
    public void UpdateShader()
    {
        if (_isInited) return;
        // This is because of Unity shitty scripts lifecycle, meshrenderer sometimes is attached later then this one
        try {
            var meshRenderer = GetComponent<MeshRenderer>();
            if (meshRenderer != null) {
                _sharedMaterial = new Material(meshRenderer.sharedMaterial);
                _sharedMaterial.shader = Shader.Find("Custom/StandardClippable");
                if (meshRenderer.materials.Length > 1) {
                    var materials = new Material[meshRenderer.materials.Length];
                    for (int i = 0; i < materials.Length; i++) {
                        materials[i] = _sharedMaterial;
                    }
                    meshRenderer.materials = materials;
                }
                meshRenderer.sharedMaterial = _sharedMaterial;
            } else {
                var spriteRenderer = GetComponent<SpriteRenderer>();
                if (spriteRenderer.sharedMaterial.shader.name != "Custom/StandardClippable") {
                    _sharedMaterial = new Material(spriteRenderer.sharedMaterial);
                    _sharedMaterial.shader = Shader.Find("Custom/StandardClippable");
                    spriteRenderer.sharedMaterial = _sharedMaterial;
                    spriteRenderer.material.SetFloat("_Glossiness", 0f);
                    spriteRenderer.material.SetFloat("_Metallic", 0.5f);
                }
            }
            // We're using pretty specific modification of original script, so it's enough to use only this keyword
            //_sharedMaterial.EnableKeyword(CLIP_FOUR);
            
            _isInited = true;
        } catch (Exception ex) {
            Debug.LogError(ex);
            return;
        }


    }
}