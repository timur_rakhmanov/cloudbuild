﻿using System;
using Assets._Project.Components.CompanySelectionMenu.Shared.Scripts;
using Assets._Project.Components.CompanySelectionMenu.Shared.Scripts.Events;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Models;
using Assets._Project.Shared.Scripts.Identity;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.UI;


namespace Assets._Project.Components.CompanySelectionMenu.Mobile.Scripts
{
    /// <summary>
    /// Controls selection company for mobile implementation.
    /// </summary>
    public class CompanySelectionMenuController : MonoBehaviour, ICompanySelectionMenuController
    {
        #region Editor configs

        public GameObject AddCompanyButtonPrefab;
        public GameObject CompanyButtonPrefab;
        public GameObject SuperDeviceCompanyButtonPrefab;
        public GameObject CompaniesButtonsContainer;

        #endregion

        #region Constants

        private const string CLOSE_COMPANY_SELECTION_MENU_BUTTON = "CompanySelectionMenuCanvas/CloseButton";

        #endregion

        #region Private data

        private bool _allowsToAddCompany;

        #endregion
        #region Public events access point

        public void OnCompanySelected(UnityAction handler)
        {
            _companySelected.AddListener(handler);
        }

        public void OnAddNewCompanyRequested(UnityAction handler)
        {
            _addNewCompanyRequested.AddListener(handler);
        }

        #endregion

        #region Events

        [HideInInspector]
        private CompanySelectedEvent _companySelected;

        [HideInInspector]
        private UnityEvent _addNewCompanyRequested;

        #endregion

        #region Mono behaviour pipeline

        // ReSharper disable once UnusedMember.Local
        private void Awake()
        {
            _companySelected = new CompanySelectedEvent();
            _addNewCompanyRequested = new UnityEvent();
        } 

        // ReSharper disable once UnusedMember.Local
        private void Start()
        {
            Assert.IsNotNull(AddCompanyButtonPrefab);
            Assert.IsNotNull(CompanyButtonPrefab);
            Assert.IsNotNull(SuperDeviceCompanyButtonPrefab);
            Assert.IsNotNull(CompaniesButtonsContainer);

            _companySelected.AddListener(CompanySelectedHandler);

            var deviceRole = IdentityManager.Instance.GetDeviceRole();

            switch (deviceRole) {
                case DeviceRole.Device:
                    LoadDeviceCompaniesToMenu(_allowsToAddCompany);
                    break;
                case DeviceRole.SuperDevice:
                    LoadSuperDeviceCompaniesToMenu();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion

        #region Commands
        public void Init(bool allowsToAddCompany, bool isCloasable)
        {
            _allowsToAddCompany = allowsToAddCompany;
            transform.Find(CLOSE_COMPANY_SELECTION_MENU_BUTTON).gameObject.SetActive(isCloasable);
        }

        public void NotifyCompanySelected()
        {
            _companySelected.Invoke();
        }

        #endregion

        #region Ui event handlers

        public void AddCompanyButtonClick()
        {
            AddCompanyNewCompany();
        }

        public void CloseMenu()
        {
            gameObject.GetComponentInChildren<Canvas>().enabled = false;
        }


        #endregion

        #region Unity events handlers

        private void CompanySelectedHandler()
        {
            CloseMenu();
        }

        #endregion

        #region Private methods

        private void AddCompanyNewCompany()
        {
            CloseMenu();
            _addNewCompanyRequested.Invoke();
        }

        private void LoadDeviceCompaniesToMenu(bool allowToAddCompany)
        {
            LoadCompaniesToMenu(CompanyButtonPrefab, allowToAddCompany);
        }

        private void LoadSuperDeviceCompaniesToMenu()
        {
            LoadCompaniesToMenu(SuperDeviceCompanyButtonPrefab, false);
        }

        private void LoadCompaniesToMenu(GameObject companyButtonPrefab, bool allowToAddCompany)
        {
            var companies = ExecutionContext.Instance.DeviceCompanies;

            CompaniesButtonsContainer.transform.parent.parent.gameObject.SetActive(true);

            foreach (var company in companies) {
                var companyButtonGameObject = Instantiate(companyButtonPrefab);
                companyButtonGameObject.GetComponent<CompanyButtonController>().InitializeButton(company);
                companyButtonGameObject.transform.SetParent(CompaniesButtonsContainer.transform, false);
            }

            if (allowToAddCompany) {
                // Add "AddCompany" button for Device.
                var addCompanyButtonGameObject = Instantiate(AddCompanyButtonPrefab);
                addCompanyButtonGameObject.transform.SetParent(CompaniesButtonsContainer.transform, false);

                var addCompanyButton = addCompanyButtonGameObject.GetComponent<Button>();
                Assert.IsNotNull(addCompanyButton);
                addCompanyButton.onClick.AddListener(AddCompanyButtonClick);
            }
        }

        #endregion
    }
}