﻿using System.Collections;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Assets._Project.Shared.Scripts.Identity;
using UnityEngine;
using UnityEngine.UI;


namespace Assets._Project.Components.CompanySelectionMenu.Shared.Scripts
{
    public class CompanyButtonController : MonoBehaviour
    {
        public Text CompanyName;
        public Button CompanyButton;
        public Image CompanyIcon;

        public void InitializeButton(CompanyViewModel companyViewModel)
        {
            CompanyName.text = companyViewModel.Name;
            CompanyButton.onClick.AddListener(() => {
                gameObject.GetComponentInParent<ICompanySelectionMenuController>().NotifyCompanySelected();
                CompaniesManager.Instance.UpdateActiveCompany(companyViewModel);
            });
            LoadCompanyIcon(companyViewModel.LogoUrl).AsTask().RunCoroutine(this);
        }

        private IEnumerator LoadCompanyIcon(string iconUrl)
        {
            // TODO: use async dectorator for conformity.
            var www = new WWW(iconUrl);
            yield return www;
            if (www.error != null) {
                yield break;
            }
            CompanyIcon.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
           
        }
    }
}