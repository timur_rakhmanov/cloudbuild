﻿using System;
using UnityEngine.Events;


namespace Assets._Project.Components.CompanySelectionMenu.Shared.Scripts.Events
{
    [Serializable]
    public class CompaniesLoadedEvent : UnityEvent
    {
    }
}
