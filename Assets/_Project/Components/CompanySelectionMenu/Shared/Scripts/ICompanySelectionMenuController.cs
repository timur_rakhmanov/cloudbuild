﻿using Assets._Project.Components.CompanySelectionMenu.Shared.Scripts.Events;
using UnityEngine.Events;


namespace Assets._Project.Components.CompanySelectionMenu.Shared.Scripts
{
    /// <summary>
    /// Company menu selection.
    /// </summary>
    interface ICompanySelectionMenuController
    {
        #region Events

        /// <summary>
        /// Notify when another participant selects placemark on 3d map.
        /// </summary>
        void OnCompanySelected(UnityAction handler);

        /// <summary>
        /// Notify when another user deselected placemark on 3d map.
        /// </summary>
        void OnAddNewCompanyRequested(UnityAction handler);

        #endregion

        /// <summary>
        /// Init controller.
        /// </summary>
        void Init(bool allowsToAddCompany, bool isCloasable);

        /// <summary>
        /// Notify controller that company selected.
        /// </summary>
        void NotifyCompanySelected();
    }
}
