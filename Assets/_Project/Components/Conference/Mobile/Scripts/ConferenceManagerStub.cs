﻿using System.Collections;
using System.Collections.Generic;
using Assets._Project.Components.Conference.Shared.Scripts.ConferenceManager;
using Assets._Project.Components.Conference.Shared.Scripts.ConferenceManager.Dto;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using UnityEngine;
using UnityEngine.Events;


namespace Assets._Project.Components.Conference.Mobile.Scripts
{
    /// <summary>
    /// Stub for conference manager that isn't used in Mobile platform.
    /// </summary>
    public class ConferenceManagerStub: MonoBehaviour, IConferenceManager
    {
        public void OnPlacemarkSelected(UnityAction<PlacemarkIdDto> handler)
        {
        }

        public void OnPlacemarkDeselected(UnityAction handler)
        {
        }

        public bool IsConferenceOnline()
        {
            return false;
        }

        public bool IsUserInConference()
        {
            return false;
        }

        public string GetStatus()
        {
            return null;
        }

        public ConferenceStateDto GetCurrentConferenceStateInfo()
        {
            return null;
        }

        public ConferenceStateDto GetOnlineConferenceStateInfo()
        {
            return null;
        }

        public Transform GetConferenceCentralPoint()
        {
            return null;
        }

        public void StartServerAsync(CompanyViewModel company)
        {
        }

        public IEnumerator StartConference(GeoCoordinate startGeoPosition, float zoom, IList<MapLayerDto> enabledMapOptions, GameObject map, GameObject anchorPlaceholder, PlacemarkIdDto selectedPlacemark = null)
        {
            yield return null;
        }

        public IEnumerator JoinConference(GameObject map, GameObject anchorPlaceholder)
        {
            yield return null;
        }

        public IEnumerator LeaveConference()
        {
            yield return null;
        }

        public void LeaveConferenceAsync()
        {
        }

        public void NotifyThatPlacemarkIsSelected(PlacemarkIdDto placemarkIdDto)
        {
        }

        public void NotifyThatPlacemarkDeselected()
        {
        }

        public void NotifyAboutNewMapZoom(float mapZoom)
        {
        }

        public void NotifyAboutNewMapPosition(GeoCoordinate mapCoordinates)
        {
        }

        public void NotifyAboutNewMapOptions(List<MapLayerDto> enabledMapOptions)
        {
        }
    }
}
