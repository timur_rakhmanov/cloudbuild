﻿using System.Collections.Generic;
using System.Linq;
using Assets._Project.Shared.Scripts.Core;
using UnityEngine;


namespace Assets._Project.Components.Conference.Shared.Scripts.ConferenceManager.Dto
{
    /// <summary>
    /// Conference state.
    /// </summary>
    public class ConferenceStateDto
    {
        public ConferenceStateDto()
        {
            EnabledMapOptions = new List<MapLayerDto>();
            Participants = new List<ParticipantDto>();
        }

            
        public string ConferenceName { get; set; }
        public GeoCoordinate MapGeoPosition { get; set; }

        public PlacemarkIdDto SelectedPlacemark { get; set; }

        /// <summary>
        /// List of the map options that is enabled now.
        /// </summary>
        public List<MapLayerDto> EnabledMapOptions { get; set; }

        /// <summary>
        /// Zoom of the map.
        /// </summary>
        public float MapZoom { get; set; }

        /// <summary>
        /// Id of the room related to this conference.
        /// </summary>
        public long RoomId { get; set; }

        /// <summary>
        /// Id of the user that created this conference.
        /// </summary>
        public string CreatorUserId { get; set; }

        /// <summary>
        /// Name of the user that created this conference.
        /// </summary>
        public string CreatorUserName { get; set; }

        /// <summary>
        /// Users in conference.
        /// </summary>
        public List<ParticipantDto> Participants { get; set; }

        /// <summary>
        /// Mac address of conference creator's wifi point..
        /// </summary>
        public string CreatorWifiPointMacAddress { get; set; }


        /// <summary>
        /// Name of conference creator's wifi point.
        /// </summary>
        public string CreatorWifiPointName { get; set; }

        public Vector3 MapLocalPosition { get; set; }
        public Vector3 MapLocalRotation { get; set; }
        public Vector3 MapLocalScale { get; set; }

        public override string ToString()
        {
            return string.Format("ConferenceName {0}, MapGeoCoordinates {1}, RoomId {2}, CreatorUserId {3}, CreatorUserName {4}, Participants {5}, " +
                "CreatorWifiPointMacAddress {6}, CreatorWifiPointName {7}, MapPosition {8}, MapRotation {9}, MapScale {10}," +
                "SelectedPlacemark {11}, EnabledMapOptions {12}, MapZoom {13}", ConferenceName, MapGeoPosition, RoomId, CreatorUserId, CreatorUserName,
                string.Join("/", Participants.Select(p => string.Format("{0}({1}|{2}|{3}|{4})", p.Name, p.GeoCoordinate, p.Azimuth, p.ConferenceRelatedPosition, p.ConferenceRelatedRotation)).ToArray()), CreatorWifiPointMacAddress, CreatorWifiPointName, MapLocalPosition, 
                MapLocalRotation, MapLocalScale, SelectedPlacemark, string.Join("/", EnabledMapOptions.Select(o => o.ToString()).ToArray()),
                MapZoom);
        }
    }
}