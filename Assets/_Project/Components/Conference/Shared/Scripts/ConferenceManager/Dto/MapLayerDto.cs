﻿namespace Assets._Project.Components.Conference.Shared.Scripts.ConferenceManager.Dto
{
    public enum MapLayerDto
    {
        Placemarks = 0,
        OSMBuildings = 1,
        CustomBuildings = 2,
        ArcGisImagery = 3,
        Landscape = 4,
        BingImagery = 5
    }
}