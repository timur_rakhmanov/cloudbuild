﻿using Assets._Project.Shared.Scripts.Core;
using UnityEngine;


namespace Assets._Project.Components.Conference.Shared.Scripts.ConferenceManager.Dto
{
    /// <summary>
    /// Conference participant's data.
    /// </summary>
    public class ParticipantDto
    {
        public string Id { get; set; }

        public long ConferenceId { get; set; }

        public string Name { get; set; }
        public GeoCoordinate GeoCoordinate { get; set; }
        public double Azimuth { get; set; }

        public Vector3 ConferenceRelatedPosition { get; set; } 
        public Quaternion ConferenceRelatedRotation { get; set; }
    }
}