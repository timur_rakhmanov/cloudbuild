﻿using System;


namespace Assets._Project.Components.Conference.Shared.Scripts.ConferenceManager.Dto
{
    /// <summary>
    /// Full id of the placemark in context of the event.
    /// </summary>
    public class PlacemarkIdDto
    {
        /// <summary>
        /// Internal unique id per data source.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Data source of this placemark.
        /// </summary>
        public Guid DataSourceId { get; set; }

        /// <summary>
        /// Id of the related company.
        /// </summary>
        public Guid CompanyId { get; set; }

        public override string ToString()
        {
            return string.Format("{0}/{1}/{2}", CompanyId, DataSourceId, Id);
        }
    }
}