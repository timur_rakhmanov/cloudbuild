﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets._Project.Components.Conference.Shared.Scripts.ConferenceManager.Dto;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using UnityEngine;
using UnityEngine.Events;


namespace Assets._Project.Components.Conference.Shared.Scripts.ConferenceManager
{
    /// <summary>
    /// Control conference process.
    /// </summary>
    public interface IConferenceManager
    {
        #region Events

        /// <summary>
        /// Notify when another participant selects placemark on 3d map.
        /// </summary>
        void OnPlacemarkSelected(UnityAction<PlacemarkIdDto> handler);

        /// <summary>
        /// Notify when another user deselected placemark on 3d map.
        /// </summary>
        void OnPlacemarkDeselected(UnityAction handler);

        #endregion

        #region Queries
        /// <summary>
        /// Indicates if conference is online.
        /// </summary>
        /// <returns></returns>
        bool IsConferenceOnline();

        /// <summary>
        /// Indicates if current user takes part in conference now.
        /// </summary>
        /// <returns></returns>
        bool IsUserInConference();

        /// <summary>
        /// Returns current state of manager.
        /// </summary>
        /// <returns></returns>
        string GetStatus();

        /// <summary>
        /// Returns conference's data.
        /// </summary>
        ConferenceStateDto GetCurrentConferenceStateInfo();

        /// <summary>
        /// Returns info about online conference (exists event if user doesn't take part in conference).
        /// </summary>
        /// <returns></returns>
        ConferenceStateDto GetOnlineConferenceStateInfo();

        /// <summary>
        /// Returns transformation of central point of conference.
        /// Can be used for place participants related to each other.
        /// </summary>
        /// <returns></returns>
        Transform GetConferenceCentralPoint();
        #endregion

        #region Commands
        /// <summary>
        /// Start server for company.
        /// </summary>
        /// <param name="company"></param>
        void StartServerAsync(CompanyViewModel company);

        /// <summary>
        /// Start conference.
        /// </summary>
        IEnumerator StartConference(GeoCoordinate startGeoPosition, float zoom, IList<MapLayerDto> enabledMapOptions,
            GameObject map, GameObject anchorPlaceholder, PlacemarkIdDto selectedPlacemark = null);

        /// <summary>
        /// Join to the existent conference.
        /// </summary>
        /// <returns></returns>
        IEnumerator JoinConference(GameObject map, GameObject anchorPlaceholder);

        /// <summary>
        /// Leave conference.
        /// </summary>
        IEnumerator LeaveConference();

        /// <summary>
        /// Leave conference without waiting result.
        /// </summary>
        void LeaveConferenceAsync();

        /// <summary>
        /// Send info to other user's in conference about selected placemark.
        /// </summary>
        void NotifyThatPlacemarkIsSelected(PlacemarkIdDto placemarkIdDto);

        /// <summary>
        /// Send info to other user's in conference about deselected placemark.
        /// </summary>
        void NotifyThatPlacemarkDeselected();

        /// <summary>
        /// Send info to other user's in conference about new map's zoom.
        /// </summary>
        /// <param name="mapZoom"></param>
        void NotifyAboutNewMapZoom(float mapZoom);

        /// <summary>
        /// Send info to other user's in conference about new map's position. (lat, long, alt)
        /// </summary>
        /// <param name="mapCoordinates"></param>
        void NotifyAboutNewMapPosition(GeoCoordinate mapCoordinates);

        /// <summary>
        /// Enabled options on the map.
        /// </summary>
        /// <param name="enabledMapOptions"></param>
        void NotifyAboutNewMapOptions(List<MapLayerDto> enabledMapOptions);

        #endregion
    }
}