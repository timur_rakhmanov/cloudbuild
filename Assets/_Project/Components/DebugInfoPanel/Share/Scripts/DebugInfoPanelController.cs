﻿using System.Globalization;
using Assets._Project.Shared.Scripts.Core;
using HoloToolkit.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


namespace Assets._Project.Components.DebugInfoPanel.Share.Scripts
{
    /// <summary>
    /// Control information that should be displayed on debug info panel.
    /// </summary>
    public class DebugInfoPanelController: Singleton<DebugInfoPanelController>
    {
        private const string GEO_POSITION_DATA = "Panel/GeoPositionRow/Data";
        private const string UNITY_POSITION_DATA = "Panel/UnityPositionRow/Data";
        private const string FPS_DATA = "Panel/FpsRow/Data";

        private bool _isEnabled = false;

        public void Show()
        {
            gameObject.GetComponent<Canvas>().enabled = true;
            _isEnabled = true;
        }

        public void Hide()
        {
            gameObject.GetComponent<Canvas>().enabled = false;
            _isEnabled = false;
        }

        public bool IsShown()
        {
            return gameObject.GetComponent<Canvas>().enabled;
        }

        public void DisplayGeoPosition(GnssDataViewModel position)
        {
            GetTextElement(GEO_POSITION_DATA).text = position.ToString();
        }

        public void DisplayUnityPosition(Vector3 position)
        {
            GetTextElement(UNITY_POSITION_DATA).text = string.Format("X:{0:0.00} Y:{1:0.00} Z:{2:0.00}", position.x, position.y, position.z);
        }

        public void DisplayFps(float fps)
        {
            GetTextElement(FPS_DATA).text = fps.ToString(CultureInfo.InvariantCulture);
        }

        private Text GetTextElement(string path)
        {
            var element = gameObject.transform.Find(path);
            Assert.IsNotNull(element);
            var text = element.GetComponent<Text>();
            Assert.IsNotNull(text);
            return text;
        }
    }
}
