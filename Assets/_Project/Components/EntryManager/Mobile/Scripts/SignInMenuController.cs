﻿using Assets._Project.Components.EntryManager.Shared.Scripts;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Events;


namespace Assets._Project.Components.EntryManager.Mobile.Scripts
{
    public class SignInMenuController: MonoBehaviour, ISignInMenuController
    {
        [HideInInspector]
        private UnityEvent CloseSignInRequested;

        #region Injections
        [HideInInspector]
        public bool AllowToClose;

        public GameObject CloseButton;
        #endregion

        public SignInMenuController()
        {
            // We create it directly in constructor since this event can be used before start is called.
            CloseSignInRequested = new UnityEvent();
        }

        private void Start()
        {
            Assert.IsNotNull(CloseButton);

            CloseButton.SetActive(AllowToClose);
        }

        #region Public events access points
        public void OnCloseSignInRequested(UnityAction handler)
        {
            CloseSignInRequested.AddListener(handler);
        }
        #endregion

        public void InitData(bool allowToClose)
        {
            AllowToClose = allowToClose;
        }

        public void Show()
        {
            gameObject.GetComponent<Canvas>().enabled = true;
        }

        public void Hide()
        {
            gameObject.GetComponent<Canvas>().enabled = false;
        }

        public GameObject GetGameObject()
        {
            return gameObject;
        }

        public void OnCloseButtonClicked()
        {
            CloseSignInRequested.Invoke();
        }
    }
}
