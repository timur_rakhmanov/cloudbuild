﻿using System;
using System.Collections;
using System.Linq;
using Assets._Project.Components.CompanySelectionMenu.Shared.Scripts;
using Assets._Project.Components.EntryManager.Mobile.Scripts;
using Assets._Project.Components.QrCodeDetector.Shared.Scripts;
using Assets._Project.Components.UserNotifications.Shared.Scripts;
using Assets._Project.Shared.Scripts.Core.Extensions;
using Assets._Project.Shared.Scripts.Core.Fsm;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Assets._Project.Shared.Scripts.Identity;
using Meemim.VGis.Unity.Plugins.Interfaces.Dto;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;


namespace Assets._Project.Components.EntryManager.Shared.Scripts
{
    /// <summary>
    /// Controls entry to the application process.
    /// Use next fsm for workflow https://www.lucidchart.com/invitations/accept/a518708e-9159-42fe-990e-49c3645f3bdf
    /// </summary>
    public class EntryManager: MonoBehaviour
    {
        private const char QR_CODE_RESULT_SPLIT_SYMBOL = '|';
        private const int QR_CODE_RESULT_TOKEN_INDEX = 0;
        private const int QR_CODE_RESULT_COMPANY_INDEX = 1;
        private const string SIGN_IN_ISNT_VALID_MESSAGE = "Sign in token isn't valid or already used.";

        #region Events

        public UnityEvent CompanySelected;
        public UnityEvent SignInCanceled;

        #endregion


        public EntryManager()
        {
            // We create it directly in constructor since this event can be used before start is called.
            CompanySelected = new UnityEvent();
            SignInCanceled = new UnityEvent();
        }

        /// <summary>
        /// Specify what task should be done by entry manager.
        /// </summary>
        public enum TaskType
        {
            SignIn,
            AddingCompany
        }

        #region Injections

        /// <summary>
        /// Initializer that can be used instead of direct set to fields.
        /// </summary>
        public void InitData(MonoBehaviour qrCodeDetector, IExternalSignInRequestAdapter externalSignInRequestAdapter,
            GameObject companySelectionMenuPrefab, GameObject signInMenuPrefab, TaskType task)
        {
            if (qrCodeDetector == null) {
                throw new ArgumentNullException("qrCodeDetector");
            }
            if (externalSignInRequestAdapter == null) {
                throw new ArgumentNullException("externalSignInRequestAdapter");
            }
            if (companySelectionMenuPrefab == null) {
                throw new ArgumentNullException("companySelectionMenuPrefab");
            }
            if (signInMenuPrefab == null) {
                throw new ArgumentNullException("signInMenuPrefab");
            }
            if (qrCodeDetector == null) {
                throw new ArgumentNullException("qrCodeDetector");
            }

            QrCodeDetector = qrCodeDetector;
            ExternalSignInRequestAdapter = externalSignInRequestAdapter;
            CompanySelectionMenuPrefab = companySelectionMenuPrefab;
            SignInMenuPrefab = signInMenuPrefab;
            Task = task;
        }

        /// <summary>
        /// Object that detects qr code.
        /// </summary>
        public MonoBehaviour QrCodeDetector;
        public IExternalSignInRequestAdapter ExternalSignInRequestAdapter;
        public GameObject CompanySelectionMenuPrefab;
        public GameObject SignInMenuPrefab;
        public TaskType Task = TaskType.SignIn; 
        #endregion

        private static ILog _log;
        private TransitionTableFsm<Event, State, EntryManager> _fsm;
        private IdentityManager _identityManager;
        private CompaniesManager _companiesManager;
        private IQrCodeDetector _qrCodeDetector;
        private Coroutine _readQrCodeProcess;
        private GameObject _copmanySelectionMenu;
        private ISignInMenuController _signInMenu;
        private NotificationManager _notificationManager;

        #region Interstate data

        private IdentityViewModel _identity;
        private string _signInToken;
        private string _companySlug;

        #endregion

        #region Monobehaviour pipeline 
        private void Start()
        {
            _log = LogManager.GetLogger(typeof(EntryManager));
            _log.LogInfo("Start.");

            _identityManager = IdentityManager.Instance;
            Assert.IsNotNull(_identityManager);

            Assert.IsNotNull(QrCodeDetector);
            _qrCodeDetector = QrCodeDetector.GetComponent<IQrCodeDetector>();
            Assert.IsNotNull(_qrCodeDetector);

            _companiesManager = CompaniesManager.Instance;
            Assert.IsNotNull(_companiesManager);
            _companiesManager.OnActiveCompanyUpdated.AddListener(() => _fsm.TriggerEvent(Event.CompanySelected));

            Assert.IsNotNull(CompanySelectionMenuPrefab);
            Assert.IsNotNull(ExternalSignInRequestAdapter);

            Assert.IsNotNull(NotificationManager.Instance);
            _notificationManager = NotificationManager.Instance.GetComponent<NotificationManager>();
            Assert.IsNotNull(_notificationManager);

            InitFsm();
            _fsm.StartFsm()
                .AsTask()
                .RunCoroutine(this);
        }

        #endregion

        #region FSM declaration

        private enum State
        {
            TokenRefreshing,
            SignIn,
            CompanySelection,
            End
        }

        private enum Event
        {
            // Main events.
            AccessTokenRefreshed,
            RefreshTokenIsnotFound,
            RefreshAccessTokenFailed,
            RefreshTokenSkipped,
            SignedIn,
            SignInCanceled,
            CompanySelected,
            
            // Third party events.
            QrCodeDetected,
            DeepLinkTriggered,
            DebugSignInTriggered
        }

        private void InitFsm()
        {
            _fsm = new TransitionTableFsm<Event, State, EntryManager>(this, State.TokenRefreshing, "EntryManager");

            // Init states
            _fsm.CreateState(State.TokenRefreshing).Enter(fsm => RefreshAccessToken());
            // TODO:22864 consider to add multiple actions per event for table fsm and call them event if previous failed.
            _fsm.CreateState(State.SignIn).Enter(fsm => {
                fsm.ShowSignInMenu();
                fsm.StartReadQrCode();
                fsm.StartListenDeepLinks();
                fsm.StartListenDebugSignIn();
            }).Exit(fsm => {
                fsm.HideSignInMenu();
                fsm.StopReadQrCode();
                fsm.StopListenDeepLinks();
                fsm.StopListenDebugSignIn();
            });

            _fsm.CreateState(State.CompanySelection)
                .Enter(fsm => {
                    Assert.IsNotNull(_identity);
                    fsm.StartCompanySelection(_identity);
                })
                .Exit(fsm => fsm.StopCompanySelection());

            _fsm.CreateState(State.End);

            // Init transitions.
            _fsm.AddTransition(State.TokenRefreshing, Event.AccessTokenRefreshed, State.CompanySelection, null);
            _fsm.AddTransition(State.TokenRefreshing, Event.RefreshTokenIsnotFound, State.SignIn, null);
            _fsm.AddTransition(State.TokenRefreshing, Event.RefreshAccessTokenFailed, State.SignIn, null);
            _fsm.AddTransition(State.TokenRefreshing, Event.RefreshTokenSkipped, State.SignIn, null);

            _fsm.AddTransition(State.SignIn, Event.AccessTokenRefreshed, State.CompanySelection, null);
            _fsm.AddTransition(State.SignIn, Event.SignedIn, State.CompanySelection, null);
            _fsm.AddTransition(State.SignIn, Event.QrCodeDetected, null, fsm => {
                Assert.IsFalse(string.IsNullOrEmpty(_signInToken));
                Assert.IsFalse(string.IsNullOrEmpty(_companySlug));
                fsm.SignIn(_signInToken, _companySlug);
            });
            _fsm.AddTransition(State.SignIn, Event.DeepLinkTriggered, null, fsm => {
                Assert.IsFalse(string.IsNullOrEmpty(_signInToken));
                Assert.IsFalse(string.IsNullOrEmpty(_companySlug));
                fsm.SignIn(_signInToken, _companySlug);
            });
            _fsm.AddTransition(State.SignIn, Event.DebugSignInTriggered, null, fsm => fsm.SignInByDeviceId());
            // TODO: support end state transition in fsm.
            _fsm.AddTransition(State.SignIn, Event.SignInCanceled, State.End, fsm => fsm.NotifyAboutSignInCanceled() );

            // TODO:22864 add end state support.
            _fsm.AddTransition(State.CompanySelection, Event.CompanySelected, State.End, fsm => fsm.NotifyAboutSelectedCompany());
        }
        #endregion

        #region FSM actions

        #region Token refreshing actions

        private IEnumerator RefreshAccessToken()
        {
            _log.LogInfo("Refresh access token.");
            // We assume that token exists if adding company requested.
            if (Task == TaskType.AddingCompany) {
                _fsm.TriggerEvent(Event.RefreshTokenSkipped);
                yield break;
            }

            var refreshToken = _identityManager.GetDeviceRefreshToken();

            if (string.IsNullOrEmpty(refreshToken)) {
                _log.LogInfo("Can't find refersh token.");
                _fsm.TriggerEvent(Event.RefreshTokenIsnotFound);
                yield break;
            }

            // Get access token by refresh token.
            var accessTokenTask = _identityManager.AccessTokenRefresh(refreshToken).AsTask<IdentityViewModel>();
            yield return accessTokenTask.Run();

            if (accessTokenTask.IsFaulted) {
                _log.LogInfo("Can't refresh access token, sign in is required.");
                _fsm.TriggerEvent(Event.RefreshAccessTokenFailed);
            } else {
                _identity = accessTokenTask.Result;
                _fsm.TriggerEvent(Event.AccessTokenRefreshed);
            }
        }

        #endregion

        #region SignIn actions

        private void ShowSignInMenu()
        {
            _log.LogInfo("Show sign in menu.");
            if (_signInMenu != null) {
                Destroy(_signInMenu.GetGameObject());
            }

            _signInMenu = Instantiate(SignInMenuPrefab).AddToRoot().GetComponent<ISignInMenuController>();
            Assert.IsNotNull(_signInMenu);
            _signInMenu.InitData(Task == TaskType.AddingCompany);
            _signInMenu.Show();
            _signInMenu.OnCloseSignInRequested(() => { _fsm.TriggerEvent(Event.SignInCanceled); });
        }

        private void StartReadQrCode()
        {
            _log.LogInfo("Start read qr code.");
            ReadQrCodeWithRetry();
        }

        private void StopReadQrCode()
        {
            _log.LogInfo("Stop read qr code.");
            _qrCodeDetector.CancelQrCodeScanning();
        }

        private void StartListenDeepLinks()
        {
            _log.LogInfo("Start listen deep links.");
            ExternalSignInRequestAdapter.OnSignInByTokenRequested(DeepLinkSignInListener);
        }

        private void HideSignInMenu()
        {
            _log.LogInfo("Hide sign in menu.");
            Destroy(_signInMenu.GetGameObject());
        }

        private void StopListenDeepLinks()
        {
            _log.LogInfo("Stop listen deep links.");
            ExternalSignInRequestAdapter.UnsubscribeSignInByTokenRequsted(DeepLinkSignInListener);
        }

        private void StartListenDebugSignIn()
        {
            _log.LogInfo("Start listen debug sign in.");
            ExternalSignInRequestAdapter.OnSingInByDeviceIdRequested(DebugSignInListener);
        }

        private void StopListenDebugSignIn()
        {
            _log.LogInfo("Stop listen debug sign in.");
            ExternalSignInRequestAdapter.UnsubscribeSignInByDeviceIdRequested(DebugSignInListener);
        }

        // TODO: make it return IEnumerator.
        private void SignIn(string signInToken, string company)
        {
            _log.LogInfo("Sign in.");
            _identityManager.SignIn(signInToken, company)
                .AsTask<IdentityViewModel>()
                .ContinueWith(t => {
                    // TODO: consider to check status code 400 and NotValid key in order to detect this error more precise and don't show
                    // this message in case of 404/500 etc.
                    if (t.IsFaulted) {
                        _notificationManager.ShowNotification(NotificationType.Warning, SIGN_IN_ISNT_VALID_MESSAGE);
                    }
                    _identity = t.Result;
                    _fsm.TriggerEvent(Event.SignedIn);
                })
                .RunCoroutine(this);
        }

        private void SignInByDeviceId()
        {
            _log.LogInfo("Sign in by device id.");
            _identityManager.SignInByDeviceId()
                .AsTask<IdentityViewModel>()
                .ContinueWith(t => {
                    _identity = t.Result;
                    _fsm.TriggerEvent(Event.SignedIn);
                })
                .RunCoroutine(this);
        }

        private void NotifyAboutSignInCanceled()
        {
            _log.LogInfo("Notify about sign in canceled.");

            SignInCanceled.Invoke();
        }


        #region Sign in methods

        private void ReadQrCodeWithRetry()
        {
            _log.LogInfo("Read qr code.");
            Assert.IsNull(_readQrCodeProcess);
            _readQrCodeProcess = ReadQrCodeProcess()
                .AsTask()
                .ContinueWith(t => {
                    if (t.IsFaulted) {
                        _log.LogInfo("Retry qr code read.");
                        _log.LogException(t.Exception);
                        _readQrCodeProcess = null;
                        ReadQrCodeWithRetry();
                    }
                })
                .RunCoroutine(this);
        }

        private IEnumerator ReadQrCodeProcess()
        {
            var qrCodeScanningTask = _qrCodeDetector.ScanQrCodeWithRetry().AsTask<QRCodeResult>();
            yield return qrCodeScanningTask.Run();

            // Stop process if scanning cancelled.
            if (qrCodeScanningTask.Result == null) {
                yield break;
            }

            var tokenAndAddedCompany = qrCodeScanningTask.Result.Text.Split(QR_CODE_RESULT_SPLIT_SYMBOL);
            _signInToken = tokenAndAddedCompany[QR_CODE_RESULT_TOKEN_INDEX];
            _companySlug = tokenAndAddedCompany[QR_CODE_RESULT_COMPANY_INDEX];

            _fsm.TriggerEvent(Event.QrCodeDetected);
        }

        private void DeepLinkSignInListener(string signInToken, string companySlug)
        {
            _signInToken = signInToken;
            _companySlug = companySlug;
            _fsm.TriggerEvent(Event.DeepLinkTriggered);
        }

        private void DebugSignInListener()
        {
            _fsm.TriggerEvent(Event.DebugSignInTriggered);
        }

        #endregion

        #endregion

        #region CompanySelection actions

        private void StartCompanySelection(IdentityViewModel identityViewModel)
        {
            _log.LogInfo("Start company selection.");

            var isSignInTokenWasForSpecifiedCompany = !string.IsNullOrEmpty(_companySlug) && !_identityManager.IsSignInTokenForSuperdevice(_companySlug);
            if (identityViewModel.CompaniesSlugs.Count == 1) {
                UpdateActiveCompany(identityViewModel.CompaniesSlugs.SingleOrDefault());
            // Just select company automatically if it was adding company task.
            } else if (Task == TaskType.AddingCompany && isSignInTokenWasForSpecifiedCompany) {
                UpdateActiveCompany(_companySlug);
            } else {
                CreateCompanySelectionMenu();
            }
        }

        private void StopCompanySelection()
        {
            _log.LogInfo("Stop company selection.");
            Destroy(_copmanySelectionMenu);
        }

        private void NotifyAboutSelectedCompany()
        {
            _log.LogInfo("Notify about selected company.");
            CompanySelected.Invoke();
        }

        #region Company selection methods

        private void CreateCompanySelectionMenu()
        {
            if (_copmanySelectionMenu != null) {
                Destroy(_copmanySelectionMenu);
            }

            _copmanySelectionMenu = Instantiate(CompanySelectionMenuPrefab).AddToRoot();
            var controller = _copmanySelectionMenu.GetComponent<ICompanySelectionMenuController>();
            Assert.IsNotNull(controller);
            controller.Init(false, false);
        }

        private void UpdateActiveCompany(string companySlug)
        {
            _companiesManager.UpdateActiveCompany(companySlug)
                .AsTask()
                .RunCoroutine(this);
        }

        #endregion
        #endregion

        #endregion
    }
}
