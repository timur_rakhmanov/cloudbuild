﻿using UnityEngine.Events;


namespace Assets._Project.Components.EntryManager.Shared.Scripts
{
    /// <summary>
    /// Adapter for sign in commands from external system.
    /// </summary>
    public interface IExternalSignInRequestAdapter
    {
        void OnSignInByTokenRequested(UnityAction<string, string> handler);
        void UnsubscribeSignInByTokenRequsted(UnityAction<string, string> handler);
        void OnSingInByDeviceIdRequested(UnityAction handler);
        void UnsubscribeSignInByDeviceIdRequested(UnityAction handler);
    }
}