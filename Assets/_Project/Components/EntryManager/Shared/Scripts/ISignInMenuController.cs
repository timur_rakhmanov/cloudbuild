﻿using UnityEngine;
using UnityEngine.Events;


namespace Assets._Project.Components.EntryManager.Shared.Scripts
{
    public interface ISignInMenuController
    {
        /// <summary>
        /// Notify when close sign in requested.
        /// </summary>
        /// <param name="handler"></param>
        void OnCloseSignInRequested(UnityAction handler);

        void InitData(bool allowToClose);

        void Show();

        void Hide();

        GameObject GetGameObject();
    }
}