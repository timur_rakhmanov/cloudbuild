﻿using UnityEngine;


namespace Assets._Project.Components.Loader.Shared.Scripts
{
    public class GlobalLoaderController : MonoBehaviour
    {

        public float SpinSpeed = 100f;

        private GameObject _valve;

        void Start()
        {
            _valve = transform.Find("LoaderCanvas/Valve").gameObject;
            transform.Find("LoaderCanvas").gameObject.GetComponent<Canvas>().worldCamera = Camera.main;
        }
	
        void Update ()
        {
            _valve.transform.Rotate(Vector3.back, SpinSpeed * Time.deltaTime);
        }
    }
}
