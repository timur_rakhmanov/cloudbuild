﻿namespace Assets._Project.Components.Loader.Shared.Scripts
{
    /// <summary>
    /// Shows/hides loaders.
    /// </summary>
    public interface ILoaderController
    {
        /// <summary>
        /// Show loader.
        /// Throws exception if loaded isn't hidden after timeout.
        /// </summary>
        void ShowLoader();

        /// <summary>
        /// Hide loader.
        /// </summary>
        void HideLoader();

        /// <summary>
        /// Reset loader after timeout.
        /// </summary>
        void ResetLoaderAfterTimeout();
    }
}
