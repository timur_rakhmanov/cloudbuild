﻿using System;
using System.Collections;
using Assets._Project.Shared.Scripts.Core.Tasks;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;


namespace Assets._Project.Components.Loader.Shared.Scripts
{
    public class LoaderController: MonoBehaviour, ILoaderController
    {
        private const int PERIOD_TIME = 1;
        private const int TIMEOUT_SECONDS = 45;

        /// <summary>
        /// Notify about timeout instead of automatically stop loader.
        /// </summary>
        public bool WithTimoutNotification = false;

        /// <summary>
        /// Loader that should be shown/hidden.
        /// </summary>
        public GameObject LoaderPrefab;

        /// <summary>
        /// Is called when loader is shown too long.
        /// </summary>
        public UnityEvent OnLoaderTimout;

        private GameObject _loaderInstance;
        private int _runLoaders = 0;
        private DateTime _timeoutTime;

        // TODO: add unittests.
        // TODO: implement timeout.
        public void ShowLoader()
        {
            Assert.IsNotNull(LoaderPrefab);
            
            _timeoutTime = DateTime.UtcNow.AddSeconds(TIMEOUT_SECONDS);

            // Attach to game object.
            if (_runLoaders == 0) {
                if (_loaderInstance == null) {
                    _loaderInstance = Instantiate(LoaderPrefab);
                    _loaderInstance.transform.SetParent(gameObject.transform);
                }

                DetectTimout()
                    .AsTask()
                    .ContinueWith(t => {
                            if (t.IsFaulted) {
                                Debug.LogErrorFormat("Exception at the loader.");
                                Debug.LogException(t.Exception);
                                ResetLoader();
                            }
                        }
                    )
                    .RunCoroutine(this);
            }
            
            ++_runLoaders;
        }

        public void HideLoader()
        {
            if (_runLoaders > 0) {
                --_runLoaders;
            }

            if (_runLoaders == 0) {
                ResetLoader();
            }
        }

        /// <summary>
        /// Reset loader after timeout.
        /// </summary>
        public void ResetLoaderAfterTimeout()
        {
            ResetLoader();
        }

        private void ResetLoader()
        {
            _timeoutTime = DateTime.MaxValue;
            _runLoaders = 0;
            StopAllCoroutines();
            if (_loaderInstance != null) {
                Destroy(_loaderInstance);
                _loaderInstance = null;
            }
        }

        private IEnumerator DetectTimout()
        {
            while (true) {
                if (_timeoutTime < DateTime.UtcNow) {
                    Debug.LogWarningFormat("Loader {0} is timed out.", gameObject.name);
                    if (WithTimoutNotification) {
                        _timeoutTime = DateTime.UtcNow.AddSeconds(TIMEOUT_SECONDS);
                        OnLoaderTimout.Invoke();
                    } else {
                        ResetLoader();
                        yield break;
                    }
                }
                yield return new WaitForSeconds(PERIOD_TIME);
            }
        }
    }
}