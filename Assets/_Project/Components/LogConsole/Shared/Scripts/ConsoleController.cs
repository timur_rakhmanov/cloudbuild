﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


namespace Assets._Project.Components.LogConsole.Shared.Scripts
{
    public class ConsoleController : MonoBehaviour
    {

        private const int MAX_LINES = 20;
        private Queue<string> _buffer = new Queue<string>();
        private bool _isEnabled = false;

        public void Show()
        {
            gameObject.GetComponent<Canvas>().enabled = true;
            _isEnabled = true;
            RenderText();
        }

        public void Hide()
        {
            gameObject.GetComponent<Canvas>().enabled = false;
            _isEnabled = false;
        }

        public bool IsShown()
        {
            return gameObject.GetComponent<Canvas>().enabled;
        }

        /// <summary>
        /// Register the HandleLog function on scene start to fire on debug.log events 
        /// </summary>
        private void Awake()
        {
            gameObject.GetComponent<Canvas>().worldCamera = Camera.main;
            Application.logMessageReceivedThreaded += HandleLog;
        }

        private void OnApplicationQuit()
        {
            Application.logMessageReceivedThreaded -= HandleLog;
        }

        private void HandleLog(string logString, string stackTrace, LogType type)
        {
            if (_buffer.Count > MAX_LINES) {
                _buffer.Dequeue();
            }
            _buffer.Enqueue(type + ": " + logString);

            if (_isEnabled) {
               RenderText();
            }
        }

        private void RenderText()
        {
            var console = gameObject.GetComponentInChildren<Text>();
            console.text = "";
            _buffer.ToList().ForEach(o => console.text += System.Environment.NewLine + o);
        }
    }
}