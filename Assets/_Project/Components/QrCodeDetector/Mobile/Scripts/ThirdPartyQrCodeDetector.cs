﻿using System;
using System.Collections;
using Assets._Project.Components.QrCodeDetector.Shared.Scripts;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using UnityEngine;


namespace Assets._Project.Components.QrCodeDetector.Mobile.Scripts
{
    /// <summary>
    /// Qr code detector that uses third party plugin https://github.com/kefniark/UnityBarcodeScanner
    /// </summary>
    public class ThirdPartyQrCodeDetector: QrCodeDetectorBase
    {
        private static ILog _log;

        private new void Start()
        {
            // TODO: check if it is correct way to call start in parent.
            base.Start();
            _log = LogManager.GetLogger(typeof(ThirdPartyQrCodeDetector));
        }

        protected override IEnumerator ScanQrCode(TimeSpan timeout)
        {
            // TODO:22866 implement this part with help of https://github.com/kefniark/UnityBarcodeScanner
            _log.LogInfo("Scan qr code isn't implemented.");
            yield return new WaitForSeconds(1);
        }

        public void CloseQrCodeScanningScreen()
        {
            Destroy(gameObject);
        }
    }
}