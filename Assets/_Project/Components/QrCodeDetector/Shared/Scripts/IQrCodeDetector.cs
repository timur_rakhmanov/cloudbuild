﻿using System.Collections;

namespace Assets._Project.Components.QrCodeDetector.Shared.Scripts
{
    public interface IQrCodeDetector
    {
        /// <summary>
        /// Start scan qr code with retry. Reading are stopped only on success.
        /// </summary>
        /// <returns>Returns <see cref="Meemim.VGis.Unity.Plugins.Interfaces.Dto.QRCodeResult"/> or null if reading was canceled.</returns>
        IEnumerator ScanQrCodeWithRetry();

        /// <summary>
        /// Cancel qr code scanning.
        /// </summary>
        void CancelQrCodeScanning();
    }
}
