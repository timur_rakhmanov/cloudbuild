﻿using System;
using System.Collections;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Extensions;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Meemim.VGis.Unity.Plugins.Interfaces.Dto;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using UnityEngine;
using UnityEngine.Assertions;


namespace Assets._Project.Components.QrCodeDetector.Shared.Scripts
{

    /// <summary>
    /// Base class for qr code detectory.
    /// Implements template pattern for delegating reading implementation to the children.
    /// </summary>
    public abstract class QrCodeDetectorBase: MonoBehaviour, IQrCodeDetector
    {
        private const int RETRY_DELAY_SECONDS = 3;
        private const int READING_TIMEOUT_SECONDS = 20;
        private static ILog _log;

        #region Editor configurations 

        /// <summary>
        /// Prefab that is used while qr code is scanned.
        /// </summary>
        public GameObject QrCodeScanningSplashScreenPrefab;
        #endregion

        private CancellationToken _cancellation;

        /// <summary>
        /// Indicates that process is cancelled.
        /// </summary>
        private class CancellationToken
        {
            private bool _canceled = false;

            /// <summary>
            /// Cancel process.
            /// </summary>
            public void Cancel()
            {
                _canceled = true;
            }

            /// <summary>
            /// Detect if process cancelled.
            /// </summary>
            /// <returns></returns>
            public bool IsCancelled()
            {
                return _canceled;
            }
        }

        private class QrCodeScanningSplashScreen : IDisposable
        {
            private readonly GameObject _splashScreen;
            private readonly CancellationToken _cancellationToken;

            public QrCodeScanningSplashScreen(GameObject splashScreenPrefab, 
                CancellationToken cancellationToken, 
                MonoBehaviour baseMonoBehaviour)
            {
                Assert.IsNotNull(splashScreenPrefab);
                Assert.IsNotNull(cancellationToken);
                Assert.IsNotNull(baseMonoBehaviour);

                _cancellationToken = cancellationToken;

                _splashScreen = Instantiate(splashScreenPrefab).AddToRoot();
                DetectCancelation()
                    .AsTask()
                    .RunCoroutine(baseMonoBehaviour);
            }

            public void Dispose()
            {
                if (!_cancellationToken.IsCancelled()) {
                    Destroy(_splashScreen);
                }
            }

            private IEnumerator DetectCancelation()
            {
                while (true) {
                    if (_cancellationToken.IsCancelled()) {
                        Destroy(_splashScreen);
                        yield break;
                    }
                    yield return null;
                }
            }
        }

        /// <summary>
        /// Method that should be called at children at the start.
        /// </summary>
        protected void Start()
        {
            Assert.IsNotNull(QrCodeScanningSplashScreenPrefab);
            _log = LogManager.GetLogger(typeof(QrCodeDetectorBase));
        }

        /// <summary>
        /// Scan qr code with given timeout.
        /// </summary>
        /// <param name="timeout">Timeout seconds.</param>
        /// <returns>Returns <see cref="Meemim.VGis.Unity.Plugins.Interfaces.Dto.QRCodeResult"/></returns>
        protected abstract IEnumerator ScanQrCode(TimeSpan timeout);

        public IEnumerator ScanQrCodeWithRetry()
        {
            _log.LogInfo("Starting QR code scanning.");
            _cancellation = new CancellationToken();
            using (new QrCodeScanningSplashScreen(QrCodeScanningSplashScreenPrefab, _cancellation, this)) {
                var task = ScanQrCodeWithDelay().AsTask<QRCodeResult>();
                yield return task.Run();
                while (task.IsFaulted) {
                    if (_cancellation.IsCancelled()) {
                        yield return null;
                        yield break;
                    }

                    // TODO: now we don't see difference between timeout error and real error so log all as info.
                    // Need to get more specified error from plugin and logs timeout as info and all other as errors.
                    _log.LogInfoFormat("Failed to read qr code, reasone {0}", task.Exception);
                    // Wait few seconds before retry.
                    yield return new WaitForSeconds(RETRY_DELAY_SECONDS);
                    _log.LogInfo("Retry QR code scanning.");
                    task = ScanQrCodeWithDelay().AsTask<QRCodeResult>();
                    yield return task.Run();
                }

                _log.LogInfo("QR code successfully scanned.");
                yield return task.Result;
            }
        }

        public void CancelQrCodeScanning()
        {
            _cancellation.Cancel();
        }

        private IEnumerator ScanQrCodeWithDelay()
        {
            var task = ScanQrCode(TimeSpan.FromSeconds(READING_TIMEOUT_SECONDS)).AsTask<QRCodeResult>();
            yield return task.Run();
            yield return task.Result;
        }
    }
}