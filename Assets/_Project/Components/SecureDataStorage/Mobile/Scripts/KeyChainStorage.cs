﻿using Assets._Project.Components.SecureDataStorage.Shared.Scripts;
using FSG.iOSKeychain;
using Newtonsoft.Json;
using UnityEngine;


namespace Assets._Project.Components.SecureDataStorage.Mobile.Scripts
{
    /// <summary>
    /// Store data in IOS key chain storage in json format.
    /// </summary>
    public class KeyChainStorage: MonoBehaviour, ISecureDataStorage
    {
        public void Save<T>(string key, T data) where T: class
        {
            var stringData = JsonConvert.SerializeObject(data);
            Keychain.SetValue(key, stringData);
        }

        public T Load<T>(string key) where T: class
        {
            var stringData = Keychain.GetValue(key);
            if (string.IsNullOrEmpty(stringData)) {
                return null;
            }

            return JsonConvert.DeserializeObject<T>(stringData);
        }

        public void Remove(string key)
        {
            Keychain.DeleteValue(key);
        }
    }
}
