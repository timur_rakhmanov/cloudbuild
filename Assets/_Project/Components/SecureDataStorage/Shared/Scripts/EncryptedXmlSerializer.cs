﻿using System.IO;
using System.Xml.Serialization;
using UnityEngine;


#if NETFX_CORE
using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;
#endif

namespace Assets._Project.Components.SecureDataStorage.Shared.Scripts
{
    /// <summary>
    /// Class provides methods to serialize and encrypt data to XML
    /// </summary>
    public class EncryptedXmlSerializer : MonoBehaviour, ISecureDataStorage
    {
       //private readonly string _privateKey = ExecutionContext.Instance.DeviceId;

       //private const string AES_IV = "15CV1/ZOnVI3rY4wk4INBg=="; 

        private const string FILE_EXTENSION = "xml";
#region API

        /// <summary>
        /// Reads and decrypts file at specified path
        /// </summary>
        /// <param name="key">Patht to file</param>
        /// <typeparam name="T">Type of the serialized object</typeparam>
        /// <returns>Decrypted deserialized object or null if file does not exist</returns>
        public T Load<T>(string key) where T : class
        {
            T result;

            var filePath = GetFilePath(key);

            if (!File.Exists(filePath)) {
                Debug.Log("File " + filePath + " does not exist!");
                return null;
            }

            string data;
#if NETFX_CORE
            using (var reader = new StreamReader(new FileStream(filePath, FileMode.Open)))
            {
                //data = DecryptData(reader.ReadToEnd());
                data = reader.ReadToEnd();
            }
#else
            using (var reader = new StreamReader(filePath)) {
                //data = DecryptData(reader.ReadToEnd());
                data = reader.ReadToEnd();
            }
#endif

            var stream = new MemoryStream();
            using (var streamWriter = new StreamWriter(stream) { AutoFlush = true }) {
                streamWriter.WriteLine(data);
                stream.Position = 0;
                result = new XmlSerializer(typeof(T)).Deserialize(stream) as T;
            }

            return result;
        }

        public void Remove(string key)
        {
            var filePath = GetFilePath(key);
            if (!File.Exists(filePath)) {
                Debug.LogFormat("File '{0}' does not exist!", filePath);
            }
            // NOTE: isn't tested in uwp.
            File.Delete(filePath);
        }

        public void Save<T>(string key, T value) where T : class
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var stream = new MemoryStream())
            {
                serializer.Serialize(stream, value);
                stream.Flush();
                stream.Position = 0;
                var data = new StreamReader(stream).ReadToEnd();
                var filePath = GetFilePath(key);
                var fileStream = new FileStream(filePath, FileMode.Create);
                var streamWriter = new StreamWriter(fileStream);
                streamWriter.WriteLine(data);

                //streamWriter.WriteLine(EncryptData(data));
#if NETFX_CORE
                streamWriter.Dispose();
                fileStream.Dispose();
#else
                streamWriter.Close();
                fileStream.Close();
#endif
            }
        }

#endregion

//#region private methods

//        private string EncryptData(string toEncrypt)
//        {
//#if NETFX_CORE
//            var key = Convert.FromBase64String(_privateKey).AsBuffer();
//            var iv = Convert.FromBase64String(AES_IV).AsBuffer();
//            SymmetricKeyAlgorithmProvider provider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);

//            IBuffer bufferMsg = CryptographicBuffer.ConvertStringToBinary(toEncrypt, BinaryStringEncoding.Utf8);
//            IBuffer bufferEncrypt = CryptographicEngine.Encrypt(provider.CreateSymmetricKey(key), bufferMsg, iv);
//            var resultArray = bufferEncrypt.ToArray();
//#else
//            var toEncryptArray = Encoding.UTF8.GetBytes(toEncrypt);
//            var rDel = CreateRijndaelManaged();
//            var cryptoTransform = rDel.CreateEncryptor();
//            var resultArray = cryptoTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
//#endif

//            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
//        }

//        private string DecryptData(string toDecrypt)
//        {
//#if NETFX_CORE
//            var key = Convert.FromBase64String(_privateKey).AsBuffer();
//            var iv = Convert.FromBase64String(AES_IV).AsBuffer();
//            var toDecryptArray = Encoding.UTF8.GetBytes(toDecrypt);
//            SymmetricKeyAlgorithmProvider provider = SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithmNames.AesCbcPkcs7);

//            IBuffer bufferDecrypt = CryptographicEngine.Decrypt(provider.CreateSymmetricKey(key), toDecryptArray.AsBuffer(), iv);
//            var resultArray = bufferDecrypt.ToArray();
//#else
//            var toEncryptArray = Convert.FromBase64String(toDecrypt);
//            var rijndaelManaged = CreateRijndaelManaged();
//            var cryptoTransform = rijndaelManaged.CreateDecryptor();
//            var resultArray = cryptoTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

//#endif
//            return Encoding.UTF8.GetString(resultArray);
//        }

//#if !NETFX_CORE
//        private RijndaelManaged CreateRijndaelManaged()
//        {
//            var keyArray = Encoding.UTF8.GetBytes(_privateKey);
//            var result = new RijndaelManaged();

//            var newKeysArray = new byte[16];
//            Array.Copy(keyArray, 0, newKeysArray, 0, 16);

//            result.Key = newKeysArray;
//            result.Mode = CipherMode.ECB;
//            result.Padding = PaddingMode.PKCS7;
//            return result;
//        }
//#endif

//#endregion

        private string GetFileName(string key)
        {
            return key + "." + FILE_EXTENSION;
        }

        private string GetFilePath(string key)
        {
            return Path.Combine(Application.persistentDataPath, GetFileName(key));
        }
    }
}