﻿namespace Assets._Project.Components.SecureDataStorage.Shared.Scripts
{
    public interface ISecureDataStorage
    {
        /// <summary>
        /// Save secure data with specified key.
        /// Override existent data.
        /// </summary>
        /// <typeparam name="T">Type of object to save</typeparam>
        /// <param name="key">Key that is used for identify data.</param>
        /// <param name="data">Secure data</param>
        void Save<T>(string key, T data) where T : class;

        /// <summary>
        /// Reading secure data with specified key.
        /// Returns null if data with key doesn't exist.
        /// </summary>
        /// <typeparam name="T">Type of the loaded object</typeparam>
        /// <param name="key">Data key.</param>
        /// <returns></returns>
        T Load<T>(string key) where T : class;

        /// <summary>
        /// Remove data with specified key.
        /// Doesnt throw any exceptions if key doesn't exist.
        /// </summary>
        /// <param name="key">Data key.</param>
        void Remove(string key);
    }
}
