﻿using System.Collections;
using Assets._Project.Shared.Scripts.Core.Tasks;
using UnityEngine;
using UnityEngine.UI;


namespace Assets._Project.Components.UserNotifications.Shared.Scripts
{
    public class NotificationController : MonoBehaviour
    {
        #region Constants

        private const float MESSAGE_LIFETIME = 5.0f;

        #endregion

        #region Editor configuration

        public Text NotificationText;
        public bool IsPermanent;

        #endregion

        #region Mono behaviour pipeline

        void Start()
        {
            if (!IsPermanent) {
                NotificationLife().AsTask().RunCoroutine(this);
            }
        }

        #endregion

        #region Private data

        private IEnumerator NotificationLife()
        {
            yield return new WaitForSeconds(MESSAGE_LIFETIME);
            
            Destroy(gameObject);
        }

        #endregion
    }
}
