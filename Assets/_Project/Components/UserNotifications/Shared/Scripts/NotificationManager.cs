﻿using System;
using System.Collections.Generic;
using HoloToolkit.Unity;
using UnityEngine;


namespace Assets._Project.Components.UserNotifications.Shared.Scripts
{
    public class NotificationManager : Singleton<NotificationManager>
    {
        #region Editor configuration

        public GameObject InfoNotificationPrefab;
        public GameObject SuccessNotificationPrefab;
        public GameObject WarningNotificationPrefab;
        public GameObject ErrorNotificationPrefab;

        #endregion

        #region Private data

        private Dictionary<NotificationType, GameObject> _notificationPrefabs;

        private readonly Dictionary<Guid, GameObject> _notificationDictionary = new Dictionary<Guid, GameObject>();

        #endregion

        #region Mono behaviour pipeline

        void Start()
        {
            _notificationPrefabs = new Dictionary<NotificationType, GameObject> {
                { NotificationType.Info, InfoNotificationPrefab },
                { NotificationType.Success, SuccessNotificationPrefab },
                { NotificationType.Warning, WarningNotificationPrefab },
                { NotificationType.Error, ErrorNotificationPrefab }
            };
        }

        #endregion

        #region Commands

        /// <summary>
        /// Show new not permanent notification
        /// </summary>
        /// <param name="type">Notification type</param>
        /// <param name="notificationText">Notification text message</param>
        public void ShowNotification(NotificationType type, string notificationText)
        {
            var notification = Instantiate(_notificationPrefabs[type]);
            notification.transform.SetParent(transform, false);

            var notificationController = notification.GetComponent<NotificationController>();
            notificationController.IsPermanent = false;
            notificationController.NotificationText.text = notificationText;
        }

        /// <summary>
        /// Show new notification woth message
        /// </summary>
        /// <param name="type">Notification type</param>
        /// <param name="notificationText">Notification text message</param>
        /// <returns></returns>
        public Guid ShowNotificationPermanent(NotificationType type, string notificationText)
        {
            var notificationGuid = Guid.NewGuid();

            var notification = Instantiate(_notificationPrefabs[type]);
            notification.transform.SetParent(transform, false);

            var notificationController = notification.GetComponent<NotificationController>();
            notificationController.IsPermanent = true;
            notificationController.NotificationText.text = notificationText;

            _notificationDictionary.Add(notificationGuid, notification);

            return notificationGuid;
        }

        /// <summary>
        /// Destroy notification and clear data
        /// </summary>
        /// <param name="notificationGuid"></param>
        public void HideNotification(Guid notificationGuid)
        {
            if (!_notificationDictionary.ContainsKey(notificationGuid)) {
                throw new ArgumentException(string.Format("Notification with id '{0}' does not exist", notificationGuid));
            }

            Destroy(_notificationDictionary[notificationGuid]);

            _notificationDictionary.Remove(notificationGuid);
        }

        #endregion
    }
}
