﻿namespace Assets._Project.Components.UserNotifications.Shared.Scripts
{
    /// <summary>
    /// Describe ui notification type
    /// </summary>
    public enum NotificationType : byte
    {
        Info = 0,
        Success,
        Warning,
        Error
    }
}
