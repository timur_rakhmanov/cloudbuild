﻿using HoloToolkit.Unity;
using UnityEngine;


namespace Assets._Project.Mobile.Scripts.Core
{
    /// <summary>
    /// Rotates/move camera, code is taken here http://coffeebreakcodes.com/move-zoom-and-rotate-camera-unity3d/
    /// </summary>
    public class EditorCameraMovement : MonoBehaviour
    {
        private readonly float speed = 2.0f;
        private readonly float zoomSpeed = 2.0f;

        public float minX = -360.0f;
        public float maxX = 360.0f;

        public float minY = -45.0f;
        public float maxY = 45.0f;

        public float sensX = 100.0f;
        public float sensY = 100.0f;

        private float rotationY = 0.0f;
        private float rotationX = 0.0f;

        void Update()
        {
            // We control camera only in the editor since in real build device's position will control it.
#if UNITY_EDITOR
            float scroll = Input.GetAxis("Mouse ScrollWheel");
            transform.Translate(0, scroll * zoomSpeed, scroll * zoomSpeed, Space.World);

            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) {
                transform.position += transform.right * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)) {
                transform.position += -transform.right * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W)) {
                transform.position += transform.forward * speed * Time.deltaTime;
            }
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)) {
                transform.position += -transform.forward * speed * Time.deltaTime;
            }

            if (Input.GetMouseButton(0)) {
                rotationX += Input.GetAxis("Mouse X") * sensX * Time.deltaTime;
                rotationY += Input.GetAxis("Mouse Y") * sensY * Time.deltaTime;
                rotationY = Mathf.Clamp(rotationY, minY, maxY);
                transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
            }
#endif
        }
    }
}