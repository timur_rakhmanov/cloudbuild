﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using RestSharp.Contrib;
using UnityEngine;


namespace Assets._Project.Mobile.Scripts.GlobalManagers.Deeplinking
{
    public class DeeplinkingManager : MonoBehaviour
    {
        [HideInInspector]
        public LinkSignInRequestedEvent SignInTriggered;

        private static ILog _log;

        private const string TOKEN_KEY = "token";
        private const string COMPANY_SLUG_KEY = "company-slug";

        private IList<SignInData> _signInRequestedOldEvents = new List<SignInData>();

        /// <summary>
        /// Returns latest sign in triggered event.
        /// </summary>
        /// <returns></returns>
        public SignInData GetLatestSignInTriggeredEvent()
        {
            return _signInRequestedOldEvents.LastOrDefault();
        }

#if UNITY_IOS
        [DllImport("__Internal")]
        private static extern void UnityDeeplinks_init(string gameObject = null, string deeplinkMethod = null);
#endif

        /// <summary>
        /// Use this for initialization.
        /// </summary>
        // ReSharper disable once UnusedMember.Local
        private void Start()
        {
            _log = LogManager.GetLogger(typeof(DeeplinkingManager));

            if (SignInTriggered == null) {
                SignInTriggered = new LinkSignInRequestedEvent();
            }

#if UNITY_IOS
            if (Application.platform == RuntimePlatform.IPhonePlayer) {
                UnityDeeplinks_init(gameObject.name);
            }
#endif
        }


        // Update is called once per frame
        // ReSharper disable once UnusedMember.Local
        private void Update()
        {

        }

        // TODO:22864 check if it should be public and consider camel case name.
        // ReSharper disable once InconsistentNaming
        public void onDeeplink(string deeplink)
        {
            //Handling and parsing deeplink
            _log.LogInfo("onDeeplink " + deeplink);
            
            //Pass access token after parsing
            var signInToken = GetDeeplinkValueByKey(deeplink, TOKEN_KEY);
            var companySlug = GetDeeplinkValueByKey(deeplink, COMPANY_SLUG_KEY);
            SignInTriggered.Invoke(signInToken, companySlug);
            _signInRequestedOldEvents.Add(new SignInData(signInToken, companySlug));
        }

        private string GetDeeplinkValueByKey(string deeplink, string key)
        {
            Uri uri = new Uri(deeplink);

            return HttpUtility.ParseQueryString(uri.Query).Get(key);
        }
    }
}
