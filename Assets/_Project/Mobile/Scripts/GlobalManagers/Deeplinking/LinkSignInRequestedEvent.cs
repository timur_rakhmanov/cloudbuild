﻿using UnityEngine.Events;


namespace Assets._Project.Mobile.Scripts.GlobalManagers.Deeplinking
{
    [System.Serializable]
    public class LinkSignInRequestedEvent : UnityEvent<string, string>
    {
    }
}