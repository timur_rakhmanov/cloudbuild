﻿namespace Assets._Project.Mobile.Scripts.GlobalManagers.Deeplinking
{
    public class SignInData
    {
        public SignInData(string signInToken, string companySlug)
        {
            SignInToken = signInToken;
            CompanySlug = companySlug;
        }
        public string SignInToken { get; private set; }
        public string CompanySlug { get; private set; }
    }
}