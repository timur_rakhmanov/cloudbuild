﻿using UnityEngine;


namespace Assets._Project.Mobile.Scripts.GlobalManagers
{
    public class GPSData
    {
        public bool IsSuccess { get; set; }

        public LocationInfo LocationInfo { get; set; }
    }
}
