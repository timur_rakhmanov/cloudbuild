﻿using System;
using HoloToolkit.Unity;
using UnityEngine;


namespace Assets._Project.Mobile.Scripts.GlobalManagers
{
    public class GPSLocationManager : Singleton<GPSLocationManager>
    {
        private void Start()
        {
            if (!Input.location.isEnabledByUser) {
                throw new Exception("GPS Location is not enabled by user.");
            }

            Input.location.Start();
            Input.compass.enabled = true;
        }

        void OnDestroy()
        {
            Input.location.Stop();
            Input.compass.enabled = false;
        }

        public GPSData GetCurrentLocationInfo()
        {
            return Input.location.status == LocationServiceStatus.Running ? new GPSData { IsSuccess = true , LocationInfo = Input.location.lastData } : new GPSData { IsSuccess = false };
        }

        public float GetCurrentCompassTrueHeading()
        {
            return Input.compass.trueHeading;
        }
    }
}
