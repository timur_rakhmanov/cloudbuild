﻿using System;
using System.Collections;
using Assets._Project.Components.CompanySelectionMenu.Shared.Scripts;
using Assets._Project.Components.DebugInfoPanel.Share.Scripts;
using Assets._Project.Components.EntryManager.Shared.Scripts;
using Assets._Project.Shared.Scripts.Core.Fsm;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Assets._Project.Shared.Scripts.Identity;
using HoloToolkit.Unity;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using Assets._Project.Components.LogConsole.Shared.Scripts;
using Assets._Project.Components.UserNotifications.Shared.Scripts;
using Assets._Project.Mobile.Scripts.GlobalManagers.Deeplinking;
using Assets._Project.Mobile.Scripts.Ui;
using Assets._Project.Mobile.Scripts.Ui.CalibrationModule;
using Assets._Project.Mobile.Scripts.Ui.InfoCard;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Configuration;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Extensions;
using Assets._Project.Shared.Scripts.Infrastructure;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;


namespace Assets._Project.Mobile.Scripts.GlobalManagers
{
    /// <summary>
    /// Control mobile main scene. 
    /// TODO: this link only for signed in users, use shared readonly link (this feature was disabled for some time)
    /// Use next fsm for workflow https://www.lucidchart.com/invitations/accept/63b4fdc3-f337-4dcf-9bd4-24ef032dc138 
    /// </summary>
    public class MobileMainSceneManager : Singleton<MobileMainSceneManager>
    {
        private const int DEFAULT_RADIUS = 100;
        private const string SIGNEDIN_MESSAGE = "You already signed in.";
        private const string SIGNEDOUT_MESSAGE = "You already signed out.";
        private const string COMPANY_SELECTED = "Connected to {0}";

        #region Injections
        public GameObject MainMenuPrefab;
        public GameObject DebugMenuPrefab;
        public GameObject CalibrationMenuPrefab;
        public GameObject LogConsole;
        public GameObject DebugInfoMenu;
        public GameObject DeeplinkingPrefab;
        public GameObject CompanySelectionMenuPrefab;
        public GameObject SignInMenuPrefab;
        public MonoBehaviour QrCodeDetector;
        public GameObject InfoCardPrefab;
        public GameObject SceneScreenInfoPanelPrefab;
        
        #endregion

        private static ILog _log;
        private IdentityManager _identityManager;
        private TransitionTableFsm<Event, State, MobileMainSceneManager> _fsm;
        private MainMenuController _mainMenuController;
        private DebugMenuController _debugMenuController;
        private ConsoleController _logConsoleController;
        private DebugInfoPanelController _debugInfoPanelController;
        private SceneLayersManager _sceneLayersManager;
        private PlacemarksManager _placemarksManager;
        private GPSLocationManager _gpsLocationManager;
        private DeeplinkingManager _deeplinkingManager;
        private MobileCalibrationManager _calibrationManager;
        private EntryManager _entryManager;
        private CalibrationResult _startSceneLocation;
        private NotificationManager _notificationManager;
        private GameObject _companySelectionMenu;
        private GameObject _infoCardInstance;
        private PlacemarkViewModel _currentPlacemark;
        private SceneScreenInfoPanelController _sceneScreenInfoPanelController;


        private class ExternalSignInRequestAdapter : IExternalSignInRequestAdapter
        {
            private readonly DeeplinkingManager _deeplinkingManager;
            private readonly DebugMenuController _debugMenuController;
            private readonly bool _resendPreviousEvents;

            public ExternalSignInRequestAdapter(
                [NotNull] DeeplinkingManager deeplinkingManager, 
                [NotNull] DebugMenuController debugMenuController,
                bool resendPreviousEvents)
            {
                if (deeplinkingManager == null) {
                    throw new ArgumentNullException("deeplinkingManager");
                }
                if (debugMenuController == null) {
                    throw new ArgumentNullException("debugMenuController");
                }
                _deeplinkingManager = deeplinkingManager;
                _debugMenuController = debugMenuController;
                _resendPreviousEvents = resendPreviousEvents;
            }

            public void OnSignInByTokenRequested(UnityAction<string, string> handler)
            {
                _deeplinkingManager.SignInTriggered.AddListener(handler);
                // Process events that were occurred before subscribing.
                var latestEvent = _deeplinkingManager.GetLatestSignInTriggeredEvent();
                if (_resendPreviousEvents && latestEvent != null) {
                    handler.Invoke(latestEvent.SignInToken, latestEvent.CompanySlug);
                }
            }

            public void UnsubscribeSignInByTokenRequsted(UnityAction<string, string> handler)
            {
                _deeplinkingManager.SignInTriggered.RemoveListener(handler);
            }

            public void OnSingInByDeviceIdRequested(UnityAction handler)
            {
                _debugMenuController.SignInRequested.AddListener(handler);
            }

            public void UnsubscribeSignInByDeviceIdRequested(UnityAction handler)
            {
                _debugMenuController.SignInRequested.RemoveListener(handler);
            }
        }

        #region Interstate data

        #endregion

        #region Mono behavior pipeline 
        // ReSharper disable once UnusedMember.Local
        private void Start()
        {
            _log = LogManager.GetLogger(typeof(MobileMainSceneManager));
            _log.LogInfo("Start");

            Assert.IsNotNull(CompanySelectionMenuPrefab);

            Assert.IsNotNull(LogConsole);
            _logConsoleController = LogConsole.GetComponent<ConsoleController>();

            Assert.IsNotNull(DebugInfoMenu);
            _debugInfoPanelController = DebugInfoMenu.GetComponent<DebugInfoPanelController>();

            Assert.IsNotNull(MainMenuPrefab);
            _mainMenuController = Instantiate(MainMenuPrefab).AddToRoot().GetComponent<MainMenuController>();
            _mainMenuController.OnDebugMenuRequested.AddListener(() => _fsm.TriggerEvent(Event.DebugMenuRequested));
            _mainMenuController.OnCompanySelectionMenuRequested.AddListener(() => _fsm.TriggerEvent(Event.SelectCompanyMenuRequested));
            _mainMenuController.OnRecalibrationRequested.AddListener(() => _fsm.TriggerEvent(Event.RecalibrationRequested));
            _mainMenuController.OnMenuHidden.AddListener(() => _fsm.TriggerEvent(Event.HideMainMenuRequested));

            Assert.IsNotNull(DebugMenuPrefab);
            _debugMenuController = Instantiate(DebugMenuPrefab).AddToRoot().GetComponent<DebugMenuController>();
            _debugMenuController.Hide();
            _debugMenuController.LogConsoleSwitchRequested.AddListener(() => _fsm.TriggerEvent(Event.LogConsoleSwitchRequested));
            _debugMenuController.DebugInfoSwitchRequested.AddListener(() => _fsm.TriggerEvent(Event.DebugInfoSwitchRequested));
            _debugMenuController.CloseRequested.AddListener(() => _fsm.TriggerEvent(Event.CloseDebugMenuRequested));
            _debugMenuController.SignInRequested.AddListener(() => _fsm.TriggerEvent(Event.SignInByDeviceIdRequested));
            _debugMenuController.SignOutRequested.AddListener(() => _fsm.TriggerEvent(Event.SignOutRequested));
            
            Assert.IsNotNull(DeeplinkingPrefab);
            _deeplinkingManager = Instantiate(DeeplinkingPrefab).AddToRoot().GetComponent<DeeplinkingManager>();
            _deeplinkingManager.SignInTriggered.AddListener((signInToken, companySlug) => {
                _fsm.TriggerEvent(Event.SignInByDeepLinkRequested);
            });

            _identityManager = IdentityManager.Instance;
            Assert.IsNotNull(_identityManager);

            _sceneLayersManager = SceneLayersManager.Instance;
            Assert.IsNotNull(_sceneLayersManager);

            _placemarksManager = PlacemarksManager.Instance;
            Assert.IsNotNull(_placemarksManager);
            _placemarksManager.OnPlacemarkSelected.AddListener((placemark) => {
                _currentPlacemark = placemark;
                _fsm.TriggerEvent(Event.InfoCardRequested);
            });
            _placemarksManager.OnPlacemarkDeselected.AddListener(() => _fsm.TriggerEvent(Event.CloseInfoCardRequested));

            _gpsLocationManager = GPSLocationManager.Instance;
            Assert.IsNotNull(_gpsLocationManager);

            Assert.IsNotNull(QrCodeDetector);

            Assert.IsNotNull(NotificationManager.Instance);
            _notificationManager = NotificationManager.Instance.GetComponent<NotificationManager>();
            Assert.IsNotNull(_notificationManager);

            Assert.IsNotNull(SignInMenuPrefab);

            Assert.IsNotNull(SceneScreenInfoPanelPrefab);
            _sceneScreenInfoPanelController = Instantiate(SceneScreenInfoPanelPrefab).AddToRoot().GetComponent<SceneScreenInfoPanelController>();
            _sceneScreenInfoPanelController.OnMainMenuRequested.AddListener(() => _fsm.TriggerEvent(Event.ShowMainMenuRequested));

            InitFsm();
            _fsm.StartFsm()
                .AsTask()
                .RunCoroutine(this);
        }

        // ReSharper disable once UnusedMember.Local
        private void Update()
        {
        }

        #endregion

        #region FSM declaration

        private enum State
        {
            Authentication,
            Calibration,
            Scene,
            AddingNewCompanyAtTheScene,
            AddingNewCompanyAtTheCalibration
        }

        private enum Event
        {
            // Main events.
            CompanySelected,
            Calibrated,
            AddingNewCompanyRequested,
            CanceledAddingNewCompany,
            CompanyAdded,

            // Second part events.
            SignInByDeepLinkRequested,
            SelectCompanyMenuRequested,
            RecalibrationRequested,
            CalibrationCancelRequested,

            // Main menu events.
            DebugMenuRequested,

            // Debug menu events.
            SignInByDeviceIdRequested,
            SignOutRequested,
            LogConsoleSwitchRequested,
            DebugInfoSwitchRequested,
            CloseDebugMenuRequested,

            ShowMainMenuRequested,
            HideMainMenuRequested,

            InfoCardRequested,
            CloseInfoCardRequested
        }

        private void InitFsm()
        {
            _fsm = new TransitionTableFsm<Event, State, MobileMainSceneManager>(this, State.Authentication, "MobileMainSceneManager");

            // Init states.
            _fsm.CreateState(State.Authentication).Enter(fsm => {
                // TODO: remove this when we can click on area for calling debug menu.
                if (AppSettings.Instance.IsDebugLayerEnabled) {
                }
                fsm.Authorize();
            }).Exit(fsm => {
                // TODO: remove this when we can click on area for calling debug menu.
                if (AppSettings.Instance.IsDebugLayerEnabled) {
                }
                StopAuthorization();
            });
            _fsm.CreateState(State.Calibration).Exit(fsm => DestroyCalibrationSystem());
            _fsm.CreateState(State.Scene)
                .Enter(fsm => {
                    fsm.ShowSceneScreenInfoPanel();
                })
                .Exit(fsm => {
                    fsm.HideSceneScreenInfoPanel();
                    fsm.HideMainMenu();
                    fsm.HideInfoCard();
                });

            _fsm.CreateState(State.AddingNewCompanyAtTheScene)
                .Enter(fsm => AddNewCompany(false))
                .Exit(fsm => StopAddingNewCompany())
                .Parent(State.Scene);
            _fsm.CreateState(State.AddingNewCompanyAtTheCalibration)
                .Enter(fsm => AddNewCompany(true))
                .Exit(fsm => StopAddingNewCompany())
                .Parent(State.Calibration);

            // Global actions.
            _fsm.AddGlobalAction(Event.DebugMenuRequested, fsm => OpenDebugMenu());
            _fsm.AddGlobalAction(Event.CloseDebugMenuRequested, fsm => CloseDebugMenu());
            _fsm.AddGlobalAction(Event.LogConsoleSwitchRequested, fsm => OpenOrCloseLogConsole());
            _fsm.AddGlobalAction(Event.DebugInfoSwitchRequested, fsm => OpenOrCloseDebugInfoMenu());

            // Init transitions.
            _fsm.AddTransition(State.Authentication, Event.CompanySelected, State.Calibration, fsm => StartCalibration(false));
            _fsm.AddTransition(State.Authentication, Event.SignInByDeepLinkRequested, null, fsm => {
                // NOTE: This event will be processed by entry manager.
            });
            _fsm.AddTransition(State.Authentication, Event.SignInByDeviceIdRequested, null, fsm => {
                // NOTE: THis event will be processed by entry manager.
            });
            _fsm.AddTransition(State.Authentication, Event.SignOutRequested, null, fsm => fsm.AlreadySignedOut());

            _fsm.AddTransition(State.Calibration, Event.Calibrated, State.Scene, fsm => fsm.LoadScene());
            _fsm.AddTransition(State.Calibration, Event.SignInByDeviceIdRequested, null, fsm => AlreadySignedIn());
            _fsm.AddTransition(State.Calibration, Event.SignInByDeepLinkRequested, State.AddingNewCompanyAtTheCalibration, null);
            _fsm.AddTransition(State.Calibration, Event.SignOutRequested, State.Authentication, fsm => fsm.SignOut());
            _fsm.AddTransition(State.Calibration, Event.CalibrationCancelRequested, State.Scene, null);

            _fsm.AddTransition(State.Scene, Event.CompanySelected, State.Scene, fsm => fsm.LoadScene());
            _fsm.AddTransition(State.Scene, Event.SelectCompanyMenuRequested, null, fsm => CreateCompanySelectionMenu());
            _fsm.AddTransition(State.Scene, Event.SignInByDeviceIdRequested, null, fsm => AlreadySignedIn());
            _fsm.AddTransition(State.Scene, Event.SignInByDeepLinkRequested, State.AddingNewCompanyAtTheScene, null);
            _fsm.AddTransition(State.Scene, Event.AddingNewCompanyRequested, State.AddingNewCompanyAtTheScene, null);
            _fsm.AddTransition(State.Scene, Event.SignOutRequested, State.Authentication, fsm => fsm.SignOut());
            _fsm.AddTransition(State.Scene, Event.RecalibrationRequested, State.Calibration, fsm => StartCalibration(true));

            _fsm.AddTransition(State.Scene, Event.InfoCardRequested, null, fsm => {
                HideMainMenu();
                HideSceneScreenInfoPanel();
                ShowInfoCard();
            });
            _fsm.AddTransition(State.Scene, Event.CloseInfoCardRequested, null, fsm => {
                HideInfoCard();
                ShowSceneScreenInfoPanel();
            });

            _fsm.AddTransition(State.Scene, Event.ShowMainMenuRequested, null, fsm => {
                HideSceneScreenInfoPanel();
                ShowMainMenu();
            });
            _fsm.AddTransition(State.Scene, Event.HideMainMenuRequested, null, fsm => {
                HideMainMenu();
                ShowSceneScreenInfoPanel();
            });

            _fsm.AddTransition(State.AddingNewCompanyAtTheScene, Event.CanceledAddingNewCompany, State.Scene, null);
            _fsm.AddTransition(State.AddingNewCompanyAtTheScene, Event.SignInByDeviceIdRequested, null, fsm => AlreadySignedIn());
            _fsm.AddTransition(State.AddingNewCompanyAtTheScene, Event.CompanyAdded, State.Scene, fsm => LoadScene());
            _fsm.AddTransition(State.AddingNewCompanyAtTheScene, Event.SignOutRequested, State.Authentication, fsm => fsm.SignOut());

            _fsm.AddTransition(State.AddingNewCompanyAtTheCalibration, Event.CanceledAddingNewCompany, State.Calibration, null);
            _fsm.AddTransition(State.AddingNewCompanyAtTheCalibration, Event.SignInByDeviceIdRequested, null, fsm => AlreadySignedIn());
            _fsm.AddTransition(State.AddingNewCompanyAtTheCalibration, Event.CompanyAdded, State.Calibration, null);
            _fsm.AddTransition(State.AddingNewCompanyAtTheCalibration, Event.SignOutRequested, State.Authentication, fsm => fsm.SignOut());
        }

        #endregion

        #region FSM actions

        #region Global actions.

        private void AlreadySignedIn()
        {
            _log.LogInfo("FSM ACTION Already signed in.");

            _notificationManager.ShowNotification(NotificationType.Warning, SIGNEDIN_MESSAGE);
        }

        private void SignOut()
        {
            _log.LogInfo("FSM ACTION Sign out.");
            _identityManager.SignOut();
        }

        private void OpenDebugMenu()
        {
            _log.LogInfo("FSM ACTION Open debug menu.");
            _debugMenuController.Show();
        }

        private void OpenOrCloseLogConsole()
        {
            _log.LogInfo("FSM ACTION Open/close log console.");
            if (_logConsoleController.IsShown()) {
                _logConsoleController.Hide();
            } else {
                _logConsoleController.Show();
            }
        }

        private void OpenOrCloseDebugInfoMenu()
        {
            _log.LogInfo("FSM ACTION Open/close debug info menu.");
            if (_debugInfoPanelController.IsShown()) {
                _debugInfoPanelController.Hide();
            } else {
                _debugInfoPanelController.Show();
                GetFps().AsTask().RunCoroutine(this);
            }
        }

        // TODO: Move this code from here
        private IEnumerator GetFps()
        {
            while (true)
            {
                _debugInfoPanelController.DisplayFps(1.0f / Time.smoothDeltaTime);
                yield return new WaitForSeconds(1);
            }
        }

        private void CloseDebugMenu()
        {
            _log.LogInfo("FSM ACTION Close debug menu.");
            _debugMenuController.Hide();
        }

        #endregion
        #region Authorization system actions

        private void AlreadySignedOut()
        {
            _log.LogInfo("FSM ACTION Already signed out.");

            _notificationManager.ShowNotification(NotificationType.Warning, SIGNEDOUT_MESSAGE);
        }

        private void Authorize()
        {
            _log.LogInfo("FSM ACTION Authorize action.");

            _entryManager = gameObject.AddComponent<EntryManager>();
            var externalSignInRequestAdapter = new ExternalSignInRequestAdapter(_deeplinkingManager, _debugMenuController, true);

            _entryManager.InitData(QrCodeDetector, externalSignInRequestAdapter,
                CompanySelectionMenuPrefab, SignInMenuPrefab, EntryManager.TaskType.SignIn);
            _entryManager.CompanySelected.AddListener(() => { _fsm.TriggerEvent(Event.CompanySelected); });
            _entryManager.SignInCanceled.AddListener(() => { _fsm.TriggerEvent(Event.CanceledAddingNewCompany); });
        }

        private void StopAuthorization()
        {
            _log.LogInfo("FSM ACTION Stop authorization.");

            Assert.IsNotNull(_entryManager);
            Destroy(_entryManager);
            _entryManager = null;
        }

        #endregion

        #region Calibration system

        private void StartCalibration(bool closable)
        {
            _log.LogInfo("FSM ACTION Calibration action in main scene.");

            Assert.IsNotNull(CalibrationMenuPrefab);
            _calibrationManager = Instantiate(CalibrationMenuPrefab).AddToRoot().GetComponent<MobileCalibrationManager>();
            _calibrationManager.OnCalibrationFinished.AddListener(calibrationResult => {
                _startSceneLocation = calibrationResult;
                _fsm.TriggerEvent(Event.Calibrated);
            });
            _calibrationManager.OnCalibrationCanceled.AddListener(() => {
                _fsm.TriggerEvent(Event.CalibrationCancelRequested);
            });

            var gpsData = _gpsLocationManager.GetCurrentLocationInfo();
            var compassData = _gpsLocationManager.GetCurrentCompassTrueHeading();
            var companyViewModel = ExecutionContext.Instance.DeviceCurrentCompany;

            _calibrationManager.StartCalibration(gpsData.IsSuccess
                                                     ? new GeoCoordinate(gpsData.LocationInfo.latitude, gpsData.LocationInfo.longitude)
                                                     : new GeoCoordinate(companyViewModel.DefaultLatitude, companyViewModel.DefaultLongitude), compassData, closable);
        }

        private void DestroyCalibrationSystem()
        {
            _log.LogInfo("FSM ACTION DestroyCalibrationSystem action in main scene.");

            Destroy(_calibrationManager.gameObject);
        }

        #endregion


        #region Scene actions

        private void LoadScene()
        {
            _log.LogInfo("FSM ACTION Load scene.");

            if (_startSceneLocation == null) {
                _log.LogErrorFormat("Can't load scene since start location isn't set.");
                return;
            }

            var sceneLayersOptions = _sceneLayersManager.GetSceneLayersOptions();

            _placemarksManager.LoadScene(ExecutionContext.Instance.Company.Slug,
                                         _startSceneLocation.GnssData.Location.Latitude, _startSceneLocation.GnssData.Location.Longitude, DEFAULT_RADIUS,
                                         (float) _startSceneLocation.GnssData.Azimuth, _startSceneLocation.UserPosition, _startSceneLocation.UserRotation, sceneLayersOptions)
                .AsTask()
                .ContinueWith(t => {
                    if (t.IsFaulted) {
                        Debug.LogException(t.Exception);
                        // TODO: show message for user.
                    } else {
                        // Set world directions according to the configured scene.
                        var worldDirections = GameObject.Find(Constants.WORLD_DIRECTIONS);
                        Assert.IsNotNull(worldDirections);
                        worldDirections.transform.rotation = Quaternion.Euler(
                                                                              0, _startSceneLocation.UserRotation.y - (float) _startSceneLocation.GnssData.Azimuth, 0);
                        
                        _notificationManager.ShowNotification(NotificationType.Success, 
                            string.Format(COMPANY_SELECTED, ExecutionContext.Instance.DeviceCurrentCompany.Name));
                    }
                })
                .RunCoroutine(this);
        }

        private void CreateCompanySelectionMenu()
        {
            _log.LogInfo("FSM ACTION Create company selection menu.");

            if (_companySelectionMenu != null)
            {
                Destroy(_companySelectionMenu);
            }

            _companySelectionMenu = Instantiate(CompanySelectionMenuPrefab);
            _companySelectionMenu.AddToRoot();

            var comcompanySelectionMenuController = _companySelectionMenu.GetComponent<ICompanySelectionMenuController>();
            comcompanySelectionMenuController.Init(true, true);

            comcompanySelectionMenuController.OnCompanySelected(() => _fsm.TriggerEvent(Event.CompanySelected));
            comcompanySelectionMenuController.OnAddNewCompanyRequested(() => _fsm.TriggerEvent(Event.AddingNewCompanyRequested));
        }

        private void ShowMainMenu()
        {
            _log.LogInfo("FSM ACTION Show main menu.");
            _mainMenuController.Show();
        }

        private void HideMainMenu()
        {
            _log.LogInfo("FSM ACTION Hide main menu.");
            _mainMenuController.Hide();
        }

        private void ShowSceneScreenInfoPanel()
        {
            _log.LogInfo("FSM ACTION Show scene screen info panel.");
            _sceneScreenInfoPanelController.Show();
        }

        private void HideSceneScreenInfoPanel()
        {
            _log.LogInfo("FSM ACTION Hide scene screen info panel.");
            _sceneScreenInfoPanelController.Hide();
        }

        private void ShowInfoCard()
        {
            _log.LogInfo("FSM ACTION Show info card.");

            Assert.IsNotNull(InfoCardPrefab);
            Assert.IsNotNull(_currentPlacemark);
            if (_infoCardInstance != null) {
                Destroy(_infoCardInstance);
            }
            _infoCardInstance = Instantiate(InfoCardPrefab);
            _infoCardInstance.GetComponent<InfoCardController>().Init(_currentPlacemark);
            _infoCardInstance.AddToRoot();
        }

        private void HideInfoCard()
        {
            _log.LogInfo("FSM ACTION Hide info card.");

            if (_infoCardInstance != null) {
                Destroy(_infoCardInstance);
            }
        }
        #endregion

        #region Adding new company actions
        private void AddNewCompany(bool resendPreviousEvent)
        {
            _log.LogInfo("FSM ACTION Add new company.");
            
            _entryManager = gameObject.AddComponent<EntryManager>();
            _entryManager.InitData(QrCodeDetector, new ExternalSignInRequestAdapter(_deeplinkingManager, _debugMenuController, resendPreviousEvent),
                                   CompanySelectionMenuPrefab, SignInMenuPrefab, EntryManager.TaskType.AddingCompany);
            _entryManager.CompanySelected.AddListener(() => { _fsm.TriggerEvent(Event.CompanyAdded); });
            _entryManager.SignInCanceled.AddListener(() => { _fsm.TriggerEvent(Event.CanceledAddingNewCompany); });
        }

        private void StopAddingNewCompany()
        {
            _log.LogInfo("FSM ACTION Stop adding new company.");
            Assert.IsNotNull(_entryManager);
            Destroy(_entryManager);
            _entryManager = null;
        }

        #endregion
        #endregion
    }
}