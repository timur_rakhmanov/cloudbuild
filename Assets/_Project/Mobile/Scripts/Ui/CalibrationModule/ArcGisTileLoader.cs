﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets._Project.Shared.Scripts.APIClient;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core.Tasks;
using UnityEngine;
using UnityEngine.UI;


namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule
{
    public class ArcGisTileLoader : MonoBehaviour, ITileLoader
    {
        public GameObject TilePrefab;
        public GameObject TileContainer;

        private Dictionary<TileId, GameObject> _tiles = new Dictionary<TileId, GameObject>();

        public IEnumerator AddNewTile(TileId tileId, TileId centerTileId)
        {
            if (_tiles.ContainsKey(tileId)) {
                yield break;
            }

            var mainSpriteTask = DownloadTileImage(tileId).AsTask<Texture2D>();
            yield return mainSpriteTask.Run();
            
            var mainTexture = mainSpriteTask.Result;
            mainTexture.wrapMode = TextureWrapMode.Clamp;

            var tile = Instantiate(TilePrefab);
            
            tile.GetComponent<Image>().sprite = Sprite.Create(mainTexture, new Rect(0, 0, 1024, 1024), new Vector2(0.5f, 0.5f));
            tile.transform.SetParent(TileContainer.transform, false);

            var initializeTask = tile.GetComponent<MapTile>().Initialize(tileId, centerTileId).AsTask();
            yield return initializeTask.Run();
            initializeTask.Wait();

            _tiles.Add(tileId, tile);
        }

        public void RemoveTile(TileId tileId)
        {
            if (_tiles.ContainsKey(tileId)) {
                Destroy(_tiles[tileId]);
                _tiles.Remove(tileId);
            }
        }

        private IEnumerator DownloadTileImage(TileId tileId)
        {
            var imageUrlTask = new ApiClient().GetTileData(tileId.ToString(), MapTileDataType.ArcGisImagery).AsTask<string>();
            yield return imageUrlTask.Run();

            var url = imageUrlTask.Result;
            // Get texture
            var www = new WWW(url);
            yield return www;
            if (www.error != null) {
                throw new Exception(www.error);
            }

            var texture = new Texture2D(1024, 1024, TextureFormat.RGB24, false);
            www.LoadImageIntoTexture(texture);
            yield return texture;
        }
    }
}
