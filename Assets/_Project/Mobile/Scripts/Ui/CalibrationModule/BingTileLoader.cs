﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets._Project.Shared.Scripts.APIClient.Coroutines;
using Assets._Project.Shared.Scripts.Configuration;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Newtonsoft.Json.Linq;
using RestSharp;
using UnityEngine;
using UnityEngine.UI;


namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule
{
    public class BingTileLoader : MonoBehaviour, ITileLoader
    {
        private const string METADATA_REQUEST_TEMPLATE = "/REST/V1/Imagery/Metadata/Aerial/{0}?zl={1}&key={2}";
        private const string BING_DOMAIN = "http://dev.virtualearth.net";

        private Dictionary<TileId, GameObject> _tiles = new Dictionary<TileId, GameObject>();
        
        private IRestClient _restClient = new RestClient(BING_DOMAIN);

        public GameObject TilePrefab;
        public GameObject TileContainer;

        public IEnumerator AddNewTile(TileId tileId, TileId centerTileId)
        {
            if (_tiles.ContainsKey(tileId))
            {
                yield break;
            }
            
            var mainSpriteTask = DownloadBingTileImage(tileId).AsTask<Texture2D>();
            yield return mainSpriteTask.Run();
            var mainTexture = mainSpriteTask.Result;
            mainTexture.wrapMode = TextureWrapMode.Clamp;

            var tile = Instantiate(TilePrefab);
            tile.GetComponent<Image>().sprite = Sprite.Create(mainTexture, new Rect(0, 0, 256, 256), new Vector2(0.5f, 0.5f));
            tile.transform.SetParent(TileContainer.transform, false);

            var initializeTask = tile.GetComponent<MapTile>().Initialize(tileId, centerTileId).AsTask();
            yield return initializeTask.Run();
            initializeTask.Wait();

            _tiles.Add(tileId, tile);
        }

        public void RemoveTile(TileId tileId)
        {
            if (_tiles.ContainsKey(tileId))
            {
                Destroy(_tiles[tileId]);
                _tiles.Remove(tileId);
            }
        }

        private IEnumerator DownloadBingTileImage(TileId tile)
        {
            // Request url for tile
            var metadataUrl = string.Format(METADATA_REQUEST_TEMPLATE, tile.Center, GetComponentInParent<MobileCalibrationManager>().Zoom, AppSettings.Instance.BingApiKey);
            var request = new RestRequest(metadataUrl);
            var task = RestCoroutineFactory.CreateRestCoroutine(_restClient, request)
                .AsTask<IRestResponse>();
            yield return task.Run();

            var response = task.Result;
            if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                throw new Exception(response.ErrorMessage, response.ErrorException);
            }

            // Parse response
            var token = JToken.Parse(response.Content);
            var url = token["resourceSets"].First["resources"].First["imageUrl"].ToString();
            // Get texture
            WWW www = new WWW(url);
            yield return www;
            if (www.error != null)
            {
                throw new Exception(www.error);
            }
            yield return www.texture;
        }
    }
}
