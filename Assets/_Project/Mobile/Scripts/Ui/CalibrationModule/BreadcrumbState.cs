﻿using System;
using UnityEngine;
using UnityEngine.UI;


namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule
{
    public class BreadcrumbState : MonoBehaviour
    {
        public enum State : byte
        {
            Active = 0,
            Inactive = 1,
            Finished = 2
        }

        public Text StateLable;
        public GameObject ActiveStateIcon;
        public GameObject FinishedStateIcon;

        public void ChangeState(State state)
        {
            switch (state) {
                case State.Active:
                    FinishedStateIcon.SetActive(false);
                    ActiveStateIcon.SetActive(true);
                    StateLable.color = Color.white;
                    break;
                case State.Inactive:
                    FinishedStateIcon.SetActive(false);
                    ActiveStateIcon.SetActive(false);
                    StateLable.color = new Color(1, 1, 1, 0.6f);
                    break;
                case State.Finished:
                    FinishedStateIcon.SetActive(true);
                    ActiveStateIcon.SetActive(false);
                    StateLable.color = new Color(1, 1, 1, 0.6f);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("state", state, null);
            }
        }
    }
}
