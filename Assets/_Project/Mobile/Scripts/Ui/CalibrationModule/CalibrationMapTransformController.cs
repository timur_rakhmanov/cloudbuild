﻿using System;
using System.Collections;
using Assets._Project.Shared.Scripts.Core.Extensions;
using Assets._Project.Shared.Scripts.Core.Tasks;
using TouchScript.Gestures.TransformGestures;
using TouchScript.Gestures.TransformGestures.Base;
using UnityEngine;
using UnityEngine.UI;


namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule
{
    public class CalibrationMapTransformController : MonoBehaviour
    {
        public Transform ScalingTransform;
        public Vector3 RegularScale;

        private void OnEnable()
        {
            GetComponent<TransformGestureBase>().TransformCompleted += TransformCompletedHandler;
        }

        private void OnDisable()
        {
            GetComponent<TransformGestureBase>().TransformCompleted -= TransformCompletedHandler;
        }

        private void TransformCompletedHandler(object sender, EventArgs eventArgs)
        {
            if (!ScalingTransform.localScale.x.AlmostEquals(RegularScale.x, 3) || !ScalingTransform.localScale.y.AlmostEquals(RegularScale.y, 3)) {
                var diffVector = ScalingTransform.localScale - RegularScale;
                var diff = diffVector / 20;

                GetComponent<Image>().enabled = false;
                GetComponent<TransformGestureBase>().enabled = false;

                RestoreScale(diff).AsTask().ContinueWith(t => {
                    t.Wait();

                    GetComponent<Image>().enabled = true;
                    GetComponent<TransformGestureBase>().enabled = true;
                }).RunCoroutine(this);
            }
        }

        private IEnumerator RestoreScale(Vector3 diff)
        {
            for (var i = 0; i < 19; i++) {
                yield return new WaitForSeconds(0.01f);

                ScalingTransform.localScale -= diff;
            }

            yield return new WaitForSeconds(0.01f);

            ScalingTransform.localScale = RegularScale;
        }
    }
}
