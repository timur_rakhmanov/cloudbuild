﻿using System;
using UnityEngine.Events;

namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule.Events
{
    [Serializable]
    public class CalibrationCanceledEvent : UnityEvent
    {
    }
}
