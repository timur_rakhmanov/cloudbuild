﻿using System;
using Assets._Project.Shared.Scripts.Core;
using UnityEngine.Events;


namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule.Events
{
    [Serializable]
    public class CalibrationFinishedEvent : UnityEvent<CalibrationResult>
    {
    }
}
