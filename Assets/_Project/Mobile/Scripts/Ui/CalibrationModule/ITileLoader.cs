﻿using System.Collections;


namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule
{
    public interface ITileLoader
    {
        IEnumerator AddNewTile(TileId tileId, TileId centerTileId);

        void RemoveTile(TileId tileId);
    }
}
