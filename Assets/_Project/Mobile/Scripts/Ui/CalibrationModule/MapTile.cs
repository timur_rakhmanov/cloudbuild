﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets._Project.Mobile.Scripts.Core;
using Assets._Project.Shared.Scripts.APIClient;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Extensions;
using Assets._Project.Shared.Scripts.Core.Extensions.Vector2d;
using Assets._Project.Shared.Scripts.Core.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Bounds = Assets._Project.Shared.Scripts.Core.Extensions.Vector2d.Bounds;


namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule
{
    public class MapTile : MonoBehaviour
    {
        public TileId TileId { get; private set; }

        [HideInInspector]
        public Bounds MercatorBounds { get; private set; }

        private List<GameObject> _placemarks = new List<GameObject>();

        public IEnumerator Initialize(TileId tileId, TileId centerTileId)
        {
            TileId = tileId;
            gameObject.name = tileId.ToString();

            MercatorBounds = GetMercatorBounds(tileId);

            var centerTileBounds = GetMercatorBounds(centerTileId);

            var position = new Vector3((MercatorBounds.Center.x - centerTileBounds.Center.x).ToFloat(), (MercatorBounds.Center.y - centerTileBounds.Center.y).ToFloat(), 0);
            transform.localPosition = position;

            var apiClient = new ApiClient();
            var getPlacemarksTask = apiClient.GetPlacemarksInBBox(ExecutionContext.Instance.Company.Slug, tileId.SouthWest.Latitude, tileId.SouthWest.Longitude, tileId.NorthEast.Latitude, tileId.NorthEast.Longitude).AsTask<ICollection<PlacemarkViewModel>>();
            yield return getPlacemarksTask.Run();

            foreach (var placemark in getPlacemarksTask.Result) {
                if (placemark.GeometryType == GeometryType.Point) {
                    PlaceObjectPin(placemark);
                }
            }
        }

        private void OnDestroy()
        {
            foreach (var placemark in _placemarks) {
                Destroy(placemark);
            }
        }

        private void PlaceObjectPin(PlacemarkViewModel placemarkViewModel)
        {
            var calibrationManager = GetComponentInParent<MobileCalibrationManager>();

            var localPosition = calibrationManager.GeoToWorldCoordinates(new GeoCoordinate(placemarkViewModel.Latitude, placemarkViewModel.Longitude));
            var objectPin = Instantiate(calibrationManager.PlacemarkPointPrefab);

            _placemarks.Add(objectPin);

            var color = ColorConverter.HexToColor(placemarkViewModel.MainColor);
            objectPin.transform.SetParent(calibrationManager.PlacemarksContainer.transform, false);
            objectPin.transform.localPosition = localPosition;
            objectPin.AddComponent<PlacemarkScaleController>().ParentScalingTransform = calibrationManager.Transformer.GetComponent<RectTransformer>().ScalingTransform;
            objectPin.GetComponent<Image>().color = color;
        }

        #region Helper methods

        public const int PROJECTION_SIZE = 256;
        private const double WORLD_SIZE_RESOLUTION = 2 * Math.PI * MobileCalibrationManager.EARTH_RADIUS_IN_METERS / PROJECTION_SIZE;
        public const double EARTH_SHIFT = 2 * Math.PI * MobileCalibrationManager.EARTH_RADIUS_IN_METERS / 2;

        /// <summary>
        /// Get tile bounds in Mercator projection
        /// </summary>
        public static Bounds GetMercatorBounds(TileId id)
        {
            var min = VirtualToRealSize(new Vector2(id.X * PROJECTION_SIZE, id.Y * PROJECTION_SIZE), id.Zoom);
            var max = VirtualToRealSize(new Vector2((id.X + 1) * PROJECTION_SIZE, (id.Y + 1) * PROJECTION_SIZE), id.Zoom);
            return new Bounds(min, max - min);
        }

        private static Vector2d VirtualToRealSize(Vector2 virtualSize, int zoom)
        {
            var resolution = GetResolutionFromZoom(zoom);
            var realSize = new Vector2d {
                x = virtualSize.x * resolution - EARTH_SHIFT,
                y = -(virtualSize.y * resolution - EARTH_SHIFT)
            };
            return realSize;
        }

        public static double GetResolutionFromZoom(int zoom)
        {
            return WORLD_SIZE_RESOLUTION / Math.Pow(2, zoom);
        }

        #endregion
    }
}
