﻿using System;
using Assets._Project.Mobile.Scripts.GlobalManagers;
using Assets._Project.Mobile.Scripts.Ui.CalibrationModule.Events;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Extensions;
using Assets._Project.Shared.Scripts.Core.Extensions.Vector2d;
using Assets._Project.Shared.Scripts.Core.Fsm;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule
{
    public class MobileCalibrationManager : MonoBehaviour
    {
        #region Private data

        #region Constants

        private const double DEFAULT_AZINUTH = 0.0;
        public const float EARTH_RADIUS_IN_METERS = 6378137;
        private const int INITIAL_COORDINATES_ACCURACY_IN_METERS = 10;

        #endregion

        private static ILog _log;
        private TransitionTableFsm<Event, State, MobileCalibrationManager> _fsm;
        private ExecutionContext _executionContext;
        private GPSLocationManager _gpsLocationManager;

        private GeoCoordinate _currentApproximateCoordinates;
        private CalibrationResult _calibrationResult;
        private Vector2d _mercatorCenter;

        #region Unity objects

        private GameObject _applyButton;
        private GameObject _directionAim;
        private GameObject _mapMask;
        
        #endregion

        #endregion

        #region Editor configs

        public int Zoom;

        public GameObject PlacemarkPointPrefab;
        public GameObject PlacemarksContainer;
        public GameObject InitialApproximateCoordinatesPlane;
        public GameObject GazeVector;
        public GameObject Transformer;

        public BreadcrumbState PositionState;
        public BreadcrumbState DirectionState;

        #endregion

        #region Events

        [HideInInspector]
        public CalibrationFinishedEvent OnCalibrationFinished;
        [HideInInspector]
        public CalibrationCanceledEvent OnCalibrationCanceled;

        #endregion

        #region FSM declaration

        private enum State : byte
        {
            Idle = 0,
            MapPositionCalibration = 1,
            WorldDirectionCalibration = 3,
            Exit = 4
        }

        private enum Event : byte
        {
            CalibrationStarted = 0,
            PositionsConfirmed = 2,
            CalibrationFinished = 3,
            CalibrationSkiped = 4,
            PositionCalibration
        }

        private void InitFsm()
        {
            _fsm = new TransitionTableFsm<Event, State, MobileCalibrationManager>(this, State.Idle, "MobileCalibrationManager");

            _fsm.CreateState(State.Idle);
            _fsm.CreateState(State.MapPositionCalibration).Enter(fsm => MapPositionCalibrationStart());
            _fsm.CreateState(State.WorldDirectionCalibration).Enter(fsm => WorldDirectionCalibrationStart());
            _fsm.CreateState(State.Exit).Enter(fst => FinishCalibration());

            _fsm.AddTransition(State.Idle, Event.CalibrationStarted, State.MapPositionCalibration, null);
            _fsm.AddTransition(State.Idle, Event.PositionsConfirmed, null, null);
            _fsm.AddTransition(State.MapPositionCalibration, Event.PositionsConfirmed, State.WorldDirectionCalibration, null);
            _fsm.AddTransition(State.MapPositionCalibration, Event.CalibrationSkiped, State.Exit, null);
            _fsm.AddTransition(State.WorldDirectionCalibration, Event.PositionCalibration, State.MapPositionCalibration, null);
            _fsm.AddTransition(State.WorldDirectionCalibration, Event.CalibrationSkiped, State.Exit, null);
            _fsm.AddTransition(State.WorldDirectionCalibration, Event.CalibrationFinished, State.Exit, null);
        }

        #endregion

        #region Mono behavior pipeline

        public void Awake()
        {
            _log = LogManager.GetLogger(typeof(MobileCalibrationManager));

            _gpsLocationManager = GPSLocationManager.Instance;
            Assert.IsNotNull(_gpsLocationManager);

            _executionContext = ExecutionContext.Instance;
            Assert.IsNotNull(_executionContext);

            _mapMask = transform.Find("MapMask").gameObject;
            _applyButton = transform.Find("Toolbar/ApplyButton").gameObject;
            _directionAim = transform.Find("DirectionAim").gameObject;

            InitFsm();
            _fsm.StartFsm()
                .AsTask()
                .RunCoroutine(this);
        }

        void OnEnable()
        {
            Screen.orientation = ScreenOrientation.Portrait;
        }

        void OnDisable()
        {
            Screen.orientation = ScreenOrientation.AutoRotation;
        }

        #endregion

        #region FSM transitions

        private void MapPositionCalibrationStart()
        {
            transform.Find("Toolbar/GPSButton").gameObject.GetComponent<Button>().enabled = true;
            transform.Find("Toolbar/DefaultCoordinatesButton").gameObject.GetComponent<Button>().enabled = true;
            transform.Find("Toolbar/GPSButton/Icon").gameObject.GetComponent<Image>().color = Color.white;
            transform.Find("Toolbar/DefaultCoordinatesButton/Icon").gameObject.GetComponent<Image>().color = Color.white;
            transform.Find("Toolbar/NextButton").gameObject.SetActive(true);
            _mapMask.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, 667);
            InitialApproximateCoordinatesPlane.SetActive(true);
            _applyButton.SetActive(false);
            _directionAim.SetActive(false);
            GazeVector.GetComponent<RectTransform>().sizeDelta = new Vector2(2, 310);

            PositionState.GetComponent<Button>().enabled = false;
            PositionState.ChangeState(BreadcrumbState.State.Active);
            DirectionState.GetComponent<Button>().enabled = true;
            DirectionState.ChangeState(BreadcrumbState.State.Inactive);

            var worldDirectionCalibrationMapTransformer = Transformer.GetComponent<WorldDirectionCalibrationMapTransformer>();
            worldDirectionCalibrationMapTransformer.enabled = false;
            worldDirectionCalibrationMapTransformer.TranslationTransform.localPosition = Vector3.zero;

            Transformer.GetComponent<RectTransformer>().enabled = true;

            var calibrationMapTransformController = Transformer.GetComponent<CalibrationMapTransformController>();
            calibrationMapTransformController.ScalingTransform = transform.Find("MapMask/Map/MapScreen/Scalable|RotatableContainer");
            calibrationMapTransformController.RegularScale = calibrationMapTransformController.ScalingTransform.localScale;
        }

        private void WorldDirectionCalibrationStart()
        {
            transform.Find("Toolbar/GPSButton").gameObject.GetComponent<Button>().enabled = false;
            transform.Find("Toolbar/DefaultCoordinatesButton").gameObject.GetComponent<Button>().enabled = false;
            transform.Find("Toolbar/GPSButton/Icon").gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 0.6f);
            transform.Find("Toolbar/DefaultCoordinatesButton/Icon").gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 0.6f);
            transform.Find("Toolbar/NextButton").gameObject.SetActive(false);
            _mapMask.GetComponent<RectTransform>().SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, 403);
            InitialApproximateCoordinatesPlane.SetActive(false);
            _applyButton.SetActive(true);
            _directionAim.SetActive(true);
            GazeVector.GetComponent<RectTransform>().sizeDelta = new Vector2(2, 600);

            PositionState.GetComponent<Button>().enabled = true;
            PositionState.ChangeState(BreadcrumbState.State.Finished);
            DirectionState.GetComponent<Button>().enabled = false;
            DirectionState.ChangeState(BreadcrumbState.State.Active);

            Transformer.GetComponent<RectTransformer>().enabled = false;
            Transformer.GetComponent<WorldDirectionCalibrationMapTransformer>().enabled = true;

            var calibrationMapTransformController = Transformer.GetComponent<CalibrationMapTransformController>();
            calibrationMapTransformController.ScalingTransform = transform.Find("MapMask/Map");
            calibrationMapTransformController.RegularScale = calibrationMapTransformController.ScalingTransform.localScale;
        }

        private void FinishCalibration()
        {
            OnCalibrationFinished.Invoke(_calibrationResult);
        }

        #endregion

        #region Commands

        public void StartCalibration(GeoCoordinate coordinates, float initialAzimuth = 0, bool closable = false)
        {
            _log.LogInfoFormat("StartCalibration coordinates {0}", coordinates);
            
            transform.Find("CloseButton").gameObject.SetActive(closable);
            
            _currentApproximateCoordinates = coordinates;

            RenderCalibrationMap(coordinates);

            InitialApproximateCoordinatesPlaneUpdate();

            Transformer.GetComponent<RectTransformer>().RotationTransform.localRotation = Quaternion.Euler(0, 0, initialAzimuth);

            _fsm.TriggerEvent(Event.CalibrationStarted);
        }

        /// <summary>
        /// Skip calibration.
        /// </summary>
        public void SkipCalibration()
        {
            _calibrationResult = GetDefaultLocation();
            _fsm.TriggerEvent(Event.CalibrationSkiped);
        }

        #endregion

        #region Event handlers

        #region Buttons handlers

        public void CheckButtonClicked()
        {
            _fsm.TriggerEvent(Event.PositionsConfirmed);
        }

        public void ApplyButtonClicked()
        {
            var position = LocalToGeoCoordinate(ConvertDisplacementToPosition(Transformer.GetComponent<RectTransformer>().TranslationTransform.localPosition));

            _calibrationResult = new CalibrationResult {
                GnssData = new GnssDataViewModel {
                    Location = new GeoCoordinate(position.Latitude, position.Longitude),
                    Azimuth = Transformer.GetComponent<RectTransformer>().RotationTransform.localRotation.eulerAngles.z + 360 % 360
                },
                UserPosition = Camera.main.transform.localPosition,
                UserRotation = Camera.main.transform.localEulerAngles
            };

            _fsm.TriggerEvent(Event.CalibrationFinished);
        }

        public void GPSButtonClicked()
        {
            var gpsData = _gpsLocationManager.GetCurrentLocationInfo();
            var compassData = _gpsLocationManager.GetCurrentCompassTrueHeading();

            var currentPosition = LocalToGeoCoordinate(ConvertDisplacementToPosition(Transformer.GetComponent<RectTransformer>().TranslationTransform.localPosition));

            GeoCoordinate gpsCoordinates;
            if (gpsData.IsSuccess) {
                gpsCoordinates = new GeoCoordinate(gpsData.LocationInfo.latitude, gpsData.LocationInfo.longitude);
            } else {
                var companyViewModel = _executionContext.DeviceCurrentCompany;
                gpsCoordinates = new GeoCoordinate(companyViewModel.DefaultLatitude, companyViewModel.DefaultLongitude);
            }

            if (currentPosition.Equals(gpsCoordinates, 3000)) {
                Transformer.GetComponent<RectTransformer>().TranslationTransform.localPosition = ConvertDisplacementToPosition(GeoToWorldCoordinates(gpsCoordinates));
            } else {
                RenderCalibrationMap(gpsCoordinates);
            }

            _currentApproximateCoordinates = gpsCoordinates;

            InitialApproximateCoordinatesPlaneUpdate();

            Transformer.GetComponent<RectTransformer>().RotationTransform.localRotation = Quaternion.Euler(0, 0, compassData);
        }

        public void DefaultCoordinatesButtonClicked()
        {
            var defaultCoordinates = new GeoCoordinate(_executionContext.DeviceCurrentCompany.DefaultLatitude, _executionContext.DeviceCurrentCompany.DefaultLongitude);
            var currentPosition = LocalToGeoCoordinate(ConvertDisplacementToPosition(Transformer.GetComponent<RectTransformer>().TranslationTransform.localPosition));
            
            if (currentPosition.Equals(defaultCoordinates, 3000)) {
                Transformer.GetComponent<RectTransformer>().TranslationTransform.localPosition = ConvertDisplacementToPosition(GeoToWorldCoordinates(defaultCoordinates));
            } else {
                RenderCalibrationMap(defaultCoordinates);
            }

            InitialApproximateCoordinatesPlaneUpdate();
        }

        public void SkipButtonClicked()
        {
            _calibrationResult = GetDefaultLocation();
            _fsm.TriggerEvent(Event.CalibrationSkiped);
        }

        public void PositionStateSelected()
        {
            _fsm.TriggerEvent(Event.PositionCalibration);
        }

        public void HelpButtonClicked()
        {
            Application.OpenURL("https://portal.meemim.com/meemim.com/boards/230/");
        }

        public void DirectionStateSelected()
        {
            _fsm.TriggerEvent(Event.PositionsConfirmed);
        }

        public void CloseButtonClicked()
        {
            OnCalibrationCanceled.Invoke();
        }

        #endregion

        #endregion

        #region Private methods

        private CalibrationResult GetDefaultLocation()
        {
            var companyViewModel = _executionContext.DeviceCurrentCompany;
            if (companyViewModel == null) {
                _log.LogErrorFormat("Company '{0}' does not available for this device", _executionContext.Company.Slug);
                throw new Exception("Current company does not available for this device");
            }

            return new CalibrationResult {
                GnssData = new GnssDataViewModel {
                    Location = new GeoCoordinate(companyViewModel.DefaultLatitude, companyViewModel.DefaultLongitude),
                    Azimuth = DEFAULT_AZINUTH
                },
                UserPosition = Camera.main.transform.localPosition,
                UserRotation = Camera.main.transform.localEulerAngles
            };
        }

        private void RenderCalibrationMap(GeoCoordinate location)
        {
            GetComponentInChildren<TileProvider>().Initialize(location, Zoom);

            var centerTile = TileId.CoordinateToTileId(location, Zoom);

            _mercatorCenter = MapTile.GetMercatorBounds(centerTile).Center;
            Transformer.GetComponent<RectTransformer>().TranslationTransform.localPosition = ConvertDisplacementToPosition(GeoToWorldCoordinates(location));
        }

        private void InitialApproximateCoordinatesPlaneUpdate()
        {
            var initialLocalPosition = GeoToWorldCoordinates(_currentApproximateCoordinates);

            var lat = _currentApproximateCoordinates.Latitude + 180 / Math.PI * (0 / EARTH_RADIUS_IN_METERS);
            var lon = _currentApproximateCoordinates.Longitude + 180 / Math.PI * (INITIAL_COORDINATES_ACCURACY_IN_METERS / EARTH_RADIUS_IN_METERS) / Math.Cos(_currentApproximateCoordinates.Latitude);

            var accuracyOffset = GeoToWorldCoordinates(new GeoCoordinate(lat, lon));
            var distance = Vector3.Distance(accuracyOffset, initialLocalPosition);

            InitialApproximateCoordinatesPlane.transform.localPosition = initialLocalPosition;
            InitialApproximateCoordinatesPlane.GetComponent<RectTransform>().sizeDelta = new Vector2(distance, distance);
        }

        public Vector2 ConvertDisplacementToPosition(Vector2 displacement)
        {
            return new Vector2(displacement.x * -1, displacement.y * -1);
        }

        public Vector2 GeoToWorldCoordinates(GeoCoordinate geoCoordinate)
        {
            var mercator = GeoCoordinateToMercator(geoCoordinate);
            return new Vector2d((mercator.x - _mercatorCenter.x) * Transformer.GetComponent<RectTransformer>().TranslationTransform.localScale.x,
                                (mercator.y - _mercatorCenter.y) * Transformer.GetComponent<RectTransformer>().TranslationTransform.localScale.y).ToVector2();
        }

        public GeoCoordinate LocalToGeoCoordinate(Vector2 position)
        {
            var pos = new Vector2d(_mercatorCenter.x + position.x / Transformer.GetComponent<RectTransformer>().TranslationTransform.localScale.x, _mercatorCenter.y + position.y / Transformer.GetComponent<RectTransformer>().TranslationTransform.localScale.y);
            return MercatorToGeoCoordinates(pos);
        }

        private static Vector2d GeoCoordinateToMercator(GeoCoordinate geoCoordinate)
        {
            var x = geoCoordinate.Longitude * MapTile.EARTH_SHIFT / 180;
            var y = Math.Log(Math.Tan((90 + geoCoordinate.Latitude) * Math.PI / 360)) / (Math.PI / 180);
            y = y * MapTile.EARTH_SHIFT / 180;
            return new Vector2d(x, y);
        }

        private static GeoCoordinate MercatorToGeoCoordinates(Vector2d m)
        {
            var lon = m.x / MapTile.EARTH_SHIFT * 180;
            var lat = m.y / MapTile.EARTH_SHIFT * 180;
            lat = 180 / Math.PI * (2 * Math.Atan(Math.Exp(lat * Math.PI / 180)) - Math.PI / 2);
            return new GeoCoordinate(lat, lon);
        }

        #endregion
    }
}
