﻿using UnityEngine;


namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule
{
    public class PlacemarkScaleController : MonoBehaviour
    {
        public Transform ParentScalingTransform;

        void Update()
        {
            if (!Mathf.Approximately(1 / transform.localScale.x, ParentScalingTransform.localScale.x)) {
                transform.localScale = new Vector3(1 / ParentScalingTransform.localScale.x, 1 / ParentScalingTransform.localScale.y, 1 / ParentScalingTransform.localScale.z);
            }
        }
    }
}
