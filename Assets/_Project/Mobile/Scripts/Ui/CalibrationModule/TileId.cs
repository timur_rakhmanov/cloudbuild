﻿using System;
using Assets._Project.Shared.Scripts.Core;


namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule
{
    public class TileId
    {
        public TileId()
        {
        }

        public TileId(int x, int y, int zoom)
        {
            X = x;
            Y = y;
            Zoom = zoom;

            NorthEast = new GeoCoordinate(TileYToLatitude(Y, Zoom), TileXToLongitude(X + 1, Zoom));
            SouthWest = new GeoCoordinate(TileYToLatitude(Y + 1, Zoom), TileXToLongitude(X, Zoom));
            Center = new GeoCoordinate((NorthEast.Latitude + SouthWest.Latitude) / 2, (NorthEast.Longitude + SouthWest.Longitude) / 2);
        }

        public int X { get; set; }
        public int Y { get; set; }
        public int Zoom { get; set; }

        public GeoCoordinate SouthWest { get; private set; }
        public GeoCoordinate NorthEast { get; private set; }
        public GeoCoordinate Center { get; private set; }

        public override string ToString()
        {
            return string.Format("{0}-{1}-{2}", Zoom, X, Y);
        }

        public override bool Equals(object obj)
        {
            var otherTile = (TileId)obj;
            return otherTile != null && Equals(otherTile);
        }

        protected bool Equals(TileId other)
        {
            return Zoom == other.Zoom && X == other.X && Y == other.Y;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Zoom;
                hashCode = (hashCode * 137) ^ X;
                hashCode = (hashCode * 137) ^ Y;
                return hashCode;
            }
        }

        public static bool operator ==(TileId left, TileId right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TileId left, TileId right)
        {
            return !Equals(left, right);
        }


        public static TileId CoordinateToTileId(GeoCoordinate coordinate, int zoom)
        {
            var x = (int)Math.Floor((coordinate.Longitude + 180) / 360 * Math.Pow(2, zoom));
            var y = (int)Math.Floor((1 - Math.Log(Math.Tan(coordinate.Latitude * Math.PI / 180)
                                                  + 1 / Math.Cos(coordinate.Latitude * Math.PI / 180)) / Math.PI) / 2 * Math.Pow(2, zoom));

            return new TileId(x, y, zoom);
        }

        /// <summary>
        ///  http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames.
        /// </summary>
        private float TileXToLongitude(int x, int zoom)
        {
            var n = Math.Pow(2.0, zoom);
            var longitude = x / n * 360.0 - 180.0;
            return (float)longitude;
        }

        /// <summary>
        /// http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames.
        /// </summary>
        private float TileYToLatitude(int y, int zoom)
        {
            var n = Math.Pow(2.0, zoom);
            var radians = Math.Atan(Math.Sinh(Math.PI * (1 - 2 * y / n)));
            var latitude = radians * 180.0 / Math.PI;
            return (float)latitude;
        }
    }
}
