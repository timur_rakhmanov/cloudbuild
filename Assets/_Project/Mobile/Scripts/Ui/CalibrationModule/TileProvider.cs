﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Tasks;
using UnityEngine;
using UnityEngine.Assertions;


namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule
{
    public class TileProvider : MonoBehaviour
    {
        public int LoadRadius = 1;
        public int CleanRadius = 3;
        public float UpdateInterval = 0.33f;

        private bool _isInitialized;
        private TileId _initialTileId;
        private GeoCoordinate _currentCoordinates;
        private TileId _currentTileId;
        private TileId _cachedTileId;
        private List<TileId> _activeTiles;

        float _elapsedTime;

        private MobileCalibrationManager _calibrationManager;

        public void Initialize(GeoCoordinate geoCoordinate, int zoom)
        {
            if (_activeTiles != null && _activeTiles.Count > 0) {
                foreach (var tile in _activeTiles) {
                    GetCurrentLoader().RemoveTile(tile);
                }
            }

            _activeTiles = new List<TileId>();

            var tileId = TileId.CoordinateToTileId(geoCoordinate, zoom);
            _initialTileId = tileId;

            AddTile(tileId);

            var tiles = new List<TileId>();
            // Render initial tiles
            for (var x = tileId.X - LoadRadius; x <= tileId.X + LoadRadius; x++) {
                for (var y = tileId.Y - LoadRadius; y <= tileId.Y + LoadRadius; y++) {
                    tiles.Add(new TileId(x, y, zoom));
                }
            }

            tiles = tiles.OrderBy(t => Math.Max(Math.Abs(tileId.X - t.X), Math.Abs(tileId.Y - t.Y))).ToList();
            foreach (var meemimTileId in tiles) {
                AddTile(meemimTileId);
            }

            _isInitialized = true;
        }

        public void Awake()
        {
            _calibrationManager = GetComponentInParent<MobileCalibrationManager>();
            Assert.IsNotNull(_calibrationManager);
        }

        public void Update()
        {
            if (!_isInitialized)
                return;

            _elapsedTime += Time.deltaTime;

            if (_elapsedTime >= UpdateInterval) {
                _elapsedTime = 0f;

                _currentCoordinates = _calibrationManager.LocalToGeoCoordinate(_calibrationManager.ConvertDisplacementToPosition(GetComponentInChildren<RectTransformer>().TranslationTransform.localPosition));
                _currentTileId = TileId.CoordinateToTileId(_currentCoordinates, _calibrationManager.Zoom);

                if (!_currentTileId.Equals(_cachedTileId)) {
                    var tiles = new List<TileId>();
                    // Load nearby tiles
                    for (int x = _currentTileId.X - LoadRadius; x <= (_currentTileId.X + LoadRadius); x++) {
                        for (int y = _currentTileId.Y - LoadRadius; y <= (_currentTileId.Y + LoadRadius); y++) {
                            tiles.Add(new TileId(x, y, _calibrationManager.Zoom));
                        }
                    }

                    tiles = tiles.OrderBy(t => Math.Max(Math.Abs(_currentTileId.X - t.X), Math.Abs(_currentTileId.Y - t.Y))).ToList();
                    foreach (var tileId in tiles) {
                        AddTile(tileId);
                    }

                    _cachedTileId = _currentTileId;
                    Cleanup(_currentTileId);
                }
            }
        }

        private void Cleanup(TileId currentTileId)
        {
            var count = _activeTiles.Count;
            for (int i = count - 1; i >= 0; i--) {
                var tile = _activeTiles[i];
                if (tile.X <= _initialTileId.X + LoadRadius && tile.X >= _initialTileId.X - LoadRadius && tile.Y <= _initialTileId.Y + LoadRadius && tile.Y >= _initialTileId.Y - LoadRadius) {
                    continue;
                }

                var dispose = tile.X > currentTileId.X + CleanRadius || tile.X < _currentTileId.X - CleanRadius;
                dispose = dispose || tile.Y > _currentTileId.Y + CleanRadius || tile.Y < _currentTileId.Y - CleanRadius;

                if (dispose) {
                    RemoveTile(tile);
                }
            }
        }

        private void AddTile(TileId tile)
        {
            if (_activeTiles.Contains(tile)) {
                return;
            }

            _activeTiles.Add(tile);

            GetCurrentLoader().AddNewTile(tile, _initialTileId).AsTask().ContinueWith(t => t.Wait()).RunCoroutine(this);
        }

        private void RemoveTile(TileId tile)
        {
            if (!_activeTiles.Contains(tile)) {
                return;
            }

            _activeTiles.Remove(tile);
            
            GetCurrentLoader().RemoveTile(tile);
        }

        private ITileLoader GetCurrentLoader()
        {
            switch (ExecutionContext.Instance.DeviceCurrentCompany.DefaultMapTileProvider) {
                case MapTileProviderViewModel.Bing:
                    return GetComponent<BingTileLoader>();
                case MapTileProviderViewModel.ArcGis:
                    return GetComponent<ArcGisTileLoader>();
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
