﻿using System;
using TouchScript;
using TouchScript.Gestures;
using TouchScript.Gestures.TransformGestures;
using TouchScript.Gestures.TransformGestures.Base;
using TouchScript.Utils.Attributes;
using UnityEngine;

namespace Assets._Project.Mobile.Scripts.Ui.CalibrationModule
{
    public class WorldDirectionCalibrationMapTransformer : MonoBehaviour
    {
        public Transform TranslationTransform;
        public Transform RotationTransform;
        public Transform ScalingTransform;

        public float MinScale = 0.1f;
        public float MaxScale = 10f;

        public float Sensitive;
        
        #region Consts

        /// <summary>
        /// State for internal Transformer state machine.
        /// </summary>
        private enum TransformerState
        {
            /// <summary>
            /// Nothing is happening.
            /// </summary>
            Idle,

            /// <summary>
            /// The object is under manual control, i.e. user is transforming it.
            /// </summary>
            Manual,

            /// <summary>
            /// The object is under automatic control, i.e. it's being smoothly moved into target position when user lifted all fingers off.
            /// </summary>
            Automatic
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Gets or sets a value indicating whether Smoothing is enabled. Smoothing allows to reduce jagged movements but adds some visual lag.
        /// </summary>
        /// <value>
        ///   <c>true</c> if Smoothing is enabled; otherwise, <c>false</c>.
        /// </value>
        public bool EnableSmoothing
        {
            get { return _enableSmoothing; }
            set { _enableSmoothing = value; }
        }

        /// <summary>
        /// Gets or sets the smoothing factor.
        /// </summary>
        /// <value>
        /// The smoothing factor. Indicates how much smoothing to apply. 0 - no smoothing, 100000 - maximum.
        /// </value>
        public float SmoothingFactor
        {
            get { return _smoothingFactor * 100000f; }
            set { _smoothingFactor = Mathf.Clamp(value / 100000f, 0, 1); }
        }

        /// <summary>
        /// Gets or sets the position threshold.
        /// </summary>
        /// <value>
        /// Minimum distance between target position and smoothed position when to stop automatic movement.
        /// </value>
        public float PositionThreshold
        {
            get { return Mathf.Sqrt(_positionThreshold); }
            set { _positionThreshold = value * value; }
        }

        /// <summary>
        /// Gets or sets the rotation threshold.
        /// </summary>
        /// <value>
        /// Minimum angle between target rotation and smoothed rotation when to stop automatic movement.
        /// </value>
        public float RotationThreshold
        {
            get { return _rotationThreshold; }
            set { _rotationThreshold = value; }
        }

        /// <summary>
        /// Gets or sets the scale threshold.
        /// </summary>
        /// <value>
        /// Minimum difference between target scale and smoothed scale when to stop automatic movement.
        /// </value>
        public float ScaleThreshold
        {
            get { return Mathf.Sqrt(_scaleThreshold); }
            set { _scaleThreshold = value * value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this transform can be changed from another script.
        /// </summary>
        /// <value>
        /// <c>true</c> if this transform can be changed from another script; otherwise, <c>false</c>.
        /// </value>
        public bool AllowChangingFromOutside
        {
            get { return _allowChangingFromOutside; }
            set { _allowChangingFromOutside = value; }
        }

        #endregion

        #region Private variables

        [SerializeField]
        [ToggleLeft]
        private bool _enableSmoothing = false;

        [SerializeField]
        private float _smoothingFactor = 1f / 100000f;

        [SerializeField]
        private float _positionThreshold = 0.01f;

        [SerializeField]
        private float _rotationThreshold = 0.1f;

        [SerializeField]
        private float _scaleThreshold = 0.01f;

        [SerializeField]
        [ToggleLeft]
        private bool _allowChangingFromOutside = false;

        private TransformerState _state;

        private TransformGestureBase _gesture;

        private TransformGesture.TransformType _transformMask;
        private Vector3 _targetPosition, _targetScale;
        private Quaternion _targetRotation;

        // last* variables are needed to detect when Transform's properties were changed outside of this script
        private Vector3 _lastPosition, _lastScale;
        private Quaternion _lastRotation;

        #endregion

        #region Unity methods

        private void OnEnable()
        {
            _gesture = GetComponent<TransformGestureBase>();
            _gesture.StateChanged += stateChangedHandler;
            TouchManager.Instance.FrameFinished += frameFinishedHandler;

            stateIdle();
        }

        private void OnDisable()
        {
            if (_gesture != null) _gesture.StateChanged -= stateChangedHandler;
            if (TouchManager.Instance != null)
                TouchManager.Instance.FrameFinished -= frameFinishedHandler;

            stateIdle();
        }

        #endregion

        #region States

        private void stateIdle()
        {
            var prevState = _state;
            setState(TransformerState.Idle);

            if (_enableSmoothing && prevState == TransformerState.Automatic) {
                if ((_transformMask & TransformGesture.TransformType.Translation) != 0) {
                    _lastPosition = _targetPosition;
                    UpdateTranslationTransform(_targetPosition);

                    _lastRotation = _targetRotation;
                    UpdateRotationTransform(_targetRotation);
                }

                if ((_transformMask & TransformGesture.TransformType.Scaling) != 0) {
                    var newLocalScale = _lastScale = _targetScale;
                    // prevent recalculating colliders when no scale occurs
                    if (newLocalScale != ScalingTransform.localScale) {
                        UpdateScalingTransform(newLocalScale);
                    }
                }
            }

            _transformMask = TransformGesture.TransformType.None;
        }

        private void stateManual()
        {
            setState(TransformerState.Manual);

            _targetPosition = _lastPosition = TranslationTransform.localPosition;
            _targetRotation = _lastRotation = RotationTransform.localRotation;
            _targetScale = _lastScale = ScalingTransform.localScale;
            _transformMask = TransformGesture.TransformType.None;
        }

        private void stateAutomatic()
        {
            setState(TransformerState.Automatic);

            if (!_enableSmoothing || _transformMask == TransformGesture.TransformType.None) stateIdle();
        }

        private void setState(TransformerState newState)
        {
            _state = newState;
        }

        #endregion

        #region Private functions

        private void update()
        {
            if (_state == TransformerState.Idle) return;

            if (!_enableSmoothing) return;

            var fraction = 1 - Mathf.Pow(_smoothingFactor, Time.unscaledDeltaTime);
            var changed = false;

            if ((_transformMask & TransformGesture.TransformType.Scaling) != 0) {
                var scale = ScalingTransform.localScale;
                if (_allowChangingFromOutside) {
                    // Changed by someone else.
                    // Need to make sure to check per component here.
                    if (!Mathf.Approximately(scale.x, _lastScale.x))
                        _targetScale.x = scale.x;
                    if (!Mathf.Approximately(scale.y, _lastScale.y))
                        _targetScale.y = scale.y;
                    if (!Mathf.Approximately(scale.z, _lastScale.z))
                        _targetScale.z = scale.z;
                }

                var newLocalScale = Vector3.Lerp(scale, _targetScale, fraction);
                // Prevent recalculating colliders when no scale occurs.
                if (newLocalScale != scale) {
                    UpdateScalingTransform(newLocalScale);

                    // Something might have adjusted our scale.
                    _lastScale = ScalingTransform.localScale;
                }

                if (_state == TransformerState.Automatic && !changed && (_targetScale - _lastScale).sqrMagnitude > _scaleThreshold) changed = true;
            }

            if ((_transformMask & TransformGesture.TransformType.Translation) != 0) {
                if (_allowChangingFromOutside) {
                    // Changed by someone else.
                    if (RotationTransform.localRotation != _lastRotation) _targetRotation = RotationTransform.localRotation;
                }

                UpdateRotationTransform(Quaternion.Lerp(RotationTransform.localRotation, _targetRotation, fraction));

                // Something might have adjusted our rotation.
                _lastRotation = RotationTransform.localRotation;

                if (_state == TransformerState.Automatic && !changed && Quaternion.Angle(_targetRotation, _lastRotation) > _rotationThreshold) changed = true;
            }

            if ((_transformMask & TransformGesture.TransformType.Translation) != 0) {
                var pos = TranslationTransform.localPosition;
                if (_allowChangingFromOutside) {
                    // Changed by someone else.
                    // Need to make sure to check per component here.
                    if (!Mathf.Approximately(pos.x, _lastPosition.x))
                        _targetPosition.x = pos.x;
                    if (!Mathf.Approximately(pos.y, _lastPosition.y))
                        _targetPosition.y = pos.y;
                    if (!Mathf.Approximately(pos.z, _lastPosition.z))
                        _targetPosition.z = pos.z;
                }

                UpdateTranslationTransform(Vector3.Lerp(pos, _targetPosition, fraction));

                // Something might have adjusted our position (most likely Unity UI).
                _lastPosition = TranslationTransform.localPosition;

                if (_state == TransformerState.Automatic && !changed && (_targetPosition - _lastPosition).sqrMagnitude > _positionThreshold) changed = true;
            }

            if (_state == TransformerState.Automatic && !changed) stateIdle();
        }

        private void manualUpdate()
        {
            if (_state != TransformerState.Manual) stateManual();

            var mask = _gesture.TransformMask;
            if ((mask & TransformGesture.TransformType.Scaling) != 0) {
                _targetScale *= _gesture.DeltaScale;
            }

            if ((mask & TransformGesture.TransformType.Translation) != 0) {
                var deltaRotation = GetDeltaRotation();
                _targetRotation = Quaternion.AngleAxis(deltaRotation * Sensitive, _gesture.RotationAxis) * _targetRotation;

                _targetPosition += GetDeltaPosition() * Sensitive / ScalingTransform.localScale.x;
            }

            _transformMask |= mask;

            _gesture.OverrideTargetPosition(_targetPosition);

            if (!_enableSmoothing) applyValues();
        }

        private Vector3 GetDeltaPosition()
        {
            return new Vector3(0 , _gesture.DeltaPosition.y, 0);
        }

        private float GetDeltaRotation()
        {
            var position = _targetPosition + _gesture.DeltaPosition;

            return Rotate(_targetPosition, position);
        }

        private void applyValues()
        {
            if ((_transformMask & TransformGesture.TransformType.Scaling) != 0) {
                UpdateScalingTransform(_targetScale);
            }

            if ((_transformMask & TransformGesture.TransformType.Translation) != 0) {
                UpdateRotationTransform(_targetRotation);

                UpdateTranslationTransform(_targetPosition);
            }

            _transformMask = TransformGesture.TransformType.None;
        }

        #endregion

        #region Event handlers

        private void stateChangedHandler(object sender, GestureStateChangeEventArgs gestureStateChangeEventArgs)
        {
            switch (gestureStateChangeEventArgs.State) {
                case Gesture.GestureState.Possible:
                    stateManual();
                    break;
                case Gesture.GestureState.Changed:
                    manualUpdate();
                    break;
                case Gesture.GestureState.Ended:
                case Gesture.GestureState.Cancelled:
                    stateAutomatic();
                    break;
                case Gesture.GestureState.Failed:
                case Gesture.GestureState.Idle:
                    if (gestureStateChangeEventArgs.PreviousState == Gesture.GestureState.Possible) stateAutomatic();
                    break;
            }
        }

        private void frameFinishedHandler(object sender, EventArgs eventArgs)
        {
            update();
        }

        #endregion

        private void UpdateTranslationTransform(Vector3 newPosition)
        {
            TranslationTransform.localPosition = newPosition;
        }

        private void UpdateRotationTransform(Quaternion newRotate)
        {
            RotationTransform.localRotation = newRotate;
        }

        private void UpdateScalingTransform(Vector3 newScale)
        {
            if (newScale.x < MaxScale && newScale.x > MinScale) {
                ScalingTransform.localScale = newScale;
            }
        }

        private float Rotate(Vector2 oldScreenPos, Vector2 newScreenPos)
        {
            var rotationCenter = new Vector2(oldScreenPos.x, oldScreenPos.y - 200);

            var oldScreenDelta = rotationCenter - oldScreenPos;
            var newScreenDelta = rotationCenter - newScreenPos;
            return (Mathf.Atan2(newScreenDelta.y, newScreenDelta.x) -
                    Mathf.Atan2(oldScreenDelta.y, oldScreenDelta.x)) * Mathf.Rad2Deg;
        }
    }
}
