﻿using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using UnityEngine;
using UnityEngine.Events;


namespace Assets._Project.Mobile.Scripts.Ui
{
    public class DebugMenuController : MonoBehaviour
    {
        [HideInInspector]
        public UnityEvent SignInRequested;

        [HideInInspector]
        public UnityEvent SignOutRequested;
        
        [HideInInspector]
        public UnityEvent DebugInfoSwitchRequested;

        [HideInInspector]
        public UnityEvent LogConsoleSwitchRequested;

        [HideInInspector]
        public UnityEvent CloseRequested;

        private ILog _log;
        // Use this for initialization
        void Start()
        {
            _log = LogManager.GetLogger(typeof(DebugMenuController));
        }

        #region Commands

        public void Show()
        {
            gameObject.GetComponent<Canvas>().enabled = true;
        }

        public void Hide()
        {
            gameObject.GetComponent<Canvas>().enabled = false;
        }

        #endregion
        
        public void SignInButtonClicked()
        {
            SignInRequested.Invoke();
        }

        public void SignOutButtonClicked()
        {
            SignOutRequested.Invoke();
        }

        public void DebugInfoButtonClicked()
        {
            DebugInfoSwitchRequested.Invoke();
        }

        public void ConsoleButtonClicked()
        {
            LogConsoleSwitchRequested.Invoke();
        }

        public void CloseButtonClicked()
        {
            CloseRequested.Invoke();
        }
    }
}
