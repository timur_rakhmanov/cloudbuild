﻿using System;
using System.Collections.Generic;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Infrastructure;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


namespace Assets._Project.Mobile.Scripts.Ui.InfoCard
{
    public class InfoCardController : MonoBehaviour
    {
        #region Editor config
        [SerializeField]
        private GameObject _infoRowPrefab;

        [SerializeField]
        private GameObject _infoDetailsContainer;
        [SerializeField]
        private Text _layerNameText;
        [SerializeField]
        private Text _titleText;
        [SerializeField]
        private Image _placemarkImage;
        [SerializeField]
        private Image _coloredLine;
        #endregion

        #region Private data

        private PlacemarkViewModel _placemarkViewModel;
        private PlacemarksManager _placemarksManager;

        private const string EXTERNAL_ID_LABEL = "External id";
        #endregion

        #region Unity lifecycle

        #endregion

        #region Public methods

        public void Init(PlacemarkViewModel placemark)
        {
            Assert.IsNotNull(placemark);
            Assert.IsNotNull(_infoDetailsContainer);
            Assert.IsNotNull(_infoRowPrefab);
            Assert.IsNotNull(_layerNameText);
            Assert.IsNotNull(_titleText);
            Assert.IsNotNull(_placemarkImage);
            Assert.IsNotNull(_coloredLine);

            _placemarksManager = PlacemarksManager.Instance;
            _placemarkViewModel = placemark;

            SetImage();

            _titleText.text = placemark.ValidateHeaderText();
            _layerNameText.text = placemark.LayerName;
            _coloredLine.color = ColorConverter.HexToColor(placemark.MainColor);

            AddInformationTextBlocks(new[] { new KeyValuePair<string, string>(EXTERNAL_ID_LABEL, _placemarkViewModel.ExternalId) });
            AddInformationTextBlocks(_placemarkViewModel.DescriptionData);
        }

        public void CloseButtonClick()
        {
            _placemarksManager.DeselectPlacemark(true);
        }
        #endregion
        #region Private methods

        private void SetImage()
        {
            // TODO: check we have such switch here with the same case mostly.
            Sprite sprite;
            switch (_placemarkViewModel.PrimaryObjectType) {
                case ObjectType.Unknown:
                    sprite = PlacemarksManager.Instance.GetObjectIcon(_placemarkViewModel.PrimaryObjectType);
                    break;
                case ObjectType.Object3D:
                    var objectTypeData = (Object3DDataViewModel)_placemarkViewModel.ObjectTypeData;
                    sprite = Instantiate(MeshLibraryProvider.Instance.GetMeshSpritePrefab(objectTypeData.ObjectMesh));
                    break;
                case ObjectType.Pipe:
                    sprite = PlacemarksManager.Instance.GetObjectIcon(_placemarkViewModel.PrimaryObjectType);
                    break;
                case ObjectType.Area:
                    sprite = PlacemarksManager.Instance.GetObjectIcon(_placemarkViewModel.PrimaryObjectType);
                    break;
                case ObjectType.Custom:
                    sprite = PlacemarksManager.Instance.GetObjectIcon(_placemarkViewModel.PrimaryObjectType);
                    break;
                case ObjectType.Model3D:
                    sprite = PlacemarksManager.Instance.GetObjectIcon(_placemarkViewModel.PrimaryObjectType);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            _placemarkImage.sprite = sprite;
        }

        private void AddInformationTextBlocks(IEnumerable<KeyValuePair<string, string>> descriptionProperties)
        {
            foreach (var property in descriptionProperties) {
                var row = Instantiate(_infoRowPrefab);

                var key = row.transform.Find("Key").GetComponent<Text>();
                var value = row.transform.Find("Value").GetComponent<Text>();

                key.text = property.Key;
                value.text = property.Value;

                row.transform.SetParent(_infoDetailsContainer.transform, false);
            }
        }
        #endregion
    }
}