﻿using System;
using System.Collections;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Assets._Project.Shared.Scripts.Identity;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


namespace Assets._Project.Mobile.Scripts.Ui
{
    public class MainMenuController : MonoBehaviour
    {
        private const float CLOSE_TIMEOUT = 5.0f;

        [HideInInspector]
        public UnityEvent OnDebugMenuRequested;

        [HideInInspector]
        public UnityEvent OnCompanySelectionMenuRequested;

        [HideInInspector]
        public UnityEvent OnRecalibrationRequested;

        [HideInInspector]
        public UnityEvent OnMenuHidden;

        public GameObject SelectCompanyRow;

        private TimeSpan _recordingTime = TimeSpan.Zero;
        private ILog _log;
        
        // Use this for initialization
        private void Start()
        {
            _log = LogManager.GetLogger(typeof(MainMenuController));

            Assert.IsNotNull(SelectCompanyRow);

            LoadCompanyIconToMenu();

            GetComponent<SwipeablePanel>().PanelHidden.AddListener(PanelHiddenHandler);

            Hide();
        }

        private void Update()
        {
            if (gameObject.GetComponent<Canvas>().enabled) {
                _recordingTime += TimeSpan.FromSeconds(Time.deltaTime);

                if (_recordingTime.TotalSeconds > CLOSE_TIMEOUT) {
                    _recordingTime = TimeSpan.Zero;
                    OnMenuHidden.Invoke();
                }
            }
        }

        public void Show()
        {
            gameObject.GetComponent<Canvas>().enabled = true;
            GetComponent<SwipeablePanel>().SwitchState(true);
            _recordingTime = TimeSpan.Zero;

            foreach (var childText in GetComponentsInChildren<Text>()) {
                childText.fontStyle = FontStyle.Bold;
                childText.fontStyle = FontStyle.Normal;
            }
        }

        public void Hide()
        {
            gameObject.GetComponent<Canvas>().enabled = false;
        }


        public void CompanySelectButtonClicked()
        {
            OnCompanySelectionMenuRequested.Invoke();

            _log.LogInfo("CompanySelect clicked");
        }

        public void RecalibrationButtonClicked()
        {
            OnRecalibrationRequested.Invoke();

            _log.LogInfo("Recalibration clicked");
        }

        public void OpenDebugMenuButtonClicked()
        {
            OnDebugMenuRequested.Invoke();
        }

        private void LoadCompanyIconToMenu()
        {
            var companyButton = SelectCompanyRow.transform.Find("Icon").gameObject;
            var companyLabel = SelectCompanyRow.transform.Find("Labels/CompanyName").gameObject;
            var text = companyLabel.GetComponent<Text>();
            // TODO: use ExecutionContext.Instance.DeviceCurrentCompany. the issue is that isn't always awailable at this step.
            text.text = ExecutionContext.Instance.Company.Slug.ToUpper();
            LoadSpriteToImage(ExecutionContext.Instance.Company.LogoUrl, companyButton.GetComponent<Image>())
                .AsTask().RunCoroutine(this);
        }

        private IEnumerator LoadSpriteToImage(string iconUrl, Image companyIcon)
        {
            var www = new WWW(iconUrl);
            yield return www;
            companyIcon.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));

            CompaniesManager.Instance.OnActiveCompanyUpdated.AddListener(LoadCompanyIconToMenu);
        }

        private void PanelHiddenHandler()
        {
            OnMenuHidden.Invoke();
        }
    }
}

