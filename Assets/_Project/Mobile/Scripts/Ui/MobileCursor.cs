﻿using UnityEngine;
using UnityEngine.Events;


namespace Assets._Project.Mobile.Scripts.Ui
{
    public class MobileCursor : MonoBehaviour
    {
        public GameObject HitObject;
        public RaycastHit HitInfo;
        public bool IsGazingAtObject;

        void Update()
        {
            UpdateGazeInfo();
        }

        private void UpdateGazeInfo()
        {
            var newGazeOrigin = Camera.main.transform.position;
            var newGazeNormal = Camera.main.transform.forward;

            RaycastHit reycastHit;
            if (Physics.Raycast(new Ray(newGazeOrigin, newGazeNormal), out reycastHit)) {
                IsGazingAtObject = true;
                HitInfo = reycastHit;
                HitObject = reycastHit.transform.gameObject;
            } else {
                IsGazingAtObject = false;
                HitInfo = new RaycastHit();
                HitObject = null;
            }
        }
    }
}
