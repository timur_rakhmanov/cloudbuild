﻿using Assets._Project.Shared.Scripts.Infrastructure;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


namespace Assets._Project.Mobile.Scripts.Ui
{
    public class SceneScreenInfoPanelController : MonoBehaviour
    {
        [HideInInspector]
        public UnityEvent OnMainMenuRequested;

        private MobileCursor _mobileCursor;
        private Image _dettailsButton;

        void Start()
        {
            _mobileCursor = GetComponentInChildren<MobileCursor>();

            _dettailsButton = transform.Find("DetailsButton/Background/Icon").GetComponent<Image>();
        }

        void Update()
        {
            if (_mobileCursor.IsGazingAtObject) {
                _dettailsButton.color = new Color(1, 1, 1, 1);
            } else {
                _dettailsButton.color = new Color(1, 1, 1, 0.2f);
            }
        }

        public void Show()
        {
            gameObject.GetComponent<Canvas>().enabled = true;
        }

        public void Hide()
        {
            gameObject.GetComponent<Canvas>().enabled = false;
        }

        public void OpenMainMenuButtonClicked()
        {
            OnMainMenuRequested.Invoke();
        }

        public void DetailsButtonClicked()
        {
            if (_mobileCursor.IsGazingAtObject) {
                var placemarkController = _mobileCursor.HitObject.GetComponentInParent<PlacemarkController>();
                if (placemarkController != null) {
                    PlacemarksManager.Instance.SelectPlacemark(placemarkController, true);
                }
            }
        }
    }
}
