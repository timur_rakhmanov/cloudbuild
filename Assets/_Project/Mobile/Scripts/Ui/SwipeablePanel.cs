﻿using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using NUnit.Framework;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


namespace Assets._Project.Mobile.Scripts.Ui
{
    /// <summary>
    /// Controls panel that can be shown/hide by swipe gesture. 
    /// </summary>
    public class SwipeablePanel : MonoBehaviour
    {
        private const int SHOW_BUTTON_HEIGHT = 16;
        private const int HIDE_BUTTON_HEIGHT = 28;

        private static ILog _log;

        #region Public events

        public UnityEvent PanelHidden;

        #endregion
        #region Unity injections

        /// <summary>
        /// Button that show pannel.
        /// </summary>
        public GameObject ShowButtonPrefab;

        /// <summary>
        /// Button that hide pannel.
        /// </summary>
        public GameObject HideButtonPrefab;

        /// <summary>
        /// Height of the content area.
        /// </summary>
        public int ContentAreaHeight;

        /// <summary>
        /// Content that should be in swipeable area.
        /// </summary>
        public GameObject Content;

        /// <summary>
        /// Current state of the panel (shown or hidden).
        /// </summary>
        public bool IsShown;

        #endregion

        #region Private data

        private RectTransform _mainPanelRectTransform;
        private Button _showButton;

        #endregion

        private void Start()
        {
            _log = LogManager.GetLogger(typeof(SwipeablePanel));

            var canvas = GetComponent<Canvas>();
            if (canvas == null) {
                _log.LogError("Swipeable panel should be attached to the game object with canvas.");
                return;
            }

            Assert.IsTrue(ContentAreaHeight != 0);

            // Create dynamic content.

            // Main panel.
            var mainPanel = CreateRootPanel(transform);
            _mainPanelRectTransform = mainPanel.GetComponent<RectTransform>();
            // Check if panel that can be shown by show button.
            if (ShowButtonPrefab != null) {
                var showButtonGameObject = CreateShowButton(ShowButtonPrefab, _mainPanelRectTransform, ShowButtonClicked);
                _showButton = showButtonGameObject.GetComponent<Button>();
            }
            var contentWrapper = CreateContentWrapper(_mainPanelRectTransform, FlickGuestureChanged, Content);
            CreateHideButton(HideButtonPrefab, contentWrapper.GetComponent<RectTransform>(), HideButtonClicked);

            SetInitialState();
        }

        private static GameObject CreateRootPanel(Transform parentTransform)
        {
            var rootPanel = new GameObject("RootPanel");
            rootPanel.AddComponent<CanvasRenderer>();
            var rectTransform = rootPanel.AddComponent<RectTransform>();
            rectTransform.SetParent(parentTransform, false);

            // Set pivot.
            rectTransform.pivot = new Vector2(0, 0);

            // Set rect transform to bottom stretch.
            rectTransform.anchorMin = new Vector2(0, 0);
            rectTransform.anchorMax = new Vector2(1, 0);

            // Reset size.
            rectTransform.offsetMin = new Vector2(0, 0);
            rectTransform.offsetMax = new Vector2(0, 0);
            return rootPanel;
        }

        private static GameObject CreateShowButton(GameObject original, RectTransform parentTransform, UnityAction clickHandler)
        {
            Assert.IsNotNull(original);
            var showButtonGameObject = Instantiate(original);
            showButtonGameObject.name = "ShowButton";
            var showButton = showButtonGameObject.GetComponent<Button>();
            Assert.IsNotNull(showButton);
            showButton.onClick.AddListener(clickHandler);
            var showHideButtonRectTransform = showButtonGameObject.GetComponent<RectTransform>();
            showHideButtonRectTransform.SetParent(parentTransform, false);
            // Set pivot.
            showHideButtonRectTransform.pivot = new Vector2(0.5f, 1);

            showHideButtonRectTransform.anchorMin = new Vector2(0.5f, 1);
            showHideButtonRectTransform.anchorMax = new Vector2(0.5f, 1);

            // Reset size.
            showHideButtonRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 60);
            showHideButtonRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, SHOW_BUTTON_HEIGHT);
            return showButtonGameObject;
        }

        private static GameObject CreateContentWrapper(RectTransform parentTransform, UnityAction<Gesture> flickHandler, GameObject content)
        {
            var contentWrapper = new GameObject("ContentWrapper");
            contentWrapper.AddComponent<CanvasRenderer>();
            var contentWrapperRectTransform = contentWrapper.AddComponent<RectTransform>();

            // Set pivot.
            contentWrapperRectTransform.pivot = new Vector2(0, 0);

            contentWrapperRectTransform.anchorMin = new Vector2(0, 0);
            contentWrapperRectTransform.anchorMax = new Vector2(1, 1);

            contentWrapperRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, -SHOW_BUTTON_HEIGHT);
            contentWrapperRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 0);
            contentWrapperRectTransform.SetParent(parentTransform, false);

            // We need image so that touch works.
            var contentWrapperImage = contentWrapper.AddComponent<Image>();
            contentWrapperImage.color = new Color(0, 0, 0, 0);

            // Configure flick.
            var flickGuesture = contentWrapper.AddComponent<FlickGesture>();
            flickGuesture.UseUnityEvents = true;
            flickGuesture.SendStateChangeEvents = true;
            flickGuesture.OnStateChange.AddListener(flickHandler);

            // Add content to the panel.
            Assert.IsNotNull(content);
            content.transform.SetParent(contentWrapperRectTransform, false);
            return contentWrapper;
        }

        private static GameObject CreateHideButton(GameObject original, RectTransform parentTransform, UnityAction clickHandler)
        {
            Assert.IsNotNull(original);
            var hideButtonGameObject = Instantiate(original);
            hideButtonGameObject.name = "HideButton";
            var hideButton = hideButtonGameObject.GetComponent<Button>();
            Assert.IsNotNull(hideButton);
            hideButton.onClick.AddListener(clickHandler);

            var hideHideButtonRectTransform = hideButton.GetComponent<RectTransform>();
            hideHideButtonRectTransform.SetParent(parentTransform, false);
            hideHideButtonRectTransform.SetAsLastSibling();

            // Set pivot.
            hideHideButtonRectTransform.pivot = new Vector2(0.5f, 1f);

            hideHideButtonRectTransform.anchorMin = new Vector2(0.5f, 1);
            hideHideButtonRectTransform.anchorMax = new Vector2(0.5f, 1);

            // Reset size.
            hideHideButtonRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 64);
            hideHideButtonRectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, HIDE_BUTTON_HEIGHT);
            return hideButtonGameObject;
        }

        private void FlickGuestureChanged(Gesture gesture)
        {
            var flickGuesture = (FlickGesture) gesture;
            if (flickGuesture.State == Gesture.GestureState.Recognized) {
                // If up flick.
                bool isDownClick = flickGuesture.ScreenFlickVector.y < 0;
                if (isDownClick) {
                    SwitchState(false);
                }
            }
        }

        private void ShowButtonClicked()
        {
            SwitchState(true);
        }

        private void HideButtonClicked()
        {
            SwitchState(false);
        }

        private void SetInitialState()
        {
            SwitchState(IsShown);
        }

        public void SwitchState(bool show)
        {
            if (show) {
                if (_showButton != null) {
                    _showButton.gameObject.SetActive(false);
                }
                _mainPanelRectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, 0, ContentAreaHeight + SHOW_BUTTON_HEIGHT);
            } else {
                if (_showButton != null) {
                    _showButton.gameObject.SetActive(true);
                }
                _mainPanelRectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Bottom, -ContentAreaHeight, ContentAreaHeight + SHOW_BUTTON_HEIGHT);
                PanelHidden.Invoke();
            }

            IsShown = show;
        }
    }
}