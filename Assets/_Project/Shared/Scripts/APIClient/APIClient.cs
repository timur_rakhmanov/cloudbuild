﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Assets._Project.Shared.Scripts.APIClient.Coroutines;
using Assets._Project.Shared.Scripts.APIClient.JsonConverters;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.APIClient.ViewModels.Inspections;
using Assets._Project.Shared.Scripts.Configuration;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.LocationDetectors;
using Assets._Project.Shared.Scripts.Core.Tasks;
using JetBrains.Annotations;
using Meemim.VGis.Unity.Plugins.Interfaces.Dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using _Project.Shared.Scripts.APIClient.ViewModels;
using Newtonsoft.Json.Utilities;

namespace Assets._Project.Shared.Scripts.APIClient
{
    // TODO: consider divide api e.g. GetGoogleMap shouldn't be here.
    // TODO: use interface.
    public class ApiClient
    {
        #region Private data
        #region Constants
        private const string API_COMPANY_URL = "/api/companies/{slug}?fields={field}";
        private const string API_COMPANIES_URL = "/api/companies?fields={field}";
        private const string API_PLACEHOLDERS_URL = "/api/companies/{slug}/placemarks?latitude={latitude}&longitude={longitude}&radiusInMeters={radiusInMeters}";
        private const string API_PLACEMARKS_URL = "/api/companies/{slug}/placemarks/bbox?minLatitude={minLatitude}&minLongitude={minLongitude}&maxLatitude={maxLatitude}&maxLongitude={maxLongitude}";
        private const string API_NOTE_CREATE_URL = "/api/companies/{companySlug}/notes";
        private const string API_CREATE_PLACEMARK_URL = "/api/companies/{companySlug}/placemarks";
        private const string API_FILE_UPLOAD_URL = "/api/media/{type}";
        private const string API_CREATE_INSPECTION_RESULT_URL = "/api/companies/{companySlug}/inspection";

        private const string API_OSM_BUILDINGS_URL = "/api/geodata/osmbuildings?minLat={0}&minLng={1}&maxLat={2}&maxLng={3}&tileId={4}";
        private const string API_TILE_DATA_URL = "/api/geodata/tiledata/{0}/{1}";

        private const string API_GOOGLE_DOMAIN = "https://www.googleapis.com";
        private const string API_GOOGLE_WIFI_LOCATION_URL = "/geolocation/v1/geolocate?key=AIzaSyC1MsHuJxj0MC1kHy-7y3pf_HoN7fCJP_c";
        private const int DEFAULT_WIFI_ZOOM = 18;

        private const string BING_ELEVATION_BBOX_URL = "REST/v1/Elevation/Bounds?bounds={0}&rows={1}&cols={2}&key={3}";
        private const string BING_ELEVATION_POINTS_URL = "REST/v1/Elevation/List?points={0}&key={1}";
        private const string BING_DOMAIN = "http://dev.virtualearth.net"; 
        #endregion

        private readonly IRestClient _restClient;
        private readonly string _apiDomain = AppSettings.Instance.BackendServerUrl; 
        #endregion

        /// <summary>
        ///  Encapsulate rest api in coroutine.
        /// </summary>

        public ApiClient()
        {
            // TODO: use constructor injections.
            _restClient = new RestClient(_apiDomain);
        }

        #region Meemim backend requests
        /// <summary>
        /// Get company object by slug
        /// </summary>
        /// <param name="slug">String company slug</param>
        /// <returns>CompanyViewModel viewmodel object</returns>
        /// <exception cref="Exception">Exception with error message if request was not succeed</exception>
        public IEnumerator GetCompany(string slug)
        {
            if (slug == null) {
                throw new ArgumentNullException("slug");
            }
            var request = new RestRequest(API_COMPANY_URL) {
                Method = Method.GET
            };
            request.AddParameter("slug", slug, ParameterType.UrlSegment);
            request.AddParameter("field", "InspectionForms, Layers", ParameterType.UrlSegment);

            // TODO: simplify it in order to avoid run.
            var task = RestCoroutineFactory.CreateAuthorizedRestCoroutine(_restClient, request)
                .AsTask<IRestResponse>();
            yield return task.Run();

            var response = task.Result;
            var company = JsonConvert.DeserializeObject<CompanyViewModel>(response.Content);
            foreach (var form in company.InspectionForms) {
                form.Fields = JsonConvert.DeserializeObject<IList<FormField>>(form.Schema, new FormFieldConverter());
            }
            yield return company;
        }

        /// <summary>
        /// Get GeoJSON string with buildings in passed bounding box
        /// </summary>
        public IEnumerator GetOSMBuldings(string companySlug, string tileId, GeoCoordinate southWest, GeoCoordinate northEast)
        {
            var request = new RestRequest(string.Format(API_OSM_BUILDINGS_URL,
                 southWest.Latitude.ToString(CultureInfo.InvariantCulture),
                 southWest.Longitude.ToString(CultureInfo.InvariantCulture),
                 northEast.Latitude.ToString(CultureInfo.InvariantCulture),
                 northEast.Longitude.ToString(CultureInfo.InvariantCulture),
                 tileId)) {
                Method = Method.GET
            };
            var task = RestCoroutineFactory.CreateAuthorizedRestCoroutine(_restClient, request).AsTask<IRestResponse>();
            yield return task.Run();
            var response = task.Result;
            yield return response.Content;
        }

        public IEnumerator GetTileData(string tileId, MapTileDataType dataType)
        {
            var request = new RestRequest(string.Format(API_TILE_DATA_URL, tileId, dataType)) {
                Method = Method.GET
            };
            var task = RestCoroutineFactory.CreateAuthorizedRestCoroutine(_restClient, request).AsTask<IRestResponse>();
            yield return task.Run();
            var response = task.Result;
            var token = JToken.Parse(response.Content);
            yield return token["Value"].Value<string>();
        }

        /// <summary>
        /// Get all companies in system. (Only for super device)
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetCompanies()
        {
            var request = new RestRequest(API_COMPANIES_URL) {
                Method = Method.GET
            };

            request.AddParameter("field", "InspectionForms, Layers", ParameterType.UrlSegment);

            var task = RestCoroutineFactory.CreateAuthorizedRestCoroutine(_restClient, request)
                .AsTask<IRestResponse>();
            yield return task.Run();

            var response = task.Result;
            var companies = JsonConvert.DeserializeObject<List<CompanyViewModel>>(response.Content);
            foreach (var company in companies) {
                foreach (var form in company.InspectionForms) {
                    form.Fields = JsonConvert.DeserializeObject<IList<FormField>>(form.Schema, new FormFieldConverter());
                }
            }

            yield return companies;
        }

        /// <summary>
        /// Get placemarks for certain company and location
        /// </summary>
        /// <param name="companySlug">Id of company</param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="radius">Radius in meters</param>
        /// <returns>List of placemarks in specified location</returns>
        /// <exception cref="Exception">Exception with error message if request was not succeed</exception>
        public IEnumerator GetPlacemarksInRadius([NotNull] string companySlug, double latitude, double longitude, short radius, HashSet<ObjectType> objectTypes = null)
        {
            if (companySlug == null) {
                throw new ArgumentNullException("companySlug");
            }
            var url = API_PLACEHOLDERS_URL;
            if (objectTypes != null && objectTypes.Count > 0) {
                var typesList = objectTypes.ToList();
                for (int i = 0; i < objectTypes.Count; i++) {
                    url += string.Format("&typeFilter[{0}]={1}", i, typesList[i]);
                }
            }

            var request = new RestRequest(url) {
                Method = Method.GET
            };
            request.AddParameter("slug", companySlug, ParameterType.UrlSegment);
            request.AddParameter("latitude", latitude.ToString(CultureInfo.InvariantCulture), ParameterType.UrlSegment);
            request.AddParameter("longitude", longitude.ToString(CultureInfo.InvariantCulture), ParameterType.UrlSegment);
            request.AddParameter("radiusInMeters", radius, ParameterType.UrlSegment);


            var task = RestCoroutineFactory.CreateAuthorizedRestCoroutine(_restClient, request)
                .AsTask<IRestResponse>();
            yield return task.Run();

            var response = task.Result;
            var placemarks = JsonConvert.DeserializeObject<ICollection<PlacemarkViewModel>>(response.Content, new GeometryConverter());

            yield return placemarks;
        }

        /// <summary>
        /// Get placemarks for certain company and location
        /// </summary>
        /// <returns>List of placemarks in specified location</returns>
        /// <exception cref="Exception">Exception with error message if request was not succeed</exception>
        public IEnumerator GetPlacemarksInBBox([NotNull] string companySlug, double minLatitude, double minLongitude, double maxLatitude, double maxLongitude, HashSet<ObjectType> objectTypes = null)
        {
            if (companySlug == null) {
                throw new ArgumentNullException("companySlug");
            }
            var url = API_PLACEMARKS_URL;
            if (objectTypes != null && objectTypes.Count > 0) {
                var typesList = objectTypes.ToList();
                for (int i = 0; i < objectTypes.Count; i++) {
                    url += string.Format("&typeFilter[{0}]={1}", i, typesList[i]);
                }
            }

            var request = new RestRequest(url) {
                Method = Method.GET
            };
            request.AddParameter("slug", companySlug, ParameterType.UrlSegment);
            request.AddParameter("minLatitude", minLatitude.ToString(CultureInfo.InvariantCulture), ParameterType.UrlSegment);
            request.AddParameter("minLongitude", minLongitude.ToString(CultureInfo.InvariantCulture), ParameterType.UrlSegment);
            request.AddParameter("maxLatitude", maxLatitude.ToString(CultureInfo.InvariantCulture), ParameterType.UrlSegment);
            request.AddParameter("maxLongitude", maxLongitude.ToString(CultureInfo.InvariantCulture), ParameterType.UrlSegment);


            var task = RestCoroutineFactory.CreateAuthorizedRestCoroutine(_restClient, request)
                .AsTask<IRestResponse>();
            yield return task.Run();

            var response = task.Result;
            var placemarks = JsonConvert.DeserializeObject<ICollection<PlacemarkViewModel>>(response.Content, new GeometryConverter());

            yield return placemarks;
        }

        /// <summary>
        /// Create note for certain company
        /// </summary>
        /// <param name="companySlug">Company slug</param>
        /// <param name="latitude">User location latitude</param>
        /// <param name="longitude">User location longitude</param>
        /// <param name="deviceId">Device id</param>
        /// <param name="screenshotUrl">Screenshot uploaded url</param>
        /// <param name="voiceRecordingUrl">Voice recording uploaded url</param>
        /// <returns></returns>
        public IEnumerator CreateNote([NotNull] string companySlug, double latitude, double longitude, string deviceId, string screenshotUrl, string voiceRecordingUrl = null)
        {
            if (companySlug == null) {
                throw new ArgumentNullException("companySlug");
            }
            var request = new RestRequest(API_NOTE_CREATE_URL) {
                Method = Method.POST
            };

            request.AddParameter("companySlug", companySlug, ParameterType.UrlSegment);

            var model = new {
                Latitude = latitude.ToString(CultureInfo.InvariantCulture),
                Longitude = longitude.ToString(CultureInfo.InvariantCulture),
                DeviceId = deviceId,
                Screenshot = screenshotUrl,
                AudioMessage = voiceRecordingUrl
            };

#if NETFX_CORE
            request.AddJsonBody(model);
#else
            request.RequestFormat = DataFormat.Json;
            request.AddBody(model);
#endif
            var task = RestCoroutineFactory.CreateAuthorizedRestCoroutine(_restClient, request)
                .AsTask<IRestResponse>();
            yield return task.Run();
            task.Wait();
        }

        /// <summary>
        /// Create company's placemark
        /// </summary>
        /// <param name="companySlug">Company slug</param>
        /// <param name="model">Placemark creation model</param>
        /// <returns></returns>
        public IEnumerator CreatePlacemark(string companySlug, CreatePlacemarkViewModel model)
        {
            if (companySlug == null) {
                throw new ArgumentNullException("companySlug");
            }

            var request = new RestRequest(API_CREATE_PLACEMARK_URL) {
                Method = Method.POST
            };
            request.AddParameter("companySlug", companySlug, ParameterType.UrlSegment);

#if NETFX_CORE
            request.AddJsonBody(model);
#else
            request.RequestFormat = DataFormat.Json;
            request.AddBody(model);
#endif
            var task = RestCoroutineFactory.CreateAuthorizedRestCoroutine(_restClient, request)
                .AsTask<IRestResponse>();
            yield return task.Run();
            task.Wait();
        }

        public IEnumerator CreateInspectionResult(InspectionResultViewModel viewModel, string companySlug)
        {
            if (companySlug == null) {
                throw new ArgumentNullException("companySlug");
            }

            var request = new RestRequest(API_CREATE_INSPECTION_RESULT_URL) {
                Method = Method.POST
            };
            request.AddParameter("companySlug", companySlug, ParameterType.UrlSegment);
#if NETFX_CORE
            request.AddJsonBody(viewModel);
#else
            request.RequestFormat = DataFormat.Json;
            request.AddBody(viewModel);
#endif
            var task = RestCoroutineFactory.CreateAuthorizedRestCoroutine(_restClient, request)
                .AsTask<IRestResponse>();
            yield return task.Run();
            task.Wait();
        }

        /// <summary>
        /// Upload file to storage
        /// </summary>
        /// <param name="type">Storage media type (container type)</param>
        /// <param name="fileName">File name</param>
        /// <param name="filepath">File path</param>
        /// <param name="fileBytes">File bytes</param>
        /// <param name="fileContentType">File MIME content type</param>
        /// <returns></returns>
        public IEnumerator UploadFile(string type, string fileName, string filepath, byte[] fileBytes, string fileContentType)
        {
            var request = new RestRequest(API_FILE_UPLOAD_URL) { Method = Method.POST };

            request.AddParameter("type", type, ParameterType.UrlSegment);

            request.AddFile(fileName, fileBytes, filepath, fileContentType);

            var task = RestCoroutineFactory.CreateAuthorizedRestCoroutine(_restClient, request)
                .AsTask<IRestResponse>();

            yield return task.Run();

            var response = task.Result;

            var fileUrl = JsonConvert.DeserializeObject<string>(response.Content);

            yield return fileUrl;
        }
        #endregion

        #region Google requests
        /// <summary>
        /// Get current location using wifi poins and google api
        /// </summary>
        /// <param name="points">List of wifi points. Can't be null or empty</param>
        /// <returns><see cref="DetectedLocation"/>Detected location</returns>
        public IEnumerator GetLocationFromWifiPoints(IList<WiFiAccessPoint> points)
        {
            if (points == null || !points.Any()) {
                throw new ArgumentException("List of points could not be null or empty", "points");
            }
            var client = new RestClient(API_GOOGLE_DOMAIN);
            var request = new RestRequest(API_GOOGLE_WIFI_LOCATION_URL) { Method = Method.POST };

            var pointsModels = points.Select(p => new { macAddress = p.MacAddress, signalStrength = p.SignalStrength }).ToList();
            var model = new { wifiAccessPoints = pointsModels };
#if NETFX_CORE
            request.AddJsonBody(model);
#else
            request.RequestFormat = DataFormat.Json;
            request.AddBody(model);
#endif

            var task = RestCoroutineFactory.CreateRestCoroutine(client, request)
                .AsTask<IRestResponse>();
            yield return task.Run();

            var response = task.Result;

            var deserializedObject = JsonConvert.DeserializeAnonymousType(response.Content, new { location = new { lat = 0d, lng = 0d }, accuracy = 0d });
            var result = new DetectedLocation(new GeoCoordinate(deserializedObject.location.lat, deserializedObject.location.lng), DEFAULT_WIFI_ZOOM, deserializedObject.accuracy);

            yield return result;
        }
        #endregion

        #region Bing requests   


        /// <summary>
        /// Get elevation of single point
        /// </summary>
        public IEnumerator GetElevationDataAtPoint(GeoCoordinate coordinate)
        {
            var client = new RestClient(BING_DOMAIN);
            var points = string.Join(",", new [] { coordinate.Latitude.ToString(CultureInfo.InvariantCulture),
                coordinate.Longitude.ToString(CultureInfo.InvariantCulture)});

            var elevationUrl = string.Format(BING_ELEVATION_POINTS_URL, points, AppSettings.Instance.BingApiKey);

            var request = new RestRequest(elevationUrl);
            var task = RestCoroutineFactory.CreateRestCoroutine(client, request)
                    .AsTask<IRestResponse>();
            yield return task.Run();

            var response = task.Result;
            if (!string.IsNullOrEmpty(response.ErrorMessage)) {
                throw new Exception(response.ErrorMessage, response.ErrorException);
            }

            // Parse response
            var token = JToken.Parse(response.Content);
            var height = token["resourceSets"].First["resources"].First["elevations"].Values<float>().FirstOrDefault();

            yield return height;
        }


        /// <summary>
        /// Get elevation data for passed bbox
        /// </summary>
        /// <param name="gridSize">Size of result grid. Currently max value is 31</param>
        public IEnumerator GetElevationDataForBBox(double minLatitude, double minLongitude, double maxLatitude, double maxLongitude, int gridSize)
        {
            if (gridSize > 31) {
                throw new ArgumentOutOfRangeException("gridSize", "Bing maps don't support grids bigger than 31x31");
            }
            var client = new RestClient(BING_DOMAIN);
            // Form bbox
            var bbox = string.Join(",", new [] {
                minLatitude.ToString(CultureInfo.InvariantCulture),
                minLongitude.ToString(CultureInfo.InvariantCulture),
                maxLatitude.ToString(CultureInfo.InvariantCulture),
                maxLongitude.ToString(CultureInfo.InvariantCulture)
            });

            // Request url for tile
            var elevationUrl = string.Format(BING_ELEVATION_BBOX_URL, bbox, gridSize + 1, gridSize + 1, AppSettings.Instance.BingApiKey);
            var request = new RestRequest(elevationUrl);
            var task = RestCoroutineFactory.CreateRestCoroutine(client, request)
                    .AsTask<IRestResponse>();
            yield return task.Run();

            var response = task.Result;
            if (!string.IsNullOrEmpty(response.ErrorMessage)) {
                throw new Exception(response.ErrorMessage, response.ErrorException);
            }

            // Parse response
            var token = JToken.Parse(response.Content);
            var heights = token["resourceSets"].First["resources"].First["elevations"].Values<float>().ToArray();
            var min = heights.Min();
            var max = heights.Max();

            yield return new ElevationData() {
                Columns = gridSize + 1,
                Rows = gridSize + 1,
                Min = min,
                Max = max,
                Heights = heights
            };
        }
        #endregion

        #region Stubs

        private void Il2cppStup()
        {
            AotHelper.EnsureList<KeyValuePair<string,string>>();
            AotHelper.EnsureList<Guid>();
            AotHelper.EnsureDictionary<string,string>();
        }

        #endregion
    }
}
