﻿using System;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using HoloToolkit;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


#if NETFX_CORE
    using System.Reflection;
#endif

namespace Assets._Project.Shared.Scripts.APIClient.Converters
{
    // TODO: rename to object data configurtion converter
    public class ObjectTypeConfigurationConverter : JsonConverter
    {
        private const string OBJECT_TYPE = "ObjectType";

        public override bool CanWrite {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) {
                return null;
            }

            var item = JObject.Load(reader);
            ObjectTypeDataViewModel deserializedObject;

            if (item[OBJECT_TYPE].ToObject<ObjectType>() == ObjectType.Object3D) {
                deserializedObject = item.ToObject<Object3DDataViewModel>();
            } else if (item[OBJECT_TYPE].ToObject<ObjectType>() == ObjectType.Pipe) {
                deserializedObject = item.ToObject<PipeDataViewModel>();
            }
            else if (item[OBJECT_TYPE].ToObject<ObjectType>() == ObjectType.Custom) {
                deserializedObject = item.ToObject<CustomDataViewModel>();
            } else {
                deserializedObject = item.ToObject<ObjectTypeDataViewModel>();
            }

            return deserializedObject;
        }

        public override bool CanConvert(Type objectType)
        {
#if NETFX_CORE
            return typeof(IObjectGeometry).GetTypeInfo().IsAssignableFrom(objectType.GetTypeInfo());
#else
            return typeof(IObjectGeometry).GetTypeInfo().IsAssignableFrom(objectType);
#endif
        }
    }
}
