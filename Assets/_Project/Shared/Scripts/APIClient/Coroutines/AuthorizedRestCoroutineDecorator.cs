﻿using System;
using System.Collections;
using System.Net;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Assets._Project.Shared.Scripts.Identity;
using JetBrains.Annotations;
using RestSharp;


namespace Assets._Project.Shared.Scripts.APIClient.Coroutines
{
    /// <summary>
    /// Try to refresh token if api returns 401 response.
    /// </summary>
    public class AuthorizedRestCoroutineDecorator: IRestCoroutine
    {
        private const string AUTHORIZATION_HEADER = "Authorization";

        private readonly IRestCoroutine _coroutine;
        private readonly RestRequest _request;
        private IdentityApiClient _identityApiClient;

        public AuthorizedRestCoroutineDecorator([NotNull] IRestCoroutine coroutine, [NotNull] RestRequest request)
        {
            if (coroutine == null) {
                throw new ArgumentNullException("coroutine");
            }
            if (request == null) {
                throw new ArgumentNullException("request");
            }
            _coroutine = coroutine;
            _request = request;
            _identityApiClient = new IdentityApiClient();
        }

        public IRestResponse Response { get { return _coroutine.Response; } }

        public IEnumerator Run()
        {
            var identity = ExecutionContext.Instance.IdentityData;
            // If refresh token doesn't exists (throws exception).
            if (identity == null) {
                throw new Exception("User should be authenticated in ourder to make authorized request.");
            }

            if (string.IsNullOrEmpty(identity.RefreshToken)) {
                throw new Exception("Authenticated user doesn't have refresh token.");
            }
            
            // Make request.
            _request.AddHeader(AUTHORIZATION_HEADER, string.Format("bearer {0}", identity.AccessToken));
            var task = _coroutine.AsTask<IRestResponse>();
            yield return task.Run();
            
            // Just pass result in case of result or any errors except unauthorized.
            if (!task.IsFaulted || Response.StatusCode != HttpStatusCode.Unauthorized) {
                yield return task.Result;
                yield break;
            }

            // Refresh token.
            // NOTE: Method shouldn't use AuthorizedRestCoroutineDecoarator in order to avoid circular dependency.
            var refreshAccessTokenTask = _identityApiClient.RefreshAccessToken(identity.RefreshToken)
                .AsTask<IdentityViewModel>();

            yield return refreshAccessTokenTask.Run();

            var identityViewModel = refreshAccessTokenTask.Result;
            ExecutionContext.Instance.UpdateIdentityData(identityViewModel);

            // Repeat request with new token.
            _request.Parameters.Remove(_request.Parameters.Find(p => p.Name.Equals(AUTHORIZATION_HEADER)));
            _request.AddHeader(AUTHORIZATION_HEADER, string.Format("bearer {0}", identityViewModel.AccessToken));
            task = _coroutine.AsTask<IRestResponse>();
            yield return task.Run();
            yield return task.Result;
        }
    }
}