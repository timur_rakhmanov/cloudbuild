﻿using Assets._Project.Shared.Scripts.Core.Tasks;
using RestSharp;


namespace Assets._Project.Shared.Scripts.APIClient.Coroutines
{
    /// <summary>
    /// Interface to the rest request coroutine wrapper.
    /// </summary>
    public interface IRestCoroutine: IAsyncDecorator
    {
        /// <summary>
        /// Provides access to response details, null if we don't get response yet (should be called when coroutine ends). 
        /// Should mostly be used only be low level code.
        /// </summary>
        // TODO: consider to avoid it when AsyncDecorator supports custom result.
        IRestResponse Response { get; }
    }
}