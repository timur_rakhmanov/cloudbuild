﻿using System;
using System.Collections;
using System.Linq;
using Assets._Project.Shared.Scripts.Core.Tasks;
using JetBrains.Annotations;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using RestSharp;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.APIClient.Coroutines
{
    /// <summary>
    /// Logs execution and exceptions.
    /// </summary>
    public class LogRestCoroutineDecorator: IRestCoroutine
    {
        // TODO: move to global constatnts.
        private const string AUTHORIZATION_HEADER = "Authorization";

        private readonly IRestCoroutine _coroutine;
        private readonly RestRequest _request;
        private readonly ILog _log;

        public LogRestCoroutineDecorator([NotNull] IRestCoroutine coroutine, [NotNull] RestRequest request)
        {
            if (coroutine == null) {
                throw new ArgumentNullException("coroutine");
            }
            if (request == null) {
                throw new ArgumentNullException("request");
            }
            _coroutine = coroutine;
            _request = request;
            _log = LogManager.GetLogger(typeof(LogRestCoroutineDecorator));
        }

        public IRestResponse Response { get { return _coroutine.Response; } }

        public IEnumerator Run()
        {
            LogTrace("Rest request start.");
            var task = _coroutine.AsTask<IRestResponse>();
            yield return task.Run();
            if (task.IsFaulted) {
                // TODO: consider if this should be log as warning since in most cases it will be written to logs at the upper level too.
                LogError(task.Exception);
            }

            LogTrace("Rest request end.");

            yield return task.Result;
        }

        private void LogTrace(string message)
        {
            _log.LogTraceFormat("Message = {0}, Method = {1}, Url = {2}, Parameters = {3}", message, _request.Method, _request.Resource, string.Join(", ", _request.Parameters
                    .Where(p => p.Name != AUTHORIZATION_HEADER && (p.Type == ParameterType.UrlSegment || p.Type == ParameterType.GetOrPost))
                    .Select(p => p.Name + "=" + p.Value.ToString()).ToArray()));
        }

        private void LogError(Exception exception)
        {
            Debug.LogError(new {
                Mesage = "Rest request failed.",
                HttpStatusCode = Response.StatusCode,
                ResponseContent = Response.Content,
                ExceptionMessage = exception.Message,
                ExceptionDetails = exception.ToString(),
                _request.Method,
                Url = _request.Resource,
                Parameters = string.Join(", ", _request.Parameters
                    .Where(p => p.Name != AUTHORIZATION_HEADER && (p.Type == ParameterType.UrlSegment || p.Type == ParameterType.GetOrPost))
                    .Select(p => p.Name + "=" + p.Value.ToString()).ToArray())
            });
        }
    }
}