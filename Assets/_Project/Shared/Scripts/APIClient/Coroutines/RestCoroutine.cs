﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Meemim.VGis.Unity.Plugins.Interfaces.Dto;
using RestSharp;


namespace Assets._Project.Shared.Scripts.APIClient.Coroutines
{
    /// <summary>
    /// Makes request to rest resource.
    /// </summary>
    public class RestCoroutine : IRestCoroutine
    {
        private readonly IRestClient _client;
        private readonly RestRequest _request;
        private IRestResponse _response;

        public RestCoroutine(IRestClient client, RestRequest request)
        {
            _client = client;
            _request = request;
        }

        public IRestResponse Response { get { return _response; } }

        public IEnumerator Run()
        {
            var asyncRequest = new AsyncDecorator<IRestResponse>((success, failure) => {
#if NETFX_CORE
                var task = _client.ExecuteTaskAsync(_request);
                task.ContinueWith((t) => {
                    var response = t.Result;
                    _response = response;
                    var isSuccess = new List<HttpStatusCode> { HttpStatusCode.Created, HttpStatusCode.OK, HttpStatusCode.NoContent }
                        .Contains(response.StatusCode);
                    if (isSuccess) {
                        success(response);
                    } else {
                        failure(response.ErrorException != null ? new ErrorReason(response.ErrorException) : new ErrorReason(response.Content));
                    }
                });
#else
                // TODO:21696 Change this logic for handle refresh token.
                _client.ExecuteAsync(_request, (response) => {
                    _response = response;
                    var isSuccess = new List<HttpStatusCode> { HttpStatusCode.Created, HttpStatusCode.OK, HttpStatusCode.NoContent }
                        .Contains(response.StatusCode);
                    if (isSuccess) {
                        success(response);
                    } else {
                        failure(response.ErrorException != null ? new ErrorReason(response.ErrorException) : new ErrorReason(response.Content));
                    }
                });
#endif
            });

            return asyncRequest.Run();
        }
    }
}