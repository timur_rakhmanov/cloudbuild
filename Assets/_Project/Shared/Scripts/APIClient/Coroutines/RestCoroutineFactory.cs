﻿using RestSharp;


namespace Assets._Project.Shared.Scripts.APIClient.Coroutines
{
    /// <summary>
    /// Factory that creates rest coroutines.
    /// </summary>
    public static class RestCoroutineFactory
    {
        public static IRestCoroutine CreateRestCoroutine(IRestClient client, RestRequest request)
        {
            return new LogRestCoroutineDecorator(
                new SessionRestCoroutineDecorator(
                    new RestCoroutine(client, request), request), request);
        }

        public static IRestCoroutine CreateAuthorizedRestCoroutine(IRestClient client, RestRequest request)
        {
            return new LogRestCoroutineDecorator(
                new SessionRestCoroutineDecorator(
                    new AuthorizedRestCoroutineDecorator( 
                        new RestCoroutine(client, request), request), request), request);
        }
    }
}
