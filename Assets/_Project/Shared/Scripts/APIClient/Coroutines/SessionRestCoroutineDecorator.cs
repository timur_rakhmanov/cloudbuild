﻿using System;
using System.Collections;
using Assets._Project.Shared.Scripts.Core;
using JetBrains.Annotations;
using RestSharp;


namespace Assets._Project.Shared.Scripts.APIClient.Coroutines
{
    /// <summary>
    /// Adds session information to the rest co-routine.
    /// </summary>
    public class SessionRestCoroutineDecorator: IRestCoroutine
    {
        private const string SESSION_ID_HEADER = "MeemimSessionId";

        private readonly IRestCoroutine _coroutine;
        private readonly RestRequest _request;

        public SessionRestCoroutineDecorator([NotNull] IRestCoroutine coroutine, [NotNull] RestRequest request)
        {
            if (coroutine == null) {
                throw new ArgumentNullException("coroutine");
            }
            if (request == null) {
                throw new ArgumentNullException("request");
            }
            _coroutine = coroutine;
            _request = request;
        }

        public IRestResponse Response { get { return _coroutine.Response; } }

        public IEnumerator Run()
        {
            _request.AddHeader(SESSION_ID_HEADER, ExecutionContext.Instance.SessionId.ToString());
            // NOTE: we don't wrap IEnumerator in task here since client of IAsyncDecorator should wraprs it.
            return _coroutine.Run();
        }
    }
}