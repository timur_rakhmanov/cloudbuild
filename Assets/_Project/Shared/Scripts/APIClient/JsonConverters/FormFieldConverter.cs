﻿using System;
using Assets._Project.Shared.Scripts.APIClient.ViewModels.Inspections;
using HoloToolkit;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
#if NETFX_CORE
using System.Reflection;
#endif


namespace Assets._Project.Shared.Scripts.APIClient.JsonConverters
{
    public class FormFieldConverter : JsonConverter
    {
        private const string FIELD_TYPE = "type";
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken item = JToken.Load(reader);
            FormField deserializedObject;
            if (item[FIELD_TYPE].Value<string>().Equals(FormFieldType.ButtonsGroup.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
                deserializedObject = item.ToObject<ButtonsGroupField>();
            } else if (item[FIELD_TYPE].Value<string>().Equals(FormFieldType.Voice.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
                deserializedObject = item.ToObject<VoiceField>();
            } else if (item[FIELD_TYPE].Value<string>().Equals(FormFieldType.Photo.ToString(), StringComparison.CurrentCultureIgnoreCase)) {
                deserializedObject = item.ToObject<PhotoField>();
            } else {
                throw new Exception("Unsupported type of field" + item[FIELD_TYPE]);
            }

            return deserializedObject;
        }

        public override bool CanConvert(Type objectType)
        {
#if NETFX_CORE
            return typeof(FormField).GetTypeInfo().IsAssignableFrom(objectType.GetTypeInfo());
#else
            return typeof(FormField).GetTypeInfo().IsAssignableFrom(objectType);
#endif
        }
    }
}