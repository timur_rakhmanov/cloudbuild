﻿using System;
using System.Collections.Generic;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using HoloToolkit;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


#if NETFX_CORE
    using System.Reflection;
#endif

namespace Assets._Project.Shared.Scripts.APIClient.JsonConverters
{
    public class GeometryConverter : JsonConverter
    {
        private const string GEOMETRY_TYPE = "GeometryType";
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken item = JToken.Load(reader);
            IObjectGeometry deserializedObject;
            if (item[GEOMETRY_TYPE].Value<int>().Equals((int)GeometryType.Point)) {
                deserializedObject = item.ToObject<PointGeometry>();
            } else if (item[GEOMETRY_TYPE].Value<int>().Equals((int)GeometryType.Line)) {
                deserializedObject = item.ToObject<LineGeometry>();
            } else if (item[GEOMETRY_TYPE].Value<int>().Equals((int)GeometryType.MultiGeometry)) {
                var geometry = new MultiGeometry { GeometryType = GeometryType.MultiGeometry };
                geometry.Geometries = JsonConvert.DeserializeObject<IList<IObjectGeometry>>(item["Geometries"].ToString(), new GeometryConverter());
                deserializedObject = geometry;
            } else if (item[GEOMETRY_TYPE].Value<int>().Equals((int) GeometryType.Polygon)) {
                deserializedObject = item.ToObject<PolygonGeometry>();
            } else if (item[GEOMETRY_TYPE].Value<int>().Equals((int)GeometryType.Model)) {
                deserializedObject = item.ToObject<ModelGeometry>();
            } else {
                throw new Exception("Unsupported type of geometry " + item[GEOMETRY_TYPE]);
            }

            return deserializedObject;
        }


        public override bool CanConvert(Type objectType)
        {
#if NETFX_CORE
            return typeof(IObjectGeometry).GetTypeInfo().IsAssignableFrom(objectType.GetTypeInfo());
#else
            return typeof(IObjectGeometry).GetTypeInfo().IsAssignableFrom(objectType);
#endif
        }
    }
}