﻿using System;
using System.Collections.Generic;
using Assets._Project.Shared.Scripts.APIClient.ViewModels.Inspections;


namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public class CompanyViewModel
    {
        public Guid Id { get; set; }
        public string Slug { get; set; }
        public string Name { get; set; }
        public string LogoUrl { get; set; }
        
        /// <summary>
        /// Main location of the company.
        /// </summary>
        public double DefaultLatitude { get; set; }

        /// <summary>
        /// Main location of the company.
        /// </summary>
        public double DefaultLongitude { get; set; }

        /// <summary>
        /// Default zoom of the company.
        /// </summary>
        public int DefaultZoom { get; set; }

        /// <summary>
        /// Default map tile provider
        /// </summary>
        public MapTileProviderViewModel DefaultMapTileProvider { get; set; }

        /// <summary>
        /// Company inspection forms
        /// </summary>
        public List<InspectionFormViewModel> InspectionForms { get; set; }

        /// <summary>
        /// Company scene layers
        /// </summary>
        public List<SceneLayerViewModel> Layers { get; set; }
    }
}
