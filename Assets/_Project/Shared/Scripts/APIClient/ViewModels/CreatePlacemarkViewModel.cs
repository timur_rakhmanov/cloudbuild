﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public class CreatePlacemarkViewModel
    {
        /// <summary>
        /// Placemark location latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Placemark location longitude
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Device created placemark id
        /// </summary>
        public string DeviceId { get; set; }

        /// <summary>
        /// Photo attached to placemark image url
        /// </summary>
        public string Screenshot { get; set; }

        /// <summary>
        /// Voice message attached to placemark audio url
        /// </summary>
        public string AudioMessage { get; set; }
    }
}
