namespace _Project.Shared.Scripts.APIClient.ViewModels
{
    public class ElevationData
    {
        public int Columns { get; set; }
        public int Rows { get; set; }
        public float Min { get; set; }
        public float Max { get; set; }
        public float[] Heights { get; set; }
    }
}