﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public enum GeometryType {
        Point = 0,
        Line,
        MultiGeometry,
        Polygon,
        Model
    }
}
