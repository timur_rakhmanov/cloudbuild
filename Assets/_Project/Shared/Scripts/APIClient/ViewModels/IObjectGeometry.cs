﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public interface IObjectGeometry
    {
        GeometryType GeometryType { get; set; }
    }
}
