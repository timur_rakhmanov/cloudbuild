﻿using System.Collections.Generic;


namespace Assets._Project.Shared.Scripts.APIClient.ViewModels.Inspections
{
    public class ButtonsGroupField : FormField
    {
        public HashSet<string> DefaultValue { get; set; }
        public HashSet<string> Options { get; set; }
        public GroupType GroupType { get; set; }
    }
}