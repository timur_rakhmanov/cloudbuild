﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels.Inspections
{
    public abstract class FormField
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public FormFieldType Type { get; set; }
        public bool IsRequired { get; set; }
}
}