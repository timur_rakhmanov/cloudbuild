﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels.Inspections
{
    public enum FormFieldType
    {
        ButtonsGroup = 0,
        Voice = 2, 
        Photo = 3
    }
}