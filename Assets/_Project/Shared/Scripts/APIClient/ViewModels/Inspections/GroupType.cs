﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels.Inspections
{
    public enum GroupType
    {
        RadioButton = 0,
        Checkbox = 1
    }
}