﻿using System;
using System.Collections.Generic;


namespace Assets._Project.Shared.Scripts.APIClient.ViewModels.Inspections
{
    public class InspectionFormViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Readable name to identify form
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// JSON string schema 
        /// http://bugs.mifprojects.com/projects/meemim-vgis/wiki/Inspection_form_engine
        /// </summary>
        public string Schema { get; set; }

        public IList<FormField> Fields { get; set; }

        public DateTime CreatedAtUtc { get; set; }
        public DateTime LastUpdatedAtUtc { get; set; }
    }
}