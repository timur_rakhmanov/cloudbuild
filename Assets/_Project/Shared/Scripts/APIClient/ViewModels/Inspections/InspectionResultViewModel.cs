﻿using System;
using System.Collections.Generic;


namespace Assets._Project.Shared.Scripts.APIClient.ViewModels.Inspections
{
    public class InspectionResultViewModel
    {
        public InspectionResultViewModel()
        {
             Fields = new List<CompletedFormFieldViewModel>();
        }
        /// <summary>
        /// Form id
        /// </summary>
        public Guid FormId { get; set; }
        /// <summary>
        /// Form name
        /// </summary>
        public string FormName { get; set; }

        /// <summary>
        /// Id of placemark
        /// </summary>
        public string PlacemarkId { get; set; }

        /// <summary>
        /// List of completed fields
        /// </summary>
        public IList<CompletedFormFieldViewModel> Fields { get; set; }

        /// <summary>
        /// Id of active company at the time of completion
        /// Could be used both for simplifying queries and extra validation
        /// </summary>
        public Guid CompanyId { get; set; }
        public Guid DataSourceId { get; set; }
        public string DeviceId { get; set; }
        public Guid SessionId { get; set; }
    }
}