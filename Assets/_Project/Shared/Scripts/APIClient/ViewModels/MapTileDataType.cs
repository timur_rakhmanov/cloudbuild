﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public enum MapTileDataType
    {
        ArcGisImagery = 0,
        OsmBuildings = 1
    }
}