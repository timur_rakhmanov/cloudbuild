﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public enum MapTileProviderViewModel : byte
    {
        Bing = 0,
        ArcGis = 1
    }
}
