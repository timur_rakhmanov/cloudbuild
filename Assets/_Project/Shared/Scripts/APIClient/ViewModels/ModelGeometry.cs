﻿using System.Collections.Generic;


namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public class ModelGeometry : IObjectGeometry
    {
        public GeometryType GeometryType { get; set; }

        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public float? Altitude { get; set; }

        // Scale
        public float ScaleX { get; set; }
        public float ScaleY { get; set; }
        public float ScaleZ { get; set; }

        // Orientation
        public float Heading { get; set; } // Unity Y
        public float Tilt { get; set; } // Unity X
        public float Roll { get; set; } // Unity Z

        public string Link { get; set; }
        public ICollection<string> Resources { get; set; }
    }
}