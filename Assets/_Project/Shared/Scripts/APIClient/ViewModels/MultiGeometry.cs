﻿using System.Collections.Generic;


namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public class MultiGeometry : IObjectGeometry
    {
        public GeometryType GeometryType { get; set; }
        public IList<IObjectGeometry> Geometries { get; set; }
    }
}
