﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public class Object3DDataViewModel : ObjectTypeDataViewModel
    {
        #region General data

        /// <summary>
        /// 3D object placement type
        /// </summary>
        public PlacementTypeViewModel? PlacementType { get; set; }

        /// <summary>
        /// Below ground object mesh for rendering
        /// </summary>
        public string ObjectMesh { get; set; }

        #endregion

        #region AboveGround data

        /// <summary>
        /// Above ground object height
        /// </summary>
        public double? ObjectHeight { get; set; }

        /// <summary>
        /// Underground connection depth
        /// </summary>
        public double? AboveGroundUndergroundPartDepth { get; set; }

        /// <summary>
        /// Describe mesh rendering
        /// </summary>
        public bool? ShowMesh { get; set; }

        #endregion

        #region OnGround data

        /// <summary>
        /// Default diameter of the underground part.
        /// </summary>
        public double? Diameter { get; set; }

        /// <summary>
        /// Form of the underground part.
        /// </summary>
        public OnGroundObject3DForm? Form { get; set; }

        /// <summary>
        /// Should be defauld material used
        /// </summary>
        public bool? DefaultMaterial { get; set; }

        /// <summary>
        /// Should the base be similar diameter with cylinder
        /// </summary>
        public bool? SimilarSizeBaze { get; set; }

        #endregion

        #region BelowGround data

        /// <summary>
        /// Should sphere be rendered
        /// </summary>
        public bool? ShowSphere { get; set; }

        /// <summary>
        /// Sphere radius
        /// </summary>
        public double? SphereRadius { get; set; }

        /// <summary>
        /// Below ground object underground connection depth
        /// </summary>
        public double? BelowGroundUndergroundPartDepth { get; set; }

        #endregion
    }
}
