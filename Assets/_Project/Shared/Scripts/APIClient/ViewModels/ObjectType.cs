﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public enum ObjectType : byte
    {
        Unknown = 0,
        Object3D,
        Pipe,
        Area,
        Custom,
        Model3D
    }
}
