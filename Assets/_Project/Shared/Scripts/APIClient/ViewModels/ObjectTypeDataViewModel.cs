﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public class ObjectTypeDataViewModel
    {
        public ObjectType ObjectType { get; set; }
    }
}
