﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public class PipeDataViewModel : ObjectTypeDataViewModel
    {
        /// <summary>
        /// Pipe diameter in meters.
        /// </summary>
        public double? Diameter { get; set; }
    }
}
