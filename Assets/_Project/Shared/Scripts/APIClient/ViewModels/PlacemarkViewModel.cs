﻿using System;
using System.Collections.Generic;
using Assets._Project.Shared.Scripts.APIClient.Converters;
using Newtonsoft.Json;


namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public class PlacemarkViewModel {
        /// <summary>
        /// Internal unique id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Id of the related company.
        /// </summary>
        public Guid CompanyId { get; set; }

        /// <summary>
        /// Data source of this placemarkViewModel.
        /// </summary>
        public Guid DataSourceId { get; set; }

        /// <summary>
        /// External id that can be used for get information from external system.
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        /// Title that can be used for identify placemark
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Name of the placemarkViewModel.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Type of the placemarkViewModel's geometry.
        /// </summary>
        public GeometryType GeometryType { get; set; }

        public string Description { get; set; }

        /// <summary>
        /// Original placemark information for object information menu.
        /// </summary>
        public IList<KeyValuePair<string, string>> DescriptionData { get; set; }

        /// <summary>
        /// LayerId id where this placemark is placed.
        /// </summary>
        public string LayerId { get; set; }

        /// <summary>
        /// LayerId id where this placemark is placed.
        /// </summary>
        public string LayerName { get; set; }

        /// <summary>
        /// Full id of the layer where placemark is placed (includes parent layer's ids). e.g. parentLayerid/layerId
        /// </summary>
        public string LayerFullId { get; set; }

        /// <summary>
        /// Subtype of the placemarkViewModel (influence on displaying settings).
        /// </summary>
        public string Subtype { get; set; }

        /// <summary>
        /// PlacemarkViewModel center latitude.
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// PlacemarkViewModel center latitude.
        /// </summary>
        public double Longitude { get; set; }

        /// <summary>
        /// Placemark's altitude
        /// </summary>
        public double Altitude { get; set; }

        public IObjectGeometry Geometry { get; set; }

        /// <summary>
        /// Additional properties of placemarkViewModel. They can be specified different for each placemarkViewModel object.
        /// </summary>
        public IDictionary<string, string> DynamicProperties { get; set; }

        /// <summary>
        /// Indicates if placemark is valid for displaying.
        /// </summary>
        public int ProcessingResult { get; set; }
        
        /// <summary>
        /// Set of inspection forms for placemark. Maximum - 2
        /// </summary>
        public HashSet<Guid> InspectionForms { get; set; }

        /// <summary>
        /// Set custom layer for filtering placemark.
        /// </summary>
        public Guid? SceneLayer { get; set; }

        #region Configuration data

        /// <summary>
        /// Main color of the placemark.
        /// </summary>
        public string MainColor { get; set; }

        /// <summary>
        /// Second color of the placemark.
        /// </summary>
        public string SecondColor { get; set; }

        /// <summary>
        /// Object type of current placemark.
        /// </summary>
        public ObjectType PrimaryObjectType { get; set; }

        /// <summary>
        /// Object type configuration
        /// </summary>
        [JsonConverter(typeof(ObjectTypeConfigurationConverter))]
        public ObjectTypeDataViewModel ObjectTypeData { get; set; }

        #endregion

        #region Helpers
        private const string ARCGIS_NULL_FIELD_VALUE = "&lt;Null&gt;";

        public string ValidateHeaderText()
        {
            if (!string.IsNullOrEmpty(Title) && Title != ARCGIS_NULL_FIELD_VALUE) {
                return Title;
            }

            if (!string.IsNullOrEmpty(ExternalId) && ExternalId != ARCGIS_NULL_FIELD_VALUE) {
                return ExternalId;
            }

            return Id;
        } 
        #endregion
    }
}
