﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public enum PlacementTypeViewModel : byte
    {
        AboveGround = 0,
        OnGround = 1,
        BelowGround = 2
    }
}
