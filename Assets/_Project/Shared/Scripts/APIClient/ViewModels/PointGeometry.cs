﻿namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public class PointGeometry : IObjectGeometry
    {
        public GeometryType GeometryType { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double? Altitude { get; set; }
    }
}
