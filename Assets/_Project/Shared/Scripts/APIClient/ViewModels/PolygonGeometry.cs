﻿using System.Collections.Generic;


namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public class PolygonGeometry: IObjectGeometry
    {
        public GeometryType GeometryType { get; set; }

        public IList<PointGeometry> Points { get; set; }
    }
}