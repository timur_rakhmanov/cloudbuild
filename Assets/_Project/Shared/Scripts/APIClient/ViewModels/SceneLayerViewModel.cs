﻿using System;


namespace Assets._Project.Shared.Scripts.APIClient.ViewModels
{
    public class SceneLayerViewModel
    {
        /// <summary>
        /// Unique id of the layer per system.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Readable name to identify layer.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Describe that layer should  be disabled by default
        /// </summary>
        public bool DisabledByDefault { get; set; }
    }
}
