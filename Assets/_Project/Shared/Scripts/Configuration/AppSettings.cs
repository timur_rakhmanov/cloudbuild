﻿using Assets._Project.Shared.Scripts.Core.Models;
using Newtonsoft.Json;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Configuration
{
    public class AppSettings
    {
        private const string EDITOR_CONFIG_PATH = "Configuration/editor.config";
        private const string PLATFORM_CONFIG_PATH = "Configuration/active.config";

        private static AppSettings _instance;
        public static AppSettings Instance { get { return _instance ?? Init(); } }

        public bool IsDebugLayerEnabled { get; set; }

        /// <summary>
        /// Indicates if test sign in should be used (by device id without QR-code, should be used only for test).
        /// </summary>
        public bool UseTestSingInButton { get; set; }
        public string GoogleApiKey { get; set; }
        public string BingApiKey { get; set; }
        public string BackendServerUrl { get; set; }
        public string IdentityServerUrl { get; set; }

        public string ConferenceServerAdress { get; set; }
        public int ConferenceServerPort { get; set; }

        // TODO:21696 remove default company since now we have authentication.
        public Company DefaultCompany { get; set; }
        public float RecordingMaxLenght { get; set; }

        public string LogentriesToken { get; set; }

        /// <summary>
        /// Indicates if logs should be written to local file.
        /// </summary>
        public bool FileLogEnabled { get; set; }

        /// <summary>
        /// Level of depth for tiling. 
        /// 0 - 1 sprite per tile,
        /// 1 - 4 sprites per tile
        /// 2 - 16 sprites per tile
        /// </summary>
        public int DetailedMapsLevel { get; set; }

        private static AppSettings Init()
        {
            TextAsset configAsset;
#if UNITY_EDITOR
            configAsset = Resources.Load<TextAsset>(EDITOR_CONFIG_PATH);
#else
            configAsset = Resources.Load<TextAsset>(PLATFORM_CONFIG_PATH);
#endif
            _instance = JsonConvert.DeserializeObject<AppSettings>(configAsset.text);
            return _instance;
        }
    }
}
