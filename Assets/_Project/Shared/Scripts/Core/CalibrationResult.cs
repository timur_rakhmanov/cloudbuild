﻿using UnityEngine;


namespace Assets._Project.Shared.Scripts.Core
{
    /// <summary>
    /// All information that we get after calibration.
    /// </summary>
    public class CalibrationResult
    {   
        /// <summary>
        /// Geo data that we get from calibration system.
        /// </summary>
        public GnssDataViewModel GnssData { get; set; }

        /// <summary>
        /// Users position that was used for calibration.
        /// </summary>
        public Vector3 UserPosition { get; set; }

        /// <summary>
        /// Users rotation in angles that was used for calibration.
        /// </summary>
        public Vector3 UserRotation { get; set; }

        public CalibrationResult()
        {
            GnssData = new GnssDataViewModel();
        }
    }
}