﻿using UnityEngine;


namespace Assets._Project.Shared.Scripts.Core
{
    public static class ColorConverter
    {
        public static Color HexToColor(string hexColor)
        {
            Color color;
            ColorUtility.TryParseHtmlString(hexColor, out color);

            return color;
        }
    }
}
