﻿namespace Assets._Project.Shared.Scripts.Core
{
    public static class Constants
    {
        public static class GlobalVoiceCommands
        {
            public const string CLOSE = "close";
            public const string OPEN_CONSOLE = "open console";
            public const string CLOSE_CONSOLE = "close console";
            public const string OPEN_TELEPORT = "open teleport";
        }

        public const float USER_HEIGHT = 1.7f;

        /// <summary>
        /// All dynamic elements should be attached to this placeholder.
        /// </summary>
        public const string ROOT = "DynamicObjects";

        /// <summary>
        ///     Main menu
        /// </summary>
        public const string MAIN_MENU = "MainMenu";

        /// <summary>
        /// Placemarks should be part of this placeholder.
        /// </summary>
        public const string PLACEMARKS_ROOT = "Placemarks";

        public const string WORLD_DIRECTIONS = "StaticObjects/WorldDirections";

        public const int TOOLTIP_DELAY = 1;
        /// <summary>
        /// Zoom in the google scale (0 - 20)
        /// </summary>
        public const int DEFAULT_ZOOM = 19;

        /// <summary>
        /// Device id that is used for sign in as super device in editor.
        /// </summary>
        public const string EDITOR_DEVICE_ID = "eeeeeeee-eeee-eeee-eeee-eeeeeeeeeeee";

        /// <summary>
        /// Device id that is used for sign in as super device in hololens emulator
        /// </summary>
        public const string HOLOLENS_EMULATOR_DEVICE_ID = "mmmmmmmm-mmmm-mmmm-mmmm-mmmmmmmmmmmm";

        /// <summary>
        /// Hololens emulator name for sign in by device id
        /// </summary>
        public const string HOLOLENS_EMULATOR_NAME = "Emulator-vGIS";

        /// <summary>
        /// Highlight shader name
        /// </summary>
        public const string HIGHLIGHT_SHADER_NAME = "HoloToolkit/SpecularHighlight";

        /// <summary>
        /// FBX file extension
        /// </summary>
        public const string FBX_FILE_EXTENSION = ".fbx";

        public const string MAP_SURFACE_TAG = "MapSurface";

        public static class TelemetryEvents
        {
            public const string APP_START = "app_started";
            public const string APP_END = "app_ended";
            public const string APP_SUSPEND = "app_suspended";
            public const string APP_RESUME = "app_resumed";
            public const string CALIBRATION_START = "calibration_started";
            public const string CALIBRATION_END = "calibration_ended";
            public const string CALIBRATION_STATE_CHANGED = "calibration_state_changed";
            public const string CALIBRATION_ZOOM_CHANGED = "calibration_zoom_changed";
            public const string CALIBRATION_CONFIRM_POSITION = "calibration_map_position_confirmed";
            public const string CALIBRATION_CONFIRM_DIRECTION = "calibration_map_direction_confirmed";
            public const string NOTE_CREATION = "note_created";
            public const string FILE_UPLOADING = "file_uploaded";

            public const string USER_LOCATION = "user_location";
        }
    }
}