﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Configuration;
using Assets._Project.Shared.Scripts.Core.Models;
using Assets._Project.Shared.Scripts.Identity;
using UnityEngine;
using DeviceType = Assets._Project.Shared.Scripts.Identity.DeviceType;


namespace Assets._Project.Shared.Scripts.Core
{
    public class ExecutionContext
    {
        private ExecutionContext()
        {
            SessionId = Guid.NewGuid();
            DeviceName = GetDeviceName();
            DeviceId = GetDeviceId();
            IdentityData = new IdentityViewModel();
            DeviceCompanies = new List<CompanyViewModel>();
        }

        public Guid SessionId { get; private set; }

        /// <summary>
        /// Detects device type.
        /// </summary>
        /// <returns></returns>
        public DeviceType DeviceyType
        {
            get
            {
#if UNITY_EDITOR
                return DeviceType.Editor;
#elif UNITY_WSA
            return DeviceType.Hololens;
#elif UNITY_IOS
            return DeviceType.Ios;
#elif UNITY_ANDROID
            return DeviceType.Android;
#else
            throw new NotImplementedForPlatformException("Can't detect device type.");
#endif
            }
        }

        public string DeviceId { get; private set; }

        public string DeviceName { get; private set; }

        private Company _company;

        public Company Company
        {
            get
            {
                if (_company != null) {
                    return _company;
                }

                Debug.Log("Defualt company is used. Avoid this and remove.");
                return AppSettings.Instance.DefaultCompany;
            }
        }

        public CompanyViewModel DeviceCurrentCompany
        {
            get { return DeviceCompanies.Single(x => x.Id == Company.Id); }
        }

        public IdentityViewModel IdentityData { get; private set; }
        public List<CompanyViewModel> DeviceCompanies { get; private set; }

        private static ExecutionContext _instance;

        public static ExecutionContext Instance
        {
            get { return _instance ?? (_instance = new ExecutionContext()); }
        }

        public void UpdateCompany(Company company)
        {
            if (company == null) throw new ArgumentNullException("company");
            _company = company;
        }

        public void UpdateIdentityData(IdentityViewModel identityViewModel)
        {
            if (identityViewModel == null) {
                throw new ArgumentNullException("identityViewModel");
            }
            IdentityData = identityViewModel;
        }

        public void SaveDeviceCompanies(List<CompanyViewModel> deviceCompanies)
        {
            if (deviceCompanies == null) {
                throw new ArgumentNullException("deviceCompanies");
            }

            DeviceCompanies = deviceCompanies;
        }

        private string GetDeviceId()
        {
#if NETFX_CORE
            if (GetDeviceName().Trim().ToLower().Equals(Constants.HOLOLENS_EMULATOR_NAME.Trim().ToLower())
                && AppSettings.Instance.UseTestSingInButton) {
                Debug.Log("deviceName: " + GetDeviceName());
                Debug.Log("HololensEmulatorDeviceId: " + Constants.HOLOLENS_EMULATOR_DEVICE_ID);
                return Constants.HOLOLENS_EMULATOR_DEVICE_ID;
            }
            
            // NOTE: we used Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation but it isn't uniqeuy for 2 devices
            // for some reason TODO: add task link here.
            // Use https://docs.microsoft.com/en-us/uwp/api/Windows.System.Profile.SystemIdentification for get unique id of the device (for specified 
            // publisher).
            var systemIdentificationInfo = Windows.System.Profile.SystemIdentification.GetSystemIdForPublisher();
            var dataReader = Windows.Storage.Streams.DataReader.FromBuffer(systemIdentificationInfo.Id);
            // NOTE: we can't just read string, since get unicode error https://stackoverflow.com/questions/18206590/winrt-no-mapping-for-the-unicode-character-exists-in-the-target-multi-byte-code
            byte[] fileContent = new byte[dataReader.UnconsumedBufferLength];
            dataReader.ReadBytes(fileContent);
            var id = Convert.ToBase64String(fileContent);
            var source = systemIdentificationInfo.Source.ToString();
            Debug.LogFormat("Device id '{0}' detected from source '{1}'", id, source);
            return id;
#elif UNITY_EDITOR
            return Constants.EDITOR_DEVICE_ID;
#elif UNITY_IOS
            return SystemInfo.deviceUniqueIdentifier;
#endif
        }

        private string GetDeviceName()
        {
#if NETFX_CORE
            var deviceInfo = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();
            return deviceInfo.FriendlyName.ToString();
#elif UNITY_EDITOR
            return "UnityEditorDeviceName";
#elif UNITY_IOS
            return SystemInfo.deviceName;
#endif
        }
    }
}