﻿using System.Collections.Generic;
using System.Linq;


namespace Assets._Project.Shared.Scripts.Core.Extensions
{
    public static class EnumerableExtensions
    {
        /// <summary>
        /// Butch elements based on chunk size.
        /// Code is taken here http://stackoverflow.com/a/13731823/509674 
        /// </summary>
        // NOTE: this code is from https://github.com/morelinq/MoreLINQ/blob/master/MoreLinq/Batch.cs ,
        // I just didn't want to connect full library for one function, need to connect library if later we will use more functions from it.
        public static IEnumerable<IEnumerable<TSource>> Batch<TSource>(
                  this IEnumerable<TSource> source, int size)
        {
            TSource[] bucket = null;
            var count = 0;

            foreach (var item in source) {
                if (bucket == null)
                    bucket = new TSource[size];

                bucket[count++] = item;
                if (count != size)
                    continue;

                yield return bucket;

                bucket = null;
                count = 0;
            }

            if (bucket != null && count > 0)
                yield return bucket.Take(count);
        }
    }
}
