﻿using System;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Core.Extensions
{
    public static class GameObjectExtensions
    {
        /// <summary>
        /// Attach transform to the Root (makes it child of the root).
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static GameObject AddToRoot(this GameObject instance)
        {
            var root = GameObject.Find(Constants.ROOT);
            if (root == null) {
                throw new Exception("Can't find root of the scene.");
            }

            instance.transform.SetParent(root.transform);

            return instance;
        }
    }
}
