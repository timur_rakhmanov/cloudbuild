﻿using UnityEngine;


namespace Assets._Project.Shared.Scripts.Core.Extensions
{
    public static class VectorExtensions
    {
        public static Vector3 XyToXz(this Vector2 original, float y = 0)
        {
            return new Vector3(original.x, y, original.y);
        }

        public static Vector3 SkipY(this Vector3 original)
        {
            return new Vector3(original.x, 0, original.z);
        }

        public static Vector2d.Vector2d ToVector2d(this Vector2 original)
        {
            return new Vector2d.Vector2d(original.x, original.y);
        }

        public static Vector2d.Vector2d ToVector2d(this Vector3 original)
        {
            return new Vector2d.Vector2d(original.x, original.z);
        }

        public static Vector2 ToVector2(this Vector2d.Vector2d original)
        {
            return new Vector2(original.x.ToFloat(), original.y.ToFloat());
        }

        public static Vector2 ToVector2Xz(this Vector3 original)
        {
            return new Vector2(original.x, original.z);
        }

        public static Vector4 ToVector4(this Vector3 original)
        {
            return new Vector4(original.x, original.y, original.z, 0);
        }

        public static bool ContainsXZ(this Bounds bounds, Vector3 vector)
        {
            return vector.x >= bounds.min.x && vector.x <= bounds.max.x &&
                   vector.z >= bounds.min.z && vector.z <= bounds.max.z;
        }
    }
}