﻿using System;


namespace Assets._Project.Shared.Scripts.Core.Fsm
{
    /// <summary>
    /// Base calss for events and it's data.
    /// </summary>
    public class EventContext<TEvent, TFsm> where TEvent: struct, IConvertible
    {
        /// <summary>
        /// Event.
        /// </summary>
        public TEvent Event { get; private set; }

        public EventContext(TEvent @event)
        {
            Event = @event;
        }

        /// <summary>
        /// Initialize fsm by data from context.
        /// </summary>
        /// <param name="fsm"></param>
        public virtual void SetFsmData(TFsm fsm)
        {
        }
    }
}