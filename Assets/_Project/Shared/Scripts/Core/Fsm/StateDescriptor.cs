﻿using System;
using System.Collections;


namespace Assets._Project.Shared.Scripts.Core.Fsm
{
    /// <summary>
    /// Describe states entry exit actions.
    /// </summary>
    public class StateDescriptor<TState, TFsm> where TState: struct, IConvertible
    {
        public TState State { get; private set; }

        private Action<TFsm> _enter;
        private Func<TFsm, IEnumerator> _enterCoroutine;
        private Action<TFsm> _exit;
        private Func<TFsm, IEnumerator> _exitCoroutine;
        private TState? _parentState;

        public StateDescriptor(TState state)
        {
            State = state;
        }

        public IEnumerator RunEnter(TFsm targetFsm)
        {
            if (_enter != null) {
                _enter(targetFsm);
                return null;
            }

            if (_enterCoroutine != null) {
                return _enterCoroutine(targetFsm);
            }

            return null;
        }

        public IEnumerator RunExit(TFsm targetFsm)
        {
            if (_exit != null) {
                _exit(targetFsm);
                return null;
            }

            if (_exitCoroutine != null) {
                return _exitCoroutine(targetFsm);
            }

            return null;
        }

        public bool HasParent()
        {
            return _parentState.HasValue;
        }

        public TState GetParent()
        {
            if (!_parentState.HasValue) {
                throw new Exception(string.Format("State {0} doesn't have parent.", State));
            }
            return _parentState.Value;
        }

        /// <summary>
        /// Add enter coroutine to the state.
        /// </summary>
        /// <param name="enterCoroutine"></param>
        /// <returns></returns>
        public StateDescriptor<TState, TFsm> Enter(Func<TFsm, IEnumerator> enterCoroutine)
        {
            if (enterCoroutine == null) {
                throw new ArgumentNullException("enterCoroutine");
            }

            if (_enterCoroutine != null || _enter != null) {
                throw new Exception("Enter action or coroutine already set.");
            }

            _enterCoroutine = enterCoroutine;

            return this;
        }

        /// <summary>
        /// Add enter action to the state.
        /// </summary>
        /// <param name="enterAction"></param>
        /// <returns></returns>
        public StateDescriptor<TState, TFsm> Enter(Action<TFsm> enterAction)
        {
            if (enterAction == null) {
                throw new ArgumentNullException("enterAction");
            }

            if (_enterCoroutine != null || _enter != null) {
                throw new Exception("Enter action or coroutine already set.");
            }

            _enter = enterAction;

            return this;
        }

        /// <summary>
        /// Add enter coroutine to the state.
        /// </summary>
        /// <param name="exitCoroutine"></param>
        /// <returns></returns>
        public StateDescriptor<TState, TFsm> Exit(Func<TFsm, IEnumerator> exitCoroutine)
        {
            if (exitCoroutine == null) {
                throw new ArgumentNullException("exitCoroutine");
            }

            if (_exitCoroutine != null || _exit != null) {
                throw new Exception("Exit action or coroutine already set.");
            }

            _exitCoroutine = exitCoroutine;

            return this;
        }

        /// <summary>
        /// Add enter action to the state.
        /// </summary>
        /// <param name="exitCoroutine"></param>
        /// <returns></returns>
        public StateDescriptor<TState, TFsm> Exit(Action<TFsm> exitAction)
        {
            if (exitAction == null) {
                throw new ArgumentNullException("exitAction");
            }

            if (_exitCoroutine != null || _exit != null) {
                throw new Exception("Exit action or coroutine already set.");
            }

            _exit = exitAction;

            return this;
        }

        /// <summary>
        /// Set parent state for the current state.
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public StateDescriptor<TState, TFsm> Parent(TState state)
        {
            if (_parentState.HasValue) {
                throw new Exception("Parent state already set.");
            }

            _parentState = state;

            return this;
        }
    }
}