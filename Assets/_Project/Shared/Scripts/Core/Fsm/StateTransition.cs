﻿using System;


namespace Assets._Project.Shared.Scripts.Core.Fsm
{
    /// <summary>
    /// Describe transitions and actions for state.
    /// </summary>
    public class StateTransition<TEvent, TState, TFsm>
        where TEvent : struct, IConvertible
        where TState : struct, IConvertible
    {
        public TState? From { get; private set; }
        public TEvent Event { get; private set; }
        public TState? To { get; private set; }
        public Action<TFsm> Action { get; private set; }

        public StateTransition(TState? from, TEvent @event, TState? to, Action<TFsm> action)
        {
            From = from;
            Event = @event;
            To = to;
            Action = action;
        }
    }
}