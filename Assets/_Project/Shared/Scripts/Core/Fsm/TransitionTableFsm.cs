﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets._Project.Shared.Scripts.Core.Tasks;
using UnityEngine;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;

namespace Assets._Project.Shared.Scripts.Core.Fsm
{
    /// <summary>
    /// Transition table finite state machine implementation.
    /// </summary>
    public class TransitionTableFsm<TEvent, TState, TFsm> 
        where TEvent: struct, IConvertible 
        where TState: struct, IConvertible
    {
        // TODO: support few actions per event.
        // TODO: support parameters moving.
        // TODO: support end state (exit).
        private const float EVENTS_DELAY = 0.1f;
        private TState _currentState;
        /// <summary>
        /// Object that is controlled by fsm.
        /// </summary>
        private TFsm _fsmTarget;

        private static ILog _log;

        private Queue<EventContext<TEvent, TFsm>> _eventsQueue = new Queue<EventContext<TEvent, TFsm>>();
        private List<StateDescriptor<TState, TFsm>>  _stateDescriptors = new List<StateDescriptor<TState, TFsm>>();
        private List<StateTransition<TEvent, TState, TFsm>> _transitionTable = new List<StateTransition<TEvent, TState, TFsm>>();
        /// <summary>
        /// FSM don't throw exception if state doesn't handle this events (just left in the same state).
        /// </summary>
        private List<TEvent> _silentEvents = new List<TEvent>(); 

        /// <summary>
        /// Global actions that can be used if state don't handle event directly.
        /// </summary>
        private List<StateTransition<TEvent, TState, TFsm>> _globalActions =  new List<StateTransition<TEvent, TState, TFsm>>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fsmTarget">Object that is controlled by fsm.</param>
        /// <param name="initialState">Initial state of fsm.</param>
        /// <param name="logPrefix">Prefix that is used before log message.</param>
        public TransitionTableFsm(TFsm fsmTarget, TState initialState, string logPrefix = "")
        {
            _fsmTarget = fsmTarget;
            _currentState = initialState;
            _log = LogManager.GetLogger(logPrefix);
        }

        /// <summary>
        /// Adds new state to fsm.
        /// </summary>
        /// <param name="state">State.</param>
        /// <returns>Added state.</returns>
        public StateDescriptor<TState, TFsm> CreateState(TState state)
        {
            if (_stateDescriptors.Any(d => d.State.Equals(state))) {
                throw new Exception(string.Format("State {0} already created.", state));
            }

            var stateDescriptor = new StateDescriptor<TState, TFsm>(state);
            _stateDescriptors.Add(stateDescriptor);
            return stateDescriptor;
        }

        /// <summary>
        /// Adds new transition for fsm.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="event"></param>
        /// <param name="to"></param>
        /// <param name="action"></param>
        public void AddTransition(TState? from, TEvent @event, TState? to, Action<TFsm> action)
        {
            _transitionTable.Add(new StateTransition<TEvent, TState, TFsm>(from, @event, to, action));
        }

        /// <summary>
        /// Adds event that shouldn't occur errors if state doesn't support them.
        /// </summary>
        public void AddSilentEvent(TEvent @event)
        {
            _silentEvents.Add(@event);
        }

        // TODO: consider to use it as default action.
        public void AddGlobalAction(TEvent @event, Action<TFsm> action)
        {
            _globalActions.Add(new StateTransition<TEvent, TState, TFsm>(null, @event, null, action));
        }

        /// <summary>
        /// Send event to fsm.
        /// </summary>
        /// <param name="eventContext">Event and it's context with data.</param>
        public void TriggerEvent(EventContext<TEvent, TFsm> eventContext)
        {
            _eventsQueue.Enqueue(eventContext);
        }

        /// <summary>
        /// Sends event to fsm.
        /// </summary>
        public void TriggerEvent(TEvent @event)
        {
            _eventsQueue.Enqueue(new EventContext<TEvent, TFsm>(@event));
        }

        /// <summary>
        /// Starts fsm.
        /// Fsm works until coroutine is stopped.
        /// </summary>
        public IEnumerator StartFsm()
        {
            // Skip current frame in order to avoid locks and race conditions (in case if is called in start).
            yield return null;

            // Run entry to initial state if it is set.
            yield return RunStartStateEntry();

            // For now fsm works until coroutine doesn't stop by caller.
            while (true) {
                if (_eventsQueue.Count > 0) {
                    _log.LogInfo("Handle event");
                    IEnumerator enumerator = null;
                    // TODO: consider how to catch exception before and after yield without adding additional try catch.
                    try {
                        enumerator = HandleEvent(_eventsQueue.Dequeue());
                    } catch (Exception ex) {
                        _log.LogException(ex);
                    }

                    if (enumerator != null) {
                        var handleEventTask = enumerator.AsTask();
                        yield return handleEventTask.Run();
                        if (handleEventTask.IsFaulted) {
                            _log.LogException(handleEventTask.Exception);
                        }
                    }
                } else {
                    // Don't check events too often.
                    yield return new WaitForSeconds(EVENTS_DELAY);
                }
            }
        }

        /// <summary>
        /// Returns true if fsm in the specified state.
        /// </summary>
        /// <param name="state">State that is expected.</param>
        /// <returns></returns>
        public bool IsInState(TState state)
        {
            return _currentState.Equals(state);
        }

        /// <summary>
        /// Returns current state.
        /// </summary>
        /// <returns></returns>
        public TState GetCurrentState()
        {
            return _currentState;
        }

        /// <summary>
        /// Handle event.
        /// </summary>
        /// <param name="eventContext"></param>
        private IEnumerator HandleEvent(EventContext<TEvent, TFsm> eventContext)
        {
            eventContext.SetFsmData(_fsmTarget);
            var task = HandleEvent(eventContext.Event).AsTask();
            yield return task.Run();
            task.Wait();
        }

        /// <summary>
        /// Handle event by current state and make transition according to the transition table.
        /// Any exceptions in actions are ignored and don't stop transition.
        /// </summary>
        // TODO: add unittests.
        private IEnumerator HandleEvent(TEvent @event)
        {
            // Check if current state can handle event.
            var stateTransition = _transitionTable.SingleOrDefault(ts => ts.From.Equals(_currentState) && ts.Event.Equals(@event)) 
                ?? _globalActions.SingleOrDefault(st => st.Event.Equals(@event));

            if (stateTransition == null) {
                if (_silentEvents.Contains(@event)) {
                    yield break;
                }
                throw new Exception(string.Format("FSM. State {0} can't handle event {1}.", _currentState, @event));
            }

            _log.LogInfo(string.Format("FSM. Handle event {0} in state {1}.", @event, _currentState));
            // Make transition if it exists.
            if (stateTransition.To != null) {
                var currentStateDescriptor = GetStateDescriptor(_currentState);
                var destinationState = stateTransition.To.Value;
                var destinationStateDescriptor = GetStateDescriptor(destinationState);

                bool isTransitionFromParentToChild = destinationStateDescriptor.HasParent() && 
                    destinationStateDescriptor.GetParent().Equals(_currentState);

                if (!isTransitionFromParentToChild) {
                    _log.LogInfo(string.Format("FSM. Exit from state {0}.", _currentState));
                    yield return RunAction(currentStateDescriptor.RunExit);
                }

                // Exit from parent state if to state isn't parent.
                bool isTransitionFromChildrenToParent = currentStateDescriptor.HasParent() && 
                    currentStateDescriptor.GetParent().Equals(destinationState);
                if (currentStateDescriptor.HasParent() && !isTransitionFromChildrenToParent) {
                    _log.LogInfo(string.Format("FSM. Exit from parent state {0}", currentStateDescriptor.GetParent()));
                    var parentStateDescriptor = GetStateDescriptor(currentStateDescriptor.GetParent());
                    yield return RunAction(parentStateDescriptor.RunExit);
                }

                // Enter to parent first if we don't make transition from parent.
                if (destinationStateDescriptor.HasParent() && !isTransitionFromParentToChild) {
                    var destinationParentStateDescription = GetStateDescriptor(destinationStateDescriptor.GetParent());
                    _log.LogInfo(string.Format("FSM. Enter to parent state {0}.", destinationStateDescriptor.GetParent()));
                    yield return RunAction(destinationParentStateDescription.RunEnter);
                }

                _log.LogInfo(string.Format("FSM. Transit to state {0}.", destinationState));
                _currentState = destinationState;

                if (!isTransitionFromChildrenToParent) {
                    _log.LogInfo(string.Format("FSM. Enter to state {0}.", destinationState));
                    yield return RunAction(destinationStateDescriptor.RunEnter);
                }
            }

            // Call action. 
            // TODO: consider to support IEnumerator actions.
            if (stateTransition.Action != null) {
                _log.LogInfo(string.Format("FSM. Call action in state {0}.", _currentState));
                try {
                    stateTransition.Action(_fsmTarget);
                } catch (Exception ex) {
                    _log.LogInfo(string.Format("FSM. Call action in state {0} failed.", _currentState));
                    _log.LogException(ex);
                }
            }
        }

        private IEnumerator RunAction(Func<TFsm, IEnumerator> action) 
        {
            // Just log exception (both on call and continuation) and continue transition.
            // TODO: check with unittests if we need to handle exception at the start.

            IEnumerator enumerator = null;
            try {
                // Don't looks very nice, need to improve.
                enumerator = action(_fsmTarget);
            } catch (Exception ex) {
                _log.LogException(ex);
            }
            if (enumerator != null) {
                var exitTask = enumerator.AsTask();
                yield return exitTask.Run();
                if (exitTask.IsFaulted) {
                    _log.LogException(exitTask.Exception);
                }
            }
        }

        /// <summary>
        /// Run entry action of the initial state of fsm during fsm start.
        /// </summary>
        /// <returns></returns>
        private IEnumerator RunStartStateEntry()
        {
            // TODO: handle case when start state, e.g. _currentState is inner state.
            var startStateDescriptor = GetStateDescriptor(_currentState);
            yield return RunAction(startStateDescriptor.RunEnter);
        }

        private StateDescriptor<TState, TFsm> GetStateDescriptor(TState state)
        {
            return _stateDescriptors.Single(s => s.State.Equals(state));
        }
    }
}