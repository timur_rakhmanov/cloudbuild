﻿namespace Assets._Project.Shared.Scripts.Core
{
    public class GeoBoundingBox
    {
        public GeoCoordinate Min { get; set; }
        public GeoCoordinate Max { get; set; }
    }
}