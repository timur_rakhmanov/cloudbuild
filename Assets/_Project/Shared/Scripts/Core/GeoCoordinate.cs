﻿using System;


namespace Assets._Project.Shared.Scripts.Core
{
    /// <summary>
    /// Represents geo coordinate.
    /// </summary>
    public struct GeoCoordinate
    {
        /// <summary>
        /// Precision that is used for equality, is selected according to this post http://gis.stackexchange.com/questions/8650/measuring-accuracy-of-latitude-and-longitude
        /// </summary>
        private const double COORDINATE_DELTA_PER_METER = 0.00001;
        /// <summary>
        /// Default precision is point within 1 sm.
        /// </summary>
        private const double DEFAULT_PRECISION = 0.01 * COORDINATE_DELTA_PER_METER;

        private readonly double _latitude;
        private readonly double _longitude;
        private readonly double _altitude;

        public double Latitude { get { return _latitude; } }
        public double Longitude { get { return _longitude; } }

        public double Altitude { get { return _altitude;} }

        public GeoCoordinate(double latitude, double longitude, double altitude = 0)
        {
            _latitude = latitude;
            _longitude = longitude;
            _altitude = altitude;
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2}", Latitude, Longitude, Altitude);
        }

        public override bool Equals(object other)
        {
            return other is GeoCoordinate && EqualsWithPrecision((GeoCoordinate)other, DEFAULT_PRECISION);
        }

        /// <summary>
        /// Compares two coordinates with give precision
        /// </summary>
        /// <param name="other">Other object for comparison.</param>
        /// <param name="precision">Precision in meters.</param>
        /// <returns></returns>
        public bool Equals(GeoCoordinate other, double precision)
        {
            return EqualsWithPrecision(other, precision * COORDINATE_DELTA_PER_METER);
        }

        private bool EqualsWithPrecision(GeoCoordinate other, double precision)
        {
            // NOTE: points are equal if they are placed within specified square, will
            // be more precise to calculate distance, but current method is enough for us.
            return Math.Abs(Latitude - other.Latitude) < precision 
                && Math.Abs(Longitude - other.Longitude) < precision
                && Math.Abs(Altitude - other.Altitude) < precision;
        }

        public override int GetHashCode()
        {
            return Latitude.GetHashCode() ^ Longitude.GetHashCode();
        }
    }
}
