﻿using System;
using System.Diagnostics;


namespace Assets._Project.Shared.Scripts.Core.GeoHelpers
{
    public class MercatorProjection
    {
        private const double TOLERANCE = 0.001;
        private const double DEFAULT_PROJECTION_WIDTH = 256;
        private const double DEFAULT_PROJECTION_HEIGHT = 256;

        private readonly double _centerLatitude;
        private readonly double _centerLongitude;
        private readonly int _areaWidthPx;
        private readonly int _areaHeightPx;
        // the scale that we would need for the a projection to fit the given area into a world view (1 = global, expect it to be > 1)
        private readonly double _areaScale;

        private readonly double _projectionWidth;
        private readonly double _projectionHeight;
        private readonly double _pixelsPerLonDegree;
        private readonly double _pixelsPerLonRadian;

        private readonly double _projectionCenterPx;
        private readonly double _projectionCenterPy;
        public MercatorProjection() { }

        public MercatorProjection(double centerLatitude,
            double centerLongitude,
            int areaWidthPx,
            int areaHeightPx,
            double areaScale)
        {
            this._centerLatitude = centerLatitude;
            this._centerLongitude = centerLongitude;
            this._areaWidthPx = areaWidthPx;
            this._areaHeightPx = areaHeightPx;
            this._areaScale = Math.Pow(2, areaScale);

            // TODO stretch the projection to match to deformity at the center lat/lon?
            this._projectionWidth = DEFAULT_PROJECTION_WIDTH;
            this._projectionHeight = DEFAULT_PROJECTION_HEIGHT;
            this._pixelsPerLonDegree = this._projectionWidth / 360;
            this._pixelsPerLonRadian = this._projectionWidth / (2 * Math.PI);

            var centerPoint = ProjectLocation(this._centerLatitude, this._centerLongitude);
            this._projectionCenterPx = centerPoint.X; //* this._areaScale;
            this._projectionCenterPy = centerPoint.Y; //* this._areaScale;
        }

        public MercatorProjection (
            double centerLatitude,
            double centerLongitude,
            int areaWidthPx,
            int areaHeightPx,
            double defaultProjectionHeight,
            double defaultProjectionWidth,
            double areaScale
        )
        {
            this._centerLatitude = centerLatitude;
            this._centerLongitude = centerLongitude;
            this._areaWidthPx = areaWidthPx;
            this._areaHeightPx = areaHeightPx;
            this._areaScale = Math.Pow(2, areaScale);

            // TODO stretch the projection to match to deformity at the center lat/lon?
            this._projectionHeight = defaultProjectionHeight;
            this._projectionWidth = defaultProjectionWidth;
            this._pixelsPerLonDegree = this._projectionWidth / 360;
            this._pixelsPerLonRadian = this._projectionWidth / (2 * Math.PI);

            var centerPoint = ProjectLocation(this._centerLatitude, this._centerLongitude);
            this._projectionCenterPx = centerPoint.X; //* this._areaScale;
            this._projectionCenterPy = centerPoint.Y; //* this._areaScale;
        }

        public GeoCoordinate GetLocation(double px, double py)
        {
            var x = this._projectionCenterPx + (px - this._areaWidthPx / 2) / this._areaScale;
            var y = this._projectionCenterPy + (py - this._areaHeightPx / 2) / this._areaScale;

            return ProjectPx(x,y);
        }

        public PointF GetPoint(double latitude, double longitude)
        {
            var point = ProjectLocation(latitude, longitude);
     

            var x = (point.X - this._projectionCenterPx) * this._areaScale + _areaWidthPx / 2;
            var y = (point.Y - this._projectionCenterPy) * this._areaScale + _areaHeightPx / 2;

            return new PointF(x, y);
        }

        // from http://stackoverflow.com/questions/12507274/how-to-get-bounds-of-a-google-static-map

        private GeoCoordinate ProjectPx(double px, double py)
        {
            var longitude = (px - this._projectionWidth / 2) / this._pixelsPerLonDegree;
            var latitudeRadians = (py - this._projectionHeight / 2) / -this._pixelsPerLonRadian;
            var latitude = Rad2Deg(2 * Math.Atan(Math.Exp(latitudeRadians)) - Math.PI / 2);
            return new GeoCoordinate(latitude, longitude);
        }

        private PointF ProjectLocation(double latitude, double longitude)
        {
            var px = this._projectionWidth / 2 + longitude * this._pixelsPerLonDegree;
            var siny = Math.Sin(Deg2Rad(latitude));
            var py = this._projectionHeight / 2 + 0.5 * Math.Log((1 + siny) / (1 - siny)) * -this._pixelsPerLonRadian;
            var result = new PointF(px, py);
            return result;
        }

        private double Rad2Deg(double rad)
        {
            return (rad * 180) / Math.PI;
        }

        private double Deg2Rad(double deg)
        {
            return (deg * Math.PI) / 180;
        }

        public void testExample()
        {

            // tests against values in http://stackoverflow.com/questions/10442066/getting-lon-lat-from-pixel-coords-in-google-static-map

            double centerLatitude = 47;
            double centerLongitude = 1.5;

            int areaWidth = 480;
            int areaHeight = 480;

            // google (static) maps zoom level
            int zoom = 5;

            MercatorProjection projection = new MercatorProjection(
                    centerLatitude,
                    centerLongitude,
                    areaWidth,
                    areaHeight,
                    Math.Pow(2, zoom)
            );

            PointF centerPoint = projection.ProjectLocation(centerLatitude, centerLongitude);
            Debug.WriteLine(Math.Abs(129.06666666666666 - centerPoint.X) < TOLERANCE);
            Debug.WriteLine(Math.Abs(90.04191318303863 - centerPoint.Y) < TOLERANCE);

            GeoCoordinate topLeftByProjection = projection.ProjectPx(127.1875, 82.53125);
             Debug.WriteLine(53.72271667491848 == topLeftByProjection.Latitude);
             Debug.WriteLine(-1.142578125 == topLeftByProjection.Longitude);

            // NOTE sample has some pretty serious rounding errors
            GeoCoordinate topLeftByPixel = projection.GetLocation(0, 0);
             Debug.WriteLine(Math.Abs(53.72271667491848 - topLeftByPixel.Latitude) < 0.05);
            // the math for this is wrong in the sample (see comments)
             Debug.WriteLine(Math.Abs(-9 - topLeftByPixel.Longitude) < 0.05);

            PointF reverseTopLeftBase = projection.ProjectLocation(topLeftByPixel.Latitude, topLeftByPixel.Longitude);
             Debug.WriteLine(Math.Abs(121.5625 - reverseTopLeftBase.X) < 0.1);
             Debug.WriteLine(Math.Abs(82.53125 - reverseTopLeftBase.Y) < 0.1);

            PointF reverseTopLeft = projection.GetPoint(topLeftByPixel.Latitude, topLeftByPixel.Longitude);
             Debug.WriteLine(0 == reverseTopLeft.X);
             Debug.WriteLine(0 == reverseTopLeft.Y);

            GeoCoordinate bottomRightLocation = projection.GetLocation(areaWidth, areaHeight);
            PointF bottomRight = projection.GetPoint(bottomRightLocation.Latitude, bottomRightLocation.Longitude);
             Debug.WriteLine(areaWidth == bottomRight.X);
             Debug.WriteLine(areaHeight == bottomRight.Y);
        }
    }
}
