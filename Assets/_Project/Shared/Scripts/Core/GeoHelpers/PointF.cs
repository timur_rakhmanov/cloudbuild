﻿namespace Assets._Project.Shared.Scripts.Core.GeoHelpers
{
    public struct PointF {
        private readonly double x;
        private readonly double y;

        public double X { get { return x; } }
        public double Y { get { return y; } }

        public PointF(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return string.Format("{0},{1}", X, Y);
        }

        public override bool Equals(object other)
        {
            return other is PointF && Equals((PointF)other);
        }

        public bool Equals(PointF other)
        {
            return X == other.X && Y == other.Y;
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }
    }
}
