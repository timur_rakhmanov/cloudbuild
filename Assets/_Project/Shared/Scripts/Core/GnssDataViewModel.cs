﻿using System;


namespace Assets._Project.Shared.Scripts.Core
{
    // TODO: Check if it fits NMEA specs
    [Serializable]
    public class GnssDataViewModel
    {
        public GeoCoordinate Location { get; set; }
        public double Azimuth { get; set; }
        public double Accuracy { get; set; }

        private bool Equals(GnssDataViewModel otherGnssDataViewModel)
        {
            return this.Location.Equals(otherGnssDataViewModel.Location) && this.Azimuth.Equals(otherGnssDataViewModel.Azimuth);
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) {
                return false;
            }
            return Equals((GnssDataViewModel)obj);
        }

        public override int GetHashCode()
        {
            unchecked {
                int hashCode = Location.GetHashCode();
                // Why 397? Because! http://stackoverflow.com/questions/102742/why-is-397-used-for-resharper-gethashcode-override
                hashCode = (hashCode * 397) ^ Azimuth.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return string.Format("Lat: {0}; Lng: {1}; Alt: {2}; Az: {3}", Location.Latitude, Location.Longitude, Location.Altitude, Azimuth);
        }
    }
}
