﻿using System;


namespace Assets._Project.Shared.Scripts.Core
{
    // Some helpers for converting GPS readings from the WGS84 geodetic system to a local North-East-Up cartesian axis.

    // The implementation here is according to the paper:
    // "Conversion of Geodetic coordinates to the Local Tangent Plane" Version 2.01.
    // "The basic reference for this paper is J.Farrell & M.Barth 'The Global Positioning System & Inertial Navigation'"
    // Also helpful is Wikipedia: http://en.wikipedia.org/wiki/Geodetic_datum
    // Code is taken here https://gist.github.com/govert/1b373696c9a27ff4c72a
    // TODO: improve code style.
    public static class GpsUtils
    {
        // WGS-84 geodetic constants
        private const double A = 6378137;           // WGS-84 Earth semimajor axis (m)
        private const double B = 6356752.3142;      // WGS-84 Earth semiminor axis (m)
        private const double F = (A - B) / A;           // Ellipsoid Flatness
        private const double E_SQ = F * (2 - F);    // Square of Eccentricity
        private const double DEGREES_TO_RADIANS = Math.PI / 180.0;
        private const double RADIANS_TO_DEGREES = 180.0 / Math.PI;
        private const double EARTH_RADIUS = 6378137.0;

        // Converts the geodetic WGS-84 coordinated (lat, lon, h) to 
        // East-North-Up coordinates in A Local Tangent Plane that is centered at the 
        // (WGS-84) Geodetic point (lat0, lon0, h0).
        public static void GeodeticToEnu(double lat, double lon, double h,
            double lat0, double lon0, double h0,
            out double xEast, out double yNorth, out double zUp)
        {
            double x, y, z;
            GeodeticToEcef(lat, lon, h, out x, out y, out z);
            EcefToEnu(x, y, z, lat0, lon0, h0, out xEast, out yNorth, out zUp);
        }

        // Converts WGS-84 Geodetic point (lat, lon, h) to the 
        // Earth-Centered Earth-Fixed (ECEF) coordinates (x, y, z).
        private static void GeodeticToEcef(double lat, double lon, double h,
            out double x, out double y, out double z)
        {
            // Convert to radians in notation consistent with the paper:
            var lambda = ConvertToRadians(lat);
            var phi = ConvertToRadians(lon);
            var s = Math.Sin(lambda);
            var n = A / Math.Sqrt(1 - E_SQ * s * s);

            var sinLambda = Math.Sin(lambda);
            var cosLambda = Math.Cos(lambda);
            var cosPhi = Math.Cos(phi);
            var sinPhi = Math.Sin(phi);

            x = (h + n) * cosLambda * cosPhi;
            y = (h + n) * cosLambda * sinPhi;
            z = (h + (1 - E_SQ) * n) * sinLambda;
        }

        // Converts the Earth-Centered Earth-Fixed (ECEF) coordinates (x, y, z) to 
        // East-North-Up coordinates in A Local Tangent Plane that is centered at the 
        // (WGS-84) Geodetic point (lat0, lon0, h0).
        private static void EcefToEnu(double x, double y, double z,
            double lat0, double lon0, double h0,
            out double xEast, out double yNorth, out double zUp)
        {
            // Convert to radians in notation consistent with the paper:
            var lambda = ConvertToRadians(lat0);
            var phi = ConvertToRadians(lon0);
            var s = Math.Sin(lambda);
            var n = A / Math.Sqrt(1 - E_SQ * s * s);

            var sinLambda = Math.Sin(lambda);
            var cosLambda = Math.Cos(lambda);
            var cosPhi = Math.Cos(phi);
            var sinPhi = Math.Sin(phi);

            double x0 = (h0 + n) * cosLambda * cosPhi;
            double y0 = (h0 + n) * cosLambda * sinPhi;
            double z0 = (h0 + (1 - E_SQ) * n) * sinLambda;

            double xd, yd, zd;
            xd = x - x0;
            yd = y - y0;
            zd = z - z0;

            // This is the matrix multiplication
            xEast = -sinPhi * xd + cosPhi * yd;
            yNorth = -cosPhi * sinLambda * xd - sinLambda * sinPhi * yd + cosLambda * zd;
            zUp = cosLambda * cosPhi * xd + cosLambda * sinPhi * yd + sinLambda * zd;
        }

        private static double ConvertToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }

        private static double ConvertToDegrees(double radians)
        {
            return 180.0 * radians / Math.PI;
        }

        /// <summary>
        /// Calculates the end-point from a given source at a given range (meters) and bearing (degrees).
        /// This methods uses simple geometry equations to calculate the end-point.
        /// 
        /// Method is taken from here http://stackoverflow.com/a/38233843/509674
        /// </summary>
        /// <param name="source">Point of origin</param>
        /// <param name="range">Range in meters</param>
        /// <param name="bearing">Bearing in degrees</param>
        /// <returns>End-point from the source given the desired range and bearing.</returns>
        public static GeoCoordinate CalculateDerivedPosition(GeoCoordinate source, double range, double bearing)
        {

            var latA = source.Latitude * DEGREES_TO_RADIANS;
            var lonA = source.Longitude * DEGREES_TO_RADIANS;
            var angularDistance = range / EARTH_RADIUS;
            var trueCourse = bearing * DEGREES_TO_RADIANS;

            var lat = Math.Asin(
                Math.Sin(latA) * Math.Cos(angularDistance) +
                Math.Cos(latA) * Math.Sin(angularDistance) * Math.Cos(trueCourse));

            var dlon = Math.Atan2(
                Math.Sin(trueCourse) * Math.Sin(angularDistance) * Math.Cos(latA),
                Math.Cos(angularDistance) - Math.Sin(latA) * Math.Sin(lat));

            var lon = ((lonA + dlon + Math.PI) % (Math.PI * 2)) - Math.PI;

            return new GeoCoordinate(
                lat * RADIANS_TO_DEGREES,
                lon * RADIANS_TO_DEGREES,
                source.Altitude);
        }

        public static GeoBoundingBox BBoxFromCoordinate(GeoCoordinate coordinate, double radius)
        {
            // Bounding box surrounding the point at given coordinates,
            // assuming local approximation of Earth surface as a sphere
            // of radius given by WGS84
            var lat = ConvertToRadians(coordinate.Latitude);
            var lon = ConvertToRadians(coordinate.Longitude);

            // Radius of Earth at given latitude
            var earthRadius = WGS84EarthRadius(lat);
            // Radius of the parallel at given latitude
            var pradius = earthRadius * Math.Cos(lat);

            var latMin = lat - radius / earthRadius;
            var latMax = lat + radius / earthRadius;
            var lonMin = lon - radius / pradius;
            var lonMax = lon + radius / pradius;

            return new GeoBoundingBox {
                Min = new GeoCoordinate(ConvertToDegrees(latMin), ConvertToDegrees(lonMin)),
                Max = new GeoCoordinate(ConvertToDegrees(latMax), ConvertToDegrees(lonMax))
            };
        }

        // Earth radius at a given latitude, according to the WGS-84 ellipsoid [m]
        //https://stackoverflow.com/a/14314146/952023
        private static double WGS84EarthRadius(double lat)
        {
            // http://en.wikipedia.org/wiki/Earth_radius
            var An = A * A * Math.Cos(lat);
            var Bn = B * B * Math.Sin(lat);
            var Ad = A * Math.Cos(lat);
            var Bd = B * Math.Sin(lat);
            return Math.Sqrt((An * An + Bn * Bn) / (Ad * Ad + Bd * Bd));
        }

        // TODO: move to unittest (probably need to move this utils to separate library)
#if false
        /// <summary>
        /// Just check that we get the same values as the paper for the main calculations.
        /// </summary>
        private static void Test()
        {
            var latLA = 34.00000048;
            var lonLA = -117.3335693;
            var hLA = 251.702;

            double x0, y0, z0;
            GeodeticToEcef(latLA, lonLA, hLA, out x0, out y0, out z0);

            Debug.Assert(AreClose(-2430601.8, x0));
            Debug.Assert(AreClose(-4702442.7, y0));
            Debug.Assert(AreClose(3546587.4, z0));

            // Checks to read out the matrix entries, to compare with the paper
            double x, y, z;
            double xEast, yNorth, zUp;

            // First column
            x = x0 + 1;
            y = y0;
            z = z0;
            EcefToEnu(x, y, z, latLA, lonLA, hLA, out xEast, out yNorth, out zUp);
            Debug.Assert(AreClose(0.88834836, xEast));
            Debug.Assert(AreClose(0.25676467, yNorth));
            Debug.Assert(AreClose(-0.38066927, zUp));

            x = x0;
            y = y0 + 1;
            z = z0;
            EcefToEnu(x, y, z, latLA, lonLA, hLA, out xEast, out yNorth, out zUp);
            Debug.Assert(AreClose(-0.45917011, xEast));
            Debug.Assert(AreClose(0.49675810, yNorth));
            Debug.Assert(AreClose(-0.73647416, zUp));

            x = x0;
            y = y0;
            z = z0 + 1;
            EcefToEnu(x, y, z, latLA, lonLA, hLA, out xEast, out yNorth, out zUp);
            Debug.Assert(AreClose(0.00000000, xEast));
            Debug.Assert(AreClose(0.82903757, yNorth));
            Debug.Assert(AreClose(0.55919291, zUp));

        }

        private static bool AreClose(double x0, double x1)
        {
            var d = x1 - x0;
            return (d * d) < 1e16;
        }
#endif
    }
}
