﻿using UnityEngine;


namespace Assets._Project.Shared.Scripts.Core
{
    /// <summary>
    /// Causes a Hologram to maintain a fixed angular size, which is to say it
    /// occupies the same pixels in the view regardless of its distance from
    /// the camera. Use linear scale.
    /// </summary>
    public class LinearScale: MonoBehaviour
    {
        [Tooltip("Object is displayed as if it always near this distance from camera.")]
        public float FixedCameraDistance = 2;

        private Vector3 _startingScale;
        private float _ratio;

        private void Start()
        {
            _startingScale = transform.localScale;
            _ratio = 1 / FixedCameraDistance;
        }

        private void Update()
        {
            var distanceToHologram = Vector3.Distance(Camera.main.transform.position, transform.position);
            transform.localScale = _startingScale * (distanceToHologram * _ratio);
        }
    }
}