﻿using System;
using System.Collections;
using JetBrains.Annotations;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Core.LocationDetectors
{
    /// <summary>
    /// Select optimal workflow for getting user's location.
    /// </summary>
    public class CombinedLocationDetector: ILocationDetector
    {
        private readonly ILocationDetector _wifiLocationDetector;
        private readonly ILocationDetector _defaultLocationDetector;

        private const int ACCURACY_TRESHOLD = 200;

        private DetectedLocation _wifiLocation = null; 

        public CombinedLocationDetector([NotNull] ILocationDetector wifiLocationDetector, [NotNull] ILocationDetector defaultLocationDetector)
        {
            if (wifiLocationDetector == null) {
                throw new ArgumentNullException("wifiLocationDetector");
            }
            if (defaultLocationDetector == null) {
                throw new ArgumentNullException("defaultLocationDetector");
            }
            _wifiLocationDetector = wifiLocationDetector;
            _defaultLocationDetector = defaultLocationDetector;
            OnLocationDetected = new LocationDetectedEvent();
            OnDetectionFailure = new DetectionFailureEvent();
        }

        public IEnumerator StartDetection(string companySlug)
        {
            // Check location by wifi.
            // Analyze accuracy in handler.
            // If accuracy is appropriate send event.
            // In other case detect by default detector.

            var isWifiDetectionFinished = false;

            _wifiLocationDetector.OnLocationDetected.AddListener(location => {
                _wifiLocation = location;
                isWifiDetectionFinished = true;
            });

            _wifiLocationDetector.OnDetectionFailure.AddListener(errorReason => {
                isWifiDetectionFinished = true;
            });

            yield return _wifiLocationDetector.StartDetection(companySlug);

            //TODO: Consider adding timeout here
            while (!isWifiDetectionFinished) {
                yield return new WaitForSeconds(0.5f);
            }

            _wifiLocationDetector.StopDetection();

            if (_wifiLocation != null && _wifiLocation.Accuracy <= ACCURACY_TRESHOLD) {
                OnLocationDetected.Invoke(_wifiLocation);
            } else {
                _defaultLocationDetector.OnLocationDetected.AddListener(
                    defaultLocation => {
                        OnLocationDetected.Invoke(defaultLocation);
                        _defaultLocationDetector.StopDetection();
                    }
                    );
                _defaultLocationDetector.OnDetectionFailure.AddListener(errorReason => {
                    OnDetectionFailure.Invoke(errorReason);
                    _defaultLocationDetector.StopDetection();
                });
                // TODO: check if we should to warp coroutine in task here since we can't be sure that it is wrapperd at the below level. 
                yield return _defaultLocationDetector.StartDetection(companySlug);
            }
        }

        public void StopDetection()
        {
            OnLocationDetected.RemoveAllListeners();
            OnDetectionFailure.RemoveAllListeners();
        }

        public LocationDetectedEvent OnLocationDetected { get; set; }
        public DetectionFailureEvent OnDetectionFailure { get; set; }
    }
}