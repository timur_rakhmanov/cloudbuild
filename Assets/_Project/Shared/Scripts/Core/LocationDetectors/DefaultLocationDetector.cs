﻿using System;
using System.Collections;
using Assets._Project.Components.Loader.Shared.Scripts;
using Assets._Project.Shared.Scripts.APIClient;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Meemim.VGis.Unity.Plugins.Interfaces.Dto;


namespace Assets._Project.Shared.Scripts.Core.LocationDetectors
{
    /// <summary>
    /// Found default coordinates for company.
    /// </summary>
    public class DefaultLocationDetector: ILocationDetector
    {
        public LocationDetectedEvent OnLocationDetected { get; set; }
        public DetectionFailureEvent OnDetectionFailure { get; set; }
        private readonly ApiClient _apiClient;
        private readonly ILoaderController _loaderController;

        private const int EARTH_LENGTH = 40000;
        private const int MERCATOR_TILE_SIZE = 256;
        private const int IMAGE_SIZE = 640;

        public DefaultLocationDetector(ILoaderController loaderController)
        {
            if (loaderController == null) {
                throw new ArgumentNullException("loaderController");
            }
            _loaderController = loaderController;

            OnLocationDetected = new LocationDetectedEvent();
            OnDetectionFailure = new DetectionFailureEvent();
            _apiClient = new ApiClient();
        }

        public IEnumerator StartDetection(string companySlug)
        {
            _loaderController.ShowLoader();

            try {
                var task = _apiClient.GetCompany(companySlug).AsTask<CompanyViewModel>();
                yield return task.Run();

                if (task.IsFaulted) {
                    OnDetectionFailure.Invoke(new ErrorReason(task.Exception));
                    yield break;
                }

                var company = task.Result;
                OnLocationDetected.Invoke(new DetectedLocation(
                    new GeoCoordinate(company.DefaultLatitude, company.DefaultLongitude),
                    company.DefaultZoom, GetAccuracyFromZoom(company.DefaultZoom)));
            } finally {
                _loaderController.HideLoader();
            }
        }

        public void StopDetection()
        {   
            OnLocationDetected.RemoveAllListeners();
            OnDetectionFailure.RemoveAllListeners();
        }

        private int GetAccuracyFromZoom(int zoom)
        {
            var pixelsAmount = MERCATOR_TILE_SIZE * Math.Pow(2, zoom);
            var pixelSize = EARTH_LENGTH / pixelsAmount;
            var radiusInMeters = (int)Math.Floor(IMAGE_SIZE * pixelSize / 2 * 1000);
            return radiusInMeters;
        }
    }
}