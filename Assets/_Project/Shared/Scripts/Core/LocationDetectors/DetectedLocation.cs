﻿namespace Assets._Project.Shared.Scripts.Core.LocationDetectors
{
    /// <summary>
    /// Location detected by detector.
    /// </summary>
    public class DetectedLocation
    {
        /// <summary>
        /// Detected coordinate.
        /// </summary>
        public GeoCoordinate Coordinate { get; private set; }

        /// <summary>
        /// Recommended zoom (min 0 max 20) according to the expected accuracy.
        /// </summary>
        public int RecommendedZoom { get; private set; }

        /// <summary>
        /// Accuracy of detected coordinates in meters. Null if detector doesn't provide accuracy info.
        /// </summary>
        public double? Accuracy { get; private set; }

        public DetectedLocation(GeoCoordinate coordinate, int recommendedZoom, double? accuracy = null)
        {
            Coordinate = coordinate;
            RecommendedZoom = recommendedZoom;
            Accuracy = accuracy;
        }
        
    }
}