﻿using System;
using Meemim.VGis.Unity.Plugins.Interfaces.Dto;
using UnityEngine.Events;


namespace Assets._Project.Shared.Scripts.Core.LocationDetectors
{
    [Serializable]
    public class DetectionFailureEvent : UnityEvent<ErrorReason> {
    }
}
