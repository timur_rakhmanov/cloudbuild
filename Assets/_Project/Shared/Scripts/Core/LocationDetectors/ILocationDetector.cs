﻿using System.Collections;


namespace Assets._Project.Shared.Scripts.Core.LocationDetectors
{
    /// <summary>
    /// Detect current user location.
    /// </summary>
    public interface ILocationDetector
    {
        /// <summary>
        /// Initializer all data and starts detection workflow.
        /// </summary>
        IEnumerator StartDetection(string companySlug);

        /// <summary>
        /// Stop detection process. Release all used resources.
        /// </summary>
        void StopDetection();

        LocationDetectedEvent OnLocationDetected { get; set; }

        DetectionFailureEvent OnDetectionFailure { get; set; }
    }
}