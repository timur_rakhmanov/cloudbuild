﻿using System;
using UnityEngine.Events;


namespace Assets._Project.Shared.Scripts.Core.LocationDetectors
{
    [Serializable]
    public class LocationDetectedEvent: UnityEvent<DetectedLocation>
    {
    }
}