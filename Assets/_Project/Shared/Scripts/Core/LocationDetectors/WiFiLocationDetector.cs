﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets._Project.Components.Loader.Shared.Scripts;
using Assets._Project.Shared.Scripts.APIClient;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Meemim.VGis.Unity.Plugins;
using Meemim.VGis.Unity.Plugins.Interfaces.Dto;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Core.LocationDetectors
{
    /// <summary>
    /// Detects user's location by wifi points.
    /// In case when there are no wifi points - event will contain location with negative accuracy
    /// </summary>
    public class WiFiLocationDetector: ILocationDetector
    {
        private readonly ILoaderController _loaderController;

        public WiFiLocationDetector(ILoaderController loaderController)
        {
            if (loaderController == null) {
                throw new ArgumentNullException("loaderController");
            }
            _loaderController = loaderController;

            OnLocationDetected = new LocationDetectedEvent();
            OnDetectionFailure = new DetectionFailureEvent();
        }
        
        public IEnumerator StartDetection(string companySlug)
        {
            _loaderController.ShowLoader();

            try {
                var scanner = new WiFiScanner();
                var task = new AsyncDecorator<IList<WiFiAccessPoint>>(scanner.GetWiFiAccessPoints)
                    .AsTask<IList<WiFiAccessPoint>>();
                yield return task.Run();

                if (task.IsFaulted) {
                    OnDetectionFailure.Invoke(new ErrorReason(task.Exception));
                    yield break;
                }
                
                var points = task.Result;
                if (!points.Any()) {
                    OnDetectionFailure.Invoke(new ErrorReason("No points found"));
                    yield break;
                }

                var apiClient = new ApiClient();
                var getLocationTask = apiClient.GetLocationFromWifiPoints(points).AsTask<DetectedLocation>();
                yield return getLocationTask.Run();

                if (getLocationTask.IsFaulted) {
                    OnDetectionFailure.Invoke(new ErrorReason(getLocationTask.Exception));
                    Debug.LogWarning(getLocationTask.Exception);
                    yield break;
                }

                var location = getLocationTask.Result;
                OnLocationDetected.Invoke(location);
            } finally {
                _loaderController.HideLoader();
            }
        }

        public void StopDetection()
        {
            OnLocationDetected.RemoveAllListeners();
            OnDetectionFailure.RemoveAllListeners();
        }

        public LocationDetectedEvent OnLocationDetected { get; set; }
        public DetectionFailureEvent OnDetectionFailure { get; set; }
    }
}