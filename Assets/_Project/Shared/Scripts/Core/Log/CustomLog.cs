﻿using System;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Core.Log
{
    /// <summary>
    /// Custom logger that delegates logging to the Unity Debug.
    /// </summary>
    public class CustomLog: ILog
    {
        // TODO: support enable, disable of trace log from the unity.
        private readonly bool _isTraceEnabled = false;
        private readonly string _name;
        // TODO: log trace only if trace is enabled.
        public CustomLog()
        {
            _name = string.Empty;
        }

        public CustomLog(string name)
        {
            if (string.IsNullOrEmpty(name)) {
                throw new ArgumentNullException("name");
            }
            _name = name;
        }

        public void LogTrace(string message)
        {
            if (_isTraceEnabled) {
                Debug.Log(FormatMessage(message, true));
            }
        }

        public void LogTraceFormat(string format, params object[] args)
        {
            if (_isTraceEnabled) {
                Debug.Log(FormatMessage(format, true, args));
            }
        }

        public void LogInfo(string message)
        {
            Debug.Log(FormatMessage(message));
        }

        public void LogInfoFormat(string format, params object[] args)
        {
            Debug.Log(FormatMessage(format, false, args));
        }

        public void LogWarning(string message)
        {
            Debug.LogWarning(FormatMessage(message));
        }

        public void LogWarningFormat(string format, params object[] args)
        {
            Debug.LogWarning(FormatMessage(format, false, args));
        }

        public void LogError(string message)
        {
            Debug.LogError(FormatMessage(message));
        }

        public void LogErrorFormat(string format, params object[] args)
        {
            Debug.LogError(FormatMessage(format, false, args));
        }

        public void LogException(Exception ex)
        {
            Debug.LogException(ex);
        }

        private string FormatMessage(string message, bool isTrace = false)
        {
            var formattedMessage = "";
            if (isTrace) {
                formattedMessage = "TRACE ";
            }

            if (!string.IsNullOrEmpty(_name)) {
                formattedMessage = formattedMessage + _name + ": ";
            }

            return formattedMessage + message;
        }

        private string FormatMessage(string format, bool isTrace, params object[] args)
        {
            return FormatMessage(string.Format(format, args), isTrace);
        }
    }
}