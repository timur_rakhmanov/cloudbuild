﻿using Meemim.VGis.Unity.Plugins.Interfaces.Log;


namespace Assets._Project.Shared.Scripts.Core.Log
{
    /// <summary>
    /// Creates custom log.
    /// </summary>
    public class CustomLogFactory: ILogFactory
    {
        public ILog GetLogger(string name)
        {
            return new CustomLog(name);
        }
    }
}