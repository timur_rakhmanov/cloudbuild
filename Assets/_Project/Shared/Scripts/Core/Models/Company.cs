﻿using System;


namespace Assets._Project.Shared.Scripts.Core.Models
{
    [Serializable]
    public class Company {
        public Guid Id { get; set; }
        public string Slug { get; set; }
        public string LogoUrl { get; set; }
    }
}
