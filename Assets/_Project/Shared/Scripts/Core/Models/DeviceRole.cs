﻿namespace Assets._Project.Shared.Scripts.Core.Models
{
    public enum DeviceRole : byte
    {
        Device = 0,

        SuperDevice = 1
    }
}
