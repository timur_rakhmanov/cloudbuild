﻿using System;


namespace Assets._Project.Shared.Scripts.Core
{
    /// <summary>
    /// Indicates that code doesn't work for current platform.
    /// </summary>
    public class NotImplementedForPlatformException: NotImplementedException
    {
    }
}
