﻿using System;
using System.Collections;
using Meemim.VGis.Unity.Plugins.Interfaces.Dto;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;


namespace Assets._Project.Shared.Scripts.Core.Tasks
{
    /// <summary>
    /// Decorates async calls and returns execution to unity thread.
    /// </summary>
    // TODO: consider to puss YieldInstruction in order to control how often coroutine should be check.
    // TODO: consider to use custom error result e.g. TFailedResult.
    public class AsyncDecorator<TResult> : IAsyncDecorator
    {
        private const int DEFAULT_TIMEOUT_SECONDS = 30;

        private readonly Action<Action<TResult>, Action<ErrorReason>> _action;
        private TResult _result;
        private bool _isDone;
        private bool _isFault;
        private ErrorReason _faultReason;

        private DateTime _timeoutTime;

        // TODO: use abstract factory.
        // ReSharper disable once StaticMemberInGenericType
        private static readonly ILog _log = LogManager.GetLogger(typeof(AsyncDecorator<>));

        public AsyncDecorator(Action<Action<TResult>, Action<ErrorReason>> action)
        {
            _action = action;
        }

        /// <summary>
        /// Run async action as coroutine.
        /// 
        /// If async action newer return valud during exception (at the another thread) then 
        /// coroutine never stop, so we should hadnle all exception.
        /// </summary>
        /// <returns></returns>
        public IEnumerator Run()
        {
            // NOTE: Current method isn't enumerator, just proxy to real Run method.
            return Run(DEFAULT_TIMEOUT_SECONDS);
        }

        // TODO: consider to move this method to the IAsyncDecorator interface, the issue is that it has collision with IRestCoroutine.
        public IEnumerator Run(int timeoutSeconds)
        {
            _log.LogTrace("Run start.");
            _timeoutTime = DateTime.UtcNow.AddSeconds(timeoutSeconds);

            _action(Success, Failure);

            while (!_isDone) {
                // TODO: write unittests.
                if (_timeoutTime < DateTime.UtcNow) {
                    Failure(new ErrorReason("Async action timeout."));
                }
                // TODO: support wait operations.
                yield return null;
            }

            if (_isFault) {
                throw new Exception(string.Format("Async coroutine is failed, reason: {0}", _faultReason), _faultReason.Exception);
            } else {
                _log.LogTraceFormat("Run succeeded with result {0}", _result);
                yield return _result;
            }
            _log.LogTrace("Run end.");
        }

        // Process success processing and return to main thread in run.
        private void Success(TResult result)
        {
            _isDone = true;
            _result = result;
        }

        // Process fault processing and return to main thread in run.
        private void Failure(ErrorReason reason)
        {
            _isDone = true;
            _isFault = true;
            _faultReason = reason;
        }
    }
}