﻿using System.Collections;


namespace Assets._Project.Shared.Scripts.Core.Tasks
{
    /// <summary>
    /// Wrap async request in coroutine.
    /// </summary>
    public interface IAsyncDecorator
    {
        /// <summary>
        /// Run async request as coroutine.
        /// </summary>
        /// <returns></returns>
        IEnumerator Run();
    }
}