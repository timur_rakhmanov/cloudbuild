﻿using System;
using System.Collections;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Core.Tasks
{
    /// <summary>
    /// Encapsulates coroutines.
    /// </summary>
    public class Task
    {
        /// <summary>
        /// Indicates if task was completed due to an unhandled exception.
        /// </summary>
        public bool IsFaulted { get; set; }

        /// <summary>
        /// Indicates if task was completed.
        /// </summary>
        public bool IsCompleted { get; set; }

        /// <summary>
        /// Contains exception that stopped task work.
        /// </summary>
        public Exception Exception { get; set; }

        /// <summary>
        /// Coroutine that is run by task.
        /// </summary>
        protected readonly IEnumerator _coroutine;

        private Action<Task> _continuationAction;
        private static readonly ILog _log = LogManager.GetLogger(typeof(Task));

        public Task(IEnumerator coroutine)
        {
            _coroutine = coroutine;
        }

        /// <summary>
        /// Run coroutine.
        /// </summary>
        /// <returns></returns>
        // TODO: check if finally block will be called in the child coroutine! (since we don't call dispouse here).
        public IEnumerator Run()
        {
            _log.LogTrace("Run start.");
            while (true) {
                try {
                    if (!_coroutine.MoveNext()) {
                        _log.LogTraceFormat("Run end with value type {0}.", _coroutine.Current);
                        IsCompleted = true;
                        // NOTE: coroutine will not be finished until action is finished.
                        RunContinuation();
                        yield break;
                    }
                } catch (Exception ex) {
                    _log.LogTrace("Run exception.");
                    Exception = ex;
                    IsFaulted = true;
                    IsCompleted = true;

                    // NOTE: coroutine will not be finished until action is finished.
                    RunContinuation();
                    yield break;
                }
                yield return GetResult(_coroutine);
            }
        }

        /// <summary>
        /// Make sure that coroutine is finished success or rethrown exception.
        /// </summary>
        // TODO: consider to name AssertSuccess
        public void Wait()
        {
            if (!IsCompleted) {
                throw new Exception("Coroutine should be completed before wait call.");
            }

            // TODO: consider to implement aggregate exception like in Threads.Tasks
            if (Exception != null) {
                throw Exception;
            }
        }

        /// <summary>
        /// Calls continuation action when task is completed, passed task contains appropriate status.
        /// When continue with is called task already has completed status, but unity will not free coroutine 
        /// until action is finished so take this into account when implement action.
        /// </summary>
        /// <param name="continuationAction"></param>
        /// <returns></returns>
        public virtual Task ContinueWith(Action<Task> continuationAction)
        {
            if (HasContinuation()) {
                throw new Exception("Continuation action already set.");
            }
            _continuationAction = continuationAction;
            return this;
        }

        /// <summary>
        /// Returns tru if continuation action is set.
        /// </summary>
        /// <returns></returns>
        protected virtual bool HasContinuation()
        {
            return _continuationAction != null;
        }

        /// <summary>
        /// Run continuation.
        /// </summary>
        protected virtual void RunContinuation()
        {
            // TODO:21696 consider to log exception if continuation is null and exception is thrown.
            if (_continuationAction != null) {
                // TODO: add tests to exception in continuation.
                try {
                    _continuationAction(this);
                } catch (Exception continueationEx) {
                    Debug.LogException(continueationEx);
                }
            }
        }

        /// <summary>
        /// Prevent return custom objects except YieldInstruction, CustomYieldInstruction, IEnumerator, WWW
        /// 
        /// Some environments don't handle well custom object (e.g. UWP application) at the unity engine level.
        /// So we prevent passing them here.
        /// </summary>
        private object GetResult(IEnumerator coroutine)
        {
            // Checks null value first, since we assume that it is null in most cases and try to avoid 'is' checking 
            // (probably just micro optimization).
            if (coroutine.Current == null)
            {
                return null;
            }

            // NOTE: CustomYieldInstruction doesn't inherit YieldInstruction.
            // TODO: remove part with WWW when we use Unityt 2017, since at new versions it inherits from  CustomYieldInstruction.
            if (coroutine.Current is WWW || coroutine.Current is YieldInstruction || coroutine.Current is CustomYieldInstruction || coroutine.Current is IEnumerator)
            {
                return coroutine.Current;
            }

            // Returns null in case of any other objects except YieldInstructions and it's children.
            return null;
        }
    }
}

