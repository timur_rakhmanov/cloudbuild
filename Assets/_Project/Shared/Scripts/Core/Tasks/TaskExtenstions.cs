﻿using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;


namespace Assets._Project.Shared.Scripts.Core.Tasks
{
    /// <summary>
    /// Task related extensions.
    /// </summary>
    public static class TaskExtenstions
    {
        /// <summary>
        /// Wrap async decorator in task.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="decorator"></param>
        /// <returns></returns>
        public static Task<T> AsTask<T>(this IAsyncDecorator decorator)
        {
            return new Task<T>(decorator.Run());
        }

        public static Task AsTask(this IAsyncDecorator decorator)
        {
            return new Task(decorator.Run());
        }

        public static Task<T> AsTask<T>(this IEnumerator enumerator)
        {
            return new Task<T>(enumerator);
        }

        public static Task AsTask(this IEnumerator enumerator)
        {
            return new Task(enumerator);
        }

        public static Coroutine RunCoroutine(this Task task, MonoBehaviour behaviour)
        {
            Assert.IsNotNull(behaviour);
            return behaviour.StartCoroutine(task.Run());
        }
    }
}