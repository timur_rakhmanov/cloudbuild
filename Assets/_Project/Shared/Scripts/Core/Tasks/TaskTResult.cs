﻿using System;
using System.Collections;
using HoloToolkit;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;


namespace Assets._Project.Shared.Scripts.Core.Tasks
{
    /// <summary>
    /// Encapsulates coroutines with result.
    /// </summary>
    public class Task<TResult>: Task
    {
        // ReSharper disable once StaticMemberInGenericType
        private static readonly ILog _log = LogManager.GetLogger(typeof(Task<>));
        public Task(IEnumerator coroutine) : base(coroutine)
        {
        }

        private Action<Task<TResult>> _continuationActionWithResult;

        /// <summary>
        /// Returns task result or throw exception if it isn't completed yet or failed due exception.
        /// </summary>
        public TResult Result
        {
            get
            {
                _log.LogTrace("Result start.");
                if (!IsCompleted) {
                    throw new Exception("Coroutine should be completed before get result.");
                }

                // TODO: consider to implement aggregate exception like in Threads.Tasks
                if (Exception != null) {
                    throw Exception;
                }

                var latestElement = _coroutine.Current;
                // NOTE: we use GetTypeInfo from holotoolkit, since detecting IsValueType isn't present for Type in UWP, 
                // and GetTypeInfor isn't prsent on .net 3.5 (only from 4.5), so we have to use Holotoolkit part.
                // Could be an issue if we decide to move tasks to the separate library.
                if (latestElement == null && typeof(TResult).IsValueType()) {
                    throw new Exception("Coroutine doesn't return result or result is null for value object.");
                }

                if (latestElement == null) {
                    _log.LogTrace("Result end with null.");
                    return (TResult)latestElement;
                }
                if (!(latestElement is TResult)) {
                    throw new Exception(string.Format(
                        "Coroutine has incorrect result type, expected type is {0} but returned type is {1}", 
                            typeof(TResult), latestElement.GetType()));
                }

                _log.LogTraceFormat("Result end with value {0}.", latestElement);
                return (TResult) latestElement;
            }
        }

        public Task<TResult> ContinueWith(Action<Task<TResult>> continuationAction)
        {
            if (HasContinuation()) {
                throw new Exception("Continuation action already set.");
            }
            _continuationActionWithResult = continuationAction;
            return this;
        }

        protected override bool HasContinuation()
        {
            return _continuationActionWithResult != null || base.HasContinuation();
        }

        protected override void RunContinuation()
        {
            if (_continuationActionWithResult != null) {
                // TODO: add tests to exception in continuation.
                try {
                    _continuationActionWithResult(this);
                } catch (Exception ex) {
                    UnityEngine.Debug.LogException(ex);
                }
            } else {
                base.RunContinuation();
            }
        }

        // TODO: integrate it later when we decide to support nested coroutines without task warpper.
        private object GetResult(IEnumerator enumerator)
        {
            // Get result from nested part if needed.
            if (enumerator.Current is IEnumerator) {
                return GetResult((IEnumerator)enumerator.Current);
            }

            return enumerator.Current;
        }
    }
}
