﻿using System;
using UnityEngine.Events;


namespace Assets._Project.Shared.Scripts.Core
{
    [Serializable]
    public class ZoomEvent : UnityEvent<ZoomType> {
    }
}
