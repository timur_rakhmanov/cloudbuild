﻿namespace Assets._Project.Shared.Scripts.Core
{
    public enum ZoomType {
        ZoomIn = 0,
        ZoomOut = 1
    }
}
