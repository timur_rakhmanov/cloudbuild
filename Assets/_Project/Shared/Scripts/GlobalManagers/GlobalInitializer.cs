﻿using Assets._Project.Shared.Scripts.Core.Log;
using HoloToolkit.Unity;
using Meemim.VGis.Unity.Plugins.Interfaces.Log;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.GlobalManagers
{
    /// <summary>
    /// Make global initialization. Should be used for base initializing before any other managers/scripts are called.
    /// </summary>
    public class GlobalInitializer: Singleton<GlobalInitializer>
    {
        protected override void Awake()
        {
            base.Awake();
            // Configure logging factory.
            // NOTE: it is safe to use it at the "Start" methods.
            LogManager.SetFactory(new CustomLogFactory());
        }
    }
}