﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using Assets._Project.Shared.Scripts.Configuration;
using HoloToolkit.Unity;
using Meemim.VGis.Unity.Plugins;
using Meemim.VGis.Unity.Plugins.Interfaces;
using Meemim.VGis.Unity.Plugins.Interfaces.Dto;
using Newtonsoft.Json;
using UnityEngine;
using ExecutionContext = Assets._Project.Shared.Scripts.Core.ExecutionContext;


namespace Assets._Project.Shared.Scripts.GlobalManagers
{
    /// <summary>
    /// Manage log handlers.
    /// </summary>
    public class LogHandlersManager : Singleton<LogHandlersManager>
    {
        private const string LOG_FOLDER = "logs";
        private IExternalLog _logentriesLog;
        private IExternalLog _fileLog;

        private new void Awake()
        {
            base.Awake();
            _logentriesLog = new LogentriesLog(AppSettings.Instance.LogentriesToken, 
                ExecutionContext.Instance.DeviceyType.ToString(), ExecutionContext.Instance.DeviceId);
            Application.logMessageReceivedThreaded += LogentriesLogHandler;

            if (AppSettings.Instance.FileLogEnabled) {
                // TODO: handle folder creation! LOG_FOLDER
                _fileLog = new FileLog(Application.persistentDataPath,
                    ExecutionContext.Instance.DeviceyType.ToString(), ExecutionContext.Instance.DeviceId);
                Application.logMessageReceivedThreaded += FileLogHandler;
            }
        }

        private new void OnDestroy()
        {
            base.OnDestroy();

            Application.logMessageReceivedThreaded -= LogentriesLogHandler;
            _logentriesLog.Dispose();

            if (AppSettings.Instance.FileLogEnabled) {
                _fileLog.Dispose();
                Application.logMessageReceivedThreaded -= FileLogHandler;
            } 
        }

        private void LogentriesLogHandler(string logString, string stackTrace, LogType type)
        {
            HandleLog(_logentriesLog, logString, stackTrace, type);
        }

        private void FileLogHandler(string logString, string stackTrace, LogType type)
        {
            HandleLog(_fileLog, logString, stackTrace, type);
        }

        private void HandleLog(IExternalLog log, string logString, string stackTrace, LogType type)
        {
            string logMessage = null;

            switch (type) {
                // TODO: allow to write seession id directly to log.
                case LogType.Log:
                    logMessage = string.Format("{0} (session id {1})", logString, ExecutionContext.Instance.SessionId);
                    log.Log(logMessage);
                    break;
                case LogType.Warning:
                    logMessage = string.Format("{0} (session id {1})", logString, ExecutionContext.Instance.SessionId);
                    log.Log(logMessage, LogLevel.Warn);
                    break;
                default:
                    logMessage = string.Format("{0}: {1}\r\nStacktrace:{2}. (session id {3})", type, logString, stackTrace, ExecutionContext.Instance.SessionId);
                    log.Log(logMessage, LogLevel.Error);
                    break;
            }
        }
    }
}
