﻿using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Infrastructure;
using HoloToolkit.Unity;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.GlobalManagers
{
    public class UserLocationManager : Singleton<UserLocationManager>
    {
        #region Private data

        private GeoCoordinate _startSceneLocation;
        private double _shift;

        #endregion

        #region Commands

        public GeoCoordinate GetUserLocation()
        {
            var placemarksRoot = PlacemarksManager.Instance.GetPlacemarksRoot();
            var positionShift = Camera.main.transform.position - placemarksRoot.transform.position;
            var distanceFromPreviousSceneLoad = positionShift.magnitude;
            var angleOfNewLocation = AngleBetweenVector3(placemarksRoot.transform.forward.normalized, positionShift.normalized);
            // Convert negative angel from north to positive angel
            var azimuthOfNewLocation = (360 + angleOfNewLocation) % 360;

            var geoCoordinatesOfNewLocation = GpsUtils.CalculateDerivedPosition(_startSceneLocation, distanceFromPreviousSceneLoad, azimuthOfNewLocation);

            return geoCoordinatesOfNewLocation;
        }

        public void UpdateStartSceneLocation(GeoCoordinate startSceneLocation)
        {
            _startSceneLocation = startSceneLocation;
        }

        public double GetUserRotation()
        {
            var azimuthOfNewLocation = (360 + _shift + Camera.main.transform.rotation.eulerAngles.y) % 360;

            return azimuthOfNewLocation;
        }

        public void SetRotationShift(double shift)
        {
            _shift = shift;
        }

        #endregion

        /// <summary>
        /// Angle between 2 vectors. Solution is taken here http://answers.unity3d.com/questions/181867/is-there-way-to-find-a-negative-angle.html
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        private static float AngleBetweenVector3(Vector3 from, Vector3 to)
        {
            // calculate angle assume the sign of the cross product's Y component:
            var angle = Vector3.Angle(from, to);
            return angle * Mathf.Sign(Vector3.Cross(from, to).y);
        }
    }
}