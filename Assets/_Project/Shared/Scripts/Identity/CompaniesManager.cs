﻿using System.Collections;
using System.Collections.Generic;
using Assets._Project.Shared.Scripts.APIClient;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Models;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Assets._Project.Shared.Scripts.Identity.Events;
using HoloToolkit.Unity;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Identity
{
    public class CompaniesManager : Singleton<CompaniesManager>
    {
        #region Events

        [HideInInspector]
        public ActiveCompanyUpdatedEvent OnActiveCompanyUpdated;

        #endregion

        #region Private data

        private ApiClient _apiClient;

        #endregion

        #region Mono behaviour pipeline

        private void Start()
        {
            _apiClient = new ApiClient();
        }

        #endregion

        #region Commands

        public void UpdateActiveCompany(CompanyViewModel companyViewModel)
        {
            ExecutionContext.Instance.UpdateCompany(new Company { Id = companyViewModel.Id, Slug = companyViewModel.Slug, LogoUrl = companyViewModel.LogoUrl });
            OnActiveCompanyUpdated.Invoke();
        }

        /// <summary>
        /// Update active company by slug.
        /// </summary>
        /// <param name="slug"></param>
        /// <returns>Void</returns>
        public IEnumerator UpdateActiveCompany(string slug)
        {
            var getCompanyInTask = _apiClient.GetCompany(slug)
                .AsTask<CompanyViewModel>();
            yield return getCompanyInTask.Run();

            UpdateActiveCompany(getCompanyInTask.Result);
        }

        public IEnumerator DeviceCompaniesLoading(IEnumerable<string> slugs)
        {
            var companies = new List<CompanyViewModel>();

            foreach (var slug in slugs)
            {
                var getCompanyInTask = _apiClient.GetCompany(slug).AsTask<CompanyViewModel>();
                yield return getCompanyInTask.Run();

                companies.Add(getCompanyInTask.Result);
            }

            ExecutionContext.Instance.SaveDeviceCompanies(companies);
        }

        public IEnumerator SuperDeviceCompaniesLoading()
        {
            var getCompaniesInTask = _apiClient.GetCompanies().AsTask<List<CompanyViewModel>>();

            yield return getCompaniesInTask.Run();

            ExecutionContext.Instance.SaveDeviceCompanies(getCompaniesInTask.Result);
        }

        #endregion
    }
}