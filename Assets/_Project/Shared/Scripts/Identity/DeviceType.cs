﻿namespace Assets._Project.Shared.Scripts.Identity
{
    /// <summary>
    /// Type of the device.
    /// </summary>
    public enum DeviceType
    {
        Hololens,
        Editor,
        Ios,
        Android
    }
}