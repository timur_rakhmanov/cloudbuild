﻿using System;
using UnityEngine.Events;


namespace Assets._Project.Shared.Scripts.Identity.Events
{
    [Serializable]
    public class ActiveCompanyUpdatedEvent : UnityEvent
    {
    }
}
