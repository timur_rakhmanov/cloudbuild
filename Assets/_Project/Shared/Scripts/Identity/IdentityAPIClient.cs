﻿using System.Collections;
using Assets._Project.Shared.Scripts.APIClient.Coroutines;
using Assets._Project.Shared.Scripts.Configuration;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Newtonsoft.Json;
using RestSharp;


namespace Assets._Project.Shared.Scripts.Identity
{
    public class IdentityApiClient
    {        
        private const string REFRESH_ACCESS_TOKEN_URL = "api/identity/refreshaccesstoken";
        private const string SIGN_IN_DEVICE_URL = "api/identity/signindevice";
        private const string SIGN_IN_SUPER_DEVICE_URL = "api/identity/signinsuperdevice";
        private const string SIGN_IN_SUPER_DEVICE_BY_DEVICE_ID = "api/identity/signinsuperdevicebydeviceid";

        private readonly IRestClient _restClient;
        private readonly string _apiDomain = AppSettings.Instance.IdentityServerUrl;

        public IdentityApiClient()
        {
            _restClient = new RestClient(_apiDomain);
        }

        public IEnumerator SignInDeviceCoroutine(string secureLink, bool superDevice)
        {
            var request = new RestRequest(superDevice ? SIGN_IN_SUPER_DEVICE_URL : SIGN_IN_DEVICE_URL) {
                Method = Method.POST
            };

            var model = new
            {
                Token = secureLink,
                DeviceType = ExecutionContext.Instance.DeviceyType,
                ExecutionContext.Instance.DeviceId,
                ExecutionContext.Instance.DeviceName
            };

#if NETFX_CORE
            request.AddJsonBody(model);
#else
            request.RequestFormat = DataFormat.Json;
            request.AddBody(model);
#endif

            var task = RestCoroutineFactory.CreateRestCoroutine(_restClient, request)
                .AsTask<IRestResponse>();
            yield return task.Run();
            var response = task.Result;

            var identityViewModel = JsonConvert.DeserializeObject<IdentityViewModel>(response.Content);
            yield return identityViewModel;
        }

        /// <summary>
        /// Regresh access token.
        /// </summary>
        /// <param name="refreshToken"></param>
        /// <returns>Returns IdentityViewModel with new token.</returns>
        public IEnumerator RefreshAccessToken(string refreshToken)
        {
            var request = new RestRequest(REFRESH_ACCESS_TOKEN_URL) { Method = Method.POST };

            var model = new {
                Data = refreshToken
            };

#if NETFX_CORE
            request.AddJsonBody(model);
#else
            request.RequestFormat = DataFormat.Json;
            request.AddBody(model);
#endif

            var task = RestCoroutineFactory.CreateRestCoroutine(_restClient, request)
                .AsTask<IRestResponse>();

            yield return task.Run();

            var response = task.Result;

            var identityViewModel = JsonConvert.DeserializeObject<IdentityViewModel>(response.Content);

            yield return identityViewModel;
        }

        public IEnumerator SignInSuperDeviceByDeviceId()
        {
            var request = new RestRequest(SIGN_IN_SUPER_DEVICE_BY_DEVICE_ID) {
                Method = Method.POST
            };

            var model = new {
                // Token isn't used in this request, since it is only for tests.
                Token = "stub",
                DeviceType = ExecutionContext.Instance.DeviceyType,
                ExecutionContext.Instance.DeviceId,
                ExecutionContext.Instance.DeviceName
            };

#if NETFX_CORE
            request.AddJsonBody(model);
#else
            request.RequestFormat = DataFormat.Json;
            request.AddBody(model);
#endif
            var task = RestCoroutineFactory.CreateRestCoroutine(_restClient, request)
                .AsTask<IRestResponse>();

            yield return task.Run();

            var response = task.Result;

            var identityViewModel = JsonConvert.DeserializeObject<IdentityViewModel>(response.Content);

            yield return identityViewModel;
        }
    }
}