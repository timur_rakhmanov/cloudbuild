﻿using System;
using System.Collections;
using System.Linq;
using Assets._Project.Components.SecureDataStorage.Shared.Scripts;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Models;
using Assets._Project.Shared.Scripts.Core.Tasks;
using HoloToolkit.Unity;
using UnityEngine;
using UnityEngine.Assertions;


namespace Assets._Project.Shared.Scripts.Identity
{
    public class IdentityManager : Singleton<IdentityManager>
    {
        #region Injections

        public GameObject DataSecureStorage;

        #endregion
        #region Private data

        private IdentityApiClient _identityApiClient;
        private ISecureDataStorage _secureDataStorage;
        private CompaniesManager _companiesManager;
        #endregion

        #region Constants

        private const string REFERSH_TOKEN_KEY_PREFIX = "refreshToken";
        private const string SUPER_DEVICE_ROLE_NAME = "SuperDevice";
        private const string SYSTEM_ACCESS_TYPE = "system";

        #endregion

        #region Mono behaviour pipeline

        // ReSharper disable once UnusedMember.Local
        private void Start()
        {
            Assert.IsNotNull(DataSecureStorage);
            _secureDataStorage = DataSecureStorage.GetComponent<ISecureDataStorage>();
            Assert.IsNotNull(_secureDataStorage);

            _companiesManager = CompaniesManager.Instance;
            Assert.IsNotNull(_companiesManager);

            _identityApiClient = new IdentityApiClient();
        }

        #endregion

        #region Queries

        /// <summary>
        /// Get device role
        /// </summary>
        /// <returns></returns>
        public DeviceRole GetDeviceRole()
        {
            if (ExecutionContext.Instance.IdentityData.Roles == null) {
                throw new NullReferenceException("ExecutionContext.Instance.IdentityData.Roles");
            }

            return ExecutionContext.Instance.IdentityData.Roles.Any(x => x.ToLower().Equals(SUPER_DEVICE_ROLE_NAME.ToLower()))
                ? DeviceRole.SuperDevice
                : DeviceRole.Device;
        }

        /// <summary>
        /// Get device refresh token
        /// </summary>
        /// <returns>Return null if refresh token does not exist</returns>
        public string GetDeviceRefreshToken()
        {
            var refreshToken = ExecutionContext.Instance.IdentityData.RefreshToken;

            return !string.IsNullOrEmpty(refreshToken) ? refreshToken : GetRefreshToken();
        }

        public bool IsSignInTokenForSuperdevice(string companySlug)
        {
            return companySlug.Equals(SYSTEM_ACCESS_TYPE, StringComparison.OrdinalIgnoreCase);
        }

        #endregion

        #region Commands

        /// <summary>
        /// Refresh access token
        /// </summary>
        /// <param name="refreshToken">Device refresh token</param>
        /// <returns></returns>
        public IEnumerator AccessTokenRefresh(string refreshToken)
        {
            var task = _identityApiClient.RefreshAccessToken(refreshToken).AsTask<IdentityViewModel>();

            yield return task.Run();

            var identityViewModel = task.Result;

            ExecutionContext.Instance.UpdateIdentityData(identityViewModel);

            var loadCompaniesTask = LoadDeviceCompanies().AsTask();
            yield return loadCompaniesTask.Run();
            loadCompaniesTask.Wait();

            yield return identityViewModel;
        }

        /// <summary>
        /// Sign in by device id and returns identity view model.
        /// </summary>
        /// <returns></returns>
        public IEnumerator SignInByDeviceId()
        {
            var signInByDeviceIdTask = _identityApiClient.SignInSuperDeviceByDeviceId().AsTask<IdentityViewModel>();

            yield return signInByDeviceIdTask.Run();

            var identityViewModel = signInByDeviceIdTask.Result;

            SignedIn(identityViewModel);

            var loadDevicesTask = LoadDeviceCompanies().AsTask();
            yield return loadDevicesTask.Run();
            loadDevicesTask.Wait();

            yield return identityViewModel;
        }

        /// <summary>
        /// Sign in device by sign in token and company related to the token.
        /// </summary>
        /// <returns>Returns identityViewModel of the signed in data.</returns>
        public IEnumerator SignIn(string signInToken, string company)
        {
            // We treat system as super device when "system" is passed as company.
            var isSuperDevice = IsSignInTokenForSuperdevice(company);
            var signInTask = _identityApiClient.SignInDeviceCoroutine(signInToken, isSuperDevice)
                .AsTask<IdentityViewModel>();
            yield return signInTask.Run();
            var identityViewModel = signInTask.Result;
            SignedIn(identityViewModel);

            var loadDevicesTask = LoadDeviceCompanies().AsTask();
            yield return  loadDevicesTask.Run();
            loadDevicesTask.Wait();

            yield return identityViewModel;
        }

        public void SignOut()
        {
            RemoveRefreshToken();
            ExecutionContext.Instance.UpdateIdentityData(new IdentityViewModel());
        }

        #endregion

        #region Private methods

        private IEnumerator LoadDeviceCompanies()
        {
            var role = Instance.GetDeviceRole();

            if (role == DeviceRole.SuperDevice) {
                var task = _companiesManager.SuperDeviceCompaniesLoading().AsTask();
                yield return task.Run();
                task.Wait();
            } else {
                var task = _companiesManager.DeviceCompaniesLoading(ExecutionContext.Instance.IdentityData.CompaniesSlugs).AsTask();
                yield return task.Run();
                task.Wait();
            }
        }

        private string GetRefreshToken()
        {
            return _secureDataStorage.Load<string>(FormRefreshTokenKey());
        }

        private void SaveRefreshToken(string refreshToken)
        {
            _secureDataStorage.Save(FormRefreshTokenKey(), refreshToken);
        }

        private void RemoveRefreshToken()
        {
            _secureDataStorage.Remove(FormRefreshTokenKey());
        }

        #endregion

        #region Event handlers
        
        private void SignedIn(IdentityViewModel identityViewModel)
        {
            Debug.Log("Sign in process successfully finished.");

            ExecutionContext.Instance.UpdateIdentityData(identityViewModel);
            SaveRefreshToken(identityViewModel.RefreshToken);
        }

        private string FormRefreshTokenKey()
        {
            return REFERSH_TOKEN_KEY_PREFIX + Uri.EscapeDataString(ExecutionContext.Instance.DeviceId);
        }
        #endregion
    }
}