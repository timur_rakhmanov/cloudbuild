﻿using System;
using System.Collections.Generic;


namespace Assets._Project.Shared.Scripts.Identity
{
    public class IdentityViewModel
    {
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        public string TokenType { get; set; }

        public double ExpiresIn { get; set; }

        public DateTime Issued { get; set; }
        
        public DateTime Expires { get; set; }

        public string IdentityId { get; set; }

        public List<string> Roles { get; set; }

        public List<string> CompaniesSlugs { get; set; }
    }
}