﻿using System;
using UnityEngine.Events;


namespace Assets._Project.Shared.Scripts.Infrastructure
{
    [Serializable]
    public class ActiveSceneLayersChanged : UnityEvent<SceneLayersOptions>
    {
    }
}
