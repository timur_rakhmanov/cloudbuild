﻿namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights.Factory
{
    public abstract class HighlighterFactory
    {
        public abstract IHighlighter CreateHighlighter();
    }
}
