﻿namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights.Factory
{
    public class MainTextureHighlighterFactory : HighlighterFactory
    {
        public override IHighlighter CreateHighlighter()
        {
            return new MainTextureHighlighter();
        }
    }
}
