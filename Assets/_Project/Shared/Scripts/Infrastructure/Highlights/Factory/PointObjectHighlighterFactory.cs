﻿namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights.Factory
{
    public class PointObjectHighlighterFactory : HighlighterFactory
    {
        public override IHighlighter CreateHighlighter()
        {
            return new PointObjectHighligher();
        }
    }
}
