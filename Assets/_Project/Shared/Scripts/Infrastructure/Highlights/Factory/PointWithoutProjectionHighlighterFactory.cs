﻿namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights.Factory
{
    public class PointWithoutProjectionHighlighterFactory : HighlighterFactory
    {
        public override IHighlighter CreateHighlighter()
        {
            return new PointWithoutProjectionHighlighter();
        }
    }
}
