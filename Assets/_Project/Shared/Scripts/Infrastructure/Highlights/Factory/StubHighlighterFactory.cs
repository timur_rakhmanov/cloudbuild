﻿namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights.Factory
{
    public class StubHighlighterFactory : HighlighterFactory
    {
        public override IHighlighter CreateHighlighter()
        {
            return new StubHighlighter();
        }
    }
}
