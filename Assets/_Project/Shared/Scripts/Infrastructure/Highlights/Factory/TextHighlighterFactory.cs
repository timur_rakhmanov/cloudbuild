﻿namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights.Factory
{
    public class TextHighlighterFactory : HighlighterFactory
    {
        public override IHighlighter CreateHighlighter()
        {
            return new TextHighlighter();
        }
    }
}
