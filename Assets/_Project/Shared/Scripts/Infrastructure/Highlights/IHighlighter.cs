﻿using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights
{
    public interface IHighlighter
    {
        void HighlightObject(GameObject gameObject, Color color);

        void UndoHighlightObject();
    }
}
