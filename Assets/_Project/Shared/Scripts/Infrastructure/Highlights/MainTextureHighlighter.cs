﻿using Assets._Project.Shared.Scripts.Core;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights
{
    public class MainTextureHighlighter : IHighlighter
    {
        private GameObject _originalSelectedPlacemark;
        private Material _originalSelectedPlacemarkMaterial;

        public void HighlightObject(GameObject gameObject, Color color)
        {
            _originalSelectedPlacemark = gameObject;
            var objectRenderer = _originalSelectedPlacemark.GetComponent<Renderer>();

            _originalSelectedPlacemarkMaterial = new Material(objectRenderer.material);

            objectRenderer.material = new Material(Shader.Find(Constants.HIGHLIGHT_SHADER_NAME)) { color = color };
        }

        public void UndoHighlightObject()
        {
            if (_originalSelectedPlacemarkMaterial != null) {
                _originalSelectedPlacemark.GetComponent<Renderer>().material = _originalSelectedPlacemarkMaterial;
                _originalSelectedPlacemarkMaterial = null;
            }
        }
    }
}
