﻿using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights
{
    public class MapObjectHighligher : IHighlighter
    {
        private Color _originalColor;
        private GameObject _originalSelectedPlacemark;

        public void HighlightObject(GameObject gameObject, Color color)
        {
            _originalSelectedPlacemark = gameObject;
            var renderer = gameObject.GetComponent<Renderer>();
            _originalColor = renderer.material.color;
            float h, s, v;
            Color.RGBToHSV(color, out h, out s, out v);
            gameObject.GetComponent<Renderer>().material.color = Color.HSVToRGB(h, 1, 1);
        }

        public void UndoHighlightObject()
        {
            _originalSelectedPlacemark.GetComponent<Renderer>().material.color = _originalColor;
        }
    }
}