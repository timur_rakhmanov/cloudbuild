﻿using Assets._Project.Shared.Scripts.Core;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights
{
    public class PointObjectHighligher : IHighlighter
    {
        private GameObject _originalSelectedPlacemark;
        private GameObject _clonedSelectedPlacemark;

        public void HighlightObject(GameObject gameObject, Color color)
        {
            _originalSelectedPlacemark = gameObject;
            _clonedSelectedPlacemark = PlacemarksManager.Instance.ClonePlacemarkGameObject(gameObject);
            _originalSelectedPlacemark.SetActive(false);

            var objectRenderers = _clonedSelectedPlacemark.GetComponentsInChildren<MeshRenderer>();

            if (objectRenderers != null && objectRenderers.Length > 0) {
                foreach (var objectRenderer in objectRenderers) {
                    objectRenderer.material = new Material(Shader.Find(Constants.HIGHLIGHT_SHADER_NAME)) { color = color };
                }
            }
        }

        public void UndoHighlightObject()
        {
            if (_clonedSelectedPlacemark != null) {
                PlacemarksManager.Instance.DestroyPlacemarkGameObject(_clonedSelectedPlacemark);
            }

            if (_originalSelectedPlacemark != null) {
                _originalSelectedPlacemark.SetActive(true);
            }
        }
    }
}