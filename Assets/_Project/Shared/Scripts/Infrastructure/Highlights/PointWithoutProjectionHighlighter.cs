﻿using Assets._Project.Shared.Scripts.Core;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights
{
    public class PointWithoutProjectionHighlighter : IHighlighter
    {
        private const string PROJECTION_OBJECT_INDICATOR = "projection";

        private GameObject _originalSelectedPlacemark;
        private GameObject _clonedSelectedPlacemark;

        public void HighlightObject(GameObject gameObject, Color color)
        {
            _originalSelectedPlacemark = gameObject;
            _clonedSelectedPlacemark = PlacemarksManager.Instance.ClonePlacemarkGameObject(gameObject);
            _originalSelectedPlacemark.SetActive(false);

            var objectRenderers = _clonedSelectedPlacemark.GetComponentsInChildren<Renderer>();

            if (objectRenderers != null && objectRenderers.Length > 0) {
                foreach (var objectRenderer in objectRenderers) {
                    if (objectRenderer.name.ToLower().Contains(PROJECTION_OBJECT_INDICATOR)) {
                        continue;
                    }
                    objectRenderer.material = new Material(Shader.Find(Constants.HIGHLIGHT_SHADER_NAME)) { color = color };
                }
            }
        }

        public void UndoHighlightObject()
        {
            if (_clonedSelectedPlacemark != null) {
                PlacemarksManager.Instance.DestroyPlacemarkGameObject(_clonedSelectedPlacemark);
            }

            if (_originalSelectedPlacemark != null) {
                _originalSelectedPlacemark.SetActive(true);
            }
        }
    }
}
