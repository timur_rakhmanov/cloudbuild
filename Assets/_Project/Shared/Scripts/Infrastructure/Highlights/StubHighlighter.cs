﻿using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights
{
    public class StubHighlighter : IHighlighter
    {
        public void HighlightObject(GameObject gameObject, Color color)
        {
            // Don't make any changes.
        }

        public void UndoHighlightObject()
        {
            // Don't make any changes.
        }
    }
}
