﻿using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure.Highlights
{
    public class TextHighlighter : IHighlighter
    {
        private GameObject _originalSelectedPlacemark;
        private GameObject _clonedSelectedPlacemark;

        public void HighlightObject(GameObject gameObject, Color color)
        {
            _originalSelectedPlacemark = gameObject;
            _clonedSelectedPlacemark = PlacemarksManager.Instance.ClonePlacemarkGameObject(gameObject);
            _originalSelectedPlacemark.SetActive(false);

            var textMesh = _clonedSelectedPlacemark.GetComponentInChildren<TextMesh>();

            if (textMesh != null) {
                textMesh.color = color;
            }
        }

        public void UndoHighlightObject()
        {
            if (_clonedSelectedPlacemark != null) {
                PlacemarksManager.Instance.DestroyPlacemarkGameObject(_clonedSelectedPlacemark);
            }

            if (_originalSelectedPlacemark != null) {
                _originalSelectedPlacemark.SetActive(true);
            }
        }
    }
}
