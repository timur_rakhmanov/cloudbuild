﻿namespace Assets._Project.Shared.Scripts.Infrastructure
{
    public interface ISceneLayersManager
    {
        ActiveSceneLayersChanged OnActiveSceneLayersChanged { get; set; }
    }
}
