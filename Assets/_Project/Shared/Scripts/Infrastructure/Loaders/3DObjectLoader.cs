﻿using System;
using System.Collections.Generic;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets._Project.Shared.Scripts.Infrastructure.Loaders
{
    public class ThreeDimensionalObjectLoader : PlacemarkLoader
    {
        private readonly Dictionary<PlacementTypeViewModel, PlacemarkLoader> _3DObjectLoaders;

        public ThreeDimensionalObjectLoader(
            GameObject object3DPlanePrefab,
            GameObject squarePlanePrefab,
            GameObject aboveGroundObjectPrefab, 
            GameObject onGroundCircleObjectPrefab, 
            GameObject onGroundSquareObjectPrefab,
            GameObject belowGroundObjectSpherePrefab,
            GameObject object3DProjectionPrefab) : base(ObjectType.Object3D)
        {
            _3DObjectLoaders = new Dictionary<PlacementTypeViewModel, PlacemarkLoader> {
                { PlacementTypeViewModel.AboveGround, new AboveGround3DObjectLoader(aboveGroundObjectPrefab, object3DPlanePrefab, onGroundCircleObjectPrefab, object3DProjectionPrefab) },
                { PlacementTypeViewModel.OnGround, new OnGround3DObjectLoader(onGroundCircleObjectPrefab, onGroundSquareObjectPrefab, object3DPlanePrefab, squarePlanePrefab) },
                { PlacementTypeViewModel.BelowGround, new BelowGround3DObjectLoader(belowGroundObjectSpherePrefab, onGroundCircleObjectPrefab, object3DProjectionPrefab) }
            };
        }

        protected override GameObject RunLoad(PlacemarkViewModel placemarkViewModel, double startLatitude, double startLongitude, double startAltitude)
        {
            Assert.IsNotNull(placemarkViewModel);

            var objectTypeConfiguration = placemarkViewModel.ObjectTypeData as Object3DDataViewModel;

            if (objectTypeConfiguration == null) {
                throw new Exception(string.Format("Object type configuration should be 3DObject configuration for placemark '{0}'", placemarkViewModel.Id));
            }

            if (!objectTypeConfiguration.PlacementType.HasValue) {
                throw new Exception("3D object type should contain PlacementType value.");
            }

            var loader = _3DObjectLoaders[objectTypeConfiguration.PlacementType.Value];

            return loader.Load(placemarkViewModel, startLatitude, startLongitude, startAltitude);
        }

        protected override void ValidatePlacemarkData(PlacemarkViewModel placemarkViewModel)
        {
            Assert.IsNotNull(placemarkViewModel);

            ValidateGeometryType(placemarkViewModel, GeometryType.Point);
        }
    }
}
