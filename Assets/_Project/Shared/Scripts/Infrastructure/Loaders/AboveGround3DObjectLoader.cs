﻿using System;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using UnityEngine;
using Object = UnityEngine.Object;
using UnityEngine.Assertions;

namespace Assets._Project.Shared.Scripts.Infrastructure.Loaders
{
    public class AboveGround3DObjectLoader : PlacemarkLoader
    {
        private const double DEFAULT_HEIGHT = 2;
        private const double ABOVE_GROUND_CYLINDER_DIAMETER = 0.07;
        private const double PLANE_DIAMETER = 0.1;
        private const double BELOW_GROUND_CYLINDER_DIAMETER = 0.035;

        private readonly GameObject _aboveGroundObjectPrefab;
        private readonly GameObject _aboveGroundPlanePrefab;
        private readonly GameObject _undergroundPartPrefab;
        private readonly GameObject _object3DProjectionPrefab;

        public AboveGround3DObjectLoader(GameObject aboveGroundObjectPrefab, GameObject aboveGroundPlanePrefab, GameObject undergroundPartPrefab, GameObject object3DProjectionPrefab) : base(ObjectType.Object3D)
        {
            if (aboveGroundObjectPrefab == null) {
                throw new ArgumentNullException("aboveGroundObjectPrefab");
            }
            if (aboveGroundPlanePrefab == null) {
                throw new ArgumentNullException("aboveGroundPlanePrefab");
            }
            if (undergroundPartPrefab == null) {
                throw new ArgumentNullException("undergroundPartPrefab");
            }
            if (object3DProjectionPrefab == null) {
                throw new ArgumentNullException("object3DProjectionPrefab");
            }

            _aboveGroundObjectPrefab = aboveGroundObjectPrefab;
            _aboveGroundPlanePrefab = aboveGroundPlanePrefab;
            _undergroundPartPrefab = undergroundPartPrefab;
            _object3DProjectionPrefab = object3DProjectionPrefab;
        }

        protected override GameObject RunLoad(PlacemarkViewModel placemarkViewModel, double startLatitude, double startLongitude, double startAltitude)
        {
            Assert.IsNotNull(placemarkViewModel);

            var objectConfiguration = (Object3DDataViewModel)placemarkViewModel.ObjectTypeData;

            var originPosition = GetWorldCoordinatesFromGeodetic(placemarkViewModel.Latitude, placemarkViewModel.Longitude, startAltitude,
                                                           startLatitude, startLongitude, startAltitude);
            originPosition.y += (float) placemarkViewModel.Altitude - Constants.USER_HEIGHT;

            var objectContainer = new GameObject(placemarkViewModel.Id);
            objectContainer.AddComponent<PlacemarkController>();
            objectContainer.transform.position = originPosition;

            if (objectConfiguration.ShowMesh.HasValue && objectConfiguration.ShowMesh.Value) {
                //Mesh creation
                var meshGameObject = Object.Instantiate(MeshLibraryProvider.Instance.GetMeshPrefab(objectConfiguration.ObjectMesh));
                meshGameObject.AddComponent<MeshCollider>();
                meshGameObject.transform.position = originPosition;
                meshGameObject.transform.SetParent(objectContainer.transform);
            } else {
                //Above ground cylinder creation
                var aboveGroundObject = Object.Instantiate(_aboveGroundObjectPrefab);
                aboveGroundObject.name += placemarkViewModel.Id;
                aboveGroundObject.transform.localScale = CalculateScale(
                    aboveGroundObject.transform.localScale,
                    objectConfiguration.ObjectHeight ?? DEFAULT_HEIGHT,
                    ABOVE_GROUND_CYLINDER_DIAMETER);

                var aboveGroundCylinderPosition = originPosition;
                aboveGroundCylinderPosition.y += (float) (objectConfiguration.ObjectHeight ?? DEFAULT_HEIGHT) / 2;
                aboveGroundObject.transform.position = aboveGroundCylinderPosition;

                aboveGroundObject.GetComponent<Renderer>().material.color = ColorConverter.HexToColor(placemarkViewModel.MainColor);

                aboveGroundObject.transform.SetParent(objectContainer.transform);

                //Plane creation
                var planeGameObject = Object.Instantiate(_aboveGroundPlanePrefab);
                planeGameObject.transform.position = originPosition;
                planeGameObject.transform.localScale = CalculatePlaneScale(planeGameObject.transform.localScale, PLANE_DIAMETER);
                planeGameObject.GetComponent<Renderer>().material.color = ColorConverter.HexToColor(placemarkViewModel.MainColor);

                planeGameObject.transform.SetParent(objectContainer.transform);
            }

            //Lower connector creation
            if (objectConfiguration.AboveGroundUndergroundPartDepth.HasValue) {
                var undergroundConnection = Object.Instantiate(_undergroundPartPrefab);
                undergroundConnection.transform.localScale = CalculateScale(
                    undergroundConnection.transform.localScale,
                    objectConfiguration.AboveGroundUndergroundPartDepth.Value,
                    BELOW_GROUND_CYLINDER_DIAMETER);

                var undergroundCylinderPosition = originPosition;
                undergroundCylinderPosition.y += (float) objectConfiguration.AboveGroundUndergroundPartDepth.Value / 2;
                undergroundConnection.transform.position = undergroundCylinderPosition;

                undergroundConnection.GetComponentInChildren<Renderer>().material = new Material(Shader.Find("Standard")) { color = ColorConverter.HexToColor(placemarkViewModel.MainColor) };
                undergroundConnection.transform.SetParent(objectContainer.transform);
            }

            //On ground projection creation
            var projection = Object.Instantiate(_object3DProjectionPrefab);
            var projectionPosition = originPosition;
            projectionPosition.y = -Constants.USER_HEIGHT;
            projection.transform.position = projectionPosition;
            projection.GetComponent<Renderer>().material.color = ColorConverter.HexToColor(placemarkViewModel.SecondColor);
            projection.transform.SetParent(objectContainer.transform);

            Debug.Log(string.Format("Load above ground 3D object placemarkViewModel with id {0}, external id {1}, coordinates {2} {3}, unity position {4}, {5}, {6}",
                                    placemarkViewModel.Id, placemarkViewModel.ExternalId, placemarkViewModel.Latitude, placemarkViewModel.Longitude, 
                                    originPosition.x, originPosition.y, originPosition.z));

            return objectContainer;
        }

        protected override void ValidatePlacemarkData(PlacemarkViewModel placemarkViewModel)
        {
            Assert.IsNotNull(placemarkViewModel);

            ValidateGeometryType(placemarkViewModel, GeometryType.Point);
        }

        private static Vector3 CalculateScale(Vector3 localScale, double height, double diameter)
        {
            var scale = localScale;
            scale.x = (float)diameter;
            scale.y = (float)Math.Abs(height) / 2;
            scale.z = (float)diameter;
            return scale;
        }

        private static Vector3 CalculatePlaneScale(Vector3 localScale, double diameter)
        {
            localScale.x = (float)diameter + 0.2f;
            localScale.z = (float)diameter + 0.2f;
            return localScale;
        }
    }
}
