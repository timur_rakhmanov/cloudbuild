﻿using System;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure.Loaders
{
    public class AreaLoader : PlacemarkLoader
    {
        public AreaLoader() : base(ObjectType.Area)
        {
        }

        protected override GameObject RunLoad(PlacemarkViewModel placemarkViewModel, double startLatitude, double startLongitude, double startAltitude)
        {
            throw new NotImplementedException();
        }

        protected override void ValidatePlacemarkData(PlacemarkViewModel placemarkViewModel)
        {
        }
    }
}
