﻿using System;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using UnityEngine;
using Object = UnityEngine.Object;
using UnityEngine.Assertions;

namespace Assets._Project.Shared.Scripts.Infrastructure.Loaders
{
    public class BelowGround3DObjectLoader : PlacemarkLoader
    {
        private const double BELOW_GROUND_CYLINDER_DIAMETER = 0.035;
        
        private readonly GameObject _belowGroundObjectSpherePrefab;
        private readonly GameObject _undergroundPartPrefab;
        private readonly GameObject _object3DProjectionPrefab;

        public BelowGround3DObjectLoader(GameObject belowGroundObjectSpherePrefab, GameObject undergroundPartPrefab, GameObject object3DProjectionPrefab) : base(ObjectType.Object3D)
        {
            if (belowGroundObjectSpherePrefab == null) {
                throw new ArgumentNullException("belowGroundObjectSpherePrefab");
            }
            if (undergroundPartPrefab == null) {
                throw new ArgumentNullException("undergroundPartPrefab");
            }
            if (object3DProjectionPrefab == null) {
                throw new ArgumentNullException("object3DProjectionPrefab");
            }

            _belowGroundObjectSpherePrefab = belowGroundObjectSpherePrefab;
            _undergroundPartPrefab = undergroundPartPrefab;
            _object3DProjectionPrefab = object3DProjectionPrefab;
        }

        protected override GameObject RunLoad(PlacemarkViewModel placemarkViewModel, double startLatitude, double startLongitude, double startAltitude)
        {
            Assert.IsNotNull(placemarkViewModel);

            var objectConfiguration = (Object3DDataViewModel) placemarkViewModel.ObjectTypeData;

            var originPosition = GetWorldCoordinatesFromGeodetic(placemarkViewModel.Latitude, placemarkViewModel.Longitude, startAltitude,
                startLatitude, startLongitude, startAltitude);

            originPosition.y += (float) placemarkViewModel.Altitude - Constants.USER_HEIGHT;

            var objectContainer = new GameObject(placemarkViewModel.Id);
            objectContainer.AddComponent<PlacemarkController>();
            objectContainer.transform.position = originPosition;
            
            if (string.IsNullOrEmpty(objectConfiguration.ObjectMesh)) {
                throw new Exception(string.Format("Object mesh can't be empty or null for placemark with id {0}", placemarkViewModel.Id));
            }
            
            //Mesh creation
            var meshGameObject = Object.Instantiate(MeshLibraryProvider.Instance.GetMeshPrefab(objectConfiguration.ObjectMesh));
            meshGameObject.AddComponent<MeshCollider>();
            meshGameObject.transform.position = originPosition;
            meshGameObject.transform.SetParent(objectContainer.transform);

            //On ground projection creation
            var projection = Object.Instantiate(_object3DProjectionPrefab);
            var projectionPosition = originPosition;
            projectionPosition.y = -Constants.USER_HEIGHT;
            projection.transform.position = projectionPosition;
            projection.GetComponent<Renderer>().material.color = ColorConverter.HexToColor(placemarkViewModel.SecondColor);
            projection.transform.SetParent(objectContainer.transform);

            //Dodecahedron creation
            if (objectConfiguration.ShowSphere.HasValue && objectConfiguration.ShowSphere.Value) {
                var meshBoundsSize = meshGameObject.GetComponent<Renderer>().bounds.size;
                var meshBoundingBoxDiameter = Mathf.Sqrt(Mathf.Pow(meshBoundsSize.x, 2) + Mathf.Pow(meshBoundsSize.y, 2) + Mathf.Pow(meshBoundsSize.z, 2));

                var belowGroundSphere = Object.Instantiate(_belowGroundObjectSpherePrefab);
                
                float sphereDiameter;
                if (objectConfiguration.SphereRadius.HasValue) {
                    sphereDiameter = (float) objectConfiguration.SphereRadius.Value * 2;

                    if (sphereDiameter < meshBoundingBoxDiameter) {
                        var factor = meshBoundingBoxDiameter / sphereDiameter;
                        meshGameObject.transform.localScale /= factor;
                    }
                } else {
                    sphereDiameter = meshBoundingBoxDiameter;
                }
                belowGroundSphere.transform.localScale = new Vector3(sphereDiameter, sphereDiameter, sphereDiameter);
                belowGroundSphere.transform.position = meshGameObject.GetComponent<Renderer>().bounds.center;

                var color = ColorConverter.HexToColor(placemarkViewModel.MainColor);
                color.a = 0.6f;
                belowGroundSphere.GetComponent<Renderer>().material.color = color;

                belowGroundSphere.transform.SetParent(objectContainer.transform);
            }

            //Lower connector creation
            if (objectConfiguration.BelowGroundUndergroundPartDepth.HasValue) {
                var undergroundConnection = Object.Instantiate(_undergroundPartPrefab);
                undergroundConnection.transform.localScale = CalculateScale(
                    undergroundConnection.transform.localScale,
                    objectConfiguration.BelowGroundUndergroundPartDepth.Value,
                    BELOW_GROUND_CYLINDER_DIAMETER);

                var undergroundCylinderPosition = originPosition;
                undergroundCylinderPosition.y += (float) objectConfiguration.BelowGroundUndergroundPartDepth.Value / 2;
                undergroundConnection.transform.position = undergroundCylinderPosition;

                undergroundConnection.GetComponentInChildren<Renderer>().material = new Material(Shader.Find("Standard")) { color = ColorConverter.HexToColor(placemarkViewModel.MainColor) };
                undergroundConnection.transform.SetParent(objectContainer.transform);
            }

            Debug.Log(string.Format("Load below ground 3D object placemarkViewModel with id {0}, external id {1}, coordinates {2} {3}, unity position {4}, {5}, {6}",
                placemarkViewModel.Id, placemarkViewModel.ExternalId, placemarkViewModel.Latitude, placemarkViewModel.Longitude, originPosition.x, originPosition.y, originPosition.z));

            return objectContainer;
        }

        protected override void ValidatePlacemarkData(PlacemarkViewModel placemarkViewModel)
        {
            Assert.IsNotNull(placemarkViewModel);

            ValidateGeometryType(placemarkViewModel, GeometryType.Point);
        }

        private static Vector3 CalculateScale(Vector3 localScale, double height, double diameter)
        {
            var scale = localScale;
            scale.x = (float)diameter;
            scale.y = (float)Math.Abs(height) / 2;
            scale.z = (float)diameter;
            return scale;
        }
    }
}