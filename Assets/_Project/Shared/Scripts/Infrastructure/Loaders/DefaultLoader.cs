﻿using System;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions;
using Object = UnityEngine.Object;


namespace Assets._Project.Shared.Scripts.Infrastructure.Loaders
{
    /// <summary>
    /// Default loader that is used if we don't have any specified loader or some validation error occurred.
    /// </summary>
    public class DefaultLoader: PlacemarkLoader
    {
        private readonly GameObject _defaultPlacemarkPrefab;

        public DefaultLoader([NotNull] GameObject defaultPlacemarkPrefab) : base(ObjectType.Unknown)
        {
            if (defaultPlacemarkPrefab == null) {
                throw new ArgumentNullException("defaultPlacemarkPrefab");
            }
            _defaultPlacemarkPrefab = defaultPlacemarkPrefab;
        }

        protected override GameObject RunLoad(PlacemarkViewModel placemarkViewModel, double startLatitude, double startLongitude, double startAltitude)
        {
            Assert.IsNotNull(placemarkViewModel);

            // Set position.
            var position = GetWorldCoordinatesFromGeodetic(placemarkViewModel.Latitude, placemarkViewModel.Longitude, startAltitude,
                startLatitude, startLongitude, startAltitude);
            position.y = position.y - Constants.USER_HEIGHT;

            var gameObject = Object.Instantiate(_defaultPlacemarkPrefab);
            gameObject.transform.position = position;

            Debug.Log(string.Format("Load default placemarkViewModel with id {0}, external id {1}, coordinates {2} {3}, unity position {4}, {5}, {6}",
                placemarkViewModel.Id, placemarkViewModel.ExternalId, placemarkViewModel.Latitude, placemarkViewModel.Longitude, position.x, position.y, position.z));

            return gameObject;
        }

        protected override void ValidatePlacemarkData(PlacemarkViewModel placemarkViewModel)
        {
            // We don't have enoph info about placemarkViewModel for make any validation.
        }

        protected override void ValidateObjectType(PlacemarkViewModel placemarkViewModel)
        {
            // Object type isn't important for default loader.
        }
    }
}