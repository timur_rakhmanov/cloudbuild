﻿using System;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Infrastructure.Renderers;
using JetBrains.Annotations;
using UnityEngine;
using Object = UnityEngine.Object;
using UnityEngine.Assertions;

namespace Assets._Project.Shared.Scripts.Infrastructure.Loaders
{
    public class LateralObjectLoader : PlacemarkLoader
    {
        private const float DEFAULT_DEPTH = -2;

        private readonly GameObject _lateralPrefab;

        private const string ANGLE = "symangle";

        public LateralObjectLoader([NotNull] GameObject lateralPrefab) : base(ObjectType.Custom)
        {
            if (lateralPrefab == null) {
                throw new ArgumentNullException("lateralPrefab");
            }
            _lateralPrefab = lateralPrefab;
        }

        protected override GameObject RunLoad(PlacemarkViewModel placemarkViewModel, double startLatitude, double startLongitude, double startAltitude)
        {
            Assert.IsNotNull(placemarkViewModel);

            var point = (PointGeometry) placemarkViewModel.Geometry;

            var position = GetWorldCoordinatesFromGeodetic(point.Latitude, point.Longitude, point.Altitude.Value,
                                                           startLatitude, startLongitude, startAltitude);
            
            position.y = DEFAULT_DEPTH - Constants.USER_HEIGHT;

            var lateralGameObject = Object.Instantiate(_lateralPrefab);

            lateralGameObject.transform.position = position;

            var renderers = lateralGameObject.GetComponentsInChildren<Renderer>();

            foreach (var renderer in renderers) {
                renderer.material.color = ColorConverter.HexToColor(placemarkViewModel.MainColor);
            }

            lateralGameObject.transform.eulerAngles = new Vector3(lateralGameObject.transform.eulerAngles.x,
                                                                  GetDirectionAngle(placemarkViewModel),
                                                                  lateralGameObject.transform.eulerAngles.z);

            Debug.Log(string.Format("Load lateral with id {0}, external id {1}, coordinates {2} {3}, unity position {4}, {5}",
                                    placemarkViewModel.Id, placemarkViewModel.ExternalId, point.Latitude, point.Longitude, position.x, position.y));

            var projectionRenderer = lateralGameObject.GetComponentInChildren<ProjectionRenderer>();

            projectionRenderer.ProjectionColor = ColorConverter.HexToColor(placemarkViewModel.SecondColor);

            foreach (var vertices in projectionRenderer.Vertices) {
                vertices.Point.y = -DEFAULT_DEPTH;
            }

            return lateralGameObject;
        }

        protected override void ValidatePlacemarkData(PlacemarkViewModel placemarkViewModel)
        {
            Assert.IsNotNull(placemarkViewModel);
            ValidateGeometryType(placemarkViewModel, GeometryType.Point);
        }

        private float GetDirectionAngle(PlacemarkViewModel placemarkViewModel)
        {
            var directionAngle = 0f;

            if (!placemarkViewModel.DynamicProperties.ContainsKey(ANGLE)) {
                Debug.LogFormat("Use default radius for placemarkViewModel {0}", placemarkViewModel.Id);

                return directionAngle;
            }

            if (float.TryParse(placemarkViewModel.DynamicProperties[ANGLE], out directionAngle)) {
                return directionAngle;
            }

            Debug.LogFormat("Incorrect angle format of placemarkViewModel {0}, default diameter is used.", placemarkViewModel.Id);

            return directionAngle;
        }
    }
}
