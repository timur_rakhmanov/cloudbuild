﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Infrastructure.Renderers;
using UnityEngine;
using UnityEngine.Assertions;
using Object = UnityEngine.Object;


namespace Assets._Project.Shared.Scripts.Infrastructure.Loaders
{
    public class LineObjectLoader : PlacemarkLoader
    {
        private const float DEFAULT_RARIUS = 0.1f;
        private const string FORWARD_FLOW_DIRECTION = "forwardflowdirection";

        private readonly GameObject _lineObjectPrefab;
        private readonly GameObject _lineObjectProjectionPrefab;

        public LineObjectLoader(GameObject lineObjectPrefab, GameObject lineObjectProjectionPrefab) : base(ObjectType.Pipe)
        {
            if (lineObjectPrefab == null) {
                throw new ArgumentNullException("lineObjectPrefab");
            }
            _lineObjectPrefab = lineObjectPrefab;

            if (lineObjectProjectionPrefab == null) {
                throw new ArgumentNullException("lineObjectProjectionPrefab");
            }
            _lineObjectProjectionPrefab = lineObjectProjectionPrefab;
        }

        protected override GameObject RunLoad(PlacemarkViewModel placemarkViewModel, double startLatitude, double startLongitude, double startAltitude)
        {
            Assert.IsNotNull(placemarkViewModel);

            var pipeObjectData = placemarkViewModel.ObjectTypeData as PipeDataViewModel;
            if (pipeObjectData == null) {
                throw new Exception(string.Format("Object type data should be 'PipeDataViewModel' configuration for placemark '{0}'", placemarkViewModel.Id));
            }

            var line = GetLineGeometry(placemarkViewModel);

            var pipeVertexes = line.Points.Select(p => new TubeRenderer.TubeVertex(
                GetWorldCoordinatesFromGeodetic(p.Latitude, p.Longitude, p.Altitude ?? 0,
                    startLatitude, startLongitude, startAltitude),
                CalculateRadius(pipeObjectData),
                // TODO: check how it is used, since material has priority.
                ColorConverter.HexToColor(placemarkViewModel.MainColor)
            )).ToList();
            // Set altitude for each vertex.
            pipeVertexes.ForEach(pipeVertex => SetDepth(pipeVertex, (float)placemarkViewModel.Altitude));

            var projectionVertexes = line.Points.Select(p => new ProjectionRenderer.ProjectionVertex(
                GetWorldCoordinatesFromGeodetic(p.Latitude, p.Longitude, p.Altitude ?? 0, startLatitude, startLongitude, startAltitude),
                ColorConverter.HexToColor(placemarkViewModel.MainColor)
            )).ToList();
            // Set zero altitude for projection vertices
            projectionVertexes.ForEach(projectioVertex => projectioVertex.Point.y = -Constants.USER_HEIGHT);
            
            var sewerPipeGameObject = Object.Instantiate(_lineObjectPrefab);
            sewerPipeGameObject.name += placemarkViewModel.Id;

            var tubeRenderer = sewerPipeGameObject.GetComponent<TubeRenderer>();
            tubeRenderer.Vertices = pipeVertexes.ToArray();
            tubeRenderer.Material = new Material(_lineObjectPrefab.GetComponent<TubeRenderer>().Material) {
                color = ColorConverter.HexToColor(placemarkViewModel.MainColor)
            };

            var sewerPipeProjectionGameObject = Object.Instantiate(_lineObjectProjectionPrefab);

            var projectionRenderer = sewerPipeProjectionGameObject.GetComponent<ProjectionRenderer>();
            projectionRenderer.ProjectionColor = ColorConverter.HexToColor(placemarkViewModel.SecondColor);
            projectionRenderer.Vertices = projectionVertexes.ToArray();

            if (placemarkViewModel.DynamicProperties.ContainsKey(FORWARD_FLOW_DIRECTION)) {
                bool direction;
                if (bool.TryParse(placemarkViewModel.DynamicProperties[FORWARD_FLOW_DIRECTION], out direction)) {
                    projectionRenderer.ShowDirection = true;
                    projectionRenderer.Direction = direction;
                }
            }

            var startPoint = pipeVertexes.First().point;
            Debug.Log(string.Format("Load line with id {0}, external id {1}, coordinates {2} {3}, unity position {4}, {5}, {6}",
                placemarkViewModel.Id, placemarkViewModel.ExternalId, line.Points.First().Latitude, line.Points.First().Longitude,
                startPoint.x, startPoint.y, startPoint.z));

            sewerPipeProjectionGameObject.transform.SetParent(sewerPipeGameObject.transform);

            return sewerPipeGameObject;
        }

        protected override void ValidatePlacemarkData(PlacemarkViewModel placemarkViewModel)
        {
            Assert.IsNotNull(placemarkViewModel);
            ValidateTubeGeometry(placemarkViewModel);
        }

        #region Private methods

        private static float CalculateRadius(PipeDataViewModel pipeObjectData)
        {
            return pipeObjectData.Diameter.HasValue ? (float)pipeObjectData.Diameter.Value / 2 : DEFAULT_RARIUS;
        }

        private static void SetDepth(TubeRenderer.TubeVertex vertex, float altitude)
        {
            vertex.point.y += altitude - Constants.USER_HEIGHT;
        }

        private void ValidateTubeGeometry(PlacemarkViewModel placemarkViewModel)
        {
            ValidateGeometryType(placemarkViewModel, new List<GeometryType> { GeometryType.MultiGeometry, GeometryType.Line });

            if (placemarkViewModel.GeometryType == GeometryType.MultiGeometry)
            {
                var multiGeometry = (MultiGeometry)placemarkViewModel.Geometry;
                if (multiGeometry.Geometries.Count != 1)
                {
                    throw new Exception(string.Format("Invalid geometry of the tube placemarkViewModel with id {0}, multigeometry should have 1 line, it has {1}",
                                                      placemarkViewModel.Id, multiGeometry.Geometries.Count));
                }
                var isLineFirst = multiGeometry.Geometries.First() is LineGeometry;
                if (!isLineFirst)
                {
                    throw new Exception(string.Format("Invalid geometry of the tube placemarkViewModel with id {0}, multigeometry should have 1 line, type is incorrect {1}",
                                                      placemarkViewModel.Id, multiGeometry.Geometries.First().GetType()));
                }
            }

            var line = GetLineGeometry(placemarkViewModel);
            if (line.Points.Count < 2)
            {
                throw new Exception(string.Format("Invalid geometry of the tube placemarkViewModel with id {0}, line should have at least 2 points, it has {1}",
                                                  placemarkViewModel.Id, line.Points.Count));
            }
        }

        private LineGeometry GetLineGeometry(PlacemarkViewModel placemarkViewModel)
        {
            LineGeometry line;
            switch (placemarkViewModel.GeometryType)
            {
                case GeometryType.MultiGeometry:
                    line = (LineGeometry)((MultiGeometry)placemarkViewModel.Geometry).Geometries.First();
                    break;
                case GeometryType.Line:
                    line = (LineGeometry)placemarkViewModel.Geometry;
                    break;
                default:
                    throw new Exception("Incorrect geometry type.");
            }

            return line;
        }

        #endregion
    }
}