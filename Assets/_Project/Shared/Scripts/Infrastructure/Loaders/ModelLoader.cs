﻿using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using UnityEngine;
using UnityEngine.Assertions;


namespace Assets._Project.Shared.Scripts.Infrastructure.Loaders
{
    public class ModelLoader : PlacemarkLoader
    {
        public ModelLoader() : base(ObjectType.Model3D)
        {
        }

        protected override GameObject RunLoad(PlacemarkViewModel placemarkViewModel, double startLatitude, double startLongitude, double startAltitude)
        {
            Assert.IsNotNull(placemarkViewModel);
            var go = new GameObject();
            var model = (ModelGeometry)placemarkViewModel.Geometry;

            var position = GetWorldCoordinatesFromGeodetic(model.Latitude, model.Longitude, model.Altitude.HasValue ? model.Altitude.Value : 0,
                startLatitude, startLongitude, startAltitude);

            position.y = position.y - Constants.USER_HEIGHT;

            go.transform.position = position;

            Debug.Log(string.Format("Load geometry model with id {0}, external id {1}, coordinates {2} {3}, unity position {4}, {5}, {6}",
               placemarkViewModel.Id, placemarkViewModel.ExternalId, model.Latitude, model.Longitude, position.x, position.y, position.z));

            return go;
        }

        protected override void ValidatePlacemarkData(PlacemarkViewModel placemarkViewModel)
        {
            Assert.IsNotNull(placemarkViewModel);

            ValidateGeometryType(placemarkViewModel, GeometryType.Model);
        }
    }
}