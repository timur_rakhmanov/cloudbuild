﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure.Loaders
{
    /// <summary>
    /// Base class for rendering placemarks based on the domain type.
    /// </summary>
    public abstract class PlacemarkLoader
    {
        private readonly ObjectType _objectType;
        protected PlacemarkLoader(ObjectType objectType)
        {
            _objectType = objectType;
        }
        
        public GameObject Load(PlacemarkViewModel placemarkViewModel, double startLatitude, double startLongitude, double startAltitude)
        {
            if (placemarkViewModel == null) {
                throw new ArgumentNullException("placemarkViewModel");
            }

            ValidateObjectType(placemarkViewModel);
            ValidatePlacemarkData(placemarkViewModel);
            return RunLoad(placemarkViewModel, startLatitude, startLongitude, startAltitude);
        }
        
        protected abstract GameObject RunLoad(PlacemarkViewModel placemarkViewModel, double startLatitude, double startLongitude, double startAltitude);

        /// <summary>
        /// Validates if placemarkViewModel has appropriate object type for loader.
        /// </summary>
        /// <param name="placemarkViewModel"></param>
        protected virtual void ValidateObjectType(PlacemarkViewModel placemarkViewModel)
        {
            if (placemarkViewModel.PrimaryObjectType != _objectType) {
                throw new Exception(string.Format("Can't load domain with id {0} and type {1}, since loader is for type {2}",
                    placemarkViewModel.Id, placemarkViewModel.PrimaryObjectType, _objectType));
            }
        }

        /// <summary>
        /// Checks if placemarkViewModel has any issues with data that prevent his loading.
        /// </summary>
        protected abstract void ValidatePlacemarkData(PlacemarkViewModel placemarkViewModel);

        /// <summary>
        /// Validate if placemarkViewModel has appropriate geometry.
        /// </summary>
        protected void ValidateGeometryType(PlacemarkViewModel placemarkViewModel, GeometryType geometryType)
        {
            ValidateGeometryType(placemarkViewModel, new List<GeometryType> { geometryType });
        }

        /// <summary>
        /// Validate if placemarkViewModel has appropriate geometry.
        /// </summary>
        protected void ValidateGeometryType(PlacemarkViewModel placemarkViewModel, IList<GeometryType> expectedGeometries)
        {
            var isValidGeometry = expectedGeometries.Contains(placemarkViewModel.GeometryType)
                && placemarkViewModel.Geometry != null
                && expectedGeometries.Contains(placemarkViewModel.Geometry.GeometryType);

            if (!isValidGeometry) {
                throw new Exception(string.Format("Invalid geometry of the placemarkViewModel with id {0}, required geometry type is {1}",
                    placemarkViewModel.Id, string.Join(" or ", expectedGeometries.Select(g => g.ToString()).ToArray())));
            }
        }

        protected Vector3 GetWorldCoordinatesFromGeodetic(double latitude, double longitude, double altitude,
            double latitude0, double longitude0, double altitude0)
        {
            double x;
            double y;
            double z;

            GpsUtils.GeodeticToEnu(latitude, longitude, altitude, latitude0, longitude0, altitude0, out x, out z, out y);

            return new Vector3((float)x, (float)y, (float)z);
        }
    }
}