﻿using System;
using System.Collections.Generic;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using UnityEngine;
using UnityEngine.Assertions;

namespace Assets._Project.Shared.Scripts.Infrastructure.Loaders
{
    public class SpecialObjectLoader : PlacemarkLoader
    {
        private readonly Dictionary<CustomType, PlacemarkLoader> _specialObjectLoaders;

        public SpecialObjectLoader(GameObject lateralObjectPrefab) : base(ObjectType.Custom)
        {
            _specialObjectLoaders = new Dictionary<CustomType, PlacemarkLoader> {
                { CustomType.Lateral, new LateralObjectLoader(lateralObjectPrefab) }
            };
        }

        protected override GameObject RunLoad(PlacemarkViewModel placemarkViewModel, double startLatitude, double startLongitude, double startAltitude)
        {
            Assert.IsNotNull(placemarkViewModel);

            var objectTypeConfiguration = placemarkViewModel.ObjectTypeData as CustomDataViewModel;

            if (objectTypeConfiguration == null) {
                throw new Exception(string.Format("Object type configuration should be CustomDataDto for placemark '{0}'", placemarkViewModel.Id));
            }

            var loader = _specialObjectLoaders[objectTypeConfiguration.CustomType];

            return loader.Load(placemarkViewModel, startLatitude, startLongitude, startAltitude);
        }

        protected override void ValidatePlacemarkData(PlacemarkViewModel placemarkViewModel)
        {
            Assert.IsNotNull(placemarkViewModel);

            ValidateGeometryType(placemarkViewModel, GeometryType.Point);
        }
    }
}
