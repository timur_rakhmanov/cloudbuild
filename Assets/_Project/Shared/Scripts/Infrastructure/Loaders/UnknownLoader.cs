﻿using System;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using UnityEngine;
using UnityEngine.Assertions;
using Object = UnityEngine.Object;


namespace Assets._Project.Shared.Scripts.Infrastructure.Loaders
{
    public class UnknownLoader : LineObjectLoader
    {
        private readonly GameObject _unknownPointPrefab;

        public UnknownLoader(GameObject unknownPointPrefab, GameObject unknownLinePrefab, GameObject unknownLineProjectionPrefab) : base(unknownLinePrefab, unknownLineProjectionPrefab)
        {
            if (unknownPointPrefab == null) {
                throw new ArgumentNullException("unknownPointPrefab");
            }
            _unknownPointPrefab = unknownPointPrefab;
        }

        protected override GameObject RunLoad(PlacemarkViewModel placemarkViewModel, double startLatitude, double startLongitude, double startAltitude)
        {
            Assert.IsNotNull(placemarkViewModel);
            // TODO:22465 don't treat all multigeometry as line, better to check dominant geometry in such cases since multigeometry can have any geometry.
            if (placemarkViewModel.GeometryType == GeometryType.MultiGeometry || placemarkViewModel.GeometryType == GeometryType.Line) {
                placemarkViewModel.ObjectTypeData = new PipeDataViewModel { Diameter = 0.1, ObjectType = ObjectType.Unknown };
                return base.RunLoad(placemarkViewModel, startLatitude, startLongitude, startAltitude);
            }

            var position = GetWorldCoordinatesFromGeodetic(placemarkViewModel.Latitude, placemarkViewModel.Longitude, startAltitude,
                                                          startLatitude, startLongitude, startAltitude);
            position.y = position.y - Constants.USER_HEIGHT;

            var gameObject = Object.Instantiate(_unknownPointPrefab);
            gameObject.transform.position = position;

            Debug.Log(string.Format("Load unknown placemarkViewModel with id {0}, external id {1}, coordinates {2} {3}, unity position {4}, {5}, {6}",
                                    placemarkViewModel.Id, placemarkViewModel.ExternalId, placemarkViewModel.Latitude, placemarkViewModel.Longitude, position.x, position.y, position.z));

            return gameObject;
        }

        protected override void ValidateObjectType(PlacemarkViewModel placemarkViewModel)
        {
            //We reuse pipe here so have to override type validation.
            if (placemarkViewModel.PrimaryObjectType != ObjectType.Unknown) {
                throw new Exception(string.Format("Can't load domain with id {0} and type {1}, since loader is for type {2}",
                                                  placemarkViewModel.Id, placemarkViewModel.PrimaryObjectType, ObjectType.Unknown));
            }
        }

        protected override void ValidatePlacemarkData(PlacemarkViewModel placemarkViewModel)
        {
        }
    }
}