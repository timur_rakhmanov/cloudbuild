﻿using System;
using HoloToolkit.Unity;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure
{
    public class MeshLibraryProvider : Singleton<MeshLibraryProvider>
    {
        private const string MESHES_FOLDER_RESOURCE_PATH = "MeshLibrary/Meshes";
        private const string IMAGES_FOLDER_RESOURCE_PATH = "MeshLibrary/Images";

        public GameObject GetMeshPrefab(string fileName)
        {
            var mesh = Resources.Load<GameObject>(string.Format("{0}/{1}", MESHES_FOLDER_RESOURCE_PATH, fileName));

            if (mesh == null) {
                throw new Exception(string.Format("File with name '{0}' does not exist.", fileName));
            }

            return mesh;
        }

        public Sprite GetMeshSpritePrefab(string fileName)
        {
            var spritePrefab = Resources.Load<Sprite>(string.Format("{0}/{1}", IMAGES_FOLDER_RESOURCE_PATH, fileName));

            if (spritePrefab == null) {
                throw new Exception(string.Format("Sprite with name '{0}' does not exist.", fileName));
            }

            return spritePrefab;
        }
    }
}