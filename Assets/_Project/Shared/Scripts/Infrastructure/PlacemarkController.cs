﻿using System;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Infrastructure.Highlights;
using HoloToolkit.Unity.InputModule;
using Newtonsoft.Json;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure
{
    public class PlacemarkController : MonoBehaviour, IInputClickHandler, ISpeechHandler
    {
        private const string SELECT_VOICE_COMMAND = "select";
        
        #region Public data

        /// <summary>
        /// Placemark view model.
        /// </summary>
        public PlacemarkViewModel PlacemarkViewModel { get; set; }

        /// <summary>
        /// Placemark highlighter
        /// </summary>
        public IHighlighter Highlighter { get; set; }

        #endregion

        #region Commands

        /// <summary>
        /// Highlight placemark object
        /// </summary>
        /// <param name="color">Color to highlight</param>
        public void HighlightObject(Color32 color)
        {
            Highlighter.HighlightObject(gameObject, color);
        }

        /// <summary>
        /// Undo highlight placemark object
        /// </summary>
        public void UndoHighlightObject()
        {
            Highlighter.UndoHighlightObject();
        }

        #endregion

        #region Event handlers

        public void OnSpeechKeywordRecognized(SpeechKeywordRecognizedEventData eventData)
        {
            if (eventData.RecognizedText.Equals(SELECT_VOICE_COMMAND, StringComparison.OrdinalIgnoreCase)) {
                LogPlacemarkInfo();

                PlacemarksManager.Instance.SelectPlacemark(this, true);
            }
        }

        public void OnInputClicked(InputClickedEventData eventData)
        {
            LogPlacemarkInfo();

            PlacemarksManager.Instance.SelectPlacemark(this, true);
        }

        #endregion

        #region Private methods

        private void LogPlacemarkInfo()
        {
            if (PlacemarkViewModel == null) {
                Debug.Log("PlacemarkViewModel is null.");
                return;
            }

            var oldDescription = PlacemarkViewModel.Description;

            PlacemarkViewModel.Description = "stub";
            var placemarkText = JsonConvert.SerializeObject(PlacemarkViewModel);
            placemarkText = placemarkText.Replace(",", ",\r\n");
            Debug.Log(placemarkText);

            PlacemarkViewModel.Description = oldDescription;
        }

        #endregion
    }
}