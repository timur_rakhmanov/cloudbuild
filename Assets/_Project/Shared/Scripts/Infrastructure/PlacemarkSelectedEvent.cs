﻿using System;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using UnityEngine.Events;


namespace Assets._Project.Shared.Scripts.Infrastructure
{
    [Serializable]
    public class PlacemarkSelectedEvent : UnityEvent<PlacemarkViewModel>
    {
    }
}
