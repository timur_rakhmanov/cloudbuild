﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets._Project.Components.Conference.Shared.Scripts.ConferenceManager;
using Assets._Project.Components.Conference.Shared.Scripts.ConferenceManager.Dto;
using Assets._Project.Components.Loader.Shared.Scripts;
using Assets._Project.Shared.Scripts.APIClient;
using Assets._Project.Shared.Scripts.APIClient.ViewModels;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Extensions;
using Assets._Project.Shared.Scripts.Core.Models;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Assets._Project.Shared.Scripts.GlobalManagers;
using Assets._Project.Shared.Scripts.Identity;
using Assets._Project.Shared.Scripts.Infrastructure.Highlights.Factory;
using Assets._Project.Shared.Scripts.Infrastructure.Loaders;
using Assets._Project.Shared.Scripts.Ui;
using HoloToolkit.Unity;
using UnityEngine;
using Assert = UnityEngine.Assertions.Assert;

#if UNITY_IOS
using TouchScript.Gestures;
#endif


namespace Assets._Project.Shared.Scripts.Infrastructure
{
    // TODO: Consider to use interfaces.
    public class PlacemarksManager : Singleton<PlacemarksManager>
    {
        private const double DEFULT_START_ALTITUDE = 0;

        #region   Editor configurations

        public LoaderController Loader;
        public GameObject ConferenceManager;

        #region ObjectPrefabs

        public GameObject SewerPipePrefab;
        public GameObject PipeProjectionPrefab;
        public GameObject UnknownPointPrefab;
        public GameObject Object3DPlanePrefab;
        public GameObject SquarePlanePrefab;
        public GameObject OnGroundObjectCircleUndergraoundPart;
        public GameObject OnGroundObjectSquareUndergraoundPart;
        public GameObject AboveGroundObjectPrefab;
        public GameObject BelowGroundObjectSpherePrefab;
        public GameObject Object3DProjectionPrefab;
        public GameObject LateralObjectPrefab;

        #endregion

        #region ObjectIcons

        public Sprite DefaultIcon;
        public Sprite AreaIcon;
        public Sprite PipeIcon;
        public Sprite UnknownIcon;
        public Sprite ThreeDimensionalObjectIcon;

        #endregion

        #endregion

        #region Private data

        private ApiClient _apiClient;
        private Dictionary<ObjectType, PlacemarkLoader> _loaders;
        private Dictionary<ObjectType, Sprite> _icons;
        private Dictionary<ObjectType, HighlighterFactory> _highlighterFactories;
        private PlacemarkLoader _defaultLoader;
        private IConferenceManager _conferenceManager;
        private ISceneLayersManager _sceneLayersManager;

        private readonly IList<GameObject> _loadedPlacemarks = new List<GameObject>();
        private IList<PlacemarkViewModel> _cachedPlacemarks;

        private PlacemarkController _selectedPlacemarkController;

        #endregion

        public PlacemarkSelectedEvent OnPlacemarkSelected;
        public PlacemarkDeselectedEvent OnPlacemarkDeselected;

        #region   Mono behaviour pipeline

        private void Start()
        {
            _apiClient = new ApiClient();
            _conferenceManager = ConferenceManager.GetComponent<IConferenceManager>();
            Assert.IsNotNull(_conferenceManager);
            _sceneLayersManager = SceneLayersManager.Instance;
            Assert.IsNotNull(_sceneLayersManager);

            _icons = new Dictionary<ObjectType, Sprite> {
                { ObjectType.Area, AreaIcon },
                { ObjectType.Pipe, PipeIcon },
                { ObjectType.Unknown, UnknownIcon },
                { ObjectType.Object3D, ThreeDimensionalObjectIcon}
            };

            _loaders = new Dictionary<ObjectType, PlacemarkLoader> {
                { ObjectType.Unknown, new UnknownLoader(UnknownPointPrefab, SewerPipePrefab, PipeProjectionPrefab) },
                { ObjectType.Object3D, new ThreeDimensionalObjectLoader(
                    Object3DPlanePrefab, 
                    SquarePlanePrefab,
                    AboveGroundObjectPrefab, 
                    OnGroundObjectCircleUndergraoundPart, 
                    OnGroundObjectSquareUndergraoundPart, 
                    BelowGroundObjectSpherePrefab,
                    Object3DProjectionPrefab) },
                { ObjectType.Pipe, new LineObjectLoader(SewerPipePrefab, PipeProjectionPrefab) },
                { ObjectType.Area, new AreaLoader() },
                { ObjectType.Custom, new SpecialObjectLoader(LateralObjectPrefab) },
                { ObjectType.Model3D, new ModelLoader() }
            };

            _highlighterFactories = new Dictionary<ObjectType, HighlighterFactory> {
                { ObjectType.Area, new StubHighlighterFactory() },
                { ObjectType.Unknown,  new StubHighlighterFactory() },
                { ObjectType.Pipe, new MainTextureHighlighterFactory() },
                { ObjectType.Object3D, new PointWithoutProjectionHighlighterFactory() }
            };

            _defaultLoader = new DefaultLoader(UnknownPointPrefab);
        }

        #endregion

        /// <summary>
        /// Load scene at the specified location.
        /// </summary>
        public IEnumerator LoadScene(string companySlug, double latitude, double longitude, int radius, float azimuth,
            Vector3 initialUserPosition, Vector3 initialUserRotationAngles, SceneLayersOptions sceneLayersOptions = null)
        {
            Debug.Log(string.Format("Start load scene {0} {1} {2} {3}", companySlug, latitude, longitude, radius));

            Loader.ShowLoader();

            CleanScene();
            ResetScenePosition();

            var getPlacemarksTask = GetPlacemarks(companySlug, latitude, longitude, radius).AsTask<ICollection<PlacemarkViewModel>>();
            yield return getPlacemarksTask.Run();
            var placemarks = getPlacemarksTask.Result.ToList();

            _cachedPlacemarks = placemarks;

            var filteredPlacemarks = FilterPlacemarks(placemarks, sceneLayersOptions).ToList();

            var loadPlacemarksTask = LoadPlacemarks(filteredPlacemarks, latitude, longitude).AsTask();
            yield return loadPlacemarksTask.Run();
            loadPlacemarksTask.Wait();

            MovePlacemarksRoot(azimuth, initialUserPosition, initialUserRotationAngles);
            // TODO: uncomment this this time)
            _conferenceManager.OnPlacemarkSelected(PlacemarkSelected);
            _conferenceManager.OnPlacemarkDeselected(PlacemarkDeselected);

            _sceneLayersManager.OnActiveSceneLayersChanged.AddListener(ActiveSceneLayersChanged);

            Loader.HideLoader();
        }

        public void SelectPlacemark(PlacemarkController placemarkController, bool sendConferenceNotification)
        {
            if (_selectedPlacemarkController != null) {
                DeselectPlacemark(sendConferenceNotification);
            }

            if (placemarkController.PlacemarkViewModel == null) {
                return;
            }

            _selectedPlacemarkController = placemarkController;

            placemarkController.HighlightObject(ColorConverter.HexToColor(placemarkController.PlacemarkViewModel.MainColor));

            OnPlacemarkSelected.Invoke(placemarkController.PlacemarkViewModel);

            if (_conferenceManager.IsUserInConference() && sendConferenceNotification) {
                _conferenceManager.NotifyThatPlacemarkIsSelected(new PlacemarkIdDto {
                    DataSourceId = placemarkController.PlacemarkViewModel.DataSourceId,
                    CompanyId = placemarkController.PlacemarkViewModel.CompanyId,
                    Id = placemarkController.PlacemarkViewModel.Id
                });
            }
        }

        public void DeselectPlacemark(bool sendConferenceNotification)
        {
            if (_selectedPlacemarkController == null) {
                return;
            }

            _selectedPlacemarkController.UndoHighlightObject();

            OnPlacemarkDeselected.Invoke();

            _selectedPlacemarkController = null;

            if (_conferenceManager.IsUserInConference() && sendConferenceNotification) {
                _conferenceManager.NotifyThatPlacemarkDeselected();
            }
        }

        /// <summary>
        /// Return icon that attach to object type
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public Sprite GetObjectIcon(ObjectType objectType)
        {
            var isDefaultIcon = !_icons.ContainsKey(objectType);

            if (isDefaultIcon) {
                Debug.LogWarningFormat("Can't get icon for object type {0}.",
                                       objectType);
            }

            return isDefaultIcon ? DefaultIcon : _icons[objectType];
        }

        public IEnumerator DownloadModelGeometry(PlacemarkViewModel viewModel)
        {
            var modelGeometry = viewModel.Geometry as ModelGeometry;
            if (modelGeometry == null) {
                Debug.LogWarningFormat("Missing geometry for model: {0}", viewModel.Id);
                yield break;
            }

            var textures = new Dictionary<string, string>();
            foreach (var str in modelGeometry.Resources) {
                textures.Add(str.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault(), str);
            }
            var task = DownloadModel(modelGeometry.Link, textures).AsTask<GameObject>();
            yield return task.Run();
            yield return task.Result;
        }

        public GameObject ClonePlacemarkGameObject(GameObject originalPlacemark)
        {
            var clonedPlacemark = Instantiate(originalPlacemark, originalPlacemark.transform.parent, false);

            _loadedPlacemarks.Add(clonedPlacemark);

            return clonedPlacemark;
        }

        public void DestroyPlacemarkGameObject(GameObject placemarkGameObject)
        {
            _loadedPlacemarks.Remove(placemarkGameObject);

            Destroy(placemarkGameObject);
        }

        /// <summary>
        /// Returns root of the infrastructure.
        /// </summary>
        /// <returns></returns>
        public GameObject GetPlacemarksRoot()
        {
            var root = GameObject.Find(Constants.ROOT);
            var placemarksRoot = root.transform.Find(Constants.PLACEMARKS_ROOT).gameObject;
            Assert.IsNotNull(placemarksRoot);
            return placemarksRoot;
        }

        #region Private methods

        #region Model geometry downloading

        private IEnumerator DownloadModel(string url, Dictionary<string, string> textures)
        {
            var fileTask = DownloadFile(url).AsTask<string>();
            yield return fileTask.Run();
            var content = fileTask.Result;
            if (string.IsNullOrEmpty(content)) {
                yield break;
            }
            var go = ColladaImporter.Import(content);
            go.SetActive(false);
            var standardShader = Shader.Find("Standard");
            foreach (Renderer r in go.GetComponentsInChildren<Renderer>(true)) {
                foreach (Material m in r.materials) {
                    m.shader = standardShader;
                    if (m.mainTexture != null) {
                        var splittedName = m.mainTexture.name.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();
                        if (!textures.ContainsKey(splittedName)) {
                            continue;
                        }

                        var textureTask = DownloadTexture(textures[splittedName]).AsTask<Texture2D>();
                        yield return textureTask.Run();
                        try {
                            var texture = textureTask.Result;
                            if (texture != null) {
                                m.mainTexture = texture;
                            }
                        } catch (Exception ex) {
                            Debug.LogWarning("Error on downloading texture " + textures[m.mainTexture.name] + " " + ex.Message);
                        }
                    }
                }
            }

            yield return go;
        }

        //TODO: Do a wrapper fo such requests
        private IEnumerator DownloadTexture(string url)
        {
            WWW www = new WWW(url);
            yield return www;
            if (www.error != null) {
                throw new Exception(www.error);
            }
            yield return www.texture;
        }

        private IEnumerator DownloadFile(string url)
        {
            WWW www = new WWW(url);
            yield return www;
            if (www.error != null) {
                throw new Exception(www.error);
            }
            yield return www.text;
        }

        #endregion

        private IEnumerator GetPlacemarks(string companySlug, double latitude, double longitude, int radius)
        {
            // Get placemarks from api for company.
            var getPlacemarksTask = _apiClient.GetPlacemarksInRadius(companySlug, latitude, longitude, (short)radius)
                .AsTask<ICollection<PlacemarkViewModel>>();
            yield return getPlacemarksTask.Run();
            yield return getPlacemarksTask.Result;
        }

        private IEnumerable<PlacemarkViewModel> FilterPlacemarks(IList<PlacemarkViewModel> placemarks, SceneLayersOptions sceneLayersOptions)
        {
            if (placemarks == null) {
                throw new ArgumentException("Argument 'placemarks' cannot be null, for filter placemarks");
            }

            if (sceneLayersOptions == null) {
                return placemarks;
            }

            var filteredPlacemarks = new List<PlacemarkViewModel>();

            // Show default filter
            if (sceneLayersOptions.ShowDefault) {
                filteredPlacemarks.AddRange(placemarks.Where(p => !p.SceneLayer.HasValue));
            }

            // Enabled layers filtered
            if (sceneLayersOptions.ActiveSceneLayers != null) {
                filteredPlacemarks.AddRange(placemarks.Where(p => p.SceneLayer.HasValue && sceneLayersOptions.ActiveSceneLayers.Contains(p.SceneLayer.Value)));
            }


            // TODO:22191 consider to use such implementation.
            //var filteredPlacemarks2 = placemarks
            //    .Where(p =>
            //               p.SceneLayer.HasValue
            //               && sceneLayersOptions.ActiveSceneLayers.Contains(p.SceneLayer.Value)
            //               || (sceneLayersOptions.ShowDefault && !p.SceneLayer.HasValue)).ToList();

            return filteredPlacemarks;
        }

        /// <summary>
        /// Load placemarks got from source. 
        /// </summary>
        /// <returns></returns>
        private IEnumerator LoadPlacemarks(ICollection<PlacemarkViewModel> placemarks, double latitude, double longitude)
        {
            const int LOAD_CHUNK = 3;
            Debug.Log(string.Format("Get {0} placemarks from server.", placemarks.Count));

            // Load all placemarks on scene.
            // TODO: get root.
            var placemarksRoot = GetPlacemarksRoot();
            foreach (var placemarksChunk in placemarks.Where(x => x.PrimaryObjectType != ObjectType.Model3D).Batch(LOAD_CHUNK)) {
                placemarksChunk.Select(p => LoadPlacemark(p, placemarksRoot, latitude, longitude, DEFULT_START_ALTITUDE))
                    .Where(p => p != null)
                    .ToList().ForEach(_loadedPlacemarks.Add);
                yield return null;
            }

            foreach (var placemark in placemarks.Where(x => x.PrimaryObjectType == ObjectType.Model3D)) {
                var task = LoadBuilding(placemark, placemarksRoot, latitude, longitude, DEFULT_START_ALTITUDE).AsTask();
                yield return  task.Run();
                task.Wait();
            }

            Debug.Log(string.Format("End load scene, loaded {0} placemarks.", placemarks.Count));
        }
        
        /// <summary>
        /// Load building
        /// </summary>
        private IEnumerator LoadBuilding(PlacemarkViewModel placemarkViewModel, GameObject root, double startLatitude, double startLongitude, double startAltitude)
        {
            var buildingLoader = _loaders[ObjectType.Model3D];
            var positionGo = buildingLoader.Load(placemarkViewModel, startLatitude, startLongitude, startAltitude);
            var dowloadModelTask = DownloadModelGeometry(placemarkViewModel).AsTask<GameObject>();
            yield return dowloadModelTask.Run();
            var buildingGo = dowloadModelTask.Result;
            var modelGeometry = placemarkViewModel.Geometry as ModelGeometry;
            buildingGo.transform.position += positionGo.transform.position;
            buildingGo.transform.localRotation = Quaternion.Euler(buildingGo.transform.localRotation.eulerAngles + Vector3.up * modelGeometry.Heading);
            buildingGo.transform.SetParent(root.transform, false);
            buildingGo.SetActive(true);
            _loadedPlacemarks.Add(buildingGo);
        }

        /// <summary>
        /// Move placemarks according to current user position
        /// </summary>
        private void MovePlacemarksRoot(float azimuth, Vector3 initialUserPosition, Vector3 initialUserRotationAngles)
        {
            var placemarksRoot = GetPlacemarksRoot();
            // Move placemarks according to current user position, since now it is placed related to 0, 0, 0 coordinates.
            placemarksRoot.transform.position += initialUserPosition;
            // Rotate placemarks based on the selected azimuth, now they are placed according to the north.
            placemarksRoot.transform.rotation = Quaternion.Euler(0, initialUserRotationAngles.y - azimuth, 0);
        }

        private HighlighterFactory GetObjectHighlightMode(ObjectType objectType)
        {
            var isDefaultHighlightMode = !_highlighterFactories.ContainsKey(objectType);

            return isDefaultHighlightMode ? new PointObjectHighlighterFactory() : _highlighterFactories[objectType];
        }

        // Clean current scene.
        private void CleanScene()
        {
            _loadedPlacemarks.ToList().ForEach(Destroy);
        }

        // Reset scene position and roation.
        private void ResetScenePosition()
        {
            var placemarksRoot = GetPlacemarksRoot();
            // TODO: Move it somewhere. It's just hotfix for buildings
            placemarksRoot.transform.position = Vector3.zero;
            placemarksRoot.transform.rotation = Quaternion.identity;
        }

        private GameObject LoadPlacemark(PlacemarkViewModel placemarkViewModel, GameObject root, double startLatitude, double startLongitude, double startAltitude)
        {
            Assert.IsNotNull(placemarkViewModel);
            Assert.IsNotNull(root);

            var isDefaultLoader = !_loaders.ContainsKey(placemarkViewModel.PrimaryObjectType);

            if (isDefaultLoader) {
                Debug.LogWarningFormat("Can't detect loader for placemarkViewModel with domain type {0} and id {1}, default loader will be used.",
                                       placemarkViewModel.PrimaryObjectType, placemarkViewModel.Id);
            }

            var loader = isDefaultLoader ? _defaultLoader : _loaders[placemarkViewModel.PrimaryObjectType];
            var placemarkGameObject = LoadPlacemarkWithLoader(loader, placemarkViewModel, root, startLatitude, startLongitude, startAltitude);
            if (placemarkGameObject == null && !isDefaultLoader) {
                // Try to load with default.
                placemarkGameObject = LoadPlacemarkWithLoader(_defaultLoader, placemarkViewModel, root,
                                                              startLatitude, startLongitude, startAltitude);

                if (IdentityManager.Instance.GetDeviceRole() == DeviceRole.SuperDevice) {
                    placemarkGameObject.GetComponentInChildren<Renderer>().material.color = Color.red;
                }
            }

#if UNITY_IOS
            var tapGesture = placemarkGameObject.AddComponent<TapGesture>();
            tapGesture.Tapped += (sender, args) => SelectPlacemark(placemarkGameObject.GetComponent<PlacemarkController>(), true);
#endif

            return placemarkGameObject;
        }

        private GameObject LoadPlacemarkWithLoader(PlacemarkLoader loader, PlacemarkViewModel placemarkViewModel, GameObject root, double startLatitude, double startLongitude, double startAltitude)
        {
            // We don't stop work if some placemarks is failed to load.
            try {
                var placemarkGameObject = loader.Load(placemarkViewModel, startLatitude, startLongitude, startAltitude);
                placemarkGameObject.transform.SetParent(root.transform, false);
                var placemarkController = placemarkGameObject.GetComponent<PlacemarkController>();
                placemarkController.PlacemarkViewModel = placemarkViewModel;
                placemarkController.Highlighter = GetObjectHighlightMode(placemarkViewModel.PrimaryObjectType).CreateHighlighter();
                return placemarkGameObject;
            } catch (Exception ex) {
                Debug.LogException(ex);
            }
            return null;
        }

        private void PlacemarkSelected(PlacemarkIdDto placemarkIdDto)
        {
            var root = GameObject.Find(Constants.ROOT);
            var meemimMapManager = root.GetComponentInChildren<IMeemimMapManager>();
            if (meemimMapManager == null) {
                Debug.LogWarningFormat("Meemim map doesn't exist");
                return;
            }

            // TODO:22864 not sure if we need to check if placemark on the 3d map (but for now conference only can be when map is loaded, so probably it is ok,
            // but better to hide this on meemim manager. Or  make this relation not so high coupled.
            var meemimMapPlacemarks = meemimMapManager.GetMeemimMapPlacemarks();

            var selectedPlacemark = meemimMapPlacemarks.SingleOrDefault(p => p.PlacemarkViewModel.CompanyId.Equals(placemarkIdDto.CompanyId) &&
                                                                             p.PlacemarkViewModel.DataSourceId.Equals(placemarkIdDto.DataSourceId) &&
                                                                             p.PlacemarkViewModel.Id.Equals(placemarkIdDto.Id));

            if (selectedPlacemark == null) {
                Debug.LogWarningFormat("Placemark with id '{2}', for company '{0}', in data source '{1}' is not loaded", 
                    placemarkIdDto.CompanyId, placemarkIdDto.DataSourceId, placemarkIdDto.Id);
                return;
            }

            SelectPlacemark(selectedPlacemark, false);
        }

        private void PlacemarkDeselected()
        {
            DeselectPlacemark(false);
        }

        private void ActiveSceneLayersChanged(SceneLayersOptions sceneLayersOptions)
        {
            Loader.ShowLoader();

            CleanScene();
            
            var filteredPlacemarks = FilterPlacemarks(_cachedPlacemarks, sceneLayersOptions).ToList();

            var userLocation = UserLocationManager.Instance.GetUserLocation();

            LoadPlacemarks(filteredPlacemarks, userLocation.Latitude, userLocation.Longitude).AsTask().RunCoroutine(this);

            Loader.HideLoader();
        }

        #endregion
    }
}