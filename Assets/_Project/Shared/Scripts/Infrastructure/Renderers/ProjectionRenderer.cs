﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure.Renderers
{
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshFilter))]
    public class ProjectionRenderer : MonoBehaviour
    {
        [Serializable]
        public class ProjectionVertex
        {
            public Vector3 Point;
            public Color Color;

            public ProjectionVertex(Vector3 point, Color color)
            {
                Point = point;
                Color = color;
            }
        }

        //3 _______________________ 0
        // |                     / | 
        // |                  /    | 
        // |               /       | 
        // |            /          | 
        // |         /             | 
        // |      /                | 
        // |   /                   | 
        //2|/_____________________ | 1

        // Vertex order in polyg.

        // ^  0 _______________________ 1
        // |   |                      /|
        // |   |                   /   |
        // |   |                 /     |
        // |   |               /       |
        // | Y |             /         |
        // |   |           /           |
        // |   |         /             |
        // |   |      /                |
        // |   |   /                   |
        // |   |/______________________|
        //    3           X             2
        //    ------------------------->

        // Line is formed by to segment. Each segment connit from 2 polygons and 4 vertexes ( 2 vertexes are shared)
        // Vertex arrat is formed of list of vertexes grouped by 4.


        public ProjectionVertex[] Vertices;
        public Material Material;
        public float DashLength = 0.05f;
        public float DashWidth = 0.025f;
        public bool ShowDirection = false;
        public bool Direction = false;
        public Vector3 DirectionScale = Vector3.one;
        public bool IsMapProjection = false;
        public int DirectionSteps = 10;
        public GameObject DirectionArrowPrefab;
        public Color ProjectionColor;
        public bool Build = false;

        private MeshRenderer _projectionMeshRenderer;

        void Start()
        {
            _projectionMeshRenderer = gameObject.GetComponent<MeshRenderer>();

            _projectionMeshRenderer.material = Material;
            _projectionMeshRenderer.material.color = ProjectionColor;
        }

        public void Rebuild()
        {
            Build = false;
        }

        void LateUpdate()
        {
            if (Vertices == null || Vertices.Length <= 1) {
                return;
            }

            if (Build) {
                return;
            }

            var mesh = GetComponent<MeshFilter>().mesh;

            if (!mesh) {
                mesh = new Mesh();
            }

            mesh.vertices = CreateMeshVertices().ToArray();

            mesh.triangles = CreateMeshTriangles();

            mesh.uv = CreateMeshUvs(mesh.vertices.Length);

            mesh.RecalculateNormals();

            Build = true;
        }

        private Vector2[] CreateMeshUvs(int meshVerticesLength)
        {
            var uvs = new Vector2[meshVerticesLength];

            float totalLength = 0;

            for (var i = 1; i < Vertices.Length; ++i) {
                totalLength += (Vertices[i].Point - Vertices[i - 1].Point).magnitude;
            }

            //Debug.Log(string.Format("l {0} n {1}, length {2}", meshVertices.Length, Vertices.Length - 1, totalLength));

            var startLength = 0.0f;
            for (var i = 0; i < meshVerticesLength; i += 4) {
                var currentSegmentNumber = i / 4;
                var currentSegmentLength = (Vertices[currentSegmentNumber].Point - Vertices[currentSegmentNumber + 1].Point).magnitude;

                uvs[i] = new Vector2(0, (startLength + currentSegmentLength) / totalLength);
                uvs[i + 1] = new Vector2(1, (startLength + currentSegmentLength) / totalLength);
                uvs[i + 2] = new Vector2(1, startLength / totalLength);
                uvs[i + 3] = new Vector2(0, startLength / totalLength);

                startLength += currentSegmentLength;
            }
            //Debug.LogFormat(string.Format("1: {0}", string.Join(",", uvs.Select(v => string.Format("({0} {1})", v.x, v.y)).ToArray())));
            //Debug.LogFormat(string.Format("2: {0}", string.Join(",", meshVertices.Select(v => string.Format("({0} {1})", v.x, v.z)).ToArray())));

            var tilesCount = totalLength / (DashLength * 2);

            if (ShowDirection) {
                RenderDirectionArrows(totalLength);
            }

            _projectionMeshRenderer.material.mainTextureScale = new Vector2(1, tilesCount);

            return uvs;
        }

        private int[] CreateMeshTriangles()
        {
            var triangles = new int[3 * 2 * (Vertices.Length - 1)];

            var lastIndex = 0;

            var lastTrianglesIndex = 0;

            for (var i = 0; i < (Vertices.Length - 1) * 2; i++) {
                if (i == 0) {
                    triangles[0] = lastIndex;
                    triangles[1] = lastIndex + 1;
                    triangles[2] = lastIndex + 2;

                    lastIndex = 2;
                    lastTrianglesIndex = 2;

                    continue;
                }

                if (triangles[lastTrianglesIndex] % 2 == 0) {
                    triangles[lastTrianglesIndex + 1] = lastIndex - 2;
                    triangles[lastTrianglesIndex + 2] = lastIndex;
                    triangles[lastTrianglesIndex + 3] = lastIndex + 1;
                } else {
                    triangles[lastTrianglesIndex + 1] = lastIndex + 1;
                    triangles[lastTrianglesIndex + 2] = lastIndex + 2;
                    triangles[lastTrianglesIndex + 3] = lastIndex + 3;
                }

                lastIndex = triangles[lastTrianglesIndex + 3];

                lastTrianglesIndex += 3;
            }

            return triangles;
        }

        private List<Vector3> CreateMeshVertices()
        {
            ProjectionVertex lastProjectionVertex = null;

            var vertices = new List<Vector3>();

            foreach (var vertex in Vertices) {
                if (lastProjectionVertex == null) {
                    lastProjectionVertex = vertex;
                    continue;
                }
                var vector1 = new Vector3(lastProjectionVertex.Point.x - vertex.Point.x, lastProjectionVertex.Point.y - vertex.Point.y, lastProjectionVertex.Point.z - vertex.Point.z);
                var vector2 = new Vector3(vertex.Point.x - lastProjectionVertex.Point.x, vertex.Point.y - lastProjectionVertex.Point.y, vertex.Point.z - lastProjectionVertex.Point.z);

                var verticesForCurrentPoint = CreateMeshVerticesPoints(vector1, vertex.Point);
                vertices.AddRange(verticesForCurrentPoint);

                verticesForCurrentPoint = CreateMeshVerticesPoints(vector2, lastProjectionVertex.Point);
                vertices.AddRange(verticesForCurrentPoint);

                lastProjectionVertex = vertex;
            }

            return vertices;
        }

        private void RenderDirectionArrows(float totalLength)
        {
            var distanceBetweenArrows = DirectionSteps * DashLength * 2;

            if (totalLength < distanceBetweenArrows) {
                var distanceFromTubeStart = 0f;
                for (var i = 1; i < Vertices.Length; ++i) {
                    var segment = Vertices[i].Point - Vertices[i - 1].Point;
                    var direction = segment.normalized;
                    var segmentLength = segment.magnitude;

                    distanceFromTubeStart += segmentLength;
                    if (distanceFromTubeStart > totalLength / 2) {
                        var distabceFromSegmentStart = Math.Round(segmentLength - (distanceFromTubeStart - totalLength / 2));

                        CreateArrowGameObject(Vertices[i - 1].Point, direction, (float) distabceFromSegmentStart);

                        return;
                    }
                }
            } else {
                for (var i = 1; i < Vertices.Length; ++i) {
                    var segment = Vertices[i].Point - Vertices[i - 1].Point;
                    var direction = segment.normalized;
                    var segmentLength = segment.magnitude;
                    var distanceFromStart = distanceBetweenArrows;

                    while (distanceFromStart < segmentLength) {
                        CreateArrowGameObject(Vertices[i - 1].Point, direction, distanceFromStart);

                        distanceBetweenArrows = DirectionSteps * DashLength * 2;
                        distanceFromStart += distanceBetweenArrows;
                    }

                    distanceBetweenArrows = distanceFromStart - segmentLength;
                }
            }
        }


        private void CreateArrowGameObject(Vector3 point, Vector3 direction, float distanceFromStart)
        {
            var arrowPosition = point + direction * distanceFromStart;

            var directionArrowGameObject = Instantiate(DirectionArrowPrefab);
            if (IsMapProjection) {
                directionArrowGameObject.AddComponent<ClippableObject>();
            }
            directionArrowGameObject.transform.SetParent(gameObject.transform);
            directionArrowGameObject.GetComponent<MeshRenderer>().material.color = ProjectionColor;
            directionArrowGameObject.transform.localPosition = arrowPosition;
            directionArrowGameObject.transform.localRotation = Quaternion.LookRotation(direction);

            directionArrowGameObject.transform.localScale = new Vector3(directionArrowGameObject.transform.localScale.x * DirectionScale.x, directionArrowGameObject.transform.localScale.y * DirectionScale.y, directionArrowGameObject.transform.localScale.z * DirectionScale.z);
            // Rotate quad according direction.
            var forward = new Vector3(90, 0, 0);
            var backward = new Vector3(90, 0, 180);
            directionArrowGameObject.transform.Rotate(Direction ? forward : backward);
        }

        private List<Vector3> CreateMeshVerticesPoints(Vector3 distanceVector, Vector3 vectorStartPoint)
        {
            Vector3 rotateVector;
            Vector3 distanceRotateVector;

            rotateVector = Quaternion.Euler(0, -90, 0) * distanceVector;
            distanceRotateVector = Vector3.Normalize(rotateVector) * DashWidth;
            var leftShiftedPoint = new Vector3(vectorStartPoint.x - distanceRotateVector.x, vectorStartPoint.y - distanceRotateVector.y, vectorStartPoint.z - distanceRotateVector.z);

            rotateVector = Quaternion.Euler(0, 90, 0) * distanceVector;
            distanceRotateVector = Vector3.Normalize(rotateVector) * DashWidth;
            var rightShiftedPoint = new Vector3(vectorStartPoint.x - distanceRotateVector.x, vectorStartPoint.y - distanceRotateVector.y, vectorStartPoint.z - distanceRotateVector.z);

            return new List<Vector3> { leftShiftedPoint, rightShiftedPoint };
        }
    }
}