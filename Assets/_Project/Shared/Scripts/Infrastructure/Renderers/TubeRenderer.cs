﻿using System;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Infrastructure.Renderers
{
    [RequireComponent(typeof(MeshRenderer))]
    [RequireComponent(typeof(MeshFilter))]
    public class TubeRenderer : MonoBehaviour
    {
        /*
        TubeRenderer.cs

        This script is created by Ray Nothnagel of Last Bastion Games. It is 
        free for use and available on the Unify Wiki.

        For other components I've created, see:
        http://lastbastiongames.com/middleware/

        (C) 2008 Last Bastion Games
        */

        [Serializable]
        public class TubeVertex
        {
            public Vector3 point = Vector3.zero;
            public float radius = 1.0f;
            public Color color = Color.white;

            public TubeVertex(Vector3 pt, float r, Color c)
            {
                point = pt;
                radius = r;
                color = c;
            }
        }
        /// <summary>
        /// Indicates if mesh collider should be generated.
        /// </summary>
        public bool generateMeshCollider;
        public TubeVertex[] Vertices;
        public Material Material;

        public int crossSegments = 3;
        private Vector3[] _crossPoints;
        private int _lastCrossSegments;

        private bool _isColliderGenerated = false;
        private bool _isBuild = false;

        void Start()
        {
            MeshRenderer mr = gameObject.GetComponent<MeshRenderer>();
            mr.material = Material;
        }

        void LateUpdate()
        {
            if (null == Vertices ||
                Vertices.Length <= 1) {
                GetComponent<Renderer>().enabled = false;
                return;
            }
            GetComponent<Renderer>().enabled = true;
            
            if (!_isBuild) {
                if (crossSegments != _lastCrossSegments) {
                    _crossPoints = new Vector3[crossSegments];
                    float theta = 2.0f * Mathf.PI / crossSegments;
                    for (int c = 0; c < crossSegments; c++) {
                        _crossPoints[c] = new Vector3(Mathf.Cos(theta * c), Mathf.Sin(theta * c), 0);
                    }
                    _lastCrossSegments = crossSegments;
                }

                Vector3[] meshVertices = new Vector3[Vertices.Length * crossSegments];
                Vector2[] uvs = new Vector2[Vertices.Length * crossSegments];
                Color[] colors = new Color[Vertices.Length * crossSegments];
                int[] tris = new int[Vertices.Length * crossSegments * 6];
                int[] lastVertices = new int[crossSegments];
                int[] theseVertices = new int[crossSegments];
                Quaternion rotation = Quaternion.identity;

                for (int p = 0; p < Vertices.Length; p++) {
                    if (p < Vertices.Length - 1) rotation = Quaternion.FromToRotation(Vector3.forward, Vertices[p + 1].point - Vertices[p].point);

                    for (int c = 0; c < crossSegments; c++) {
                        int vertexIndex = p * crossSegments + c;
                        meshVertices[vertexIndex] = Vertices[p].point + rotation * _crossPoints[c] * Vertices[p].radius;
                        uvs[vertexIndex] = new Vector2((0.0f + c) / crossSegments, (0.0f + p) / Vertices.Length);
                        colors[vertexIndex] = Vertices[p].color;

                        //				print(c+" - vertex index "+(p*crossSegments+c) + " is " + meshVertices[p*crossSegments+c]);
                        lastVertices[c] = theseVertices[c];
                        theseVertices[c] = p * crossSegments + c;
                    }
                    //make triangles
                    if (p > 0)
                    {
                        for (int c = 0; c < crossSegments; c++)
                        {
                            int start = (p * crossSegments + c) * 6;
                            tris[start] = lastVertices[c];
                            tris[start + 1] = lastVertices[(c + 1) % crossSegments];
                            tris[start + 2] = theseVertices[c];
                            tris[start + 3] = tris[start + 2];
                            tris[start + 4] = tris[start + 1];
                            tris[start + 5] = theseVertices[(c + 1) % crossSegments];
                            //					print("Triangle: indexes("+tris[start]+", "+tris[start+1]+", "+tris[start+2]+"), ("+tris[start+3]+", "+tris[start+4]+", "+tris[start+5]+")");
                        }
                    }
                }

                Mesh mesh = GetComponent<MeshFilter>().mesh;
                if (!mesh) {
                    mesh = new Mesh();
                }
                mesh.vertices = meshVertices;
                mesh.triangles = tris;
                mesh.RecalculateNormals();
                mesh.uv = uvs;

                if (generateMeshCollider && !_isColliderGenerated) {
                    var meshCollider = gameObject.GetComponent<MeshCollider>();
                    if (meshCollider == null) {
                        meshCollider = gameObject.AddComponent<MeshCollider>();
                    }
                    meshCollider.sharedMesh = mesh;
                    _isColliderGenerated = true;
                }

                _isBuild = true;
            }
        }
    }
}