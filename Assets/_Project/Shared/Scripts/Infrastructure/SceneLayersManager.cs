﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets._Project.Shared.Scripts.Core;
using HoloToolkit.Unity;


namespace Assets._Project.Shared.Scripts.Infrastructure
{
    public class SceneLayersManager : Singleton<SceneLayersManager>, ISceneLayersManager
    {
        #region Private data

        private SceneLayersOptions _cachedSceneLayersOptions;

        #endregion

        #region Events
        
        public ActiveSceneLayersChanged OnActiveSceneLayersChanged { get; set; }

        #endregion

        #region Mono behavior pipeline

        void Start()
        {
            OnActiveSceneLayersChanged = new ActiveSceneLayersChanged();
        }

        #endregion

        #region Commands

        public SceneLayersOptions GetSceneLayersOptions()
        {
            if (_cachedSceneLayersOptions == null || !ExecutionContext.Instance.Company.Id.Equals(_cachedSceneLayersOptions.CompanyId)) {
                UpdateCachedSceneLayers();
                return _cachedSceneLayersOptions;
            }

            return _cachedSceneLayersOptions;
        }

        public void SetSceneLayerOptions(Guid companyId, bool showDefault, List<Guid> activeSceneLayers)
        {
            _cachedSceneLayersOptions = new SceneLayersOptions(companyId, showDefault, activeSceneLayers);
            OnActiveSceneLayersChanged.Invoke(_cachedSceneLayersOptions);
        }

        #endregion

        #region Private methods

        private void UpdateCachedSceneLayers()
        {
            var companyViewModel = ExecutionContext.Instance.DeviceCompanies.Single(x => x.Id == ExecutionContext.Instance.Company.Id);

            _cachedSceneLayersOptions = new SceneLayersOptions(companyViewModel.Id, true);
            foreach (var layer in companyViewModel.Layers) {
                if (!layer.DisabledByDefault) {
                    _cachedSceneLayersOptions.ActiveSceneLayers.Add(layer.Id);
                }
            }
        }

        #endregion
    }
}