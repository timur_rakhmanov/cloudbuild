﻿using System;
using System.Collections.Generic;


namespace Assets._Project.Shared.Scripts.Infrastructure
{
    public class SceneLayersOptions
    {
        public SceneLayersOptions(Guid companyId)
        {
            CompanyId = companyId;
            ActiveSceneLayers = new List<Guid>();
        }

        public SceneLayersOptions(Guid companyId, bool showDefault)
        {
            CompanyId = companyId;
            ShowDefault = showDefault;
            ActiveSceneLayers = new List<Guid>();
        }

        public SceneLayersOptions(Guid companyId, bool showDefault, List<Guid> activeSceneLayers)
        {
            CompanyId = companyId;
            ShowDefault = showDefault;
            ActiveSceneLayers = activeSceneLayers;
        }

        public Guid CompanyId { get; set; }

        public bool ShowDefault { get; set; }
        
        public List<Guid> ActiveSceneLayers { get; private set; }
    }
}
