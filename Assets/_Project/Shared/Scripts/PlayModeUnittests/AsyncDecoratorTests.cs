﻿using System;
using System.Collections;
using System.Threading;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Meemim.VGis.Unity.Plugins.Interfaces.Dto;
using NUnit.Framework;


namespace Assets._Project.Shared.Scripts.PlayModeUnittests
{
    [TestFixture]
    public class AsyncDecoratorTests
    {
        private void TestAsyncMethod(Action<bool> resultAction, bool isSuccess = false)
        {
            new Thread(delegate () {
                resultAction(isSuccess);
            }).Start();
        }

        private void TestAsyncMethodWithException()
        {
            throw new Exception("Exception before start parallel execution.");
        }

        private void StartCoroutine(IEnumerator coroutine)
        {
            while (coroutine.MoveNext()) {
            }
        }

        /// <summary>
        /// Returns true if coroutine was finished and false in case of timeout
        /// </summary>
        /// <param name="coroutine"></param>
        /// <returns></returns>
        private bool StartCoroutineWithCancelationAfterTime(IEnumerator coroutine)
        {
            const double MAX_EXECUTION_TIME_IN_SECONDS = 1;
            var startTime = DateTime.UtcNow;
            while (coroutine.MoveNext()) {
                var span = (DateTime.UtcNow - startTime);
                if (span.TotalSeconds > MAX_EXECUTION_TIME_IN_SECONDS) {
                    return false;
                }
            }

            return true;
        }

        [Test]
        public void TestRun_WhenAsyncMethodThrowsException_ThrowsException()
        {
            var asyncDecorator = new AsyncDecorator<int>((success, failure) =>
                TestAsyncMethodWithException());

            var ex = Assert.Throws<Exception>(() => StartCoroutine(asyncDecorator.Run()));
            Assert.AreEqual("Exception before start parallel execution.", ex.Message);
        }

        [Test]
        public void TestRun_WhenAsyncMethodNewerStops_CoroutineISFreez()
        {
            // TODO: consider to add cancelation token in order to avoid this.
            var asyncDecorator = new AsyncDecorator<int>((success, failure) =>
                TestAsyncMethod((isSucces) => { }));

            var coroutineResult = StartCoroutineWithCancelationAfterTime(asyncDecorator.Run());
            Assert.IsFalse(coroutineResult);
        }
        [Test]
        public void TestRun_WhenActionReturnsSuccessResult_ReturnsResultAsEnumeratorValue()
        {
            var asyncDecorator = new AsyncDecorator<int>((success, failure) =>
                TestAsyncMethod((isSuccess) => {
                    if (isSuccess) {
                        success(7);
                    }
                    else {
                        failure(new ErrorReason("Something went wrong."));
                    }
                }, true));
            var enumerator = asyncDecorator.Run();
            StartCoroutine(enumerator);

            Assert.AreEqual(7, enumerator.Current);
        }

        [Test]
        public void TestRun_WhenActionReturnsErrorResult_ThrowsExceptionWithReason()
        {
            var asyncDecorator = new AsyncDecorator<int>((success, failure) =>
                TestAsyncMethod((isSuccess) => {
                    if (isSuccess) {
                        success(7);
                    }
                    else {
                        failure(new ErrorReason("Something went wrong."));
                    }
                }, false));
            var enumerator = asyncDecorator.Run();

            var ex = Assert.Throws<Exception>(() => StartCoroutine(enumerator));

            Assert.AreEqual("Async coroutine is failed, reason: Something went wrong.", ex.Message);
        }
    }
}