﻿using System;
using System.Collections;
using System.Threading;
using Assets._Project.Shared.Scripts.Core.Tasks;
using Meemim.VGis.Unity.Plugins.Interfaces.Dto;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;


namespace Assets._Project.Shared.Scripts.PlayModeUnittests
{
    [TestFixture]
    public class TaskIntegrationTests
    {
        /// <summary>
        /// Base mock for test tasks.
        /// </summary>
        private abstract class MonoBehaviourBaseMock : MonoBehaviour, IMonoBehaviourTest
        {
            public static Task Task { get; private set; }

            private const int MAX_FRAMES = 100;

            private int _currentFrame;

            private bool _isTestFinished;

            private bool _isTaskStarted;

            public bool IsTestFinished
            {
                get { return _currentFrame > MAX_FRAMES || _isTestFinished; }
            }

            /// <summary>
            /// Reset mock.
            /// </summary>
            public static void Reset()
            {
                Task = null;
            }

            protected abstract Task TaskFactory();

            private void Update()
            {
                ++_currentFrame;

                if (!_isTaskStarted) {
                    _isTaskStarted = true;

                    Task = TaskFactory().ContinueWith(t => {
                        _isTestFinished = true;
                    });

                    StartCoroutine(Task.Run());
                }
            }
        }

        private class CoroutineSequencesMock : MonoBehaviourBaseMock
        {
            private const int DEFAULT_RESULT = 3;
            public static bool ThrowException;
            public static int Result;

            public new static void Reset()
            {
                ThrowException = false;
                Result = DEFAULT_RESULT;
            }

            protected override Task TaskFactory()
            {
                return new Task<int>(CoroutineSequence(ThrowException, Result));
            }

            private IEnumerator CoroutineSequence(bool throwException = false, int result = 3)
            {
                yield return 1;
                yield return 2;
                if (throwException) {
                    throw new Exception("Exception in sequence.");
                }
                yield return result;
            }

        }

        private class NestedTasksMock : MonoBehaviourBaseMock
        {
            private const int DEFAULT_RESULT = 3;
            public static bool ThrowException;
            public static int Result;

            public new static void Reset()
            {
                ThrowException = false;
                Result = DEFAULT_RESULT;
            }

            protected override Task TaskFactory()
            {
                return new Task<int>(NestedTasks(ThrowException, Result));
            }

            private IEnumerator NestedTasks(bool throwException = false, int result = 3)
            {
                var t1 = new Task<int>(CoroutineSequence());
                yield return StartCoroutine(t1.Run());
                t1.Wait();

                var t2 = new Task<int>(CoroutineSequence(throwException, result));
                yield return StartCoroutine(t2.Run());

                yield return t2.Result;
            }

            private IEnumerator CoroutineSequence(bool throwException = false, int result = 3)
            {
                yield return 1;
                yield return 2;
                if (throwException) {
                    throw new Exception("Exception in sequence.");
                }
                yield return result;
            }

        }

        private class NestedCoroutinesMock : MonoBehaviourBaseMock
        {
            private const int DEFAULT_RESULT = 3;
            public static bool ThrowException;
            public static int Result;

            public new static void Reset()
            {
                ThrowException = false;
                Result = DEFAULT_RESULT;
            }

            protected override Task TaskFactory()
            {
                return new Task<int>(NestedCoroutines(ThrowException, Result));
            }

            private IEnumerator NestedCoroutines(bool throwException = false, int result = 3)
            {
                yield return StartCoroutine(CoroutineSequence(false, 3));
                yield return StartCoroutine(CoroutineSequence(throwException, result));
            }

            private IEnumerator CoroutineSequence(bool throwException = false, int result = 3)
            {
                yield return 1;
                yield return 2;
                if (throwException) {
                    throw new Exception("Exception in sequence.");
                }
                yield return result;
            }

        }

        private class StartToEndMock : MonoBehaviourBaseMock
        {
            protected override Task TaskFactory()
            {
                return new Task<int>(RunAsyncMethodAndGetResult());
            }

            private IEnumerator RunAsyncMethodAndGetResult()
            {
                yield return null;
                yield return null;

                var asyncRequest = new AsyncDecorator<int>((success, failure) =>
                    AsyncMethod((isSuccess) => {
                        if (isSuccess) {
                            success(7);
                        }
                        else {
                            failure(new ErrorReason("Something went wrong."));
                        }
                    }, true));
                // TODO: add AsTask extension that incapsulate task creation!

                var task = new Task<int>(asyncRequest.Run());
                yield return StartCoroutine(task.Run());

                yield return task.Result;
            }

            private void AsyncMethod(Action<bool> resultAction, bool isSuccess = false)
            {
                new Thread(delegate () {
                    resultAction(isSuccess);
                }).Start();
            }
        }

        [SetUp]
        public void SetUp()
        {
            MonoBehaviourBaseMock.Reset();
            CoroutineSequencesMock.Reset();
            NestedTasksMock.Reset();
            NestedCoroutinesMock.Reset();
        }

        private IEnumerator TestCoroutineSequence(bool thowException = false, int result = 3)
        {
            yield return 1;
            yield return 2;
            if (thowException) {
                throw new Exception("Exception in sequence.");
            }
            yield return result;
        }

        private IEnumerator TestFirstClassCoroutine(bool throwException = false)
        {
            yield return StartInnerCoroutine(TestCoroutineSequence(false, 3));
            yield return StartInnerCoroutine(TestCoroutineSequence(throwException, 7));
        }

        private IEnumerator StartInnerCoroutine(IEnumerator coroutine)
        {
            while (true) {
                if (!coroutine.MoveNext()) {
                    yield break;
                }
                if (coroutine.Current is IEnumerator) {
                    yield return StartInnerCoroutine((IEnumerator)coroutine.Current);
                }
                yield return coroutine.Current;
            }
        }
        
        [UnityTest]
        public IEnumerator TestWrapCoroutineSequenceInTask_WhenCoroutineReturnsResult_GetsResult()
        {
            yield return new MonoBehaviourTest<CoroutineSequencesMock>();

            var task = (Task<int>)MonoBehaviourBaseMock.Task;
            Assert.IsTrue(task.IsCompleted);
            Assert.IsFalse(task.IsFaulted);
            Assert.AreEqual(3, task.Result);
        }

        [UnityTest]
        public IEnumerator TestWrapCoroutineSequenceInTask_WhenCoroutineThrowsException_GetExceptionResult()
        {
            CoroutineSequencesMock.ThrowException = true;
            yield return new MonoBehaviourTest<CoroutineSequencesMock>();

            var task = MonoBehaviourBaseMock.Task;

            Assert.IsTrue(task.IsCompleted);
            Assert.IsTrue(task.IsFaulted);
            Assert.AreEqual("Exception in sequence.", task.Exception.Message);            
        }

        [UnityTest]
        public IEnumerator TestWrapNestedTasksInTask_WhenCoroutineReturnsResult_GetsResult()
        {
            NestedTasksMock.Result = 7;
            yield return new MonoBehaviourTest<NestedTasksMock>();

            var task = (Task<int>) MonoBehaviourBaseMock.Task;

            Assert.IsTrue(task.IsCompleted);
            Assert.IsFalse(task.IsFaulted);
            Assert.AreEqual(7, task.Result);
        }

        [UnityTest]
        public IEnumerator TestWrapNestedTasksInTask_InnerCoroutineThrowsException_ThrowsExceptionDuringWait()
        {
            NestedTasksMock.ThrowException = true;
            yield return new MonoBehaviourTest<NestedTasksMock>();

            var task = MonoBehaviourBaseMock.Task;

            Assert.IsTrue(task.IsCompleted);
            Assert.IsTrue(task.IsFaulted);
            Assert.AreEqual("Exception in sequence.", task.Exception.Message);
        }

        [UnityTest]
        public IEnumerator TestStartToEndTasks_WhenAsyncMethodsReturnsResult_GetResultsInTask()
        {
            yield return new MonoBehaviourTest<StartToEndMock>();

            var task = (Task<int>)MonoBehaviourBaseMock.Task;

            Assert.IsTrue(task.IsCompleted);
            Assert.IsFalse(task.IsFaulted);
            Assert.AreEqual(7, task.Result);
        }

#region Possible future improvements.
        [UnityTest]
        [Ignore("Now we don't support getting values from nested yield operations")]
        // TODO: consider to check values of nested coroutine (e.g. if Current is IEnumerator)
        public IEnumerator TestNestedCoroutines_ReturnsValueOfTheLatestCoroutine()
        {
            NestedCoroutinesMock.Result = 7;
            yield return new MonoBehaviourTest<NestedCoroutinesMock>();

            var task = (Task<int>)MonoBehaviourBaseMock.Task;

            Assert.AreEqual(7, task.Result);
        }

        [UnityTest]
        [Ignore("Now we don't support handle exception of nested coroutines, they should be explicitly wrapped in task")]
        public IEnumerator TestNestedCoroutines_WhenInnerCoroutineThrowsException_ThrowsExceptionDuringWait()
        {
            NestedCoroutinesMock.ThrowException = true;
            yield return new MonoBehaviourTest<NestedCoroutinesMock>();

            var task = MonoBehaviourBaseMock.Task;

            Assert.IsTrue(task.IsCompleted);
            Assert.IsTrue(task.IsFaulted);
            Assert.AreEqual("Exception in sequence.", task.Exception.Message);
        }

#endregion

    }
}