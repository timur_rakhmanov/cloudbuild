﻿using System;
using System.Collections;
using Assets._Project.Shared.Scripts.Core.Tasks;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;


namespace Assets._Project.Shared.Scripts.PlayModeUnittests
{
    public class TaskTests
    {
        private IEnumerator TestCoroutineWithOnlyBreake()
        {
            yield break;
        }

        private IEnumerator TestCoroutineMethod(Action body)
        {
            yield return new WaitForSeconds(0);
            body();
        }

        private IEnumerator TestCoroutineMethodWithResult(Func<object> body)
        {
            yield return new WaitForSeconds(0);
            var result = body();
            yield return result;
        }

        [UnityTest]
        public IEnumerator TestTaskRun_WhenExceptionIsntThrown_SetIsComplited()
        {
            var task = new Task(TestCoroutineMethod(() => { }));

            yield return task.Run();

            Assert.IsTrue(task.IsCompleted);
            Assert.IsFalse(task.IsFaulted);
            Assert.IsNull(task.Exception);
        }

        [UnityTest]
        public IEnumerator TestTaskRun_WhenExceptionIsThrown_SavesThrownException()
        {
            var task = new Task(TestCoroutineMethod(() => { throw new Exception("test"); }));

            yield return task.Run();

            Assert.IsTrue(task.IsCompleted);
            Assert.IsTrue(task.IsFaulted);
            Assert.IsNotNull(task.Exception);
            Assert.AreEqual("test", task.Exception.Message);
        }

        [UnityTest]
        public IEnumerator TestTaskWait_WhenExceptionIsntThrownAtTheRun_DoesntThrowException()
        {
            var task = new Task(TestCoroutineMethod(() => { }));

            yield return task.Run();

            Assert.DoesNotThrow(() => task.Wait());
        }

        [Test]
        public void TestTaskWait_WhenTaskIsntCompletedYet_ThrowsException()
        {
            var task = new Task(TestCoroutineMethod(() => { }));
            Assert.Throws<Exception>(() => task.Wait());
        }

        [UnityTest]
        public IEnumerator TestTaskWait_WhenExceptionIsThrownAtTheRun_RethrownException()
        {
            var task = new Task(TestCoroutineMethod(() => { throw new Exception("test"); }));
            yield return task.Run();

            var ex = Assert.Throws<Exception>(() => task.Wait());
            Assert.AreEqual("test", ex.Message);
        }

        [UnityTest]
        public IEnumerator TestTaskResult_WhenExceptionIsntThrownAtTheRun_GetResult()
        {
            var task = new Task<int>(TestCoroutineMethodWithResult(() => 7));
            yield return task.Run();
            Assert.AreEqual(7, task.Result);
        }

        [Test]
        public void TestTaskResult_WhenTaskIsntCompletedYet_ThrowsException()
        {
            var task = new Task<int>(TestCoroutineMethodWithResult(() => 7));
            var ex = Assert.Throws<Exception>(() => {
                var result = task.Result;
            });
            Assert.AreEqual("Coroutine should be completed before get result.", ex.Message);
        }

        [UnityTest]
        public IEnumerator TestTaskResult_WhenCoroutineHasUnexpectedkResultType_ThrowsException()
        {
            var task = new Task<int>(TestCoroutineMethodWithResult(() => "7"));
            yield return task.Run();

            var ex = Assert.Throws<Exception>(() => {
                var result = task.Result;
            });
            Assert.AreEqual("Coroutine has incorrect result type, expected type is System.Int32 but returned type is System.String", ex.Message);
        }

        [UnityTest]
        public IEnumerator TestTaskResult_WhenTaskWithValueTypeAndCoroutineReturnsNull_ThrowsException()
        {
            var task = new Task<int>(TestCoroutineMethodWithResult(() => null));
            yield return task.Run();

            var ex = Assert.Throws<Exception>(() => {
                var result = task.Result;
            });
            Assert.AreEqual("Coroutine doesn't return result or result is null for value object.", ex.Message);
        }

        [UnityTest]
        public IEnumerator TestTaskResult_WhenTaskWithReferenceTypeAndCoroutineReturnsNull_ReturnsNull()
        {
            var task = new Task<GameObject>(TestCoroutineMethodWithResult(() => null));
            yield return task.Run();

            GameObject result = null;
            Assert.DoesNotThrow(() => { result = task.Result; });
            Assert.IsNull(result);
        }

        [UnityTest]
        public IEnumerator TestTaskResult_WhenExceptionIsThrownAtTheRun_RethrowException()
        {
            var task = new Task<int>(TestCoroutineMethod(() => { throw new Exception("test"); }));
            yield return task.Run();

            var ex = Assert.Throws<Exception>(() => {
                var result = task.Result;
            });
            Assert.AreEqual("test", ex.Message);
        }

        [UnityTest]
        public IEnumerator TestTaskResult_WhenTaskWithValueTypeCoroutineBreakesWithoutIterations_ThrowsException()
        {
            var task = new Task<int>(TestCoroutineWithOnlyBreake());
            yield return task.Run();

            var ex = Assert.Throws<Exception>(() => {
                var result = task.Result;
            });
            Assert.AreEqual("Coroutine doesn't return result or result is null for value object.", ex.Message);
        }

        [UnityTest]
        public IEnumerator TestTaskResult_WhenTaskWithReferenceTypeCoroutineBreakesWithoutIterations_ReturnsNull()
        {
            var task = new Task<GameObject>(TestCoroutineWithOnlyBreake());
            yield return task.Run();

            GameObject result = null;
            Assert.DoesNotThrow(() => { result = task.Result; });
            Assert.IsNull(result);
        }

        [Test]
        public void TestContinueWith_WhenContinuationAlreadySet_ThrowsException()
        {
            var task = new Task(TestCoroutineMethod(() => { })).ContinueWith((t) => { });

            var ex = Assert.Throws<Exception>(() => task.ContinueWith((t) => { }));
            Assert.AreEqual("Continuation action already set.", ex.Message);
        }

        [UnityTest]
        public IEnumerator TestContinueWith_WhenRunThrowsException_CallContinueWithWithFaultTask()
        {
            Task continuationTask = null;
            var task = new Task(TestCoroutineMethod(() => { throw new Exception(); }))
                .ContinueWith((t) => { continuationTask = t; });
            yield return task.Run();


            Assert.IsNotNull(continuationTask);
            Assert.IsTrue(continuationTask.IsCompleted);
            Assert.IsTrue(continuationTask.IsFaulted);
            Assert.IsNotNull(continuationTask.Exception);
        }

        [UnityTest]
        public IEnumerator TestContinueWith_WhenRunDoesntThrowException_CallContinueWithWithComplitedTask()
        {
            Task continuationTask = null;
            var task = new Task(TestCoroutineMethod(() => { }))
                .ContinueWith((t) => { continuationTask = t; });
            yield return task.Run();

            Assert.IsNotNull(continuationTask);
            Assert.IsTrue(continuationTask.IsCompleted);
            Assert.IsFalse(continuationTask.IsFaulted);
            Assert.IsNull(continuationTask.Exception);
        }

        [UnityTest]
        public IEnumerator TestContinueWith_WhenCoroutineWithResult_CallContinueWithWithTaskAndResult()
        {
            Task<int> continuationTask = null;
            var task = new Task<int>(TestCoroutineMethodWithResult(() => 7))
                .ContinueWith((t) => { continuationTask = t; });
            yield return task.Run();

            Assert.IsNotNull(continuationTask);
            Assert.IsTrue(continuationTask.IsCompleted);
            Assert.IsFalse(continuationTask.IsFaulted);
            Assert.IsNull(continuationTask.Exception);
            Assert.AreEqual(7, continuationTask.Result);
        }

    }
}
