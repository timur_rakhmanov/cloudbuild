﻿using System.Collections;
using System.Net;
using Assets._Project.Shared.Scripts.APIClient.Coroutines;
using Assets._Project.Shared.Scripts.Configuration;
using Assets._Project.Shared.Scripts.Core.Tasks;
using RestSharp;
using UnityEngine;
using UnityEngine.Assertions;


namespace Assets._Project.Shared.Scripts.Telemetry
{
    public class HttpEventPublisher : IEventPublisher
    {
        private const string TELEMETRY_SEND_EVENT_URL = "/api/telemetry";

        private MonoBehaviour _monoBehaviour;

        public HttpEventPublisher(MonoBehaviour behaviour)
        {
            _monoBehaviour = behaviour;
        }

        public void SendEvent(TelemetryEvent telemetryEvent)
        {
            SendEventCoroutine(telemetryEvent).AsTask().RunCoroutine(_monoBehaviour);
        }

        private IEnumerator SendEventCoroutine(TelemetryEvent telemetryEvent)
        {
            Debug.Log(AppSettings.Instance.BackendServerUrl);
            var client = new RestClient(AppSettings.Instance.BackendServerUrl);
            var request = new RestRequest(TELEMETRY_SEND_EVENT_URL) { Method = Method.POST };

#if NETFX_CORE
            request.AddJsonBody(telemetryEvent);
#else
            request.RequestFormat = DataFormat.Json;
            request.AddBody(telemetryEvent);
#endif
            var task = RestCoroutineFactory.CreateRestCoroutine(client, request)
                .AsTask<IRestResponse>();
            yield return task.Run();
            Assert.AreEqual(HttpStatusCode.Created, task.Result.StatusCode);
        }
    }
}
