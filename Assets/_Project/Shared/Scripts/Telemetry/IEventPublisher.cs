﻿

namespace Assets._Project.Shared.Scripts.Telemetry
{
    public interface IEventPublisher
    {
        void SendEvent(TelemetryEvent telemetryEvent);
    }
}
