﻿using System;
using System.Collections.Generic;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Extensions;
using UnityEngine;
using UnityEngine.Assertions;

#if NETFX_CORE
using HoloToolkit.Unity;
#endif


namespace Assets._Project.Shared.Scripts.Telemetry
{
    // TODO:22864 now this telemetry class is related to main scene manager (that in MrFolder)
    // need to remove this dependency.
    public class Telemetry
    {
        private IEventPublisher _eventPublisher;
        private string _deviceId;

        private static Telemetry _instance;

        public static Telemetry Instance
        {
            get { return _instance ?? (_instance = new Telemetry()); }
        }

        private Telemetry()
        {
            // TODO:22864 create empty game object (at the dynamic objects and use it for starting coroutines instead 
            // of the MainSceneManager.
            _deviceId = ExecutionContext.Instance.DeviceId;

            var telemetryPlaceholderObject = new GameObject("TelemetryPlaceholder");
            var telemetryPlaceholder = telemetryPlaceholderObject.AddComponent<TelemetryPlaceholder>();
            telemetryPlaceholderObject.AddToRoot();

            Assert.IsNotNull(telemetryPlaceholder);
#if NETFX_CORE
            _eventPublisher = new HttpEventPublisher(telemetryPlaceholder);
#else
            _eventPublisher = new UnityEventPublisher();
#endif
        }

        public void Send(string eventType, IDictionary<string, string> data = null)
        {
            try {
                var newEvent = new TelemetryEvent(ExecutionContext.Instance.SessionId, _deviceId, eventType, ExecutionContext.Instance.Company.Id, ExecutionContext.Instance.Company.Slug, data);
                _eventPublisher.SendEvent(newEvent);
            } catch (Exception ex) {
                Debug.LogError("Telemetry error: " + eventType + ": " + ex.Message);
            }
        }
    }
}
