﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;


namespace Assets._Project.Shared.Scripts.Telemetry
{
    public class TelemetryEvent
    {
        public TelemetryEvent(Guid sessionId, [NotNull] string deviceId, [NotNull] string eventType, Guid companyId, [NotNull] string companySlug, IDictionary<string, string> data = null)
        {
            if (deviceId == null) throw new ArgumentNullException("deviceId");
            if (eventType == null) throw new ArgumentNullException("eventType");
            if (companyId == Guid.Empty) throw new ArgumentNullException("companyId");
            if (companySlug == null) throw new ArgumentNullException("companySlug");
            Id = Guid.NewGuid();
            Data = data ?? new Dictionary<string, string>();
            CreationDateUtc = DateTime.UtcNow;
            EventType = eventType;
            SessionId = sessionId;
            DeviceId = deviceId;
            CompanyId = companyId;
            CompanySlug = companySlug;
        }
        public Guid Id { get; set; }
        public Guid SessionId { get; private set; }
        public string DeviceId { get; private set; }
        public Guid CompanyId { get; private set; }
        public string CompanySlug { get; private set; }
        public DateTime CreationDateUtc { get; private set; }
        public string EventType { get; private set; }
        public IDictionary<string, string> Data { get; private set; }
    }
}
