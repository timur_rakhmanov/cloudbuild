﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     This class uses only for starting coroutine in telemetry
/// </summary>
public class TelemetryPlaceholder : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
