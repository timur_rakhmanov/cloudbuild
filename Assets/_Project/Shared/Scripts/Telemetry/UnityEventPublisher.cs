﻿using Assets._Project.Shared.Scripts.Core;
using Newtonsoft.Json;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Telemetry
{
    public class UnityEventPublisher : IEventPublisher {

        public void SendEvent(TelemetryEvent telemetryEvent)
        {
            if (telemetryEvent.EventType == Constants.TelemetryEvents.USER_LOCATION) {
                return;
            }
            Debug.Log(JsonConvert.SerializeObject(telemetryEvent));
        }
    }
}
