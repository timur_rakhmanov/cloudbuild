﻿using System.Collections;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Tasks;
using HoloToolkit.Unity.InputModule;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


namespace Assets._Project.Shared.Scripts.Tooltips
{
    /// <summary>
    /// Allow to display tooltip on canvas object.
    /// </summary>
    public class CanvasTooltipController: MonoBehaviour
    {
        [Tooltip("Tooltip that should be displayed")]
        // TODO: check specified panel type. 
        public GameObject Tooltip;

        public int xOffset;
        public int yOffset;

        private Coroutine _displayingTooltip;

        public void FocusOn(string text)
        {
            if (_displayingTooltip != null) {
                StopCoroutine(_displayingTooltip);
            }
            _displayingTooltip = DisplayTooltipAfterDelay(text).AsTask().RunCoroutine(this);
        }

        public void FocusOff()
        {
            StopCoroutine(_displayingTooltip);
            Assert.IsNotNull(Tooltip);
            Tooltip.GetComponentInChildren<Text>().text = "";
            Tooltip.SetActive(false);
        }

        private IEnumerator DisplayTooltipAfterDelay(string text)
        {
            yield return new WaitForSeconds(Constants.TOOLTIP_DELAY);
            Assert.IsNotNull(Tooltip);
            var textPanel = Tooltip.GetComponentInChildren<Text>();
            Assert.IsNotNull(textPanel);
            textPanel.text = text;
            Tooltip.SetActive(true);

            // We assume that tooltip anchor is at the center of canvas pivot 
            // (probably need to implement calculation that avoids this limitation).
            var hitOnCanvasPostion = transform.InverseTransformPoint(GazeManager.Instance.HitInfo.point);
            Tooltip.transform.localPosition = new Vector3(hitOnCanvasPostion.x + xOffset, hitOnCanvasPostion.y + yOffset, hitOnCanvasPostion.z);
        }
    }
}