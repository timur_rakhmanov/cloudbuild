﻿using System;
using System.Collections;
using Assets._Project.Shared.Scripts.Core;
using Assets._Project.Shared.Scripts.Core.Extensions;
using Assets._Project.Shared.Scripts.Core.Tasks;
using HoloToolkit.Unity.InputModule;
using UnityEngine;


namespace Assets._Project.Shared.Scripts.Tooltips
{
    /// <summary>
    /// Allows to display tooltip on object hover.
    /// </summary>
    public class TooltipController: MonoBehaviour, IFocusable
    {
        private const float DISTANCE_BEFORE_OBJECT = 0.2f;

        public GameObject TooltipPrefab;

        
        private Coroutine _displayingTooltip;
        private GameObject _tooltipInstance;

        public void OnFocusEnter()
        {
            // Start timer.
            // When delay is made - create prefab over object (detect it's geometry).
            // TODO: consider to add animations.
            _displayingTooltip = DisplayTooltipAfterDelay().AsTask().RunCoroutine(this);
        }

        public void OnFocusExit()
        {
            StopCoroutine(_displayingTooltip);
            HideTooltip();
        }

        private IEnumerator DisplayTooltipAfterDelay()
        {
            yield return new WaitForSeconds(Constants.TOOLTIP_DELAY);
            ShowTooltip();
        }

        private void ShowTooltip()
        {
            if (TooltipPrefab == null) {
                throw new NullReferenceException("Tooltip prefab should be set.");
            }

            _tooltipInstance = Instantiate(TooltipPrefab);
            PlaceTooltip(_tooltipInstance);
            _tooltipInstance.AddToRoot();
        }

        private void HideTooltip()
        {
            if (_tooltipInstance != null) {
                Destroy(_tooltipInstance);
            }   
        }

        private void PlaceTooltip(GameObject tooltip)
        {
            var hitInfo = GazeManager.Instance.HitInfo;
            
            // Get position slightly before hit.
            var position = hitInfo.point + -Camera.main.transform.forward.normalized * DISTANCE_BEFORE_OBJECT;

            tooltip.transform.position = position;
            
        }
    }
}