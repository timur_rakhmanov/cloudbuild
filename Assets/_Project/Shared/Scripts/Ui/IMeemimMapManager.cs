﻿using System.Collections.Generic;
using Assets._Project.Shared.Scripts.Infrastructure;


namespace Assets._Project.Shared.Scripts.Ui
{
    /// <summary>
    /// Manage 3d map.
    /// </summary>
    public interface IMeemimMapManager
    {
        List<PlacemarkController> GetMeemimMapPlacemarks();
    }
}
