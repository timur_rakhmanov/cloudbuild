﻿using System.Collections;
using Assets._Project.Shared.Scripts.Core.Tasks;
using UnityEngine;
using UnityEngine.UI;


namespace Assets._Project.Shared.Scripts.Ui
{
    public class PulsingController : MonoBehaviour
    {
        private Text _text;

        void Start()
        {
            _text = GetComponent<Text>();
        }

        public void StartFlashing()
        {
            var task = FlashingText().AsTask();
            task.RunCoroutine(this);
        }

        private IEnumerator FlashingText()
        {
            yield return new WaitForSeconds(0.25f);

            _text.enabled = true;

            yield return new WaitForSeconds(0.5f);

            _text.enabled = false;
        }
    }
}
