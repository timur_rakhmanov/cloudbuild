﻿using System;
using HoloToolkit.Unity;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;


namespace Assets._Project.Shared.Scripts.Ui
{
    /// <summary>
    /// Controls screen space ui.
    /// </summary>
    // TODO:22866 consider to divide responsibility and left only container logic here, or remove it at all. 
    public class ScreenSpaceUiPanelController : Singleton<ScreenSpaceUiPanelController>
    {
        private const string PHOTO_SCREEN = "PhotoScreen";
        private const string PHOTO_SCREEN_TIMER_COUNTER = "PhotoScreen/Timer/TimerCounter";
        private const string RECORDING_SCREEN = "RecordingScreen";
        private const string RECORDING_SCREEN_TIMER_COUNTER = "RecordingScreen/Timer/TimerCounter";
        private const string RECORDING_SCREEN_STOP_MESSAGE = "RecordingScreen/RecordingStopMessage";

        public void ShowPhotoScreen()
        {
            GetGameObjectElement(PHOTO_SCREEN).SetActive(true);
        }

        public void HidePhotoScreen()
        {
            GetGameObjectElement(PHOTO_SCREEN).SetActive(false);
        }

        public void DisplayPhotoScreenTime(float time)
        {
            var timerCounterGameObject = GetTextElement(PHOTO_SCREEN_TIMER_COUNTER);

            if (timerCounterGameObject.text != time.ToString("####"))
            {
                timerCounterGameObject.text = time.ToString("####");

                timerCounterGameObject.GetComponent<PulsingController>().StartFlashing();
            }
        }

        public void ShowRecordingScreen(string text = "Click anywhere to stop")
        {
            GetGameObjectElement(RECORDING_SCREEN_STOP_MESSAGE).GetComponent<Text>().text = text;
            GetGameObjectElement(RECORDING_SCREEN).SetActive(true);
        }

        public void HideRecordingScreen()
        {
            GetGameObjectElement(RECORDING_SCREEN).SetActive(false);
        }

        public void DisplayRecordingScreenTime(TimeSpan time, bool flash)
        {
            var timerCounterGameObject = GetTextElement(RECORDING_SCREEN_TIMER_COUNTER);

            var formatedTime = string.Format("{0:mm:ss}", new DateTime(time.Ticks));

            if (timerCounterGameObject.text != formatedTime) {
                if (!flash) {
                    timerCounterGameObject.enabled = true;
                } else {
                    timerCounterGameObject.GetComponent<PulsingController>().StartFlashing();
                }

                timerCounterGameObject.text = formatedTime;
            }
        }

        private GameObject GetGameObjectElement(string path)
        {
            var element = gameObject.transform.Find(path);
            Assert.IsNotNull(element);
            return element.gameObject;
        }

        private Text GetTextElement(string path)
        {
            var element = gameObject.transform.Find(path);
            Assert.IsNotNull(element);
            var text = element.GetComponent<Text>();
            Assert.IsNotNull(text);
            return text;
        }
    }
}
